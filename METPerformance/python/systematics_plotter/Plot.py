import ROOT
import AtlasStyle
ROOT.gROOT.SetBatch(True)

def get_colour(variation, counter):
  from ROOT import kBlack,kBlue,kRed,kCyan,kYellow,kGray,kGreen
  from ROOT import kMagenta,kOrange,kSpring,kTeal,kAzure,kViolet,kPink
  # If colour is specified for this variation, use this.
  # Otherwise, pick from list.
  colours = {}
  colours['data']  = kBlack
  colours['nominal']  = kBlue
  colours['default']  = kBlack
  colour_sequence = [kRed,kCyan,kYellow,kGray,kGreen,kMagenta,kOrange,kSpring,kTeal,kAzure,kViolet,kPink]
  if variation in colours:
    colour_to_use = colours[variation]
  elif counter < len(colour_sequence) and counter >= 0:
    colour_to_use = colour_sequence[counter]
    counter += 1
  else:
    colour_to_use = colours['default']
  return colour_to_use, counter


class Plot:
  def __init__(self, sample=None):
    self.sample = sample
    self.canvas, self.pad1, self.pad2 = False, False, False
    self.plot_elements = [] # to stop these being garbaged until we've saved the plot
    self.output_path = ''
    self.plot_format = '.png'
    self.data_sample = None
    self.stack = None
  def setupCanvas(self):
    import ROOT
    from ROOT import TCanvas, TPad
    canvas = None
    if self.sample:
      canvas = TCanvas("canvas"+self.sample.name,"canvas",200,10,800,800)
    else:
      canvas = TCanvas("canvas","canvas",200,10,800,800)
    pad1 = TPad("pad1","pad1",0.0,0.5,1.0,1.0,21)
    pad2 = TPad("pad2","pad2",0.0,0.0,1.0,0.5,22)
    pad1.SetLogy(1)
    pad1.Draw()
    pad2.Draw()
    pad1.SetFillColor(ROOT.kWhite)
    pad2.SetFillColor(ROOT.kWhite)
    #pad1.SetTopMargin(0.1)
    #pad1.SetBottomMargin(0.01)
    #pad2.SetTopMargin(0.05)
    #pad2.SetBottomMargin(0.5)
    self.canvas, self.pad1, self.pad2 = canvas, pad1, pad2

  def reset(self):
    for plot_element in self.plot_elements:
      self.plot_elements.remove(plot_element)
    self.setupCanvas()

  def histDictAllVariations(self, histoname):
    '''
    Make a dict of histograms to plot.
    Plot all variations for this sample on the same canvas.
    '''
    histos = {}
    syst_hist_dict = self.sample.getHistsDict(histoname)
    for syst_name, syst_dict in syst_hist_dict.iteritems():
      for var_name, histo in syst_dict.iteritems():
        histos[var_name] = histo
    return histos

  def histDictOneSystematic(self, histoname, syst):
    '''
    Make a dict of histograms to plot.
    Plot all variations for a single systematic plus the nominal on the same canvas.
    '''
    histos = {}
    syst_hist_dict = syst.getHistsDict(histoname)
    for var_name, histo in syst_hist_dict.iteritems():
      histos[var_name] = histo
    nominal = syst.linkedNominal
    nominal_hist_dict = nominal.getHistsDict(histoname)
    histos.update(nominal_hist_dict)
    return histos

  def setOutputPath(self, output_path):
    self.output_path = output_path

  def plotDataMCStack(self, histoname):
    from Sample import Sample
    self.reset()
    hist_dict = self.stack.getTotalHistsDict(histoname)
    print hist_dict
    total_sample = Sample('Total_MC', hist_dict, histos=True)
    self.sample = total_sample
    self.plotDataMCIndivSystematicBands(histoname)
    self.plotDataMCCombinedSystematicBand(histoname)

  def plotAllVariations(self, histoname):
    self.reset()
    hist_dict = self.histDictAllVariations(histoname)
    self.plotFromHistDict(hist_dict, histoname)
    self.SaveAs(histoname+'_'+self.sample.name)

  def plotIndivSystematics(self, histoname):
    for syst in self.sample.systematics:
      self.reset()
      hist_dict = self.histDictOneSystematic(histoname, syst)
      self.plotFromHistDict(hist_dict, histoname)
      self.SaveAs(histoname+'_'+self.sample.name+'_'+syst.name)

  def plotCombinedSystematicBand(self, histoname):
    self.reset()
    nominal_hist = self.sample.getNominalHist(histoname)
    diff_hists = self.sample.getSystematicUncertaintyBand(histoname)
    varied_hists = []
    for diff_hist in diff_hists:
      varied_hist = diff_hist.Clone()
      varied_hist.Add(nominal_hist)
      varied_hists.append(varied_hist)
    hist_dict = {}
    hist_dict['nominal'] = nominal_hist
    hist_dict['varied1'] = varied_hists[0]
    hist_dict['varied2'] = varied_hists[1]
    print hist_dict
    self.plotUncertaintyBand(hist_dict, histoname)
    self.SaveAs(histoname+'_'+self.sample.name+'_combined_band')

  def plotDataMCCombinedSystematicBand(self, histoname):
    self.reset()
    nominal_hist = self.sample.getNominalHist(histoname)
    diff_hists = self.sample.getSystematicUncertaintyBand(histoname)
    varied_hists = []
    for diff_hist in diff_hists:
      varied_hist = diff_hist.Clone()
      varied_hist.Add(nominal_hist)
      varied_hists.append(varied_hist)
    hist_dict = {}
    hist_dict['nominal'] = nominal_hist
    hist_dict['varied1'] = varied_hists[0]
    hist_dict['varied2'] = varied_hists[1]
    print hist_dict
    self.plotUncertaintyBand(hist_dict, histoname, addData=True)
    self.SaveAs(histoname+'_'+self.sample.name+'_combined_band')
  def plotDataMCIndivSystematicBands(self, histoname):
    for syst in self.sample.systematics:
      self.reset()
      if syst.name == 'nominal': continue
      nominal_hist = syst.linkedNominal.getHistsDict(histoname)['nominal'].Clone()
      diff_hists = syst.getSystematicUncertaintyBand(histoname)
      varied_hists = []
      for diff_hist in diff_hists:
        varied_hist = diff_hist.Clone()
        varied_hist.Add(nominal_hist)
        varied_hists.append(varied_hist)
      hist_dict = {}
      hist_dict['nominal'] = nominal_hist
      hist_dict['varied1'] = varied_hists[0]
      hist_dict['varied2'] = varied_hists[1]
      print hist_dict
      self.plotUncertaintyBand(hist_dict, histoname, addData=True)
      self.SaveAs(histoname+'_'+self.sample.name+'_'+syst.name+'_band')

  def plotIndivSystematicBands(self, histoname):
    for syst in self.sample.systematics:
      self.reset()
      if syst.name == 'nominal': continue
      nominal_hist = syst.linkedNominal.getHistsDict(histoname)['nominal']
      diff_hists = syst.getSystematicUncertaintyBand(histoname)
      varied_hists = []
      for diff_hist in diff_hists:
        varied_hist = diff_hist.Clone()
        varied_hist.Add(nominal_hist)
        varied_hists.append(varied_hist)
      hist_dict = {}
      hist_dict['nominal'] = nominal_hist
      hist_dict['varied1'] = varied_hists[0]
      hist_dict['varied2'] = varied_hists[1]
      print hist_dict
      self.plotUncertaintyBand(hist_dict, histoname)
      self.SaveAs(histoname+'_'+self.sample.name+'_'+syst.name+'_band')

  def make_error_graph(self,histos):
    from numpy import array
    from ROOT import TGraphAsymmErrors
    nominal_hist = histos['nominal']
    n = nominal_hist.GetNbinsX()
    x=[]; ex_up=[]; ex_down=[]
    y=[]; ey_up=[]; ey_down=[]
    for i in xrange(n+1):
      x.append(nominal_hist.GetBinCenter(i))
      bin_width = nominal_hist.GetBinWidth(i)
      ex_down.append(bin_width/2.)
      ex_up.append(bin_width/2.)
      y.append(nominal_hist.GetBinContent(i))
      var1 = histos['varied1'].GetBinContent(i) - y[i]
      var2 = histos['varied2'].GetBinContent(i) - y[i]
      min_var = min([var1, var2])
      if min_var < 0: ey_down.append(-min_var)
      else: ey_down.append(0)
      max_var = max([var1, var2])
      if max_var > 0: ey_up.append(max_var)
      else: ey_up.append(0)
    x = array(x)
    y = array(y)
    ex_up   = array(ex_up)
    ex_down = array(ex_down)
    ey_up   = array(ey_up)
    ey_down = array(ey_down)
    error_graph = TGraphAsymmErrors(n+1,x,y,ex_down,ex_up,ey_down,ey_up)
    return error_graph

  def plotUncertaintyBand(self, histos, histoname, addData=False):
    '''
    Take in a dict of histograms. Plot the nominal histogram and draw the other
    TWO as an uncertainty band.
    '''
    variable_rebin_bin_list = [i for i in xrange(0,100,10)]+[100.,120.,140.,160,200.,250.]

    from ROOT import TLegend, TPaveText, TLine
    colour_counter = 0
    for label, histo in histos.iteritems():
      colour_to_use, colour_counter = get_colour(label, colour_counter)
      if variable_rebin_bin_list:
        from array import array
        nbins = len(variable_rebin_bin_list)-1
        bin_array = array('d', variable_rebin_bin_list)
        histos[label] = histo.Rebin(nbins, histo.GetName()+'_rebin', bin_array)
      else:
        histo.Rebin(8)
      histos[label].SetMarkerColor(colour_to_use)
      histos[label].SetLineColor(  colour_to_use)

    leg = TLegend(.20, .20, .45, .45)
    leg.SetFillStyle(0)
    leg.SetBorderSize(0)

    self.pad1.cd()
    draw_option = ''
    leg.AddEntry(histos['nominal'], 'Nominal MC', 'lp')
    histos['nominal'].GetXaxis().SetTitle('E_{T}^{miss} / GeV')
    histos['nominal'].GetYaxis().SetTitle('Events')
    self.plot_elements.append(histos['nominal'])
    histos['nominal'].Draw(draw_option)

    atlas_coords = (0.60,0.75,0.85,0.85,'brNDC')
    atlas_label = ROOT.TPaveText(*atlas_coords)
    atlas_label.SetFillStyle(0)
    atlas_label.SetBorderSize(0)
    atlas_label.AddText("ATLAS")
    atlas_label.SetTextAlign(11)
    atlas_label.SetTextFont(73);
    atlas_label.SetTextSize(32);
    self.plot_elements.append(atlas_label)
    atlas_label.Draw('same')
    atlas_coords2 = (0.75,0.75,0.85,0.85,'brNDC')
    atlas_label2 = ROOT.TPaveText(*atlas_coords2)
    atlas_label2.SetFillStyle(0)
    atlas_label2.SetBorderSize(0)
    atlas_label2.AddText("Internal")
    atlas_label2.SetTextFont(43);
    atlas_label2.SetTextSize(32);
    atlas_label2.SetTextAlign(11)
    #atlas_label2.SetTextFont(73);
    #atlas_label2.SetTextSize(32);
    self.plot_elements.append(atlas_label2)
    atlas_label2.Draw('same')

    textcoords = (0.60,0.45,0.85,0.70,'brNDC')
    textbox = ROOT.TPaveText(*textcoords)
    textbox.SetTextAlign(11)
    textbox.SetFillStyle(0)
    textbox.SetBorderSize(0)
    textbox.AddText("#sqrt{s} = 13 TeV")
    textbox.AddText('Z #rightarrow #mu#mu  2fb^{-1}')
    self.plot_elements.append(textbox)
    textbox.Draw('same')

    data_hist = None
    if addData:
      data_hist_dict = self.data_sample.getHistsDict(histoname)
      data_hist = data_hist_dict['data']['data'].Clone()
      colour_to_use, colour_counter = get_colour('data', colour_counter)
      if variable_rebin_bin_list:
        from array import array
        nbins = len(variable_rebin_bin_list)-1
        bin_array = array('d', variable_rebin_bin_list)
        data_hist = data_hist.Rebin(nbins, data_hist.GetName()+'_rebin', bin_array)
      else:
        data_hist.Rebin(8)
      data_hist.SetMarkerColor(colour_to_use)
      data_hist.SetLineColor(  colour_to_use)
      self.plot_elements.append(data_hist)
      leg.AddEntry(data_hist, 'Data', 'lp')
      data_hist.Draw('same')

    error_graph = self.make_error_graph(histos)
    self.plot_elements.append(error_graph)
    error_graph.SetFillStyle(3001)
    error_graph.SetFillColor(2)
    error_graph.Draw('E2')

    self.plot_elements.append(leg)
    leg.Draw()

    self.pad2.cd()
    blank_ratio_hist = histos['nominal'].Clone('nominal_ratio')
    blank_ratio_hist.Reset()
    #blank_ratio_hist.Divide(histos['nominal'])
    blank_ratio_hist.GetYaxis().SetRangeUser(0,2.)
    blank_ratio_hist.GetYaxis().SetTitle('Data / total MC')
    self.plot_elements.append(blank_ratio_hist)
    from ROOT import kBlack
    blank_ratio_hist.SetLineColor(kBlack)
    blank_ratio_hist.Draw()

    error_graph_ratio = error_graph.Clone()
    self.plot_elements.append(error_graph_ratio)
    n_entries = error_graph_ratio.GetN()
    for i in xrange(n_entries+1):
      x_val      = error_graph_ratio.GetX()[i]
      y_val      = error_graph_ratio.GetY()[i]
      y_err_up   = error_graph_ratio.GetEYhigh()[i]
      y_err_down = error_graph_ratio.GetEYlow()[i]
      error_graph_ratio.SetPoint(i, x_val, 1.)
      if y_val > 0:
        error_graph_ratio.SetPointEYhigh(i, y_err_up / y_val)
        error_graph_ratio.SetPointEYlow(i, y_err_down / y_val)
      else:
        from numpy import inf
        error_graph_ratio.SetPointEYhigh(i, inf)
        error_graph_ratio.SetPointEYlow(i, inf)

    error_graph_ratio.SetFillStyle(3001)
    error_graph_ratio.SetFillColor(2)
    error_graph_ratio.Draw('same E2')

    if addData:
      data_ratio = data_hist.Clone()
      data_ratio.Divide(histos['nominal'])
      self.plot_elements.append(data_ratio)
      data_ratio.Draw('same')

    axis_range = histos['nominal'].GetXaxis().GetXmin(), histos['nominal'].GetXaxis().GetXmax()
    unity_line = TLine(axis_range[0], 1., axis_range[1], 1.)
    self.plot_elements.append(unity_line)
    unity_line.Draw('same')

  def plotFromHistDict(self, histos, histoname):
    '''
    Take in a dict of histograms. Plot each histogram, with the dict key as the
    legend label.
    '''
    from ROOT import TLegend, TPaveText, TLine
    colour_counter = 0
    for label, histo in histos.iteritems():
      colour_to_use, colour_counter = get_colour(label, colour_counter)
      histo.Rebin(2)
      histo.SetMarkerColor(colour_to_use)
      histo.SetLineColor(  colour_to_use)

    leg = TLegend(.65, .45, .9, .65)
    leg.SetFillStyle(0)
    leg.SetBorderSize(0)

    # Text box for title
    title_box = TPaveText(.65, .67, .9, .87, 'NDC')
    title_box.SetFillStyle(0)
    title_box.SetBorderSize(0)
    title_box.SetTextFont(62)
    title_box.SetTextAlign(12) # left-centre
    title_box.AddText(self.sample.name)
    title_box.AddText(histoname)

    self.pad1.cd()
    first = True
    for variation in histos:
      draw_option = ''
      if first: first = False
      else: draw_option = 'same'
      leg.AddEntry(histos[variation], variation, 'lp')
      histos[variation].GetXaxis().SetTitle('E_{T}^{miss} / GeV')
      histos[variation].GetYaxis().SetTitle('Events')
      self.plot_elements.append(histos[variation])
      histos[variation].Draw(draw_option)

    self.plot_elements.append(leg)
    leg.Draw()
    self.plot_elements.append(title_box)
    title_box.Draw()

    self.pad2.cd()
    first = True
    ratio_histos = {}
    for variation in histos:
      if 'nominal' in variation: continue
      ratio_hist = histos[variation].Clone(variation+'_ratio')
      ratio_hist.Divide(histos['nominal'])
      ratio_histos[variation] = ratio_hist
      draw_option = ''
      if first: first = False
      else: draw_option = 'same'
      ratio_histos[variation].GetYaxis().SetRangeUser(0,2.)
      ratio_histos[variation].GetYaxis().SetTitle('Data / total MC')
      self.plot_elements.append(ratio_histos[variation])
      ratio_histos[variation].Draw(draw_option)
    axis_range = histos['nominal'].GetXaxis().GetXmin(), histos['nominal'].GetXaxis().GetXmax()
    unity_line = TLine(axis_range[0], 1., axis_range[1], 1.)
    self.plot_elements.append(unity_line)
    unity_line.Draw('same')

  def SaveAs(self,save_name):
    self.canvas.SaveAs(self.output_path+save_name+self.plot_format)

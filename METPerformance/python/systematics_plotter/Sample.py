
class Stack:
  '''A stack holds several samples.'''
  def __init__(self):
    self.samples = []
  def addSample(self, sample):
    self.samples.append(sample)
  def getTotalHistsDict(self, hist_name):
    '''Get a dict of histograms: the total for each variation, labelled by variation name.'''
    total_hists = {}
    for sample in self.samples:
      for syst in sample.systematics:
        syst_name = syst.name
        if syst_name not in total_hists:
          total_hists[syst_name] = {}
        syst_hist_dict = syst.getHistsDict(hist_name)
        print syst_hist_dict
        for var_name, hist in syst_hist_dict.iteritems():
          if var_name not in total_hists[syst_name]:
            total_hists[syst_name][var_name] = hist.Clone()
          else:
            total_hists[syst_name][var_name].Add(hist)
    print 'totals:', total_hists
    return total_hists

class Sample:
  '''A Sample represents a full MC or data sample, including nominal + systematic
  variations if relevant.'''
  def __init__(self, name, sample_dict=False, histos=False):
    self.name = name
    self.systematics = []
    self.nominal_systematic = None # There is a unique nominal.
    self.isData = 'data' in name
    if sample_dict:
      self.buildFromDict(sample_dict, histos)
  def buildFromDict(self, sample_dict, histos=False):
    '''
    Assemble this sample from a dict of the format
    {'systematic_name': {'variation_name' : ['slice1', 'slice2']}, }
    If histos, slice1 is a histogram, otherwise a file path.
    '''
    for name, variation_dict in sample_dict.iteritems():
      is_nominal = (name == 'nominal')
      syst = Systematic(name, variation_dict, histos)
      self.addSystematic(syst)
  def getNominalHist(self, hist_name):
    hist = self.nominal_systematic.getHistsDict(hist_name)['nominal']
    return hist
  def getHistsDict(self, hist_name):
    '''Get a dict of histograms: one for each variation, labelled by variation name.'''
    hists = {}
    for syst in self.systematics:
      name = syst.name
      syst_hist_dict = syst.getHistsDict(hist_name)
      hists[name] = syst_hist_dict
    return hists
  def getSystematicUncertaintyBand(self, hist_name):
    '''
    Return a pair of histograms for +/- variations on the nominal.
    This is the quadrature sum of all the systematics this sample knows about.
    '''
    combined_diff_hists = []
    n_bins_combined = False
    for syst in self.systematics:
      if syst.name == 'nominal': continue
      print 'Adding systematic', syst.name, 'to combined band.'
      this_syst_diff_hists = syst.getSystematicUncertaintyBand(hist_name)
      if not combined_diff_hists:
        for hist in this_syst_diff_hists:
          combined_diff_hists.append(hist.Clone())
          n_bins_combined = [hist.GetNbinsX() for hist in combined_diff_hists]
      else:
        n_bins = [hist.GetNbinsX() for hist in this_syst_diff_hists]
        if not n_bins == n_bins_combined:
          raise RuntimeError('Number of bins in histograms does not agree between systematics.')
        for i in xrange(n_bins_combined[0]+1):
          combined = [hist.GetBinContent(i) for hist in combined_diff_hists]
          this_syst = [hist.GetBinContent(i) for hist in this_syst_diff_hists]
          new_combined = addUpDownInQuadrature(combined, this_syst)
          for hist, unc in zip(combined_diff_hists, new_combined):
            hist.SetBinContent(i, unc)
    return combined_diff_hists

  def addSystematic(self, syst):
    self.systematics.append(syst)
    if syst.name == 'nominal':
      self.nominal_systematic = syst
      self.propagateNominalToSysts(syst)
    syst.linkedNominal = self.nominal_systematic
  def propagateNominalToSysts(self, nominal):
    for syst in self.systematics:
      syst.linkedNominal = nominal

def addUpDownInQuadrature(pair1, pair2):
  # Assume these are ordered as up,down.
  from numpy import sqrt
  quad_sum = [sqrt(var[0]*var[0] + var[1]*var[1]) for var in zip(pair1,pair2)]
  return quad_sum[0], -quad_sum[1]

class Systematic:
  '''A systematic is made up of one or more variations.'''
  def __init__(self, name, var_dict=False, histos=False):
    self.name = name
    self.variations = []
    self.linkedNominal = None
    if var_dict:
      self.buildFromDict(var_dict, histos)
  def addVariation(self, variation):
    self.variations.append(variation)
  def buildFromDict(self, var_dict, histos=False):
    '''
    Assemble this variation from a dict of the format
    {'variation_name' : ['slice1', 'slice2'] }
    '''
    for name, slice_list in var_dict.iteritems():
      is_nominal = (name == 'nominal')
      var = Variation(name, slice_list, is_nominal, histos)
      self.addVariation(var)
  def getHistsDict(self, hist_name):
    '''Get a dict of histograms: one for each variation, labelled by variation name.'''
    hists = {}
    for variation in self.variations:
      name = variation.name
      hist = variation.get_histogram(hist_name)
      hists[name] = hist
    return hists
  def getSystematicUncertaintyBand(self, hist_name):
    '''Return a pair of histograms for +/- variations on the nominal.'''
    print self.variations
    if len(self.variations) == 0:
      raise RuntimeError('I need some variations to work with.')
    diff_hists = []
    nominal_hist = self.linkedNominal.getHistsDict(hist_name)['nominal'].Clone()
    print 'nominal', nominal_hist
    for variation in self.variations:
      diff_hist = variation.get_histogram(hist_name).Clone()
      diff_hists.append(diff_hist)
    n = [ hist.GetNbinsX() for hist in diff_hists+[nominal_hist] ]
    if n.count(n[0]) != len(n):
      raise RuntimeError('Number of bins differs between variations!')

    # Make two histograms for the up/down parts of the symmetrized uncertainty.
    unc_hists = [diff_hists[0].Clone(), diff_hists[0].Clone()]
    for hist in unc_hists: hist.Reset()
    if len(self.variations) == 1:
      for i in xrange(n[0]+1):
        this_bin_contents = [hist.GetBinContent(i) for hist in diff_hists+[nominal_hist]]
        uncertainty_variations = uncertainty_one_variation_samples(this_bin_contents)
        unc_hists[0].SetBinContent(i, uncertainty_variations[0])
        unc_hists[1].SetBinContent(i, uncertainty_variations[1])
    elif len(self.variations) == 2:
      for i in xrange(n[0]+1):
        this_bin_contents = [hist.GetBinContent(i) for hist in diff_hists]
        uncertainty_variations = uncertainty_two_variation_samples(this_bin_contents)
        unc_hists[0].SetBinContent(i, uncertainty_variations[0])
        unc_hists[1].SetBinContent(i, uncertainty_variations[1])
    else: # Some unrecognized pattern of variations.
      raise RuntimeError('I do not know how to handle systematics with %i variations.' % len(self.variations))
    return unc_hists

def uncertainty_two_variation_samples(bin_contents):
  # uncertainty to apply is +/- .5*abs(var1-var2)
  if len(bin_contents) != 2:
    raise RuntimeError('You promised me two variations.')
  half_abs_diff = .5 * abs(bin_contents[0] - bin_contents[1])
  return (half_abs_diff, -half_abs_diff)

def uncertainty_one_variation_samples(bin_contents):
  # uncertainty to apply is +/- abs(var1-nominal))
  print 'bin contents:', bin_contents
  if len(bin_contents) != 2:
    raise RuntimeError('You promised me a variation and a nominal.')
  abs_diff = abs(bin_contents[0] - bin_contents[1])
  return (abs_diff, -abs_diff)

class Variation:
  '''A variation is a single systematic variation (or the nominal sample). One
  or more variations make up a Sample.
  If histos, the list passed in is a histogram to be used directly.
  Otherwise, it is of file paths from which to assemble slices.
  '''
  def __init__(self, name, slice_paths=[], nominal=False, histos=False):
    self.name = name
    self.nominal = nominal
    self.slices = []
    self.histo = None
    if histos:
      self.histo = slice_paths
    else:
      self.addSlicesByPath(slice_paths)
    self.isData = 'data' in name
  def get_histogram(self, hist_name):
    # if we have a histogram stored, return that
    if self.histo:
      print 'Returning stored histogram from variation', self.name
      return self.histo
    else:
      # Otherwise, sum up the relevant histogram from the contributing slices.
      hist = False
      for slice in self.slices:
        scale = not self.isData
        slice_hist = slice.get_histogram(hist_name, scale=scale)
        if slice_hist:
          if hist:
            print 'Adding slice to existing histogram for variation', self.name
            hist.Add(slice_hist)
          else:
            print 'Starting a new histogram for variation', self.name
            hist = slice_hist
      return hist
  def addSlices(self, slices):
    '''Add a list of slices to the variation.'''
    for slice in slices:
      self.addSlice(slice)
  def addSlice(self, slice):
    '''Add a slice to the sum of slices that make up the variation.'''
    self.slices.append(slice)
  def addSlicesByPath(self, slice_paths):
    '''Take a list of ROOT file paths and add these as slices.'''
    for slice_path in slice_paths:
      print 'Adding slice from', slice_path
      slice = Slice(slice_path)
      self.addSlice(slice)

class Slice:
  '''A set of histograms that may be added together with others to make up a
  variation.'''
  def __init__(self, file_path):
    from ROOT import TFile
    self.path = file_path
    self.file = TFile.Open(self.path)
    self.data_lumi = 2000. #ipb
    self.mcid = self.get_mcid_from_filename(file_path)
    self.weights_hist = self.get_histogram('h_allWeights', scale=False)
  def get_mcid_from_filename(self, file_path):
    mcid = None
    this_file_name = file_path.split('/')[-1]
    mcid = this_file_name.split('.')[3]
    return mcid
  def get_histogram(self, hist_name, scale=False):
    import uuid
    self.file.cd()
    hist = self.file.Get(hist_name)
    # use random unique name to avoid ROOT overwriting
    clone_hist = hist.Clone(str(uuid.uuid4()))
    if scale:
      # Scale to luminosity
      mc_scale = self.get_mc_scale()
      clone_hist.Scale(mc_scale)
    clone_hist.SetDirectory(0)
    return clone_hist
  def get_slice_xsec(self):
    mcid = self.mcid
    xsec = None
    xsec_list_path = '/usera/brunt/MET_studies/METPerformance_setup/Reconstruction/MET/METPerformance/data/susy_crosssections_13TeV.txt'
    found_line_list = False
    for line in open(xsec_list_path):
      line_list = line.strip().split()
      if mcid not in line_list: continue
      else:
        found_line_list = line_list
        break
    if found_line_list:
      retrieved_mcid = found_line_list[0]
      sample_name    = found_line_list[1]
      gen_xsec       = float(found_line_list[2])
      kfactor        = float(found_line_list[3])
      gen_eff        = float(found_line_list[4])
      xsec = gen_xsec * kfactor * gen_eff
      print 'Retrieved crossection from list file:', xsec, 'pb'
    else: print 'Failed to find cross-section for mcid', mcid, 'in list file.'
    return xsec #pb
  def get_mc_scale(self):
    sow = self.weights_hist.GetBinContent(5)
    lumi = self.data_lumi #ipb
    xsec = self.get_slice_xsec() #pb
    scale = lumi * xsec / sow
    return scale

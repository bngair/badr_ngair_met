
input_path = '/r03/atlas/brunt/METtesting/2015_recommendations/20151123_ZmmEvents_systematics/'
nominal_input_path = '/r03/atlas/brunt/METtesting/2015_recommendations/20151121_ZmmEvents/'

output_path = 'plots_v1/'

samples = {}

samples['data'] = {
              'data': { 'data':[ nominal_input_path+'user.bbrunt.METPerformance.data.DAOD_JETM3.20151121_ZmmEvents_combined.hadd.root'] },
                   }
samples['Powheg_Zmm'] = {
   'nominal': {
       'nominal':[ nominal_input_path+'user.bbrunt.METPerformance.361107.DAOD_JETM3.20151121_ZmmEvents_combined.hadd.root']
               },
   'MET_SoftTrk_Scale': {
       'MET_SoftTrk_ScaleUp':[ input_path+'user.bbrunt.METPerformance.361107.DAOD_JETM3.MET_SoftTrk_ScaleUp.20151123_ZmmEvents_systematics_combined.hadd.root'],
       'MET_SoftTrk_ScaleDown':[ input_path+'user.bbrunt.METPerformance.361107.DAOD_JETM3.MET_SoftTrk_ScaleDown.20151123_ZmmEvents_systematics_combined.hadd.root'],
                         },
   'MET_SoftTrk_ResoPara': {
       'MET_SoftTrk_ResoPara':[ input_path+'user.bbrunt.METPerformance.361107.DAOD_JETM3.MET_SoftTrk_ResoPara.20151123_ZmmEvents_systematics_combined.hadd.root'],
                         },
   'MET_SoftTrk_ResoPerp': {
       'MET_SoftTrk_ResoPerp':[ input_path+'user.bbrunt.METPerformance.361107.DAOD_JETM3.MET_SoftTrk_ResoPerp.20151123_ZmmEvents_systematics_combined.hadd.root'],
                         },
   }
samples['Powheg_Ztautau'] = {
   'nominal': {
       'nominal':[ nominal_input_path+'user.bbrunt.METPerformance.361108.DAOD_JETM3.20151121_ZmmEvents_combined.hadd.root']
               },
   'MET_SoftTrk_Scale': {
       'MET_SoftTrk_ScaleUp':[ input_path+'user.bbrunt.METPerformance.361108.DAOD_JETM3.MET_SoftTrk_ScaleUp.20151123_ZmmEvents_systematics_combined.hadd.root'],
       'MET_SoftTrk_ScaleDown':[ input_path+'user.bbrunt.METPerformance.361108.DAOD_JETM3.MET_SoftTrk_ScaleDown.20151123_ZmmEvents_systematics_combined.hadd.root'],
                         },
   'MET_SoftTrk_ResoPara': {
       'MET_SoftTrk_ResoPara':[ input_path+'user.bbrunt.METPerformance.361108.DAOD_JETM3.MET_SoftTrk_ResoPara.20151123_ZmmEvents_systematics_combined.hadd.root'],
                         },
   'MET_SoftTrk_ResoPerp': {
       'MET_SoftTrk_ResoPerp':[ input_path+'user.bbrunt.METPerformance.361108.DAOD_JETM3.MET_SoftTrk_ResoPerp.20151123_ZmmEvents_systematics_combined.hadd.root'],
                         },
   }
samples['Diboson'] = {
   'nominal': {
       'nominal':[
                  nominal_input_path+'user.bbrunt.METPerformance.361068.DAOD_JETM3.20151121_ZmmEvents_combined.hadd.root',
                  nominal_input_path+'user.bbrunt.METPerformance.361084.DAOD_JETM3.20151121_ZmmEvents_combined.hadd.root',
                  nominal_input_path+'user.bbrunt.METPerformance.361086.DAOD_JETM3.20151121_ZmmEvents_combined.hadd.root',
                  ]
               },
   'MET_SoftTrk_Scale': {
       'MET_SoftTrk_ScaleUp':[
                  input_path+'user.bbrunt.METPerformance.361068.DAOD_JETM3.MET_SoftTrk_ScaleUp.20151123_ZmmEvents_systematics_combined.hadd.root',
                  input_path+'user.bbrunt.METPerformance.361084.DAOD_JETM3.MET_SoftTrk_ScaleUp.20151123_ZmmEvents_systematics_combined.hadd.root',
                  input_path+'user.bbrunt.METPerformance.361086.DAOD_JETM3.MET_SoftTrk_ScaleUp.20151123_ZmmEvents_systematics_combined.hadd.root',
                              ],
       'MET_SoftTrk_ScaleDown':[
                  input_path+'user.bbrunt.METPerformance.361068.DAOD_JETM3.MET_SoftTrk_ScaleDown.20151123_ZmmEvents_systematics_combined.hadd.root',
                  input_path+'user.bbrunt.METPerformance.361084.DAOD_JETM3.MET_SoftTrk_ScaleDown.20151123_ZmmEvents_systematics_combined.hadd.root',
                  input_path+'user.bbrunt.METPerformance.361086.DAOD_JETM3.MET_SoftTrk_ScaleDown.20151123_ZmmEvents_systematics_combined.hadd.root'],
                         },
   'MET_SoftTrk_ResoPara': {
       'MET_SoftTrk_ResoPara':[
                  input_path+'user.bbrunt.METPerformance.361068.DAOD_JETM3.MET_SoftTrk_ResoPara.20151123_ZmmEvents_systematics_combined.hadd.root',
                  input_path+'user.bbrunt.METPerformance.361084.DAOD_JETM3.MET_SoftTrk_ResoPara.20151123_ZmmEvents_systematics_combined.hadd.root',
                  input_path+'user.bbrunt.METPerformance.361086.DAOD_JETM3.MET_SoftTrk_ResoPara.20151123_ZmmEvents_systematics_combined.hadd.root'],
                         },
   'MET_SoftTrk_ResoPerp': {
       'MET_SoftTrk_ResoPerp':[
                  input_path+'user.bbrunt.METPerformance.361068.DAOD_JETM3.MET_SoftTrk_ResoPerp.20151123_ZmmEvents_systematics_combined.hadd.root',
                  input_path+'user.bbrunt.METPerformance.361084.DAOD_JETM3.MET_SoftTrk_ResoPerp.20151123_ZmmEvents_systematics_combined.hadd.root',
                  input_path+'user.bbrunt.METPerformance.361086.DAOD_JETM3.MET_SoftTrk_ResoPerp.20151123_ZmmEvents_systematics_combined.hadd.root',
                               ],
                         },
   }
samples['ttbar'] = {
   'nominal': {
       'nominal':[ nominal_input_path+'user.bbrunt.METPerformance.410000.DAOD_JETM3.20151121_ZmmEvents_combined.hadd.root']
               },
   'MET_SoftTrk_Scale': {
       'MET_SoftTrk_ScaleUp':[ input_path+'user.bbrunt.METPerformance.410000.DAOD_JETM3.MET_SoftTrk_ScaleUp.20151123_ZmmEvents_systematics_combined.hadd.root'],
       'MET_SoftTrk_ScaleDown':[ input_path+'user.bbrunt.METPerformance.410000.DAOD_JETM3.MET_SoftTrk_ScaleDown.20151123_ZmmEvents_systematics_combined.hadd.root'],
                         },
   'MET_SoftTrk_ResoPara': {
       'MET_SoftTrk_ResoPara':[ input_path+'user.bbrunt.METPerformance.410000.DAOD_JETM3.MET_SoftTrk_ResoPara.20151123_ZmmEvents_systematics_combined.hadd.root'],
                         },
   'MET_SoftTrk_ResoPerp': {
       'MET_SoftTrk_ResoPerp':[ input_path+'user.bbrunt.METPerformance.410000.DAOD_JETM3.MET_SoftTrk_ResoPerp.20151123_ZmmEvents_systematics_combined.hadd.root'],
                         },
                         }

from Sample import Stack, Sample
mc_stack = Stack()
data_sample = None
for sample_name, sample_dict in samples.iteritems():
  sample = Sample(sample_name, sample_dict)
  if sample_name == 'data':
    data_sample = sample
  else:
    mc_stack.addSample(sample)

histos_to_run = [
                 'Zmm/RefTST_met',
                ]

from Plot import Plot
for histoname in histos_to_run:
  plot = Plot()
  plot.setOutputPath(output_path)
  plot.data_sample = data_sample
  plot.stack = mc_stack
  plot.plotDataMCStack(histoname)
#for sample in samples_list:
#  for histoname in histos_to_run:
#    plot = Plot(sample)
#    plot.setOutputPath(output_path)
#    #plot.plotAllVariations(histoname)
#    plot.plotDataStack(histoname)
#    #plot.plotIndivSystematics(histoname)
#    #plot.plotIndivSystematicBands(histoname)
#    #plot.plotCombinedSystematicBand(histoname)

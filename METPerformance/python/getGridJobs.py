import sys
import os

def main():

  #inputListFile   = '/home/petrov/grid/file.txt'
  inputMetHisto   = '/home/${USER}/METPerformance/Reconstruction/MET/METPerformance/grid_histo_MetHistoStream.txt'
  inputEvWeights  = '/home/${USER}/METPerformance/Reconstruction/MET/METPerformance/grid_histo_EvWeights.txt'
  ### you need to set path to download directory
  downloadDir     = '/data/atlas/atlasdata3/petrov/METTestArea/METPerformance_output/'
 
  random = False 

  download(inputMetHisto, downloadDir, random)
  download(inputEvWeights, downloadDir, random)

def download(inputListFile,downloadDir,random):
 
  print 'Read grid job output list from file : ', inputListFile
  print 'Check if downloadDir (',downloadDir,') exist :' , os.path.exists(downloadDir)
  if os.path.exists(downloadDir) == False:
    sys.exit("ERROR :: Download directory doesn't exist!!")

  print ' '
  count = 0

  datasetList = open(inputListFile).read().splitlines()
  for dataset in datasetList:  
    if dataset.startswith('#'):
      print 'skipping : ' + dataset
      continue
    # Make a (temporary) batch job script (for each container)
    # 
    print 'Send datatransfer batchjob for : ',dataset

    fName = 'tmp_rucio_' + str(count) + '.sh'
    f = open(fName, 'w')
    f.write('#PBS -q datatransfer\n')
    f.write('\n')
    f.write('cd /home/${USER}/METPerformance/Reconstruction/MET/METPerformance/python/\n')
    f.write('source setup_dq2_cluster.sh\n')
    f.write('cd ' + downloadDir + ' \n')
    f.write('echo Will download in this directory \n')
    f.write('pwd\n')
    if random:
      f.write('rucio download --nrandom 1 ' + dataset + '\n')
    else:
      f.write('cd ' + dataset + '\n')
      f.write('rucio download ' + dataset + '\n')
      #f.write('dq2-get ' + dataset + '\n')
    f.close() 

    # Send script to the batch farm and delete the script
    #
    os.system('qsub ' + fName)
    os.remove(fName)
    count += 1
    print ' '

if __name__ == '__main__':
  main()

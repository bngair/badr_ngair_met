
def setupSlimAlg(JetSelection,JetColl, JetMinWeightedPt, JetMinEFrac,
                 doCST, doTST, doTrk, SystematicTerm, doEleJetOlapScan,
                 doPileupReweighting,doTriggerMatching,RequireTrigger, PRWFiles, LUMIFiles,
                 doGoodRunCleaning, GRLFiles, doCutBK,
                 EvtType, singleElectronTriggers, singleMuonTriggers, gjetTriggers,
                 doTruthMaps, isMC, Year, fJVTWP, doPFlowJVTScan, doJVTScan, JetEtaMax,CustomJetEtaForw,
                 fJVTCenPt, metCut,UseGhostMuons,
                 verbosity):
    from AthenaCommon.AppMgr import ServiceMgr, ToolSvc
    from AthenaCommon import CfgMgr
    doPFlow  = JetColl == 'EMPFlow'
    jetColl    = 'STCalibAntiKt4'+JetColl+'Jets'
    muonColl   = 'STCalibMuons'
    eleColl    = 'STCalibElectrons'
    tauColl    = 'STCalibTauJets'
    photonColl = 'STCalibPhotons'

    if SystematicTerm != 'Nominal':
        jetColl    += SystematicTerm
        muonColl   += SystematicTerm
        eleColl    += SystematicTerm
        tauColl    += SystematicTerm
        photonColl += SystematicTerm

    #don't do truth matching
    tauSmearingTool = CfgMgr.TauAnalysisTools__TauSmearingTool("TauSmearingTool",
                                                               SkipTruthMatchCheck = True,
                                                               )
    ToolSvc += tauSmearingTool

    susytools = CfgMgr.ST__SUSYObjDef_xAOD("SUSYTools",
                                           TauSmearingTool = tauSmearingTool
                                           )
    susytools.OutputLevel = verbosity

    susytools.ConfigFile= 'METPerformance/SUSYTools_METPerformance.{0}.conf'.format(JetColl)
    susytools.DebugMode= False
    print 
    if doPileupReweighting:
        susytools.PRWConfigFiles=PRWFiles
        susytools.PRWLumiCalcFiles=LUMIFiles
        susytools.AutoconfigurePRWTool=True
    else:
        susytools.PRWConfigFiles=[]
        susytools.PRWLumiCalcFiles=[]

    if doPFlow: susytools.ConfigFile = 'METPerformance/SUSYTools_METPerformance.EMPFlow.conf'
    ToolSvc += susytools

    ############################
    # Good Run List
    grlTool= CfgMgr.GoodRunsListSelectionTool('GRLTool',
                                              GoodRunsListVec=GRLFiles,
                                              PassThrough=False,
                                              )
    ToolSvc+=grlTool

    ######################################################################
    # fJVT selection tools

    fjvtLoose = CfgMgr.JetForwardJvtTool("fjvtLoose",OutputDec="passFJVTLoose");
    fjvtTight = CfgMgr.JetForwardJvtTool("fjvtTight",OutputDec="passFJVTTight",UseTightOP=True);
    fjvtNew = CfgMgr.JetForwardJvtTool("fjvtNew",CentralMaxPt=60e3,UseTightOP=True,OutputDec="passFJVTLoose"); # ForwardMaxPt, CentralMaxStochPt
    fjvt120Loose = CfgMgr.JetForwardJvtTool("fjvt120Loose",ForwardMaxPt=120e3,UseTightOP=False,OutputDec="passFJVT120Loose",CentralMaxPt=fJVTCenPt); # ForwardMaxPt, CentralMaxStochPt
    fjvt90Loose = CfgMgr.JetForwardJvtTool("fjvt90Loose",ForwardMaxPt=90e3,UseTightOP=False,OutputDec="passFJVT120Loose",CentralMaxPt=fJVTCenPt); 

    ToolSvc += fjvtLoose
    ToolSvc += fjvtTight
    ToolSvc += fjvtNew
    ToolSvc += fjvt120Loose
    ToolSvc += fjvt90Loose

    if fJVTWP=='Loose120':
        fjvtLoose=fjvt120Loose
    if fJVTWP=='Loose90':
        fjvtLoose=fjvt90Loose
    fjvtcuts = {
           'None':'',
           'Loose':'passFJVTLoose',
           'Tight':'passFJVTTight',
	   'New':'passFJVTLoose',
	   'Loose120':'passFJVT120Loose',
	   'Loose90':'passFJVT90Loose',
           }

    ######################################################################
    # Configure object and event selection
    # TileTrips currently broken?

    # METPerformance object selection
    metObjSel = CfgMgr.met__METObjSel('METObjSel',
                                      JetColl=jetColl,
                                      MuonColl=muonColl,
                                      EleColl=eleColl,
                                      TauColl=tauColl,
                                      PhotonColl=photonColl,
                                      SUSYTools = susytools,
                                      OutputLevel=verbosity,
                                      )
    ToolSvc += metObjSel

    if   EvtType == 'Zmm': triggers    = singleMuonTriggers
    elif EvtType == 'Wmv': triggers    = singleMuonTriggers
    elif EvtType == 'Wev': triggers    = singleElectronTriggers
    elif EvtType == 'Zee': triggers    = singleElectronTriggers
    elif EvtType == 'gjet':triggers    = gjetTriggers
    else : triggers                    = singleMuonTriggers+singleElectronTriggers

    # Needed for trigger SFs in METPerfAlg and METSlim_AnAlg:
    triggerSF_to_apply = ''
    if triggers == singleMuonTriggers:
      triggerSF_to_apply = 'Muon'
    elif triggers == singleElectronTriggers:
      triggerSF_to_apply = 'Electron'

    metEvtSel = CfgMgr.met__METEvtSel('METEvtSel',
                                      SUSYTools = susytools,
                                      TriggersRequired=triggers,
                                      doTriggerDecision=RequireTrigger,
                                      doTriggerMatch=doTriggerMatching,
                                      metCut=metCut,
                                      OutputLevel=verbosity)
    ToolSvc += metEvtSel

    # setup for R21 MET Calculation
    containers_tst = {
        'Muons':     'GoodMuons',
        'Electrons': 'GoodElectrons',
        'Jets':      jetColl,
        'Photons':   'GoodPhotons',
        'Taus':      'GoodTauJets',
        'MET':       'MET_RefTST'
        }
    #
    containers_cst=containers_tst.copy()
    containers_cst['MET'] = 'MET_RefCST'
    #
    containers_track=containers_tst.copy()
    containers_track['MET'] = 'MET_RefTrk'
    containers_track['Photons'] = ''
    containers_track['Taus'] = ''

    map_prefix = 'METAssoc_AntiKt4'
    core_prefix = 'MET_Core_AntiKt4'
    from METPerformance import METPerformanceJobHelper

    def getParameter(METType, METContainers, JetMinWeightedPt, JetMinEFrac):
        return METPerformanceJobHelper.getMETCalculator(METType, METContainers,
                                                        map_prefix+JetColl,
                                                        core_prefix+JetColl,
                                                        SystematicTerm,
                                                        JetMinWeightedPt=JetMinWeightedPt,
                                                        JetMinEFrac=JetMinEFrac,
                                                        doEleJetOlapScan = True,
                                                        JetSelection = JetSelection,
                                                        DoPFlow=doPFlow,
                                                        FJVTName=fjvtcuts[fJVTWP],
                                                        OutputLevel=verbosity,
                                                        DoJVT=True,
                                                        JetEtaMax=JetEtaMax,
                                                        CustomJetEtaForw=CustomJetEtaForw,
                                                        UseGhostMuons=UseGhostMuons
                                                         )

    def getJVTScanParameter(METType, METContainers, CustomJetJvtCut):
        return METPerformanceJobHelper.getMETCalculator(METType, METContainers,
                                                        map_prefix+JetColl,
                                                        core_prefix+JetColl,
                                                        SystematicTerm,
                                                        JetMinWeightedPt=JetMinWeightedPt,
                                                        JetMinEFrac=JetMinEFrac,
                                                        doEleJetOlapScan = False,
                                                        JetSelection = 'Expert',
                                                        DoPFlow=True,
                                                        FJVTName=fjvtcuts[fJVTWP],
                                                        OutputLevel=verbosity,
                                                        DoJVT=True,
                                                        doPFlowJVTScan=doPFlowJVTScan,
                                                        doJVTScan=doJVTScan,
                                                        CustomCentralJetPt=20e3,
                                                        CustomForwardJetPt=30e3,
                                                        CustomJetJvtPtMax=60e3,
                                                        CustomJetJvtCut=CustomJetJvtCut,
                                                        JetEtaMax=JetEtaMax,
                                                        CustomJetEtaForw=CustomJetEtaForw,
                                                        UseGhostMuons=UseGhostMuons
                                                        )
    def getJVTOptScanParameter(METType, METContainers, CustomJetJvtCut):
        return METPerformanceJobHelper.getMETCalculator(METType, METContainers,
                                                        map_prefix+JetColl,
                                                        core_prefix+JetColl,
                                                        SystematicTerm,
                                                        JetMinWeightedPt=JetMinWeightedPt,
                                                        JetMinEFrac=JetMinEFrac,
                                                        doEleJetOlapScan = False,
                                                        JetSelection = 'Tight',
                                                        FJVTName=CustomJetJvtCut, # hijacking the fjvt selection input
                                                        DoPFlow=doPFlow,
                                                        #FJVTName=fjvtcuts[fJVTWP], # TO be fixed
                                                        OutputLevel=verbosity,
                                                        DoJVT=True,
                                                        doPFlowJVTScan=doPFlowJVTScan,
                                                        doJVTScan=doJVTScan,
                                                        JetEtaMax=JetEtaMax,
                                                        CustomJetEtaForw=CustomJetEtaForw,
                                                        UseGhostMuons=UseGhostMuons
                                                        #CustomJetJvtCut=0.59
                                                        )
    METCalculatorTools=[]
    if doTST:
        my_fjvt = fjvtcuts[fJVTWP]
        if doJVTScan: my_fjvt=''
        tsttool=METPerformanceJobHelper.getMETCalculator('TST', containers_tst,
                                                         map_prefix+JetColl,
                                                         core_prefix+JetColl,
                                                         SystematicTerm,
                                                         JetMinWeightedPt=JetMinWeightedPt,
                                                         JetMinEFrac=JetMinEFrac,
                                                         JetSelection = JetSelection,
                                                         DoPFlow=doPFlow,
                                                         FJVTName=my_fjvt,
                                                         OutputLevel=verbosity,
                                                         DoJVT=True,
                                                         JetEtaMax=JetEtaMax,
                                                         CustomJetEtaForw=CustomJetEtaForw,
                                                         UseGhostMuons=UseGhostMuons
                                                         )
        METCalculatorTools.append(tsttool)
        if doJVTScan:
            containers_tst_FJVT = containers_tst.copy()
            containers_tst_FJVT['MET'] = 'MET_RefTST_'+fjvtcuts[fJVTWP]
            containers_tst_JVT20T60M120L = containers_tst.copy()
            containers_tst_JVT20T60M120L['MET'] = 'MET_RefTST_JVT20T60M120L'
            containers_tst_JVT40T60M120L = containers_tst.copy()
            containers_tst_JVT40T60M120L['MET'] = 'MET_RefTST_JVT40T60M120L'
            containers_tst_JVT30T60M120L = containers_tst.copy()
            containers_tst_JVT30T60M120L['MET'] = 'MET_RefTST_JVT30T60M120L'
            containers_tst_JVT20T80M120L = containers_tst.copy()
            containers_tst_JVT20T80M120L['MET'] = 'MET_RefTST_JVT20T80M120L'
            containers_tst_JVT20T60M90L = containers_tst.copy()
            containers_tst_JVT20T60M90L['MET'] = 'MET_RefTST_JVT20T60M90L'
            containers_tst_JVT20T60M150L = containers_tst.copy()
            containers_tst_JVT20T60M150L['MET'] = 'MET_RefTST_JVT20T60M150L'
            containers_tst_JVT20T60M120VL = containers_tst.copy()
            containers_tst_JVT20T60M120VL['MET'] = 'MET_RefTST_JVT20T60M120VL'
            containers_tst_JVT20T60M120VLEta25 = containers_tst.copy()
            containers_tst_JVT20T60M120VLEta25['MET'] = 'MET_RefTST_JVT20T60M120VLEta25'
            containers_tst_JVT20T60M120VLEta27 = containers_tst.copy()
            containers_tst_JVT20T60M120VLEta27['MET'] = 'MET_RefTST_JVT20T60M120VLEta27'
            scanList = [
                        ('TST_JVT20T60M120VL',containers_tst_JVT20T60M120VL,"passJVT20T60M120VL"),
                        ('TST_JVT20T60M120VLEta25',containers_tst_JVT20T60M120VLEta25,"passJVT20T60M120VLEta25"),
                        ('TST_JVT20T60M120VLEta27',containers_tst_JVT20T60M120VLEta27,"passJVT20T60M120VLEta27"),
                        ('TST_JVT20T60M150L', containers_tst_JVT20T60M150L, "passJVT20T60M150L"),
                        ('TST_JVT20T60M90L',containers_tst_JVT20T60M90L,"passJVT20T60M90L"),
                        ('TST_JVT20T80M120L',containers_tst_JVT20T80M120L,"passJVT20T80M120L"),
                        ('TST_JVT30T60M120L',containers_tst_JVT30T60M120L,"passJVT30T60M120L"),
                        ('TST_JVT20T60M120L',containers_tst_JVT20T60M120L,"passJVT20T60M120L"),
                        ('TST_JVT40T60M120L',containers_tst_JVT40T60M120L,"passJVT40T60M120L"),
                        ('TST_'+fjvtcuts[fJVTWP],containers_tst_FJVT,fjvtcuts[fJVTWP]),
                        ]
            METCalculatorTools += [getJVTOptScanParameter(METType, METContainers, CustomJetJvtCut) for (METType, METContainers, CustomJetJvtCut) in scanList] 

        if doPFlowJVTScan:
            containers_tst_0p3 = containers_tst.copy()
            containers_tst_0p3['MET'] = 'MET_RefTST_0p3'
            containers_tst_0p4 = containers_tst.copy()
            containers_tst_0p4['MET'] = 'MET_RefTST_0p4'
            containers_tst_0p5 = containers_tst.copy()
            containers_tst_0p5['MET'] = 'MET_RefTST_0p5'
            containers_tst_0p59 = containers_tst.copy()
            containers_tst_0p59['MET'] = 'MET_RefTST_0p59'
            scanList = [('TST_0p3',containers_tst_0p3,0.3),('TST_0p4',containers_tst_0p4,0.4),('TST_0p5',containers_tst_0p5,0.5),('TST_0p59',containers_tst_0p59,0.59)]
            METCalculatorTools += [getJVTScanParameter(METType, METContainers, CustomJetJvtCut) for (METType, METContainers, CustomJetJvtCut) in scanList] 

        if doEleJetOlapScan:
            containers_tst_frac00pt00=containers_tst.copy()
            containers_tst_frac00pt00['MET'] = 'MET_RefTST_frac00pt00'
            containers_tst_frac00pt10=containers_tst.copy()
            containers_tst_frac00pt10['MET'] = 'MET_RefTST_frac00pt10'
            containers_tst_frac00pt20=containers_tst.copy()
            containers_tst_frac00pt20['MET'] = 'MET_RefTST_frac00pt20'
            containers_tst_frac00pt30=containers_tst.copy()
            containers_tst_frac00pt30['MET'] = 'MET_RefTST_frac00pt30'
            containers_tst_frac02pt00=containers_tst.copy()
            containers_tst_frac02pt00['MET'] = 'MET_RefTST_frac02pt00'
            containers_tst_frac02pt10=containers_tst.copy()
            containers_tst_frac02pt10['MET'] = 'MET_RefTST_frac02pt10'
            containers_tst_frac02pt20=containers_tst.copy()
            containers_tst_frac02pt20['MET'] = 'MET_RefTST_frac02pt20'
            containers_tst_frac02pt30=containers_tst.copy()
            containers_tst_frac02pt30['MET'] = 'MET_RefTST_frac02pt30' 
            containers_tst_frac05pt00=containers_tst.copy()
            containers_tst_frac05pt00['MET'] = 'MET_RefTST_frac05pt00'
            containers_tst_frac05pt10=containers_tst.copy()
            containers_tst_frac05pt10['MET'] = 'MET_RefTST_frac05pt10'
            containers_tst_frac05pt20=containers_tst.copy()
            containers_tst_frac05pt20['MET'] = 'MET_RefTST_frac05pt20'
            containers_tst_frac05pt30=containers_tst.copy()
            containers_tst_frac05pt30['MET'] = 'MET_RefTST_frac05pt30'    
            scanList = [('TST_frac00pt00',containers_tst_frac00pt00,0.0,0.1), ('TST_frac00pt10',containers_tst_frac00pt10,10.0e3,0.1), ('TST_frac00pt20',containers_tst_frac00pt20,20.0e3,0.1), ('TST_frac00pt30',containers_tst_frac00pt30,30.0e3,0.1),
                        ('TST_frac02pt00',containers_tst_frac02pt00,0.0,0.3), ('TST_frac02pt10',containers_tst_frac02pt10,10.0e3,0.3), ('TST_frac02pt20',containers_tst_frac02pt20,20.0e3,0.3), ('TST_frac02pt30',containers_tst_frac02pt30,30.0e3,0.3),
                        ('TST_frac05pt00',containers_tst_frac05pt00,0.0,0.4), ('TST_frac05pt10',containers_tst_frac05pt10,10.0e3,0.4), ('TST_frac05pt20',containers_tst_frac05pt20,20.0e3,0.4), ('TST_frac05pt30',containers_tst_frac05pt30,30.0e3,0.4)
            ]
            METCalculatorTools += [getParameter(METType, METContainers, JetMinWeightedPt, JetMinEFrac) for (METType, METContainers, JetMinWeightedPt, JetMinEFrac) in scanList]

    if doCST:
        csttool=METPerformanceJobHelper.getMETCalculator('CST', containers_cst,
                                                         map_prefix+JetColl,
                                                         core_prefix+JetColl,
                                                         SystematicTerm,
                                                         JetSelection = JetSelection,
                                                         DoMuonEloss=True,
                                                         DoPFlow=doPFlow,
                                                         FJVTName=fjvtcuts[fJVTWP],
                                                         OutputLevel=verbosity,
                                                         DoJVT=False,
                                                         JetEtaMax=JetEtaMax,
                                                         CustomJetEtaForw=CustomJetEtaForw,
                                                         UseGhostMuons=UseGhostMuons
                                                         )
        METCalculatorTools.append(csttool)

    if doTrk:
        trktool=METPerformanceJobHelper.getMETCalculator('Track', containers_track,
                                                         map_prefix+JetColl,
                                                         core_prefix+JetColl,
                                                         SystematicTerm,
                                                         JetSelection = JetSelection,
                                                         FJVTName=fjvtcuts[fJVTWP],
                                                         OutputLevel=verbosity,
                                                         DoJVT=True,
                                                         UseGhostMuons=UseGhostMuons
                                                         )
        METCalculatorTools.append(trktool)

    if doTruthMaps:
        map_prefix = 'METAssoc_Truth_AntiKt4'
        core_prefix = 'MET_Core_Truth_AntiKt4'
        if doTST:
            tsttool=METPerformanceJobHelper.getMETCalculator('TST', containers_tst,
                                                             map_prefix+JetColl,
                                                             core_prefix+JetColl,
                                                             SystematicTerm,
                                                             JetSelection = JetSelection,
                                                             DoPFlow=doPFlow,
                                                             FJVTName=fjvtcuts[fJVTWP],
                                                             OutputLevel=verbosity,
                                        		     DoJVT=True,
                                                             UseGhostMuons=UseGhostMuons
					                     )
            METCalculatorTools.append(tsttool)

        if doCST:
            csttool=METPerformanceJobHelper.getMETCalculator('CST', containers_cst,
                                                             map_prefix+JetColl,
                                                             core_prefix+JetColl,
                                                             SystematicTerm,
                                                             JetSelection = JetSelection,
                                                             DoMuonEloss=True,
                                                             DoPFlow=doPFlow,
                                                             FJVTName=fjvtcuts[fJVTWP],
                                                             OutputLevel=verbosity,
                                              	  	     DoJVT=True,
                                                             UseGhostMuons=UseGhostMuons
					                     )
            METCalculatorTools.append(csttool)

    ######################################################################
    # Configure METPerfTool and METPerfAlg

    metPerfTool = CfgMgr.met__METPerfTool('METPerfTool',
                                          EleColl='Electrons',
                                          GammaColl='Photons',
                                          TauColl='TauJets',
                                          JetColl = 'AntiKt4'+JetColl+'Jets',
                                          SUSYTools = susytools,
                                          FJVTTool1 = fjvtLoose,
                                          FJVTTool2 = fjvtTight,
                                          ApplyFJVT = fjvtcuts[fJVTWP],
                                          CustomJetEtaForw = CustomJetEtaForw,
                                          SystematicTag=SystematicTerm,
                                          IsSimulation=isMC,
                                          OutputLevel=verbosity
                                          )
    ToolSvc += metPerfTool

    weightStreamName = "histo_EvWeight"
    weightFileName = "weight.{0}.root".format(SystematicTerm)
    from OutputStreamAthenaPool.MultipleStreamManager import MSMgr
    MSMgr.NewRootStream( weightStreamName, weightFileName )

    metSlim_AnAlg = CfgMgr.met__METSlimAlg_AnAlg('METSlimming',
                                                 DoCalib=True,
                                                 DoSelection=True,
                                                 DoCutBK=doCutBK,
                                                 DoPRW=doPileupReweighting, ##claire
                                                 ObjSelTool=metObjSel,
                                                 EvtSelTool=metEvtSel,
                                                 PerfTool=metPerfTool,
                                                 SUSYTools = susytools,
                                                 METCalculators=METCalculatorTools,
                                                 GRLTool=grlTool,
                                                 DoGRL=doGoodRunCleaning, ##Matteo
                                                 IsSimulation = isMC, ## bool added to swich on the lines to get the sum of weight from metadata
                                                 Triggers = triggers,
                                                 TriggerScaleFactor = triggerSF_to_apply,
                                                 OutputLevel=verbosity,
                                                 is20152016=(Year=='2015+2016'),
                                                 RootStreamName=weightStreamName,
                                                 RootDirName='/'+weightStreamName
                                                 )
    return metSlim_AnAlg


def setupOutputStreamXAOD(JetColl,SystematicTerm,writeMaps,writeTracks):

    outputfile='xAOD.METslim.{0}.pool.root'.format(SystematicTerm)

    p4_variables = ['pt', 'eta', 'phi', 'm']
#    jet_variables = ['pt', 'eta', 'phi', 'm', 'bjet', 'Jvt', 'bad', 'baseline', 'signal', 'passOR']
    lep_variables = ['pt', 'eta', 'phi', 'm', 'charge', 'baseline', 'signal', 'passOR']
    track_variables = ['d0', 'z0', 'vz', 'theta', 'definingParametersCovMatrix']

    from OutputStreamAthenaPool.MultipleStreamManager import MSMgr
    xaodStream = MSMgr.NewPoolRootStream( 'StreamAOD', outputfile )
    xaodStream.AddItem('xAOD::EventInfo#EventInfo')
    xaodStream.AddItem('xAOD::EventAuxInfo#EventInfoAux.')
    xaodStream.AddItem('xAOD::JetContainer#AntiKt4TruthJets')
    xaodStream.AddItem('xAOD::JetAuxContainer#AntiKt4TruthJetsAux.%s' % '.'.join(p4_variables))
    xaodStream.AddItem('xAOD::JetContainer#GoodJets')
#    xaodStream.AddItem('xAOD::JetAuxContainer#GoodJetsAux.%s' % '.'.join(jet_variables))
    xaodStream.AddItem('xAOD::JetAuxContainer#GoodJetsAux.')
    xaodStream.AddItem('xAOD::JetContainer#STCalibAntiKt4'+JetColl+'Jets')
#    xaodStream.AddItem('xAOD::ShallowAuxContainer#STCalibAntiKt4'+JetColl+'JetsAux.%s' % '.'.join(jet_variables))
    xaodStream.AddItem('xAOD::ShallowAuxContainer#STCalibAntiKt4'+JetColl+'JetsAux.')
    #xaodStream.AddItem('xAOD::ElectronContainer#Electrons')
    #xaodStream.AddItem('xAOD::ElectronAuxContainer#ElectronsAux.')
    xaodStream.AddItem('xAOD::ElectronContainer#GoodElectrons')
    xaodStream.AddItem('xAOD::AuxContainerBase#GoodElectronsAux.%s' % '.'.join(lep_variables))
    xaodStream.AddItem('xAOD::AuxContainerBase#GoodElectronsAux.')
    #xaodStream.AddItem('xAOD::PhotonContainer#Photons')
    #xaodStream.AddItem('xAOD::PhotonAuxContainer#PhotonsAux.')
    xaodStream.AddItem('xAOD::PhotonContainer#GoodPhotons')
#    xaodStream.AddItem('xAOD::AuxContainerBase#GoodPhotonsAux.%s' % '.'.join(p4_variables))
    xaodStream.AddItem('xAOD::AuxContainerBase#GoodPhotonsAux.')
    #xaodStream.AddItem('xAOD::TauJetContainer#TauJets')
    #xaodStream.AddItem('xAOD::TauJetAuxContainer#TauJetsAux.')
    xaodStream.AddItem('xAOD::TauJetContainer#GoodTauJets')
#    xaodStream.AddItem('xAOD::AuxContainerBase#GoodTauJetsAux.%s' % '.'.join(p4_variables))
    xaodStream.AddItem('xAOD::AuxContainerBase#GoodTauJetsAux.')
    #xaodStream.AddItem('xAOD::MuonContainer#Muons')
    #xaodStream.AddItem('xAOD::MuonAuxContainer#MuonsAux.')
    xaodStream.AddItem('xAOD::MuonContainer#GoodMuons')
#    xaodStream.AddItem('xAOD::AuxContainerBase#GoodMuonsAux.%s' % '.'.join(lep_variables))
    xaodStream.AddItem('xAOD::AuxContainerBase#GoodMuonsAux.')
    xaodStream.AddItem('xAOD::ElectronContainer#BestZee')
    xaodStream.AddItem('xAOD::AuxContainerBase#BestZeeAux.%s' % '.'.join(lep_variables))
    xaodStream.AddItem('xAOD::MuonContainer#BestZmm')
    xaodStream.AddItem('xAOD::AuxContainerBase#BestZmmAux.%s' % '.'.join(lep_variables))
    xaodStream.AddItem('xAOD::ElectronContainer#eleFromW')
    xaodStream.AddItem('xAOD::AuxContainerBase#eleFromWAux.%s' % '.'.join(lep_variables))
    xaodStream.AddItem('xAOD::MuonContainer#muonFromW')
    xaodStream.AddItem('xAOD::AuxContainerBase#muonFromWAux.%s' % '.'.join(lep_variables))
    xaodStream.AddItem('xAOD::MissingETContainer#MET_LocHadTopo')
    xaodStream.AddItem('xAOD::AuxContainerBase#MET_LocHadTopoAux.')
    xaodStream.AddItem('xAOD::MissingETContainer#MET_Truth')
    xaodStream.AddItem('xAOD::AuxContainerBase#MET_TruthAux.')
    xaodStream.AddItem('xAOD::MissingETContainer#MET_RefTST')
    xaodStream.AddItem('xAOD::MissingETAuxContainer#MET_RefTSTAux.')
    xaodStream.AddItem('xAOD::MissingETContainer#MET_Track')
    xaodStream.AddItem('xAOD::AuxContainerBase#MET_TrackAux.')
    xaodStream.AddItem('xAOD::MissingETContainer#MET_RefCST')
    xaodStream.AddItem('xAOD::MissingETAuxContainer#MET_RefCSTAux.')
    xaodStream.AddItem('xAOD::MissingETContainer#MET_RefTrk')
    xaodStream.AddItem('xAOD::MissingETAuxContainer#MET_RefTrkAux.')
    xaodStream.AddItem('xAOD::MissingETContainer#MET_RefTSTTruth')
    xaodStream.AddItem('xAOD::MissingETAuxContainer#MET_RefTSTTruthAux.')
    xaodStream.AddItem('xAOD::MissingETContainer#MET_RefCSTTruth')
    xaodStream.AddItem('xAOD::MissingETAuxContainer#MET_RefCSTTruthAux.')
    xaodStream.AddItem('xAOD::MissingETContainer#MET_Core_AntiKt4'+JetColl)
    xaodStream.AddItem('xAOD::MissingETAuxContainer#MET_Core_AntiKt4'+JetColl+'Aux.')
    xaodStream.AddItem('xAOD::MissingETContainer#MET_Core_Truth_AntiKt4'+JetColl)
    xaodStream.AddItem('xAOD::MissingETAuxContainer#MET_Core_Truth_AntiKt4'+JetColl+'Aux.')

    if writeMaps:
            xaodStream.AddItem('xAOD::MissingETAssociationMap#*')
            xaodStream.AddItem('xAOD::MissingETAuxAssociationMap#*')

    #xaodStream.AddItem('xAOD::CaloClusterContainer#egammaClusters');
    #xaodStream.AddItem('xAOD::AuxContainerBase#egammaClustersAux.');
    #xaodStream.AddItem('xAOD::CaloClusterContainer#CaloCalTopoCluster');
    #xaodStream.AddItem('xAOD::AuxContainerBase#CaloCalTopoClusterAux.');
    #xaodStream.AddItem('xAOD::CaloClusterContainer#*');
    #xaodStream.AddItem('xAOD::CaloClusterAuxContainer#*');

    if writeTracks:
            xaodStream.AddItem('xAOD::TrackParticleContainer#CombinedMuonTrackParticles')
            xaodStream.AddItem('xAOD::AuxContainerBase#CombinedMuonTrackParticlesAux.%s' % '.'.join(track_variables))
            xaodStream.AddItem('xAOD::TrackParticleContainer#InDetTrackParticles')
          #xaodStream.AddItem('xAOD::TrackParticleAuxContainer#InDetTrackParticlesAux.')
            xaodStream.AddItem('xAOD::AuxContainerBase#InDetTrackParticlesAux.%s' % '.'.join(track_variables))
            xaodStream.AddItem('xAOD::TrackParticleContainer#MuonSpectrometerTrackParticles')
            xaodStream.AddItem('xAOD::AuxContainerBase#MuonSpectrometerTrackParticlesAux.%s' % '.'.join(track_variables))
    xaodStream.AddItem('xAOD::VertexContainer#PrimaryVertices')
    xaodStream.AddItem('xAOD::AuxContainerBase#PrimaryVerticesAux.')
    xaodStream.AcceptAlgs(['METSlimming'])

#!/bin/env python

'''Run over lists of samples and produce ntuples of runNumber and eventNumber
to be used in checks for duplicate events.'''

import subprocess, os

username_prefix = 'user.bbrunt.METPerformance'
date_string     = '20151121_checkForDuplicates'

# These should be lists of datasets with one dataset per line
dataset_list_files = ['grid_samples_data_SUSY5.txt',
                      'grid_samples_mc_SUSY5.txt',
                      ]

def runjob(inSet, outSet, destSE=False):
  tmpdir = 'tarballs/'
  tarball = tmpdir+'athena_submit'
  command=''' pathena checkForDuplicates.py --inDS=%s --outDS=%s --outTarBall=%s --mergeOutput''' % (inSet, outSet, tarball)
  if first:
      if not os.path.isdir(tmpdir): os.makedirs(tmpdir)
  else:
      command = command.replace('outTarBall','inTarBall')
  print command
  print
  subprocess.call(command, shell=True)

def datasetID(setName):
  return setName.split('.')[1]

def typeString(setName):
  return setName.split('.')[4]

class SampleList:
  def __init__(self, list_file, isData, combine_samples=False):
    self.combine_samples = combine_samples
    self.isData = isData
    self.sample_list = []
    for line in open(list_file):
      if line.startswith('#'): continue
      else:
        line = line.strip()
        self.sample_list.append(line)
  def get_sample_type(self, sample_name):
    if not self.combine_samples:
      return typeString(sample_name)
    else:
      # check that all the samples in the list are the same and return this
      sample_types_seen = []
      for sample in self.sample_list:
        this_type = typeString(sample)
        if this_type in sample_types_seen: continue
        else: sample_types_seen.append(this_type)
      if len(sample_types_seen) == 1:
        return sample_types_seen[0]
      else:
        if len(sample_types_seen) == 0:
          print "I didn't find any types in that list."
        else:
          print "I found more than one type in that list and don't know what to do."
        return 'DATATYPE'
  def generate_in_out_lists(self):
    # list of inDS, outDS tuples
    in_out_list = []
    if not self.combine_samples:
      for sample in self.sample_list:
        inDS = sample
        outDS = self.outDS_single_sample(sample)
        in_out_list.append( (inDS, outDS) )
    elif self.isData:
      inDS = ','.join(self.sample_list)
      outDS = self.outDS_pattern('data')
      in_out_list.append( (inDS, outDS) )
    else:
      print 'I do not understand what you are trying to do'
    return in_out_list

  def outDS_single_sample(self, sample):
    '''Find the part used to name this sample, and feed it into the pattern.'''
    return self.outDS_pattern(sample)
  def outDS_pattern(self, sample):
    '''Put together the different elements of the outDS name.'''
    outDS_list = []
    outDS_list.append(username_prefix)
    if self.combine_samples:
      outDS_list.append(sample)
    else:
      mcid = datasetID(sample)
      outDS_list.append(mcid)
    outDS_list.append(self.get_sample_type(sample))
    outDS_list.append(date_string)
    return '.'.join(outDS_list)
  def print_samples(self):
    for sample in self.sample_list:
      print sample

tasks_to_launch = []
for list_file in dataset_list_files:
  isData  = True if 'data' in list_file else False
  combine = isData # combine samples into one task if data.
  sample_list = SampleList(list_file, isData, combine)
  for inDS, outDS in sample_list.generate_in_out_lists():
    tasks_to_launch.append( (inDS, outDS) )

first = True
for inDS, outDS in tasks_to_launch:
  runjob(inDS, outDS)
  if first: first = False


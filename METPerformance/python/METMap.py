from AthenaCommon import CfgMgr
def BuildMETCustomAssociationMap(Sequence):
    from METReconstruction.METAssocConfig import METAssocConfig,AssocConfig
    associators = [AssocConfig("EMJet"),
               AssocConfig('Muon'),
               AssocConfig('Ele'),
               AssocConfig('Gamma'),
               AssocConfig('Tau'),
               AssocConfig('Soft')]

    trkseltool = CfgMgr.InDet__InDetTrackSelectionTool("IDTrkSel_METAssoc_d0lt2",
                                          CutLevel="TightPrimary",
                                          maxZ0SinTheta=3,
                                          maxD0=2)
    cfg = METAssocConfig(suffix="AntiKt4EMTopo_d0lt2",
                 buildconfigs=associators,
                 doPFlow=False,
                 trksel=trkseltool)

    from METReconstruction.METRecoFlags import metFlags
    metFlags.METAssocConfigs()[cfg.suffix] = cfg
    from METReconstruction.METAssocConfig import getMETAssocAlg
    assocAlg = getMETAssocAlg('METAssociation')
    Sequence += assocAlg

#!/bin/bash
#ls -1 $W/METTestArea/DAOD_SUSY5/ > inputDir.txt 
cd $W/METTestArea/DAOD_SUSY5/
for i in $(ls -d *_MetHisto*); do echo ${i}; done > inputDir.txt
export INPUT=inputDir.txt
export inputDir=($(< ${INPUT}))
for file in "${inputDir[@]}"
do
	export var=$(echo ${file} | awk -F"_" '{print $1,$2,$3}')
	set -- $var
	source haddscript.sh $1"_"$2"_"$3 
done

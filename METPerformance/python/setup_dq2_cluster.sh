#!/bin/bash
export X509_USER_PROXY=/home/${USER}/grid/myProxy
export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase
source ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh
localSetupDQ2Client --skipConfirm
localSetupPandaClient --noAthenaCheck
localSetupPyAMI
export DQ2_LOCAL_SITE_ID=UKI-SOUTHGRID-OX-HEP_SCRATCHDISK

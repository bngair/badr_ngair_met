#!/bin/env python

import subprocess, os

def runjob(evtType, nFiles, inSet, outSet):
  tmpdir = 'tarballs/'
  tarball = tmpdir+'athena_submit'
  command=''' pathena -c "EvtMax=-1; EvtType='%s'" MakeMETPerfPlots.py --skipScout --noCompile --inDS %s --outDS %s --outTarBall=%s''' % (evtType, inSet, outSet, tarball)
  if first:
      if not os.path.isdir(tmpdir): os.makedirs(tmpdir)
  else:
      command = command.replace('outTarBall','inTarBall')

  print command
  print
  subprocess.call(command, shell=True)

def datasetID(setName):
  return setName.split('.')[3]

def typeString(setName):
  return setName.split('.')[4]

def systString(setName):
  return setName.split('.')[5]

nFiles = -1

inSets = [
          #'user.bbrunt.METSlimR20.361107.DAOD_JETM3.MET_SoftTrk_ResoCorr.20150613.Take2_StreamAOD/',
          #'user.bbrunt.METSlimR20.361107.DAOD_JETM3.MET_SoftTrk_ScaleDown.20150613.Take2_StreamAOD/',
          #'user.bbrunt.METSlimR20.361107.DAOD_JETM3.MET_SoftTrk_ScaleUp.20150613.Take3_StreamAOD/',
          #'user.bbrunt.METSlimR20.361107.DAOD_JETM3.MET_SoftTrk_ResoPara.20150613.Take2_StreamAOD/',
          #'user.bbrunt.METSlimR20.361107.DAOD_JETM3.MET_SoftTrk_ResoPerp.20150613.Take2_StreamAOD/',
          #'user.bbrunt.METSlimR20.361107.DAOD_JETM3.Nominal.20150613.Take2_StreamAOD/',

          'user.bbrunt.METSlimR20.361100.DAOD_JETM2.Nominal.20150709_newJetCalibTag_StreamAOD/',
          'user.bbrunt.METSlimR20.361101.DAOD_JETM2.Nominal.20150709_newJetCalibTag_StreamAOD/',
          'user.bbrunt.METSlimR20.361103.DAOD_JETM2.Nominal.20150709_newJetCalibTag_StreamAOD/',
          'user.bbrunt.METSlimR20.361104.DAOD_JETM2.Nominal.20150709_newJetCalibTag_StreamAOD/',
          'user.bbrunt.METSlimR20.361106.DAOD_JETM3.Nominal.20150709_newJetCalibTag_StreamAOD/',
          'user.bbrunt.METSlimR20.361107.DAOD_JETM3.Nominal.20150709_newJetCalibTag_StreamAOD/',
          'user.bbrunt.METSlimR20.410000.DAOD_JETM7.Nominal.20150709_newJetCalibTag_StreamAOD/',

          ]

evtTypes = {}
evtTypes['361100'] = 'Wev'
evtTypes['361101'] = 'Wmv'
evtTypes['361103'] = 'Wev'
evtTypes['361104'] = 'Wmv'
evtTypes['361106'] = 'Zee'
evtTypes['361107'] = 'Zmm'
evtTypes['410000'] = 'ttbar'

do_systs = False
date_string = "20150713"

first = True
for inSet in inSets:
  suffix = inSet.split('_')[-2].split('.')[-1]
  new_date_string = date_string+'_'+suffix
  evtType = evtTypes[datasetID(inSet)]
  outSet = '.'.join(["user.bbrunt.METPlotsR20",
                     datasetID(inSet),
                     typeString(inSet),
                     new_date_string
                     ])
  if do_systs:
    outSet = '.'.join(["user.bbrunt.METPlotsR20",
                       datasetID(inSet),
                       typeString(inSet),
                       systString(inSet),
                       new_date_string
                       ])
  runjob(evtType, nFiles, inSet, outSet)
  if first: first = False

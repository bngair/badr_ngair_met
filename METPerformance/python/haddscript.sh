#!/bin/bash

Name=$1
Output=${1}.hadd.root

Command="hadd "${Output}

for File in `find ${1}_MetHistoStream | grep root`
do 
  Command=${Command}" "${File}
done

for File in `find ${1}_histo_EvWeights | grep root`
do 
  Command=${Command}" "${File}
done

$Command
#echo $Command

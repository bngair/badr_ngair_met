from ROOT import *
import numpy as np
def generic_ratio_plot(master_data, list_of_histograms, rebin,**kwargs):

    """ Generic Ratio Plot Maker """

    from ROOT import TH1D, TLegend, TLatex, TColor, TCanvas, TPad, TStyle, gROOT, TH1, THStack, TLine, TGraphAsymmErrors, SetOwnership, gPad, TFile, kRed, kBlue, gStyle, kGray
    from array import array
    import ROOT as r
    if master_data.GetEntries() == 0:
        print "EMPTY DATA HISTOGRAM: ",master_data
        return

    gROOT.SetBatch(False)



    TH1.SetDefaultSumw2()
    r.gStyle.SetOptStat(0)

    #Make kBlue transparent

    sum_all = master_data.Clone("sum_all")
    sum_all.SetLineWidth(3)
    sum_all.SetFillColor(kGray)
    sum_all.SetMarkerSize(0)
    SetOwnership(sum_all, 0)

    colours = [2,4,6,8]
    counter = 0

    normalization = [0] * (len(list_of_histograms)+1)
    normalization[0] = 1.0/master_data.Integral()
    master_data.Scale(normalization[0])
    for i in range(len(list_of_histograms)):
        if(rebin): list_of_histograms[i].Rebin(2)
        normalization[i+1] = 1.0/list_of_histograms[i].Integral()
        list_of_histograms[i].Scale(normalization[i+1])
    
    print "master_data Mean: ", master_data.GetMean(), "RMS: ", master_data.GetRMS(), "Integral: ", master_data.Integral()
    for i in range(len(list_of_histograms)):    
        print 'i: ',i, 'mean: ', list_of_histograms[i].GetMean(), "RMS: ", list_of_histograms[i].GetRMS(), "Integral: ", list_of_histograms[i].Integral()
    # master_data.GetXaxis().SetRangeUser(0,400)
    # list_of_histograms[0].GetXaxis().SetRangeUser(0,400)

    for i in list_of_histograms:
        i.SetLineStyle(1)
        i.SetLineWidth(2)
        i.SetLineColor(colours[counter])
        i.SetMarkerStyle(20)
        i.SetMarkerSize(1)
        i.SetMarkerColor(colours[counter])
        counter = counter + 1

    ### Now Plot everything

    c1 = TCanvas()

    mainPad =  TPad("mainPad", "top",    0.0, 0.37, 1.0, 1.00)
    ratioPad = TPad("ratioPad","bottom", 0.0, 0.02, 1.0, 0.37)


    mainPad.SetBottomMargin(0.02)
    ratioPad.SetTopMargin(0.05)
    ratioPad.SetBottomMargin(0.5)

    mainPad.Draw()
    mainPad.cd()

    master_data.SetMaximum((master_data.GetMaximum()*1.2))
    master_data.GetXaxis().SetLabelSize(0.0)
    master_data.GetYaxis().SetTitleSize(0.06)
    master_data.GetYaxis().SetTitleOffset(0.69)
    master_data.GetYaxis().SetTitle("Events")
    master_data.SetMarkerSize(1)
    master_data.SetLineColor(1)
    master_data.SetMarkerColor(1)
    master_data.SetMarkerStyle(20)

    master_title = master_data.GetTitle()
    master_data.SetTitle(kwargs['title'])


    # master_data.Draw("E1")
    for i in list_of_histograms:
        i.Draw("E1")
    master_data.Draw("E1 SAME")
    master_data.Draw("E1 SAME")


    leg = TLegend(0.60,0.75,0.9,0.9)
    # leg.AddEntry(master_data,master_title,"p")
    # for histo in list_of_histograms:
    #     leg.AddEntry(histo,histo.GetName(),"p")
    leg.AddEntry(master_data,"ptdiff00","p")
    leg.AddEntry(list_of_histograms[0],"ptdiff10","p")
    leg.AddEntry(list_of_histograms[1],"ptdiff20","p")
    leg.AddEntry(list_of_histograms[2],"ptdiff30","p")

    leg.Draw()
    gPad.SetLogy()
    gPad.RedrawAxis();

    # Draw Ratio Plot
    c1.cd().SetLogy()

    ratioPad.Draw()
    ratioPad.cd()

    ratio_baseline = master_data.Clone("ratio_baseline")
    ratio_baseline.Divide(master_data)
    ratio_baseline.SetLineWidth(1)
    ratio_baseline.SetLineColor(14)

    ratio_baseline.SetTitle("")
    #ratio_baseline.GetYaxis().SetTitle("#frac{non-fiducial}{signal}")
    ratio_baseline.GetYaxis().SetTitle("")

    ratio_baseline.GetYaxis().SetLabelSize((master_data.GetYaxis().GetLabelSize())*3)
    ratio_baseline.GetYaxis().CenterTitle(1)
    ratio_baseline.GetYaxis().SetTitleOffset(.39)
    ratio_baseline.GetYaxis().SetTitleColor(1)
    ratio_baseline.GetYaxis().SetTitleSize(0.10)
    # ratio_baseline.GetYaxis().SetRangeUser(0,400)

    ratio_baseline.GetXaxis().SetLabelSize((master_data.GetYaxis().GetLabelSize())*3)
    ratio_baseline.GetXaxis().SetLabelSize(0.18)
    ratio_baseline.GetXaxis().SetTitleSize(0.18)
    ratio_baseline.GetXaxis().SetTitleOffset(1.01)
    ratio_baseline.GetXaxis().SetTitle(kwargs['variable'])

    ratio_baseline.SetMaximum(1.5)
    ratio_baseline.SetMinimum(0.5)

    ratio_baseline.GetYaxis().SetNdivisions(02)


    line = TLine(ratio_baseline.GetXaxis().GetXmin(),1,ratio_baseline.GetXaxis().GetXmax(),1)
    line.SetLineWidth(2)
    line.SetLineStyle(2)
    ratio_baseline.SetLineColor(0)
    ratio_baseline.Draw("")

    sum_all_clone = sum_all.Clone("sum_all_clone")
    sum_all_clone.Divide(sum_all)
    sum_all_clone.SetFillColor(kGray)
    sum_all_clone.Draw("E2 SAME")
    line.Draw("SAME")

    for i in list_of_histograms:
        new_hist = i.Clone(i.GetName() + "_clone")
        SetOwnership(new_hist, 0)
        print new_hist
        new_hist.Divide(master_data)
        new_hist.Draw("SAME")

    c1.cd().SetLogy()
    c1.Draw()
    c1.Update()
    c1.SaveAs(kwargs['filename'])

    ################
    mainPad.cd()
    gPad.RedrawAxis();
    gPad.SetLogy()
    ##################

    raw_input("WAIT")

    c1.Close()


'''
Let's begin with the main function
'''

if __name__=='__main__':

  f1 = TFile("metPerfDxAOD.Zee.EMPFlow.Nominal.root") 
  f1.cd("Zee/RefTST_frac00pt00/RefTST")
  h_1 = gDirectory.Get("met")
  f2 = TFile("metPerfDxAOD.Zee.EMPFlow.Nominal.root")
  f2.cd("Zee/RefTST_frac00pt10/RefTST")
  h_2 = gDirectory.Get("met")
  f3 = TFile("metPerfDxAOD.Zee.EMPFlow.Nominal.root") 
  f3.cd("Zee/RefTST_frac00pt20/RefTST")
  h_3 = gDirectory.Get("met")
  f4 = TFile("metPerfDxAOD.Zee.EMPFlow.Nominal.root")
  f4.cd("Zee/RefTST_frac00pt30/RefTST")
  h_4 = gDirectory.Get("met")

  generic_ratio_plot(h_1, [h_2,h_3,h_4], False, title="TST MET frac00", variable="TST MET", filename="TSTMET_frac00.eps")

  f5 = TFile("metPerfDxAOD.Zee.EMPFlow.Nominal.root") 
  f5.cd("Zee/RefTST_frac02pt00/RefTST")
  h_5 = gDirectory.Get("met")
  f6 = TFile("metPerfDxAOD.Zee.EMPFlow.Nominal.root")
  f6.cd("Zee/RefTST_frac02pt10/RefTST")
  h_6 = gDirectory.Get("met")
  f7 = TFile("metPerfDxAOD.Zee.EMPFlow.Nominal.root") 
  f7.cd("Zee/RefTST_frac02pt20/RefTST")
  h_7 = gDirectory.Get("met")
  f8 = TFile("metPerfDxAOD.Zee.EMPFlow.Nominal.root")
  f8.cd("Zee/RefTST_frac02pt30/RefTST")
  h_8 = gDirectory.Get("met")

  generic_ratio_plot(h_5, [h_6,h_7,h_8], False, title="TST MET frac02", variable="TST MET", filename="TSTMET_frac02.eps")

  f9 = TFile("metPerfDxAOD.Zee.EMPFlow.Nominal.root") 
  f9.cd("Zee/RefTST_frac05pt00/RefTST")
  h_9 = gDirectory.Get("met")
  f10 = TFile("metPerfDxAOD.Zee.EMPFlow.Nominal.root")
  f10.cd("Zee/RefTST_frac05pt10/RefTST")
  h_10 = gDirectory.Get("met")
  f11 = TFile("metPerfDxAOD.Zee.EMPFlow.Nominal.root") 
  f11.cd("Zee/RefTST_frac05pt20/RefTST")
  h_11 = gDirectory.Get("met")
  f12 = TFile("metPerfDxAOD.Zee.EMPFlow.Nominal.root")
  f12.cd("Zee/RefTST_frac05pt30/RefTST")
  h_12 = gDirectory.Get("met")

  generic_ratio_plot(h_9, [h_10,h_11,h_12], False, title="TST MET frac05", variable="TST MET", filename="TSTMET_frac05.eps")

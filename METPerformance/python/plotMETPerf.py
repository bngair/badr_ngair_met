import ROOT, AtlasStyle
import sys,os
from MetPlotter import *

ROOT.gROOT.LoadMacro("AtlasLabels.C")

nominal_sample = 'Wvm'
nominal_met_def = 'RefTST'

class ComparisonConfig:
  def __init__(self,
               met_defs = [nominal_met_def],
               samples = [nominal_sample],
               compare_samples = False, # compare samples for a single MET definition
               stack_mc = False,
               ):
    # compare_list: on each plot, compare these samples
    # compare_met_defs: plot these on each plot
    self.met_defs = met_defs
    self.samples = samples
    self.compare_samples = compare_samples
    self.data_lumi = 3210. #ipb
    self.stack_mc = stack_mc


infiles = {}
genList = sys.argv[1:]

def findInfiles(datadir , infiles, genList  ) :
  for dirName, subdirList, fileList in os.walk(datadir):
    for myfile in fileList :
      mcid = None
      generator = genList[0] 
      evtType = genList[1]
      if "hadd" in myfile and evtType in myfile:
        for substr in myfile.split(".") :
          if (substr.isdigit() or (substr == 'data')) :
            mcid = substr
      if os.path.exists('./plots') == False:
        print "Plots dir doesn't exist, Will create one"
        os.system('mkdir plots/')
        # Make directory structure for generators
      if os.path.exists('./plots/'+genList[0]) == False:
        os.system('mkdir plots/'+genList[0])
        typelist = ['Zmm','Zee','Wev','Wmv','ttbar','gjet']
        for evts in typelist:
          os.system('mkdir plots/'+genList[0]+'/'+evts)
        # Fill dictionary with keys
      if mcid :
        if '361108' in mcid or '4100' in mcid or '3610' in mcid:
          infiles[evtType + '_' + mcid] = dirName + myfile
        #uncomment the 2 lines below for Wev
        #elif '361102' in mcid or '361105' in mcid or '361106' in mcid:
        #  infiles[evtType + '_' + mcid] = dirName + myfile
        elif 'data' in mcid:
          infiles[evtType + '_' + mcid] = dirName + myfile
        else:
          #361106 - signal Zee, 361107 - signal Zmm
          if 'Powheg' in generator and ('361106' in mcid or '361107' in mcid):
            infiles[evtType + '_' + mcid] = dirName + myfile
          #361100 and 361103 - signal Wev
          #if 'Powheg' in generator and ('361100' in mcid or '361103' in mcid):
          #  infiles[evtType + '_' + mcid] = dirName + myfile
          elif 'NewSherpa' in generator and ('3633' in mcid or '3634' in mcid):
            infiles[evtType + '_' + mcid] = dirName + myfile
          elif 'OldSherpa' in generator and ('3613' in mcid or '3614' in mcid):
            infiles[evtType + '_' + mcid] = dirName + myfile
          elif 'Madgraph' in generator and '3615' in mcid:
            infiles[evtType + '_' + mcid] = dirName + myfile
          elif 'Alpgen' in generator and ('3617' in mcid or '36180' in mcid):
            infiles[evtType + '_' + mcid] = dirName + myfile
          #if '3633' in mcid or '3634' in mcid:
          #  infiles[evtType + '_' + mcid] = dirName + myfile
          #elif '3613' in mcid or '3614' in mcid:
          #  infiles[evtType + '_' + mcid] = dirName + myfile
          #else:
	  #  pass
          #if '361100' in mcid or '3614' in mcid:
	  #  pass
          #else:
          #  infiles[evtType + '_' + mcid] = dirName + myfile

#datadir = '/data/atlas/atlasdata3/petrov/METTestArea/METPerformance_hadd/'  # one folder which has all the files 
datadir = '/data/atlas/atlasdata3/petrov/METTestArea/METPerformance_hadd_proper_with_corr/'  # one folder which has all the files 
findInfiles (datadir , infiles,genList)
#print infiles

flavours = infiles.keys()
print flavours
outdir = 'plots/'+genList[0]
#all_met_defs = ['RefTST','RefCST','RefTrk']
all_met_defs = ['RefTST']

configs = []
#for flavour in flavours:
#  compare_met_defs = ComparisonConfig(all_met_defs, [flavour])
#  configs.append(compare_met_defs)

compare_met_defs = ComparisonConfig(all_met_defs, flavours,True, True)
configs.append(compare_met_defs)

do_jet_veto_plots = True
do_new_diagnostics = False

doZplots     = False
doWplots     = False
dottbarplots = False
dogjetplots = False
for flavour in flavours:
  if 'Zmm' in flavour or 'Zee' in flavour: doZplots     = True
  if 'Wmv' in flavour or 'Wev' in flavour: doWplots     = True
  if 'ttbar' in flavour:                   dottbarplots = True
  if 'gjet' in flavour:                    dogjetplots = True

# Examples:

#configs = []
#flavours = ['Zmm_noJVF','Zmm']
#compare_met_defs = ComparisonConfig(['RefTST'], flavours, True)
#configs.append(compare_met_defs)

#compare_met_defs = ComparisonConfig(['RefTST'], flavours, True)
#configs.append(compare_met_defs)

for config in configs:
    samples = config.samples
    plotter = MetPlotter(infiles, samples, config.data_lumi,
                         config.compare_samples,
                         config.stack_mc,
                         )

    metlist = config.met_defs
    basicnames = [
        'met','met_0j','met_1j','met_2j',
        'mpx','mpy','sumet','phi',
        'muon_met','muon_mpx','muon_mpy','muon_sumet','muon_phi',
        'ele_met','ele_mpx','ele_mpy','ele_sumet',
        'jet_met','jet_mpx','jet_mpy','jet_sumet','jet_phi',
        'photon_met',
        'tau_met',
        #'jet_met_0j','jet_met_1j','jet_met_2j',
        'soft_met','soft_mpx','soft_mpy','soft_sumet','soft_phi',
        #'soft_met_0j','soft_met_1j','soft_met_2j',
        #'pL_pthard','pT_pthard',
        ]

    scalenames = [
        'residL_pthard','scaleL_pthard',
        'softL_pthard','softT_pthard',
        ]

    resonames = [
        #'residxy_sumet','residL_sumet','residT_sumet',
        #'residxy_LHsumet','residL_LHsumet','residT_LHsumet',
        #'residxy_TSTsumet','residL_TSTsumet','residT_TSTsumet',
        'residxy_CSTsumet','residL_CSTsumet','residT_CSTsumet',
        'residxy_mu','residL_mu','residT_mu',
        'residxy_npv','residL_npv','residT_npv',
        'residL_pthard',
        ]

    if doZplots:
      basicnames += ['pL_ptz','pT_ptz', 'mZ','ptZ']
      scalenames += [ 'residL_ptz','scaleL_ptz' ]
      #scalenames += [ 'residL_ptz' ]
      scalenames += [ 'residL_ptz0j' ]
      scalenames += [ 'residL_ptz1j' ]
      scalenames += [ 'residL_ptz2j' ]
      resonames +=  [ 'residL_ptz' ]
      if do_new_diagnostics:
        scalenames += [ 'ratio_jetpt_ptz' ]
        scalenames += [ 'ratio_jetpt_ptz_1j' ]
        scalenames += [ 'ratio_residL_ptz' ]
        scalenames += [ 'ratio_residL_ptz_1j' ]
        scalenames += [ 'ratio_residL_ptz_1j_etacut' ]
    if doWplots:
      basicnames += ['WmT']
      scalenames += ['residL_truth','biasL_truth','lin_truth']
    if dottbarplots:
      scalenames += ['residL_truth','biasL_truth','lin_truth']
    if dogjetplots:
      scalenames += ['residL_truth','biasL_truth','lin_truth']
    
    if 'Basic' in genList[2]:
      plotter.drawBasicPlots(metlist,basicnames,samples,outdir,False)
      if do_jet_veto_plots:
        plotter.drawBasicPlots(metlist,basicnames,samples,outdir,True)
    if 'Scale' in genList[2]:
      plotter.drawScalePlots(metlist,scalenames,samples,outdir,False)
      if do_jet_veto_plots:
        plotter.drawScalePlots(metlist,scalenames,samples,outdir,True)
    if 'Resolution' in genList[2]:
      plotter.drawResoPlots(metlist,resonames,samples,outdir,False)
      if do_jet_veto_plots:
        plotter.drawResoPlots(metlist,resonames,samples,outdir,True)


def setupTreeAlgs(sequence,doTST,doCST,EvtType,verbosity,doPileupReweighting):
    from AthenaCommon import CfgMgr

    streamName = 'MetTreeStream'
    fileName   = 'tree.metPerfDxAOD.'+EvtType+'.'+JetColl+'.%s.root'%(SystematicTerm)

    evtTypeEnum = {
        'None':0,
        'Zmm':1,
        'Zee':2,
        'Wmv':3,
        'Wev':4,
        'ttbar':5,
        'dijet':6,
        'gjet':7,
        }

    if doTST:
        metTreeTST = CfgMgr.met__METTreeAlg('METTreeTST',
                                            Suffix='RefTST',
                                            JetTerm='RefJet',
                                            SoftTerm='PVSoftTrk' if not doTruthMaps else 'SoftTruthChargedCentral',
                                            RootStreamName=streamName,
                                            RootDirName='/'+EvtType,
                                            EvtType=evtTypeEnum[EvtType],
                                            JetBins=True,
                                            DoTauGammaPlots = True,
                                            DoMuonElossPlots = doMuonElossPlots,
                                            WriteTree=True,
                                            WriteHist=False,
                                            SysName=SystematicTerm,
                                            OutputLevel=verbosity,
                                            doPRW=doPileupReweighting
                                            )
        sequence += metTreeTST

    if doCST:
        metTreeTST = CfgMgr.met__METTreeAlg('METTreeCST',
                                            Suffix='RefCST',
                                            JetTerm='RefJet',
                                            SoftTerm='SoftClus' if not doTruthMaps else 'SoftTruthAll',
                                            RootStreamName=streamName,
                                            RootDirName='/'+EvtType,
                                            EvtType=evtTypeEnum[EvtType],
                                            JetBins=True,
                                            DoTauGammaPlots = True,
                                            DoMuonElossPlots = doMuonElossPlots,
                                            WriteTree=True,
                                            WriteHist=False,
                                            SysName=SystematicTerm,
                                            OutputLevel=verbosity,
                                            doPRW=doPileupReweighting
                                            )
        sequence += metTreeTST

    from OutputStreamAthenaPool.MultipleStreamManager import MSMgr
    MetTreeStream = MSMgr.NewRootStream( streamName, fileName )

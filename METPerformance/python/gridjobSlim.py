#!/bin/env python

import subprocess, os, time

from optparse import OptionParser
parser = OptionParser()
parser.add_option("--user"     , help="Give an alternative username or scope", default=os.environ.get('USER'))
parser.add_option("--dsname"   , help="Give a name for your dataset", default="METPerformance")
parser.add_option("--eventType", help="Choose your event type",
                  choices=("Zmm", "Zee", "Wmv", 'Wev', 'ttbar', 'dijet', 'gjet', 'None'), default="Zmm")

parser.add_option("--version"   , help="Add a version tag", default="tag11tag")
parser.add_option("--jetSelection"   , help="Choose jet selection", choices=("Tight","Loose"), default="Tight")
parser.add_option("--jetCollection"   , help="Choose jet collection", choices=("EMTopo","EMPFlow"), default="EMTopo")
parser.add_option("--addCommand"   , help="Add more commands like the -c option", choices=("","fJVTCenPt=120e3;","JetEtaMax=4.5;","fJVTWP=None;","UseGhostMuons=True;doJVTScan=False;","CustomJetEtaForw=2.5;","CustomJetEtaForw=2.7;"), default="")

parser.add_option('--dryRun'       , help="Do a dry run",action="store_true", default=False)
parser.add_option('--noTruthPtHard', help="Ignore truth PtHard",action="store_true", default=False)
parser.add_option('--noPrw'        , help="Skip Prw",action="store_true", default=False)
parser.add_option('--doTSTSystematics', help="doTSTSystematics", action="store_true", default=False)
(options, args) = parser.parse_args()

username_prefix = '.'.join(['user', options.user, options.dsname])
date_string     = (time.strftime("%Y%m%d"))
event_type      = options.eventType
jet_selection   = options.jetSelection
version_string  = options.version
doSubmit        = not options.dryRun

noTruthPthard = options.noTruthPtHard
doPileupReweighting = not options.noPrw

if jet_selection not in ['Default', 'Tight', 'Loose']:
  print 'These are not the jets you are looking for!'

# systematics options:
do_nominal = True
do_TST_MET_systematics = options.doTSTSystematics

date_string += '_'+event_type+'Events' + (jet_selection if (jet_selection not in ["Default"]) else "") + '.' + version_string

# These should be lists of datasets with one dataset per line
dataset_list_files = []
if event_type == 'Zmm':
  dataset_list_files = [#'../METPerfAnalysis/METPerformance/data/data16_periods.txt',
    #'../METPerfAnalysis/METPerformance/data/data17.txt',
    #'../METPerfAnalysis/METPerformance/data/data18.txt',
    #'../METPerfAnalysis/METPerformance/data/grid_powheg_Zmm_mc16d_tile_AOD.txt',
    #'../METPerfAnalysis/METPerformance/data/grid_powheg_Zmm_mc16d_tile_AOD_2Module.txt',
    #'../METPerfAnalysis/METPerformance/data/grid_powheg_Zmm_mc16d_tile_AOD_8Module.txt',
    #'../METPerfAnalysis/METPerformance/data/grid_sherpa_Zmm_mc16d.txt',
    '../METPerfAnalysis/METPerformance/data/grid_powheg_Zmm_mc16d.txt',
    #'../METPerfAnalysis/METPerformance/data/grid_powheg_Zmm_fail.txt',
    #'../METPerfAnalysis/METPerformance/data/grid_sherpa_Zmm_mc16d_highpT.txt',
                        #'../METPerfAnalysis/METPerformance/data/other_ZZ_mc_p3141.txt',
                        #'../METPerfAnalysis/METPerformance/data/other_ZZ_mc_p3495.txt'
                        ]
elif event_type == 'Zee':
  dataset_list_files = [#'../METPerfAnalysis/METPerformance/data/data16_periods.txt',
    #'../METPerfAnalysis/METPerformance/data/data17.txt', 
    #'../METPerfAnalysis/METPerformance/data/data18.txt', 
    #'../METPerfAnalysis/METPerformance/data/grid_sherpa_Zee_mc16d.txt',
    #'../METPerfAnalysis/METPerformance/data/grid_powheg_Zee_mc16d.txt',
    '../METPerfAnalysis/METPerformance/data/grid_powheg_Zmm_fail.txt',
                        #'../METPerfAnalysis/METPerformance/data/other_ZZ_mc_p3495.txt'
                        ]
elif event_type == 'Wmv':
  dataset_list_files = [#'jetm2_mc.ds',
                        'jetm2_data.ds',
                        ]
elif event_type == "Wev":
  dataset_list_files = ['sherpa21_Wev.txt',
			]

elif event_type == "ttbar":
  dataset_list_files = [#'SUSY5_ttbar.txt',
			#'madgraph_ttbar.txt',
			#'powheg_ttbar.txt',
    #'../METPerfAnalysis/METPerformance/data/other_ttbar_mc_p3495.txt'
    '../METPerfAnalysis/METPerformance/data/other_VBF_mc_p3495.txt'
    #'../METPerfAnalysis/METPerformance/data/other_mc_p3141.txt'
			]
elif event_type == "dijet":
  dataset_list_files = ['SUSY5_ttbar.txt',
			'madgraph_ttbar.txt',
			'powheg_ttbar.txt',
			]
elif event_type == "gjet":
  dataset_list_files = ['gjet.txt'
			]
elif event_type == "None":
  dataset_list_files = ['SUSY5_ttbar.txt',
			'madgraph_ttbar.txt',
			'powheg_ttbar.txt',
			]

TST_MET_systematic_terms = [
                            'MET_SoftTrk_ScaleUp',
                            'MET_SoftTrk_ScaleDown',
                            'MET_SoftTrk_ResoPara',
                            'MET_SoftTrk_ResoPerp',
                            ]

def runjob(inSet, outSet, systematicTerm='Nominal', jet_selection='Default', destSE=False, isData=False):
  tmpdir = '/tmp/%s/tarballs/'%options.user
  tarball = tmpdir+'athena_submit'
  command=''' pathena METPerformance/RunMETPerformance.py -c"EvtType='%s';SystematicTerm='%s';JetSelection='%s';JetColl='%s';noTruthPthard=%s;doPileupReweighting=%s;overrideIsData=%s;runOnGrid=True;%s" --inDS=%s --outDS=%s --excludedSite=ANALY_MPPMU,ANALY_NSC,ANALY_SiGNET,ANALY_FZK,ANALY_FZK_SHORT --forceStaged --outTarBall=%s --mergeOutput''' % (event_type, systematicTerm, jet_selection, options.jetCollection, noTruthPthard, doPileupReweighting,isData,options.addCommand, inSet, outSet, tarball)
  if first:
      if not os.path.isdir(tmpdir): os.makedirs(tmpdir)
  else:
      command = command.replace('outTarBall','inTarBall')
  print command
  print
  if doSubmit: subprocess.call(command, shell=True)

def datasetID(setName):
  return setName.split('.')[1]

def typeString(setName):
  return setName.split('.')[4]

class SampleList:
  def __init__(self, list_file, isData, combine_samples=False):
    self.combine_samples = combine_samples
    self.isData = isData
    self.sample_list = []
    for line in open(list_file):
      if line.startswith('#'): continue
      else:
        line = line.strip()
        self.sample_list.append(line)
  def get_sample_type(self, sample_name):
    if not self.combine_samples:
      return typeString(sample_name)
    else:
      # check that all the samples in the list are the same and return this
      sample_types_seen = []
      for sample in self.sample_list:
        this_type = typeString(sample)
        if this_type in sample_types_seen: continue
        else: sample_types_seen.append(this_type)
      if len(sample_types_seen) == 1:
        return sample_types_seen[0]
      else:
        if len(sample_types_seen) == 0:
          print "I didn't find any types in that list."
        else:
          print "I found more than one type in that list and don't know what to do."
        return 'DATATYPE'
  def generate_in_out_lists(self):
    systematics_to_run = []
    if do_nominal: systematics_to_run += ['Nominal']
    if do_TST_MET_systematics: systematics_to_run += TST_MET_systematic_terms
    # list of inDS, outDS tuples
    in_out_list = []
    if not self.combine_samples:
      for sample in self.sample_list:
        inDS = sample
        for systematicTerm in systematics_to_run:
          outDS = self.outDS_single_sample(sample, systematicTerm)
          in_out_list.append( (inDS, outDS, systematicTerm) )
    elif self.isData:
      inDS = ','.join(self.sample_list)
      outDS = self.outDS_pattern('data','Nominal')
      if do_nominal: in_out_list.append( (inDS, outDS, 'Nominal') )
    else:
      print 'I do not understand what you are trying to do'
    return in_out_list

  def outDS_single_sample(self, sample, systematic):
    '''Find the part used to name this sample, and feed it into the pattern.'''
    return self.outDS_pattern(sample, systematic)
  def outDS_pattern(self, sample, systematic):
    '''Put together the different elements of the outDS name.'''
    outDS_list = []
    outDS_list.append(username_prefix)
    if self.combine_samples:
      outDS_list.append(sample)
    else:
      mcid = datasetID(sample)
      outDS_list.append(mcid)
    outDS_list.append(self.get_sample_type(sample))
    outDS_list.append(systematic)
    outDS_list.append(date_string)
    return '.'.join(outDS_list)
  def print_samples(self):
    for sample in self.sample_list:
      print sample

tasks_to_launch = []
for list_file in dataset_list_files:
  print list_file
  isData  = True if 'data' in list_file.split('/')[-1] else False
  combine = isData # combine samples into one task if data.
  sample_list = SampleList(list_file, isData, combine)
  for inDS, outDS, systematicTerm in sample_list.generate_in_out_lists():
    tasks_to_launch.append( (inDS, outDS, systematicTerm, isData) )

print tasks_to_launch
first = True
for inDS, outDS, systematicTerm, isData in tasks_to_launch:
  runjob(inDS, outDS, systematicTerm, isData = isData, jet_selection=jet_selection, destSE=False)
  if first: first = False


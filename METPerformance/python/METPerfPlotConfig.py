#####################################################################
def setupHistogramAlgs(sequence,EvtType,JetColl,
                       doCST,doTST,doTrack,
                       SystematicTerm,
                       doTruthMaps,doMuonElossPlots,doTracksPlot,
                       doPileupReweighting, doEleJetOlapScan,
                       doFwdJetVeto,
                       FwdJetPtCut,
                       doPFlowJVTScan,
                       doJVTScan,
                       verbosity):
    from AthenaCommon import CfgMgr
    def setScanHistParameter(seq, suffix):
      return CfgMgr.met__METPerfAlg(seq + (('Truth' if doTruthMaps else '')),
                                            Suffix=suffix + ('Truth' if doTruthMaps else ''),
                                            JetTerm='RefJet',
                                            SoftTerm='PVSoftTrk' if not doTruthMaps else 'SoftTruthChargedCentral',
                                            RootStreamName=streamName,
                                            RootDirName='/{0}/{1}/{2}/'.format(EvtType,suffix,'RefTST'),
                                            EvtType=evtTypeEnum[EvtType],
                                            JetBins=True,
                                            DoTauGammaPlots = True,
                                            DoMuonElossPlots = doMuonElossPlots,
					    DoEleJetOlapScan = doEleJetOlapScan,
                                            OutputLevel=verbosity,
                                            DoPRW=doPileupReweighting,
                                            DoPlotTracks=doTracksPlot,
                                            DoFwdJetVeto=False,
					    FwdJetPtCut=FwdJetPtCut
                                            )

    streamName = 'MetHistoStream'
    fileName   = 'metPerfDxAOD.{0}.{1}.{2}.root'.format(EvtType,JetColl,SystematicTerm)

    evtTypeEnum = {
        'None':0,
        'Zmm':1,
        'Zee':2,
        'Wmv':3,
        'Wev':4,
        'ttbar':5,
        'dijet':6,
        'gjet':7,
        }

    #####################################################################
    if doTST:
        metPerfTST_Fwdveto = CfgMgr.met__METPerfAlg('METPerfTST_Fwdveto' + (('Truth' if doTruthMaps else '')),
                                            Suffix='RefTST' + ('Truth' if doTruthMaps else ''),
                                            JetTerm='RefJet',
                                            SoftTerm='PVSoftTrk' if not doTruthMaps else 'SoftTruthChargedCentral',
                                            RootStreamName=streamName,
                                            RootDirName='/{0}/vetoFwdjet/{1}/'.format(EvtType,'RefTST'),
                                            EvtType=evtTypeEnum[EvtType],
                                            JetBins=True,
                                            DoTauGammaPlots = True,
                                            DoMuonElossPlots = doMuonElossPlots,
                                            OutputLevel=verbosity,
                                            DoPRW=doPileupReweighting,
                                            DoPlotTracks=doTracksPlot,
                                            DoFwdJetVeto=True,
                                            FwdJetPtCut=FwdJetPtCut
                                            )
#        sequence +=  metPerfTST_Fwdveto
        metPerfTST = CfgMgr.met__METPerfAlg('METPerfTST' + (('Truth' if doTruthMaps else '')),
                                            Suffix='RefTST' + ('Truth' if doTruthMaps else ''),
                                            JetTerm='RefJet',
                                            SoftTerm='PVSoftTrk' if not doTruthMaps else 'SoftTruthChargedCentral',
                                            RootStreamName=streamName,
                                            RootDirName='/{0}/incljet/{1}/'.format(EvtType,'RefTST'),
                                            EvtType=evtTypeEnum[EvtType],
                                            JetBins=True,
                                            DoTauGammaPlots = True,
                                            DoMuonElossPlots = doMuonElossPlots,
                                            OutputLevel=verbosity,
                                            DoPRW=doPileupReweighting,
                                            DoPlotTracks=doTracksPlot,
                                            DoFwdJetVeto=False
                                            )
        sequence +=  metPerfTST

        metPerfTST_0j = CfgMgr.met__METPerfAlg('METPerfTST_0j' + ('Truth' if doTruthMaps else ''),
                                               Suffix='RefTST' + ('Truth' if doTruthMaps else ''),
                                               JetTerm='RefJet',
                                               SoftTerm='PVSoftTrk' if not doTruthMaps else 'SoftTruthChargedCentral',
                                               RootStreamName=streamName,
                                               RootDirName='/{0}/zerojet/{1}/'.format(EvtType,'RefTST'),
                                               EvtType=evtTypeEnum[EvtType],
                                               DoTauGammaPlots = True,
                                               DoMuonElossPlots = doMuonElossPlots,
                                               DoJetVeto= True,
                                               OutputLevel=verbosity,
                                               DoPRW=doPileupReweighting,
                                               DoPlotTracks=doTracksPlot
                                               )
 #       sequence += metPerfTST_0j
        if doEleJetOlapScan:
          #add sequence/Dir name into scanList
          scanList = [('METPerfTST_frac00pt00','RefTST_frac00pt00'),('METPerfTST_frac00pt10','RefTST_frac00pt10'), ('METPerfTST_frac00pt20','RefTST_frac00pt20'), ('METPerfTST_frac00pt30','RefTST_frac00pt30'),
                      ('METPerfTST_frac02pt00','RefTST_frac02pt00'),('METPerfTST_frac02pt10','RefTST_frac02pt10'), ('METPerfTST_frac02pt20','RefTST_frac02pt20'), ('METPerfTST_frac02pt30','RefTST_frac02pt30'),
                      ('METPerfTST_frac05pt00','RefTST_frac05pt00'),('METPerfTST_frac05pt10','RefTST_frac05pt10'), ('METPerfTST_frac05pt20','RefTST_frac05pt20'), ('METPerfTST_frac05pt30','RefTST_frac05pt30'),
          ]
          sequence += [setScanHistParameter(seq,suffix) for (seq,suffix) in scanList]

        if doPFlowJVTScan:
          scanList = [('METPerfTST_0p3','RefTST_0p3'),('METPerfTST_0p4','RefTST_0p4'),('METPerfTST_0p5','RefTST_0p5'),('METPerfTST_0p59','RefTST_0p59')]
          sequence += [setScanHistParameter(seq,suffix) for (seq,suffix) in scanList]
        if doJVTScan:
          scanList = [('METPerfTST_JVT20T60M120L','RefTST_JVT20T60M120L'),
                      ('METPerfTST_JVT40T60M120L','RefTST_JVT40T60M120L'),
                      ('METPerfTST_JVT30T60M120L','RefTST_JVT30T60M120L'),
                      ('METPerfTST_JVT20T80M120L','RefTST_JVT20T80M120L'),
                      ('METPerfTST_JVT20T60M90L','RefTST_JVT20T60M90L'),
                      ('METPerfTST_JVT20T60M150L','RefTST_JVT20T60M150L'),
                      ('METPerfTST_JVT20T60M120VL','RefTST_JVT20T60M120VL'),
                      ('METPerfTST_JVT20T60M120VLEta25','RefTST_JVT20T60M120VLEta25'),
                      ('METPerfTST_JVT20T60M120VLEta27','RefTST_JVT20T60M120VLEta27'),
                      ('METPerfTST_passFJVTLoose','RefTST_passFJVTLoose'),
                      ]
          sequence += [setScanHistParameter(seq,suffix) for (seq,suffix) in scanList]

    #####################################################################
    if doCST:
        metPerfCST = CfgMgr.met__METPerfAlg('METPerfCST' + ('Truth' if doTruthMaps else ''),
                                            Suffix='RefCST' + ('Truth' if doTruthMaps else ''),
                                            JetTerm='RefJet',
                                            SoftTerm='SoftClus' if not doTruthMaps else 'SoftTruthAll',
                                            RootStreamName=streamName,
                                            RootDirName='/{0}/incljet/{1}/'.format(EvtType,'RefCST'),
                                            EvtType=evtTypeEnum[EvtType],
                                            JetBins=True,
                                            DoTauGammaPlots = True,
                                            DoMuonElossPlots = doMuonElossPlots,
                                            OutputLevel=verbosity,
                                            DoPRW=doPileupReweighting,
                                            DoPlotTracks=doTracksPlot
                                            )
        sequence += metPerfCST
        metPerfCST_0j = CfgMgr.met__METPerfAlg('METPerfCST_0j' + ('Truth' if doTruthMaps else ''),
                                               Suffix='RefCST' + ('Truth' if doTruthMaps else ''),
                                               JetTerm='RefJet',
                                               SoftTerm='SoftClus' if not doTruthMaps else 'SoftTruthAll',
                                               RootStreamName=streamName,
                                               RootDirName='/{0}/zerojet/{1}/'.format(EvtType,'RefCST'),
                                               EvtType=evtTypeEnum[EvtType],
                                               DoTauGammaPlots = True,
                                               DoMuonElossPlots = doMuonElossPlots,
                                               DoJetVeto= True,
                                               OutputLevel=verbosity,
                                               DoPRW=doPileupReweighting,
                                               DoPlotTracks=doTracksPlot
                                               )
        sequence += metPerfCST_0j


    #####################################################################
    if doTrack:
        metPerfTrk = CfgMgr.met__METPerfAlg('METPerfTrack',
                                            Suffix='RefTrk',
                                            JetTerm='RefJet',
                                            SoftTerm='PVSoftTrk',
                                            RootStreamName=streamName,
                                            RootDirName='/{0}/incljet/{1}/'.format(EvtType,'RefTrk'),
                                            EvtType=evtTypeEnum[EvtType],
                                            JetBins=True,
                                            DoTauGammaPlots = False,
                                            OutputLevel=verbosity,
                                            DoPRW=doPileupReweighting,
                                            DoPlotTracks=doTracksPlot,
                                            DoFwdJetVeto=doFwdJetVeto
                                            )
        sequence += metPerfTrk

        metPerfTrk_0j = CfgMgr.met__METPerfAlg('METPerfTrack_0j',
                                               Suffix='RefTrk',
                                               JetTerm='RefJet',
                                               SoftTerm='PVSoftTrk',
                                               RootStreamName=streamName,
                                               RootDirName='/{0}/zerojet/{1}/'.format(EvtType,'RefCST'),
                                               EvtType=evtTypeEnum[EvtType],
                                               DoTauGammaPlots = False,
                                               DoJetVeto= True,
                                               OutputLevel=verbosity,
                                               DoPRW=doPileupReweighting,
                                               DoPlotTracks=doTracksPlot
                                               )
        sequence += metPerfTrk_0j
    #####################################################################

    from OutputStreamAthenaPool.MultipleStreamManager import MSMgr
    MetHistoStream = MSMgr.NewRootStream( streamName, fileName )

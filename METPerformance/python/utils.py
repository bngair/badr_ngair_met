# @Author: Marco Valente <valentem>
# @Date:   1970-01-01T01:00:00+01:00
# @Email:  marco.valente@cern.ch
# @Last modified by:   valentem
# @Last modified time: 2016-11-03T17:27:40+01:00

#print all options in here

import os,sys
from DefaultOptions import *

def GetUsername():
    return os.environ.get('USER')

def setupDefaultOptions(options, dopt=default_options):
    #default_options is defined in DefaultOptions.py
    username = GetUsername()
    if username != None: print 'Hi %s, I am setting up default options for you...' % (username)
    if not (username in dopt.keys()):
        print 'Ops... Unfortunately I can\'t find default options for you. :( I am going to set up the standard ones. :)'
        username = 'default'
    for key,option in dopt['default'].iteritems():
        if key in dopt[username].keys():
            options.setdefault(key,dopt[username][key])
            continue
        if key in options:
            print 'Option ',key,' is already set to ',options[key],'. Skipping...'
            continue;
        options.setdefault(key,option)
    print 'JetSelection:',options['JetSelection']
    if options['JetSelection']=='Tight':
        options.setdefault('FwdJetPtCut',30.0e3)
    elif options['JetSelection']=='Loose':
        options.setdefault('FwdJetPtCut',20.0e3)
    elif options['JetSelection']=='Tenacious':
        options.setdefault('FwdJetPtCut',35.0e3)
    elif options['JetSelection']=='Tighter':
        options.setdefault('FwdJetPtCut',35.0e3)
    else:
        print 'Jet Selection is not defined. Please enter Loose or Tight into util.py'
        sys.exit(0)
    if 'doPFlowJVTScan' in options:
        print 'Pflow JVT scan: ',str(options['doPFlowJVTScan'])
    if 'doJVTScan' in options:
        print 'JVT scan: ',str(options['doJVTScan'])

    print '\nYour Default Options are: \n'
    for key in dopt[username]:
      print key, ': ', dopt[username][key]     
        
def AddLastSlash(string):
    if string != '':
        if string[-1] != '/': string += '/'
    return string

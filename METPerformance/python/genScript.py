import sys
import os

inDir        = '/home/petrov/METPerformance/Reconstruction/MET/METPerformance/python'
command = 'python plotMETPerf.py ' 

print 'Check if inDir (',inDir,') exist :' , os.path.exists(inDir)
if os.path.exists(inDir) == False:
  sys.exit("ERROR :: Dataset Directory doesn't exist!!")

print ' '
count = 0

genList = ['NewSherpa','OldSherpa','Powheg','Alpgen','Madgraph']
#genList = ['NewSherpa','Powheg','Alpgen','Madgraph']
types = ['Zee','Zmm','Wev']
#plots = ['Basic','Scale','Resolution']
plots = ['Basic']
for evt in types:
 for gen in genList:  
   for plot in plots:
     fName = 'tmp_plots_' + evt + '_' + plot + '_' + str(count) + '.sh'
     f = open(fName, 'w')
     f.write('#PBS -l cput=01:59:59\n')
     f.write('#PBS -l walltime=01:59:59\n')
     f.write('\n')
     f.write('cd ' + inDir + ' \n')
     f.write('source /home/petrov/grid/setup_cluster.sh \n')
     f.write('localSetupROOT\n')
     f.write(command + gen + ' ' + evt + ' ' + plot +' \n')
     f.close()
     count += 1
     print ' '
 
     #
     # Change batch script permission
     #
     preliminary_command = 'chmod +x %s' % fName
     os.system(preliminary_command)
     
     #
     # Send batch script to batch farm
     #
     sendBatchScript_command = 'qsub %s' % fName
     os.system(sendBatchScript_command)
     os.system('rm -f ' + fName)

import sys
import os

inDir   = '/home/${USER}/METPerformance/Reconstruction/MET/METPerformance/python'
### you need to set your output and hadd directories
outDir  = '/data/atlas/atlasdata3/petrov/METTestArea/METPerformance_output'
haddDir = '/data/atlas/atlasdata3/petrov/METTestArea/METPerformance_hadd'
command = 'bash %s/haddscript.sh ' % (inDir)

print 'Check if outputDir (',outDir,') exist :' , os.path.exists(outDir)
print 'Check if haddDir (',haddDir,') exist :' , os.path.exists(haddDir)
if os.path.exists(outDir) == False:
  sys.exit("ERROR :: Dataset Directory doesn't exist!!")
if os.path.exists(haddDir) == False:
  sys.exit("ERROR :: Target hadd directory doesn't exist!!")

os.system('cd '+outDir)
os.system('for i in $(ls -d *_MetHisto*); do echo ${i}; done > inputDir.txt')
inputFiles = outDir+'/inputDir.txt' 
print 'Read target hadd file names from file : ', inputFiles

print ' '
count = 0

input_files = open(inputFiles).read().splitlines()
for file in input_files:  
  #reduce filename i.e. remove _MetHistoStream or histo_EvWeights
  filename = ''
  if '_MetHistoStream' in file:
    filename = file.replace("_MetHistoStream", "")  
    
  fName = 'tmp_hadd_' + str(count) + '.sh'
  f = open(fName, 'w')
  f.write('#PBS -l cput=01:59:59\n')
  f.write('#PBS -l walltime=01:59:59\n')
  f.write('\n')
  f.write('source '+inDir+'/setup_cluster.sh \n')
  f.write('cd ' + outDir + ' \n')
  f.write('localSetupROOT\n')
  f.write(command + filename +' \n')
  f.write('mv ' + filename + '.hadd.root ' + haddDir + '/.' +' \n')
  f.close()
  count += 1
  print ' '

  #
  # Change batch script permission
  #
  preliminary_command = 'chmod +x %s' % fName
  os.system(preliminary_command)
  
  #
  # Send batch script to batch farm
  #
  sendBatchScript_command = 'qsub %s' % fName
  os.system(sendBatchScript_command)
  os.system('rm -f ' + fName)

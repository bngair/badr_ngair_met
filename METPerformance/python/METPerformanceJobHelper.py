def getMETCalculator(metType, objcolldict, metMapName, metCoreName, metsystTerm,
		     OutputLevel=3,
                     JetSelection='Tight',
                     FJVTName="",
                     DoMuonEloss = False,
                     JetMinWeightedPt=0.0, JetMinEFrac=0.0, doEleJetOlapScan=False,
		     DoTruth = True,
                     DoPFlow = False,
                     DoTruthJets = False, # Add truth jets to the truth-built MET
                     DoTruthLeptons = False, # Add truth leptons to the truth-built MET
                     CorrectJetPhi=False,
                     DoJVT=True,
		     doPFlowJVTScan=False,
                     doJVTScan=False,
                     CustomCentralJetPt=20e3,
                     CustomForwardJetPt=30e3,
                     CustomJetJvtPtMax=60e3,
                     CustomJetJvtCut=0.2,
                     JetEtaMax=-1.0,
                     CustomJetEtaForw=2.4,
                     UseGhostMuons=True
):
    from AthenaCommon.AppMgr import ToolSvc
    from AthenaCommon import CfgMgr

    DoSoftTruth = ('Truth' in metMapName)
    tooltag = metType
    if DoSoftTruth: tooltag += 'Truth'
    metMaker = getMETMaker(tooltag,objcolldict,
                           JetSelection, FJVTName,
                           DoMuonEloss,
                           DoPFlow,
                           JetMinWeightedPt,
                           JetMinEFrac,
			   DoTruth,
                           DoSoftTruth,DoTruthJets,DoTruthLeptons,
                           CorrectJetPhi=CorrectJetPhi,
                           OutputLevel=OutputLevel,
                           CustomCentralJetPt=CustomCentralJetPt,
                           CustomForwardJetPt=CustomForwardJetPt,
                           CustomJetJvtPtMax=CustomJetJvtPtMax,
                           CustomJetJvtCut=CustomJetJvtCut,
                           JetEtaMax=JetEtaMax,
                           CustomJetEtaForw=CustomJetEtaForw,
                           UseGhostMuons=UseGhostMuons)
 

    metSystTool = None
    if not DoSoftTruth:
        metSystTool = getMETSystTool(tooltag,OutputLevel)

    # setting the jvt. for the CST, we don't
    metCalculator= CfgMgr.met__METCalculator('METCalculator_'+tooltag,
                                             MuonContainerName=objcolldict["Muons"],
                                             ElectronContainerName=objcolldict["Electrons"],
                                             JetContainerName=objcolldict["Jets"],
                                             PhotonContainerName=objcolldict["Photons"],
                                             TauContainerName=objcolldict["Taus"],
                                             METContainerName=objcolldict["MET"]+("Truth" if DoSoftTruth else ""),
                                             METCoreName=metCoreName,
                                             METMapName=metMapName,
                                             METType=tooltag,
					     doEleJetOlapScan = doEleJetOlapScan,
                                             METMakerTool=metMaker,
                                             METSystematicTool=metSystTool,
                                             METSystematicTerm=metsystTerm,
                                             DoJVT=DoJVT,
                                             OutputLevel=OutputLevel,
                                             DoTruth=DoSoftTruth,
					     doPFlowJVTScan = doPFlowJVTScan,
                                             doJVTScan=doJVTScan
                                             #DoTruthLeptons=DoTruthLeptons
                                             )

    ToolSvc+=metCalculator
    return metCalculator

def getMETMaker(tooltag,objcolldict,
                JetSelection, JetRejectionDec,
                DoMuonEloss,
                DoPFlow,
                JetMinWeightedPt,
                JetMinEFrac,
                DoTruth,
                DoSoftTruth, # Use truth maps for the soft term
                DoTruthJets, # Add truth jets to the truth-built MET
                DoTruthLeptons, # Add truth leptons to the truth-built MET
                CorrectJetPhi,
                OutputLevel,
                CustomCentralJetPt,
                CustomForwardJetPt,
                CustomJetJvtPtMax,
                CustomJetJvtCut,
                JetEtaMax,
                CustomJetEtaForw,
                UseGhostMuons
                ):
    from AthenaCommon.AppMgr import ToolSvc
    from AthenaCommon import CfgMgr

    jetconstscale = ''
    if 'EMTopo' in objcolldict["Jets"]:
        jetconstscale = 'JetLCScaleMomentum'
    elif DoTruth and DoTruthJets:
        jetconstscale = ''
    else:
        jetconstscale = 'JetConstitScaleMomentum'

    metMaker = CfgMgr.met__METMaker('METMaker_'+tooltag,
                                    JetConstitScaleMom = jetconstscale,
                                    DoRemoveMuonJets = True,
                                    DoSetMuonJetEMScale = True,
                                    JetSelection=JetSelection,
                                    JetRejectionDec = JetRejectionDec,
                                    DoMuonEloss = DoMuonEloss,
                                    DoPFlow=DoPFlow,
                                    JetMinWeightedPt=JetMinWeightedPt,
                		    JetMinEFrac=JetMinEFrac,
                                    DoSoftTruth=DoSoftTruth,
                                    DoJetTruth=DoTruthJets,
                                    CorrectJetPhi=CorrectJetPhi,
                                    OutputLevel=OutputLevel,
	                            CustomCentralJetPt=CustomCentralJetPt,
                                    CustomForwardJetPt=CustomForwardJetPt,
                                    CustomJetJvtPtMax=CustomJetJvtPtMax,
                                    CustomJetJvtCut=CustomJetJvtCut,
                                    CustomJetEtaMax=JetEtaMax,
                                    CustomJetEtaForw=CustomJetEtaForw,
                                    VeryGreedyPhotons=False,
                                    UseGhostMuons=UseGhostMuons)

    ToolSvc+=metMaker
    if DoSoftTruth:
        metMaker.JetConstitScaleMom = ""
        metMaker.JetSelection="Expert"
        metMaker.CustomJetJvtCut=-1
        metMaker.CorrectJetPhi=True

    return metMaker

def getMETSystTool(tooltag,OutputLevel):
    from AthenaCommon.AppMgr import ToolSvc
    from AthenaCommon import CfgMgr

    syst_config_file_TST = 'TrackSoftTerms.config'
    # For now we only have a single common configuration file

    # if JetSelection == 'Default':
    #   syst_config_file_TST  = 'TrackSoftTerms.config'
    # elif JetSelection == 'Tight':
    #   syst_config_file_TST  = 'TrackSoftTerms.config'
    # elif 'PFlow' in JetSelection:
    #   syst_config_file_TST = 'TrackSoftTerms.config'

    #you can find the most up-to-date configuration files here:
    # /cvmfs/atlas.cern.ch/repo/sw/database/GroupData/METUtilities

    # ConfigPrefix not specified -- use the default
    # Add this to the configuration if desired
    metSystTool=CfgMgr.met__METSystematicsTool("METSystematicsTool_"+tooltag,
                                               ConfigSoftTrkFile  = syst_config_file_TST,
                                               ConfigSoftCaloFile = "",
                                               OutputLevel        = OutputLevel
                                               )
    ToolSvc+=metSystTool
    return metSystTool

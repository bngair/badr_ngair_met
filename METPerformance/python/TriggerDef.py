# @Author: Marco Valente <valentem>
# @Date:   1970-01-01T01:00:00+01:00
# @Email:  marco.valente@cern.ch
# @Last modified by:   mscornaj
# @Last modified time: 2016-09-30T14:24:32+02:00

# WARNING: the ordering of the trigger in the lists is important for the application of SF. Be careful with that!

trigger_def = { '2015' : { 'singleElectronTriggers' : ['HLT_e24_lhmedium_iloose_L1EM20VH','HLT_e60_lhmedium','HLT_e120_lhloose'],
                           'singleMuonTriggers'     : ["HLT_mu24_imedium", "HLT_mu20_iloose_L1MU15", "HLT_mu50"],
                           'singleLeptonTriggers'   : ["HLT_mu24_imedium", "HLT_mu20_iloose_L1MU15", "HLT_mu50", 'HLT_e24_lhmedium_L1EM20VH', 'HLT_e60_lhmedium', 'HLT_e120_lhloose'],
                           'dijetTriggers'          : ["HLT_j15", "HLT_j15_320eta490", "HLT_j25", "HLT_j25_320eta490", "HLT_j35", "HLT_j35_320eta490", "HLT_j45", "HLT_j45_320eta490", "HLT_j60", "HLT_j60_320eta490", "HLT_j110", "HLT_j110_320eta490", "HLT_j175", "HLT_j175_320eta490", "HLT_j260", "HLT_j260_320eta490", "HLT_j360", "HLT_j360_320eta490"],
				'gjetTriggers'   :['HLT_g140_loose','HLT_e300_etcut']
                         },
                '2016' : { 'singleElectronTriggers' : ["HLT_e26_lhtight_nod0_ivarloose", "HLT_e60_lhmedium_nod0", "HLT_e140_lhloose_nod0"],
                           'singleMuonTriggers'     : ["HLT_mu26_ivarmedium","HLT_mu50"],
                           'singleLeptonTriggers'   : ["HLT_mu50", "HLT_mu24_ivarmedium", "HLT_e24_lhtight_nod0_ivarloose", "HLT_e60_lhmedium_nod0", "HLT_e140_lhloose_nod0"],
                           'dijetTriggers'          : ["HLT_j15", "HLT_j15_320eta490", "HLT_j25", "HLT_j25_320eta490", "HLT_j35", "HLT_j35_320eta490", "HLT_j45", "HLT_j45_320eta490", "HLT_j60", "HLT_j60_320eta490", "HLT_j110", "HLT_j110_320eta490", "HLT_j175", "HLT_j175_320eta490", "HLT_j260", "HLT_j260_320eta490", "HLT_j360", "HLT_j360_320eta490"] ,
				'gjetTriggers'   :['HLT_g140_loose','HLT_e300_etcut']
                         },
                '2015+2016': { 'singleElectronTriggers' : ['HLT_e24_lhmedium_L1EM20VH','HLT_e60_lhmedium','HLT_e120_lhloose',"HLT_e26_lhtight_nod0_ivarloose", "HLT_e60_lhmedium_nod0", "HLT_e140_lhloose_nod0"],
                               'singleMuonTriggers'     : ["HLT_mu24_imedium", "HLT_mu20_iloose_L1MU15", "HLT_mu50","HLT_mu26_ivarmedium","HLT_mu50"],
                               'singleLeptonTriggers'   : [],
                               'dijetTriggers'          : ["HLT_j15", "HLT_j15_320eta490", "HLT_j25", "HLT_j25_320eta490", "HLT_j35", "HLT_j35_320eta490", "HLT_j45", "HLT_j45_320eta490", "HLT_j60", "HLT_j60_320eta490", "HLT_j110", "HLT_j110_320eta490", "HLT_j175", "HLT_j175_320eta490", "HLT_j260", "HLT_j260_320eta490", "HLT_j360", "HLT_j360_320eta490"],
				'gjetTriggers'   :['HLT_g140_loose','HLT_e300_etcut']
                         },
		'2017':{  'singleElectronTriggers':['HLT_e26_lhtight_nod0_ivarloose', 'HLT_e60_lhmedium_nod0', 'HLT_e140_lhloose_nod0'], #, 'HLT_e300_etcut'],
			   'singleMuonTriggers'   :['HLT_mu26_ivarmedium', 'HLT_mu50'], #, 'HLT_mu60_0eta105_msonly'],
			  'singleLeptonTriggers'  :['HLT_e26_lhtight_nod0_ivarloose', 'HLT_e60_lhmedium_nod0', 'HLT_e140_lhloose_nod0', 'HLT_mu26_ivarmedium', 'HLT_mu50'], #, 'HLT_mu60_0eta105_msonly'],
				'dijetTriggers'   :[],
				'gjetTriggers'   :['HLT_g140_loose','HLT_e300_etcut']
	  },
		'2018':{  'singleElectronTriggers':['HLT_e26_lhtight_nod0_ivarloose', 'HLT_e60_lhmedium_nod0', 'HLT_e140_lhloose_nod0'], #, 'HLT_e300_etcut'],
			   'singleMuonTriggers'   :['HLT_mu26_ivarmedium', 'HLT_mu50'], #, 'HLT_mu60_0eta105_msonly'],
			  'singleLeptonTriggers'  :['HLT_e26_lhtight_nod0_ivarloose', 'HLT_e60_lhmedium_nod0', 'HLT_e140_lhloose_nod0', 'HLT_mu26_ivarmedium', 'HLT_mu50'], #, 'HLT_mu60_0eta105_msonly'],
				'dijetTriggers'   :[],
				'gjetTriggers'   :['HLT_g140_loose','HLT_e300_etcut']
	  },
              }

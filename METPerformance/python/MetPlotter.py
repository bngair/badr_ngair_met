import ROOT, AtlasStyle
import math
import sys
from RootStyles import *
ROOT.gROOT.SetBatch(True)
ROOT.gStyle.SetOptStat(0)

do_ratio_plots = True

sample_xsec_times_effs = {
    # This list bypasses anything in the provided cross-section list file.
    #'410000': 831.76 * 1. * 0.543 * 2.48, # 2.48 scale fudge
    }

def get_sample_xsec(mcid):
  xsec = False
  if mcid in sample_xsec_times_effs:
    # if sample in bypass list
    xsec = sample_xsec_times_effs[mcid]
    print 'Retrieved cross-section from bypass list:', xsec,'pb'
  else: # look in list file instead
    xsec_list_path = '../data/susy_crosssections_13TeV.txt'

    found_line_list = False
    for line in open(xsec_list_path):
      line_list = line.strip().split()
      if mcid not in line_list: continue
      else:
        found_line_list = line_list
        break
    if found_line_list:
      retrieved_mcid = found_line_list[0]
      sample_name    = found_line_list[1]
      gen_xsec       = float(found_line_list[2])
      kfactor        = float(found_line_list[3])
      gen_eff        = float(found_line_list[4])
      xsec = gen_xsec * kfactor * gen_eff
      print 'Retrieved crossection * kfactor*gen_xsec from list file:', xsec, 'pb'
    else: print 'Failed to find cross-section in list file.'
  return xsec #pb

sample_aod_sow_hack = {
    # Ugly. Use avg weight / event (DAOD) * number of events in AOD.
    # Should use CutBk value, but this is giving odd results.
    #'Zmm_361107': 1889. * 19942848,
    #'Zmm_361108': 1929. * 19197457,
    #'Zmm_410000': 1.007 * 19958779,

    #'Zmm_361396': .67143 * 3969600,
    #'Zmm_361397': .67143 * 1770800,
    #'Zmm_361398': .67143 * 2463600,
    }

colours = {
    'RefCST': ROOT.kBlack,
    'RefTST': ROOT.kGreen+2,
    'RefTrk': ROOT.kBlue,
    'RefCSTRhoJvf': ROOT.kRed,
    'RefTSTRhoJvf': ROOT.kBlue,
    'RefCSTNegE': ROOT.kGray+2,
    'RefCST25ns': ROOT.kOrange+1,
    #'Zmm': ROOT.kGreen+2,
    'Zee': ROOT.kYellow,
    'Wmv': ROOT.kOrange,
    'ttbar': ROOT.kBlack,
    'Zee_361106': ROOT.kGreen+2,
    'Zee_361108': ROOT.kRed+3,
    'Zee_410000': ROOT.kBlue,
    'Zee_361084': ROOT.kCyan,
    'Zee_361086': ROOT.kYellow,
    'Zmm_361107': ROOT.kGreen+2,
    'Zmm_361108': ROOT.kRed+3,
    'Zmm_410000': ROOT.kBlue,
    'Zmm_361084': ROOT.kCyan,
    'Zmm_361086': ROOT.kYellow,
    'Zee_410011': ROOT.kGray,
    'Zee_410012': ROOT.kGray,
    'Zee_410013': ROOT.kGray,
    'Zee_410014': ROOT.kGray,
    'Zmm_410011': ROOT.kGray,
    'Zmm_410012': ROOT.kGray,
    'Zmm_410013': ROOT.kGray,
    'Zmm_410014': ROOT.kGray,
    'Wmv_data'  : ROOT.kBlack,
    'Wev_data'  : ROOT.kBlack,
    'Wev_361068': ROOT.kCyan,#diboson
    'Wev_361081': ROOT.kCyan,
    'Wev_361082': ROOT.kCyan,
    'Wev_361083': ROOT.kCyan,
    'Wev_361084': ROOT.kCyan,
    'Wev_361085': ROOT.kCyan,
    'Wev_361086': ROOT.kCyan,
    'Wev_361087': ROOT.kCyan,
    'Wev_361100': ROOT.kGreen+2,#wenu
    'Wev_361800': ROOT.kGreen+2,#wenu
    'Wev_363460': ROOT.kGreen+2,#wenu
    'Wev_361300': ROOT.kGreen+2,#wenu
    #'Wev_361103': ROOT.kGreen+2,#wenu
    #'Wmv_361101': ROOT.kGreen,#wmnu
    #'Wmv_361104': ROOT.kGreen,#wmnu
    'Wev_361102': ROOT.kRed+3,#wtaunu
    'Wev_361105': ROOT.kRed+3,#wtaunu
    'Wev_361106' : ROOT.kMagenta,#zee
    'Wmv_361107' : ROOT.kCyan,#zmumu
    'Wev_361108' : ROOT.kCyan,#ztautau
    'Wev_410000' : ROOT.kRed,#ttbar
    'Wev_410013' : ROOT.kGray,#ttbar
    }
styles = {
    'RefCST': ROOT.kFullSquare,
    'RefTST': ROOT.kFullCircle,
    'RefTrk': ROOT.kFullTriangleUp,
    'RefCSTRhoJvf': ROOT.kOpenCircle,
    'RefTSTRhoJvf': ROOT.kOpenSquare,
    'RefCSTNegE': ROOT.kFullCross,
    'RefCST25ns': 33,
    'Zmm': ROOT.kFullCircle,
    'Wmv': ROOT.kFullTriangleUp,
    'ttbar': ROOT.kFullSquare,
    'Zmm_data':ROOT.kFullCircle,
    'Zee_data':ROOT.kFullCircle,
    #'Zee_361106':ROOT.kFullTriangleUp,
    #'Zmm_361107':ROOT.kFullTriangleDown,
    'Wev_361100':ROOT.kFullTriangleUp,
    'Wev_361300':ROOT.kFullTriangleDown,
    'Wev_361800':ROOT.kOpenSquare,
    'Wev_361520':ROOT.kFullSquare,
    'Wev_363460':ROOT.kOpenCircle,
    #'Zmm_361396': ROOT.kFullCircle,
    #'Zmm_363364': ROOT.kOpenCircle,
    #'Zmm_361505': ROOT.kFullTriangleUp,
    #'Zee_361500': ROOT.kFullTriangleUp,
    #'Zee_363388': ROOT.kOpenCircle,
    #'Zee_361372': ROOT.kFullCircle,
    #'Zee_361700': ROOT.kOpenSquare,
    #'Zee_361705': ROOT.kOpenSquare,
    #'Zmm_361710': ROOT.kOpenSquare,
    #'Zmm_361715': ROOT.kOpenSquare,
    #'Zee_361106': ROOT.kFullSquare,
    #'Zmm_361107': ROOT.kFullSquare,
    }
xtitles = {
    'met':  'E_{T}^{miss} [GeV]',
    'met_0j':  'E_{T}^{miss} [GeV]',
    'met_1j':  'E_{T}^{miss} [GeV]',
    'met_2j':  'E_{T}^{miss} [GeV]',
    'mpx':  'E_{x}^{miss} [GeV]',
    'mpy':  'E_{y}^{miss} [GeV]',
    'phi':  '#phi^{miss}',
    'sumet':'#SigmaE_{T} [GeV]',
    'muon_met':  'E_{T}^{miss} (Muons) [GeV]',
    'muon_mpx':  'E_{x}^{miss} (Muons) [GeV]',
    'muon_mpy':  'E_{y}^{miss} (Muons) [GeV]',
    'muon_phi':  '#phi^{miss} (Muons)',
    'muon_sumet':'#SigmaE_{T} (Muons) [GeV]',
    'photon_met':  'E_{T}^{miss} (Photons) [GeV]',
    'tau_met':  'E_{T}^{miss} (Taus) [GeV]',
    'ele_met':  'E_{T}^{miss} (Electrons) [GeV]',
    'ele_mpx':  'E_{x}^{miss} (Electrons) [GeV]',
    'ele_mpy':  'E_{y}^{miss} (Electrons) [GeV]',
    'ele_phi':  '#phi^{miss} (Electrons)',
    'ele_sumet':'#SigmaE_{T} (Electrons) [GeV]',
    'jet_met':  'E_{T}^{miss} (RefJet) [GeV]',
    'jet_mpx':  'E_{x}^{miss} (RefJet) [GeV]',
    'jet_mpy':  'E_{y}^{miss} (RefJet) [GeV]',
    'jet_phi':  '#phi^{miss} (RefJet)',
    'jet_sumet':'#SigmaE_{T} (RefJet) [GeV]',
    'soft_met':  'E_{T}^{miss} (Soft) [GeV]',
    'soft_met_0j':  'E_{T}^{miss} (Soft) [GeV]',
    'soft_met_1j':  'E_{T}^{miss} (Soft) [GeV]',
    'soft_met_2j':  'E_{T}^{miss} (Soft) [GeV]',
    'soft_mpx':  'E_{x}^{miss} (Soft) [GeV]',
    'soft_mpy':  'E_{y}^{miss} (Soft) [GeV]',
    'soft_phi':  '#phi^{miss} (Soft)',
    'soft_sumet':'#SigmaE_{T} (Soft) [GeV]',
    'pL_pthard':'E_{T,#parallel}^{miss} (p_{T}^{hard})',
    'pT_pthard':'E_{T,#perp}^{miss} (p_{T}^{hard})',
    'pL_ptz':'E_{T,#parallel}^{miss} (p_{T}^{Z})',
    'pT_ptz':'E_{T,#perp}^{miss} (p_{T}^{Z})',
    'mZ':  'Z boson mass [GeV]',
    'ptZ': 'Z boson p_{T} [GeV]',
    'residL_pthard':'p_{T}^{hard} [GeV]',
    'scaleL_pthard':'p_{T}^{hard} [GeV]',
    'softL_pthard':'p_{T}^{hard} [GeV]',
    'softT_pthard':'p_{T}^{hard} [GeV]',
    'residL_ptz':'p_{T}^{Z} [GeV]',
    'scaleL_ptz':'p_{T}^{Z} [GeV]',
    'residL_sumet':'#SigmaE_{T} [GeV]',
    'residT_sumet':'#SigmaE_{T} [GeV]',
    'residxy_sumet':'#SigmaE_{T} [GeV]',
    'residL_LHsumet':'#SigmaE_{T}(LocHadTopo) [GeV]',
    'residT_LHsumet':'#SigmaE_{T}(LocHadTopo) [GeV]',
    'residxy_LHsumet':'#SigmaE_{T}(LocHadTopo) [GeV]',
    'residL_CSTsumet':'#SigmaE_{T}(event) [GeV]',
    'residT_CSTsumet':'#SigmaE_{T}(event) [GeV]',
    'residxy_CSTsumet':'#SigmaE_{T}(event) [GeV]',
    'residL_mu':'#LT#mu#GT',
    'residT_mu':'#LT#mu#GT',
    'residxy_mu':'#LT#mu#GT',
    'residL_npv':'N_{PV}',
    'residT_npv':'N_{PV}',
    'residxy_npv':'N_{PV}',
    'residL_truth':'E_{T}^{miss} (truth) [GeV]',
    'biasL_truth':'E_{T}^{miss} (truth) [GeV]',
    'lin_truth':'E_{T}^{miss, True} [GeV]',
    'residPhi_truth':'E_{T}^{miss} (truth) [GeV]',
    }
ytitles = {
    'residL_pthard':'#LT#DeltaE_{T,#parallel}^{miss} (p_{T}^{hard})#GT [GeV]',
    #'scaleL_pthard':'#LT#DeltaE_{T,#parallel}^{miss} / p_{T}^{hard}#GT',
    'scaleL_pthard':'1 + #LT (E_{T}^{miss,reco} - E_{T}^{miss,True})_{#parallel} / p_{T}^{hard}#GT',
   #'residL_ptz':'#LT#DeltaE_{T,#parallel}^{miss} (p_{T}^{Z})#GT [GeV]',
    'residL_ptz':'#LT E_{T}^{miss}#upoint A_{Z} #GT [GeV]',
    'residL_ptz0j':'#LT E_{T}^{miss}#upoint A_{Z} #GT [GeV]',
    #'scaleL_ptz':'#LT#DeltaE_{T,#parallel}^{miss} / p_{T}^{Z}#GT',
    'scaleL_ptz':'1 + #LT (E_{T}^{miss,reco} - E_{T}^{miss,True})_{#parallel} / p_{T}^{Z}#GT',
    'softL_pthard':'#LT#DeltaE_{T,#parallel}^{miss}#GT_{soft}',
    'softT_pthard':'#LT#DeltaE_{T,#perp}^{miss}#GT_{soft}',
    'residL_truth':'#LT#DeltaE_{T,#parallel}^{miss} (p_{T}^{true})#GT [GeV]',
    'biasL_truth':'#LT#DeltaE_{T,#parallel}^{miss} / p_{T}^{true}#GT',
    'lin_truth':'#LT (E_{T}^{miss,reco} - E_{T}^{miss,True}) / E_{T}^{miss,True} #GT',
    }
xranges = {
    'met':(0,1000),
    'soft_met':(0,300),
    'soft_met_0j':(0,300),
    'soft_met_1j':(0,300),
    'soft_met_2j':(0,300),
    'residxy_mu':(0.,40),
    'residL_mu':(0.,40),
    'residT_mu':(0.,40),
   #'residL_ptz':(0.,60.),
   #'residL_ptz':(0.,100.), # Run 1
    'residL_ptz':(0.,200.),
    'residL_ptz0j':(0.,60.),
    'residL_pthard':(0.,100.),
    'residxy_npv':(0.,35),
    'residL_npv':(0.,35),
    'residT_npv':(0.,35),
    'residxy_sumet':(0.,1000),
    'residL_sumet':(0.,1000),
    'residT_sumet':(0.,1000),
    'residxy_LHsumet':(0.,1000),
    'residL_LHsumet':(0.,1000),
    'residT_LHsumet':(0.,1000),
    'residxy_CSTsumet':(0.,1000),
    'residL_CSTsumet':(0.,1000),
    'residT_CSTsumet':(0.,1000),
    'biasL_truth':(0,200),
    'residL_truth':(0,400),
    'lin_truth':(0,200),
    'scaleL_ptz':(0.,200),
#   'biasL_truth':(0.,100.),
    }
xranges_jetveto = {
    'residL_ptz':(0.,100.),
    'soft_met':(0,150),
    }
xrangesreso = {
    'residL_npv':(0.,30),
    'residT_npv':(0.,30),
    'residxy_npv':(0.,30),
    'residL_mu':(0.,25),
    'residT_mu':(0.,25),
    'residxy_mu':(0.,25),
    }
zoomedDiagnostic = True
yranges = {
    'mpx':(1e-1,1e8),
    'mpy':(1e-1,1e8),
    'met':(1e-1,1e8),
    'met_1j':(1e-1,1e7),
    'met_2j':(1e-1,1e7),
    'soft_met':(1e-1,1e8),
    'soft_met_0j':(1e-1,1e7),
    'soft_met_1j':(1e-1,1e7),
    'soft_met_2j':(1e-1,1e7),
    'sumet':(1e-1,1e8),
    'jet_met':(1e-1,1e8),
    'ele_met':(1e-1,1e8),
    'muon_met':(1e-1,1e8),
    'photon_met':(1e-1,1e6),
    'tau_met':(1e-1,1e6),
    'ptZ':(1e-1,1e8),
    'residL_pthard':(-100.,40.),
   #'residL_ptz':(-30.,10.),
    'residL_ptz':(-100.,15.), # MG's
   #'residL_ptz':(-15.,5.), # Huajie's
    'residL_ptz0j':(-50.,15.),
    'residL_truth':(-10,15.),
    'biasL_truth':(-0.5,0.5),
    'lin_truth':(-0.5,1.),
    'scaleL_pthard':(0.,2.5),
    'scaleL_ptz':(0.,2.5),
    }
yranges_jetveto = {
    'residL_ptz':(-70.,15.),
    'mpx':(1e-1,1e7),
    'mpy':(1e-1,1e7),
    'met':(1e-1,1e7),
    'met_1j':(1e-1,1e7),
    'met_2j':(1e-1,1e7),
    'soft_met':(1e-1,1e7),
    'soft_met_0j':(1e-1,1e7),
    'soft_met_1j':(1e-1,1e7),
    'soft_met_2j':(1e-1,1e7),
    'photon_met':(1e-1,1e6),
    'tau_met':(1e-1,1e6),
    'sumet':(1e-1,1e7),
    'jet_met':(1e-1,1e7),
    'ele_met':(1e-1,1e7),
    'muon_met':(1e-1,1e7),
    'ptZ':(1e-1,1e7),
    }
if zoomedDiagnostic: yranges['residL_ptz'] = (-15.,5.)
yrangesreso = {
    'residPhi_truth':(0.,1.),
    'residxy_npv':(0.,50.),
    }
if zoomedDiagnostic: 
	yrangesreso['residL_npv'] = (0.,20.)
	yrangesreso['residT_npv'] = (0.,20.)
	yrangesreso['residxy_npv'] = (0.,20.)
	yrangesreso['residL_mu'] = (0.,20.)
	yrangesreso['residT_mu'] = (0.,20.)
	yrangesreso['residxy_mu'] = (0.,20.)
yrangesresottbar = {
    'residxy_LHsumet':(0.,80.),
    'residxy_CSTsumet':(0.,80.),
                    }
ytitlesreso = {
    'residL_pthard':'#sigma(#DeltaE_{T,#parallel}^{miss} (p_{T}^{hard})) [GeV]',
    'residL_ptz':'#sigma(#DeltaE_{T,#parallel}^{miss} (p_{T}^{Z})) [GeV]',
    'residL_sumet':'#sigma(#DeltaE_{T,#parallel}^{miss})',
    'residT_sumet':'#sigma(#DeltaE_{T,#perp}^{miss})',
    'residxy_sumet': '#sigma(#DeltaE_{x,y}^{miss})',
    'residL_LHsumet':'#sigma(#DeltaE_{T,#parallel}^{miss})',
    'residT_LHsumet':'#sigma(#DeltaE_{T,#perp}^{miss})',
    'residxy_LHsumet':'E^{miss}_{x},E^{miss}_{y} RMS Resolution [GeV]',
    'residL_CSTsumet':'#sigma(#DeltaE_{T,#parallel}^{miss})',
    'residT_CSTsumet':'#sigma(#DeltaE_{T,#perp}^{miss})',
    'residxy_CSTsumet':'E^{miss}_{x},E^{miss}_{y} RMS Resolution [GeV]',
    'residL_mu':'#sigma(#DeltaE_{T,#parallel}^{miss})',
    'residT_mu':'#sigma#Delta(E_{T,#perp}^{miss})',
    'residxy_mu':'#sigma(#DeltaE_{x,y}^{miss})',
    'residL_npv':'#sigma(#DeltaE_{T,#parallel}^{miss})',
    'residT_npv':'#sigma(#DeltaE_{T,#perp}^{miss})',
    'residxy_npv':'E^{miss}_{x},E^{miss}_{y} RMS Resolution [GeV]',
    'residPhi_truth':'#sigma(#Delta#phi^{miss})'
    }

logylist = [
    'ptZ',
    'mpx','mpy','sumet',
    'met','met_0j','met_1j','met_2j',
    'jet_met','jet_mpx','jet_mpy','jet_sumet',
    'jet_met_0j','jet_met_1j','jet_met_2j',
    'muon_met','muon_mpx','muon_mpy','muon_sumet',
    'ele_met','ele_mpx','ele_mpy','ele_sumet',
    'soft_met','soft_mpx','soft_mpy','soft_sumet',
    'soft_met_0j','soft_met_1j','soft_met_2j',
    'photon_met','tau_met',
    ]

rebinlist = {
             'met':2, 'met_0j':2,'met_1j':2,'met_2j':2,
             'sumet':2,
             'ptZ':2,
             'soft_met':2, 'soft_met_0j':2,'soft_met_1j':2,'soft_met_2j':2,
             'jet_met':2, 'jet_met_0j':2,'jet_met_1j':2,'jet_met_2j':2,
}
if zoomedDiagnostic:
  rebinlist['residL_ptz']='variable'
  rebinlist['residL_ptz1j']='variable'
  rebinlist['residL_ptz2j']='variable'
  #rebinlist['met']='variable'

variableRebin = {
             'residL_ptz':[i for i in range(0,110,10)]+[110,120, 140,160,200],
             'residL_ptz1j':[i for i in range(0,110,10)]+[110,120, 140,160,200],
             'residL_ptz2j':[i for i in range(0,110,10)]+[110,120, 140,160,200],
             #'met':[i for i in range(0,170,5)]+[170,180,200,250],
             #'softL_pthard':[0, 10, 20, 30, 40, 60, 80, 100, 200],
             #'softT_pthard':[0, 10, 20, 30, 40, 60, 80, 100, 200],
             'softL_pthard':[0, 10, 20, 30, 40, 60, 80, 100, 120, 140, 200],
             'softT_pthard':[0, 10, 20, 30, 40, 60, 80, 100, 120, 140, 200],
                  }
variableRebin_jetveto = {
             #'softL_pthard':[0, 10, 20, 40, 200],
             #'softT_pthard':[0, 10, 20, 40, 200],
             'softL_pthard':[0, 10, 20, 30, 40, 60, 80, 100, 140, 200],
             'softT_pthard':[0, 10, 20, 30, 40, 60, 80, 100, 140, 200],
                         }
legends = {
         'Zee_data':  1,
         'Zmm_data':  1,
}
stacks = {
         'Zee_data':  1,
         'Zmm_data':  1,
         'Wev_data':  1,
}

names = {
         'RefTST':'TST E_{T}^{miss}',
         'RefCST':'CST E_{T}^{miss}',
         'RefTrk':'Track E_{T}^{miss}',
         'Zee_data':  'Data',
         'Zee_361106':'Powheg Z #rightarrow ee',
         'Zee_361108':'Powheg Z #rightarrow #tau#tau',
         'Zee_361084':'Sherpa VV, V=W,Z',
         'Zee_361086':'Sherpa VV, V=W,Z',
         'Zee_410000':'Powheg t#bar{t}',
         'Zee_410011':'Sherpa Single top ',
         'Zee_410012':'Sherpa Single top ',
         'Zee_410013':'Sherpa Single top ',
         'Zee_410014':'Sherpa Single top ',
         'Zee_410015':'Sherpa Single top ',
         'Zee_410016':'Sherpa Single top ',
         'Zmm_data':  'Data',
         'Zmm_361107':'Powheg Z #rightarrow #mu#mu',
         'Zmm_361108':'Powheg Z #rightarrow #tau#tau',
         'Zmm_410000':'Powheg t#bar{t}',
         'Zmm_361084':'Sherpa VV, V=W,Z',
         'Zmm_361086':'Sherpa VV, V=W,Z',
         'Zmm_410011':'Sherpa Single top',
         'Zmm_410012':'Sherpa Single top',
         'Zmm_410013':'Sherpa Single top',
         'Zmm_410014':'Sherpa Single top',
         'Zmm_410015':'Sherpa Single top',
         'Zmm_410016':'Sherpa Single top',
         'Wmv_data'  : 'Data',
         'Wev_data':  'Data',
         'Wev_361100': 'Powheg We#nu'  ,#wenu
         'Wev_410000' : 'Powheg t#bar{t}',#ttbar
         #'Wev_361106' : 'Zee',#zee
         'Wev_361102': 'Powheg W #rightarrow#tau#nu',#wtaunu
         'Wmv_361107' : 'Z#mu#mu',#zmumu
         'Wev_361106' : 'Powheg Z+jets',#ztautau
         'Wev_361084' : 'Sherpa VV, V=W,Z',#ztautau
         'Wev_410013' : 'Sherpa Single top',#ztautau
         #'Wmv_361068': "Diboson",
         # 'Wmv_361081': ROOT.kRed+3,
         # 'Wmv_361082': ROOT.kRed+3,
         # 'Wmv_361083': ROOT.kRed+3,
         # 'Wmv_361084': ROOT.kRed+3,
         # 'Wmv_361085': ROOT.kRed+3,
         # 'Wmv_361086': ROOT.kRed+3,
         # 'Wmv_361087': ROOT.kRed+3,
         #'Wmv_361101': 'W#mu#nu',#wmnu

         }

basic_legend_coords = {
 #'soft_met':(0.60,0.68,0.85,0.93),
 #'soft_met_0j':(0.60,0.68,0.85,0.93),
 #'soft_met_1j':(0.60,0.68,0.85,0.93),
 #'soft_met_2j':(0.60,0.68,0.85,0.93),
 }
basic_textbox_coords = {
  #'sumet' : (0.25,0.65,0.50,0.90,'brNDC'),
  #'mpx':(0.20,0.55,0.45,0.80,'brNDC'),
  #'met':(0.60,0.38,0.85,0.63,'brNDC'),
  #'met_1j':(0.60,0.38,0.85,0.63,'brNDC'),
  #'met_2j':(0.20,0.20,0.45,0.45,'brNDC'),
  #'soft_met':   (0.63,0.42,0.88,0.67,'brNDC'),
  #'soft_met_0j':(0.63,0.42,0.88,0.67,'brNDC'),
  #'soft_met_1j':(0.63,0.42,0.88,0.67,'brNDC'),
  #'soft_met_2j':(0.63,0.42,0.88,0.67,'brNDC'),
 }
basic_textbox_coords_jet_veto = {
  #'met':(0.20,0.65,0.35,0.85,'brNDC'),
  #'met':(0.60,0.38,0.85,0.63,'brNDC'),
 }
basic_atlas_coords = {
  #'sumet':(0.19,0.22,"Internal")
 }
scale_legend_coords = {
  'residL_ptz':(0.60,0.50,0.85,0.70),
 #'lin_truth':(0.30,0.65,0.55,0.85),
 }
scale_textbox_coords = {
  #'lin_truth':(0.35,0.63,0.60,0.88,'brNDC'),
  'residL_truth':(0.2,0.65,0.45,0.90,'brNDC'),
  'biasL_truth':(0.2,0.65,0.45,0.90,'brNDC'),
  'lin_truth':(0.2,0.65,0.45,0.90,'brNDC'),
  #'residL_ptz':(0.20,0.25,0.45,0.50,'brNDC'),
  'residL_ptz':(0.2,0.65,0.45,0.90,'brNDC'),
  'scaleL_ptz':(0.2,0.65,0.45,0.90,'brNDC'),
  'residL_ptz0j':(0.20,0.25,0.45,0.50,'brNDC'),
                        }
scale_textbox_coords_jet_veto = {
  'residL_ptz':(0.19,0.2,0.44,0.45,'brNDC'),
  'scaleL_ptz':(0.19,0.65,0.44,0.90,'brNDC'),
                        }
scale_atlas_coords_jet_veto = {
  'residL_ptz':(0.20,0.87,1,0.05),
 }
scale_atlas_coords = {
  'lin_truth':(0.20,0.20,1,0.05),
  'residL_ptz':(0.20,0.20,1,0.05),
  'residL_ptz0j':(0.20,0.87,1,0.05),
 }
reso_legend_coords = {
  #'residxy_CSTsumet':(0.45,0.68,0.70,0.93),
  'residxy_CSTsumet':(0.60,0.65,0.85,0.90),
  'residxy_TSTsumet':(0.60,0.30,0.85,0.55),
  'residxy_mu':(0.60,0.25,0.85,0.5),
  'residxy_npv':(0.60,0.25,0.85,0.5),
 }
reso_textbox_coords = {
  'residxy_TSTsumet':(0.35,0.30,0.60,0.55,'brNDC'),
  'residxy_npv':(0.20,0.2,0.45,0.45,'brNDC'),
  'residxy_mu':(0.20,0.2,0.45,0.45,'brNDC'),
 }

# These plots have 0,1,2+,inc jet bins.
# Don't look in the jet_veto directory to find these.
jet_bins = [
    'met_0j','met_1j','met_2j',
    'soft_met_0j','soft_met_1j','soft_met_2j',
    'jet_met_0j','jet_met_1j','jet_met_2j',
    'residL_ptz0j', 'residL_ptz1j', 'residL_ptz2j',
            ]

listofTH2_bkgs = []
listofTH2_powheg = []
listofTH2_sherpa = []
listofTH2_oldsherpa = []
listofTH2_madgraph = []
listofTH2_alpgen = []

class MetPlotter:
    def __init__(self,filenames_dict,samples, data_lumi, compare_samples=False, stack_mc = False):
      self.normalizeHists = False
      self.normalize_to_lumi = True
      self.infiles = {}
      self.data_lumi = data_lumi
      print data_lumi

      for sample in samples:
        self.infiles[sample] = ROOT.TFile(filenames_dict[sample])

      self.addHistsToStack(samples)

      for sample in samples :
        if not 'data' in sample : print self.get_mc_scale(sample)
      #samples.sort(key=lambda sample: self.get_mc_scale(sample) if not 'data' in sample else 1, reverse=True)
      #print stacks.keys()
      #print samples
      #for sample in samples:
      #  print sample
      #  if sample not in stacks.keys():
      #    print sample
      #    samples.remove(sample)
      samples.sort(key = lambda sample: self.get_stack_pos(sample), reverse = True )
      print samples
      #sys.exit('exit')
      #import sys
      #self.infile = self.infiles[samples[0]]
      self.compare_samples = compare_samples
      self.stack_mc = stack_mc
      self.plots = {}
      self.total_mc = False
      self.diboson_mc = False
      self.singlet_mc = False

    def get_sow_from_histogram(self, sample):
      print sample
      sample_file = self.infiles[sample]
      weights_hist = sample_file.Get('h_allWeights')
      sow = weights_hist.GetBinContent(5) # this has CBk weight in current setup.
      return sow

    def get_stack_pos(self, sample):
      stack_pos = stacks[sample]
      return stack_pos

    def get_legend_pos(self, sample):
      legend_pos = legends[sample]
      return legend_pos

    def get_mc_scale(self, sample):
      #sow = 1.
      sow = self.get_sow_from_histogram(sample)
      lumi = self.data_lumi #ipb
      mcid = sample.split('_')[-1]
      xsec = get_sample_xsec(mcid) #pb
      scale = lumi * xsec / sow
      return scale

    def addHistsToStack(self,samples):
      if any("Wev" in sample for sample in samples):
       for sample in samples:
        j = 1
        if '361100' in sample:
           j+=1
           i = 361100
           stacks.update({'Wev_%s' % str(i):j})
           legends.update({'Wev_%s' % str(i):'Powheg W #rightarrow e#nu'})
           names.update({'Wev_%s' % str(i):'Powheg W #rightarrow e#nu'})
           #colours.update({'Wev_%s' % str(i):ROOT.kGreen+2})	   
           colours.update({'Wev_%s' % str(i):ROOT.kBlue+2})	   
           styles.update({'Wev_%s' % str(i):ROOT.kOpenCircle})	   
           listofTH2_powheg.append('Wev_%s' % str(i))
        if '361103' in sample:
           j+=1
           i = 361103
           stacks.update({'Wev_%s' % str(i):j})
           legends.update({'Wev_%s' % str(i):'Powheg W #rightarrow e#nu'})
           names.update({'Wev_%s' % str(i):'Powheg W #rightarrow e#nu'})
           #colours.update({'Wev_%s' % str(i):ROOT.kGreen+2})	   
           colours.update({'Wev_%s' % str(i):ROOT.kBlue+2})	   
           styles.update({'Wev_%s' % str(i):ROOT.kOpenCircle})	   
           listofTH2_powheg.append('Wev_%s' % str(i))
        if '3618' in sample:
         for i in range(361800,361806):
           j+=1
           stacks.update({'Wev_%s' % str(i):j})
           legends.update({'Wev_%s' % str(i):'Alpgen W #rightarrow e#nu'})
           names.update({'Wev_%s' % str(i):'Alpgen W #rightarrow e#nu'})
           colours.update({'Wev_%s' % str(i):ROOT.kGreen+2})	   
           styles.update({'Wev_%s' % str(i):ROOT.kFullTriangleUp})	   
           listofTH2_alpgen.append('Wev_%s' % str(i))
        if '3613' in sample:
         for i in range(361300,361324):
           j+=1
           stacks.update({'Wev_%s' % str(i):j})
           legends.update({'Wev_%s' % str(i):'Sherpa 2.1 W #rightarrow e#nu'})
           names.update({'Wev_%s' % str(i):'Sherpa 2.1 W #rightarrow e#nu'})
           colours.update({'Wev_%s' % str(i):ROOT.kGreen+2})	   
           styles.update({'Wev_%s' % str(i):ROOT.kFullTriangleDown})	   
           listofTH2_oldsherpa.append('Wev_%s' % str(i))
        if '3634' in sample:
         for i in range(363460,363484):
           j+=1
           stacks.update({'Wev_%s' % str(i):j})
           legends.update({'Wev_%s' % str(i):'Sherpa 2.2 W #rightarrow e#nu'})
           names.update({'Wev_%s' % str(i):'Sherpa 2.2 W #rightarrow e#nu'})
           #colours.update({'Wev_%s' % str(i):ROOT.kGreen+2})	   
           styles.update({'Wev_%s' % str(i):ROOT.kOpenSquare})	   
           colours.update({'Wev_%s' % str(i):ROOT.kRed+2})	   
           listofTH2_sherpa.append('Wev_%s' % str(i))
        if '3615' in sample:
         for i in range(361520,361525):
           j+=1
           stacks.update({'Wev_%s' % str(i):j})
           legends.update({'Wev_%s' % str(i):'Madgraph W#rightarrow e#nu'})
           names.update({'Wev_%s' % str(i):'Madgraph W#rightarrow e#nu'})
           #colours.update({'Wev_%s' % str(i):ROOT.kGreen+2})	   
           colours.update({'Wev_%s' % str(i):ROOT.kYellow+2})	   
           styles.update({'Wev_%s' % str(i):ROOT.kFullSquare})	   
           listofTH2_madgraph.append('Wev_%s' % str(i))
        somearray = [361102,361105,361106,361108,361084]
        for i in somearray:
          j+=1
          stacks.update({ 'Wev_%s' % str(i):j})
          listofTH2_bkgs.append('Wev_%s' % str(i))
        for i in range(361064,361069):
          j+=1
          stacks.update({'Wev_%s' % str(i):j})
          legends.update({'Wev_%s' % str(i):'Sherpa VV, V=W,Z'})
          listofTH2_bkgs.append('Wev_%s' % str(i))
        somearray2 = [361086,361081,361082,361083,410000,410013,410011,410012,410015,410016,410014]
        for i in somearray2:
          j+=1
          stacks.update({ 'Wev_%s' % str(i):j})
          listofTH2_bkgs.append('Wev_%s' % str(i))
      if any("Zee" in sample for sample in samples):
       j = 1
       for sample in samples:
        if '361106' in sample:
           j+=1
           i = 361106
           stacks.update({'Zee_%s' % str(i):j})
           legends.update({'Zee_%s' % str(i):'Powheg+bkgs Z #rightarrow ee'})
           names.update({'Zee_%s' % str(i):'Powheg+bkgs Z #rightarrow ee'})
           #colours.update({'Zee_%s' % str(i):ROOT.kGreen+2})	   
           colours.update({'Zee_%s' % str(i):ROOT.kBlue+2})
           styles.update({'Zee_%s' % str(i):ROOT.kOpenTriangleDown})
           listofTH2_powheg.append('Zee_%s' % str(i))
        if '361372' in sample:
         for i in range(361372,361396):
           j+=1
           stacks.update({'Zee_%s' % str(i):j})
           legends.update({'Zee_%s' % str(i):'Sherpa2.1+bkgs Z #rightarrow ee'})
           names.update({'Zee_%s' % str(i):'Sherpa2.1+bkgs Z #rightarrow ee'})
           #colours.update({'Zee_%s' % str(i):ROOT.kGreen+2})	   
           colours.update({'Zee_%s' % str(i):ROOT.kBlue+2})	   
           #styles.update({'Zee_%s' % str(i):ROOT.kOpenSquare})
           styles.update({'Zee_%s' % str(i):ROOT.kOpenTriangleDown})
           listofTH2_oldsherpa.append('Zee_%s' % str(i))
        if '363388' in sample or '3634' in sample:
         for i in range(363388,363412):
           j+=1
           stacks.update({'Zee_%s' % str(i):j})
           legends.update({'Zee_%s' % str(i):'Sherpa2.2+bkgs Z #rightarrow ee'})
           names.update({'Zee_%s' % str(i):'Sherpa2.2+bkgs Z #rightarrow ee'})
           #colours.update({'Zee_%s' % str(i):ROOT.kGreen+2})	   
           colours.update({'Zee_%s' % str(i):ROOT.kRed+2})
           styles.update({'Zee_%s' % str(i):ROOT.kOpenCircle})
           listofTH2_sherpa.append('Zee_%s' % str(i))
        if '361500' in sample:
         for i in range(361500,361505):
           j+=1
           stacks.update({'Zee_%s' % str(i):j})
           legends.update({'Zee_%s' % str(i):'Madgraph+bkgs Z#rightarrow ee'})
           names.update({'Zee_%s' % str(i):'Madgraph+bkgs Z#rightarrow ee'})
           colours.update({'Zee_%s' % str(i):ROOT.kGreen+2})	   
           styles.update({'Zee_%s' % str(i):ROOT.kOpenTriangleUp})
           listofTH2_madgraph.append('Zee_%s' % str(i))
        if '361700' in sample:
         for i in range(361700,361706):
           j+=1
           stacks.update({'Zee_%s' % str(i):j})
           legends.update({'Zee_%s' % str(i):'Alpgen+bkgs Z #rightarrow ee'})
           names.update({'Zee_%s' % str(i):'Alpgen+bkgs Z #rightarrow ee'})
           #colours.update({'Zee_%s' % str(i):ROOT.kGreen+2})	   
           colours.update({'Zee_%s' % str(i):ROOT.kMagenta+2})
           styles.update({'Zee_%s' % str(i):ROOT.kOpenSquare})
           listofTH2_alpgen.append('Zee_%s' % str(i))
        somearray = [361108,361084]
        for i in somearray:
          j+=1
          stacks.update({ 'Zee_%s' % str(i):j})
          listofTH2_bkgs.append('Zee_%s' % str(i))
        for i in range(361064,361069):
          j+=1
          stacks.update({'Zee_%s' % str(i):j})
          legends.update({'Zee_%s' % str(i):'Sherpa VV, V=W,Z'})
          listofTH2_bkgs.append('Zee_%s' % str(i))
        somearray2 = [361086,410000,410013,410011,410012,410015,410016,410014]
        for i in somearray2:
          j+=1
          stacks.update({ 'Zee_%s' % str(i):j})
          listofTH2_bkgs.append('Zee_%s' % str(i))
      if any("Zmm" in sample for sample in samples):
       print samples
       j = 1
       for sample in samples:
        if '361107' in sample:
           j+=1
           i = 361107
           stacks.update({'Zmm_%s' % str(i):j})
           legends.update({'Zmm_%s' % str(i):'Powheg+bkgs Z #rightarrow #mu#mu'})
           names.update({'Zmm_%s' % str(i):'Powheg+bkgs Z #rightarrow #mu#mu'})
           #colours.update({'Zmm_%s' % str(i):ROOT.kGreen+2})	   
           colours.update({'Zmm_%s' % str(i):ROOT.kBlue+2})
           styles.update({'Zmm_%s' % str(i):ROOT.kOpenTriangleDown})
           listofTH2_powheg.append('Zmm_%s' % str(i))
        if '361396' in sample:
         for i in range(361396,361420):
           j+=1
           stacks.update({'Zmm_%s' % str(i):j})
           legends.update({'Zmm_%s' % str(i):'Sherpa2.1+bkgs Z#rightarrow#mu#mu'})
           names.update({'Zmm_%s' % str(i):'Sherpa2.1+bkgs Z#rightarrow#mu#mu'})
           #colours.update({'Zmm_%s' % str(i):ROOT.kGreen+2})	   
           colours.update({'Zmm_%s' % str(i):ROOT.kBlue+2})
           styles.update({'Zmm_%s' % str(i):ROOT.kOpenTriangleDown})
           listofTH2_oldsherpa.append('Zmm_%s' % str(i))
        if '363364' in sample:
         for i in range(363364,363388):
           j+=1
           stacks.update({'Zmm_%s' % str(i):j})
           legends.update({'Zmm_%s' % str(i):'Sherpa2.2+bkgs Z#rightarrow#mu#mu'})
           names.update({'Zmm_%s' % str(i):'Sherpa2.2+bkgs Z#rightarrow#mu#mu'})
           #colours.update({'Zmm_%s' % str(i):ROOT.kGreen+2})	   
           colours.update({'Zmm_%s' % str(i):ROOT.kRed+2})
           styles.update({'Zmm_%s' % str(i):ROOT.kOpenCircle})
           listofTH2_sherpa.append('Zmm_%s' % str(i))
        if '361505' in sample:
         for i in range(361505,361510):
           j+=1
           stacks.update({'Zmm_%s' % str(i):j})
           legends.update({'Zmm_%s' % str(i):'Madgraph+bkgs Z#rightarrow#mu#mu'})
           names.update({'Zmm_%s' % str(i):'Madgraph+bkgs Z#rightarrow#mu#mu'})
           colours.update({'Zmm_%s' % str(i):ROOT.kGreen+2})	   
           #colours.update({'Zmm_%s' % str(i):ROOT.kYellow+2})
           styles.update({'Zmm_%s' % str(i):ROOT.kOpenTriangleUp})
           listofTH2_madgraph.append('Zmm_%s' % str(i))
        if '361710' in sample:
         for i in range(361710,361716):
           j+=1
           stacks.update({'Zmm_%s' % str(i):j})
           legends.update({'Zmm_%s' % str(i):'Alpgen+bkgs Z#rightarrow#mu#mu'})
           names.update({'Zmm_%s' % str(i):'Alpgen+bkgs Z#rightarrow#mu#mu'})
           colours.update({'Zmm_%s' % str(i):ROOT.kMagenta+2})	   
           styles.update({'Zmm_%s' % str(i):ROOT.kOpenSquare})
           listofTH2_alpgen.append('Zmm_%s' % str(i))
       somearray = [361108,361084]
       for i in somearray:
         j+=1
         stacks.update({ 'Zmm_%s' % str(i):j})
         listofTH2_bkgs.append('Zmm_%s' % str(i))
       for i in range(361064,361069):
         j+=1
         stacks.update({'Zmm_%s' % str(i):j})
         legends.update({'Zmm_%s' % str(i):'Sherpa VV, V=W,Z'})
         listofTH2_bkgs.append('Zmm_%s' % str(i))
       somearray2 = [361086,410000,410013,410011,410012,410015,410016,410014]
       for i in somearray2:
         j+=1
         stacks.update({ 'Zmm_%s' % str(i):j})
         listofTH2_bkgs.append('Zmm_%s' % str(i))

    def ATLAS_LABEL(self,x, y, color=1, tsize=0.075):
        l = ROOT.TLatex() # l.SetTextAlign(12); l.SetTextSize(tsize);
        l.SetNDC()
        l.SetTextFont(72)
        l.SetTextColor(color)
        l.SetTextSize( tsize )
        l.DrawLatex(x,y,"ATLAS")
        l.SetTextFont(42)
        l.DrawLatex(x+0.13,y,"Internal")
        pass

    def addHistList(self, sample,listofTH2,plot):
      listofTH2Copy=listofTH2
      newplot = plot.Clone(sample)
      newplot.Reset()
      for plo in listofTH2Copy:
        newplot.Add(plo)
      listofTH2Copy=[]
      del listofTH2[:]
      return newplot

    def addHistWithBkgsList(self, sample,listofTH2bkgs,listofTH2,plot2d):
      listofTH2Copy=listofTH2
      listofTH2Copy.extend(listofTH2bkgs)
      newplot = plot2d.Clone(sample)
      newplot.Reset()
      for plo in listofTH2Copy:
        newplot.Add(plo)
      listofTH2Copy=[]
      del listofTH2[:]
      return newplot

    def resoGraphList(self, sample,listofTH2bkgs,listofTH2,fullname,name,plot2d):
      newplot = self.addHistWithBkgsList(sample,listofTH2bkgs,listofTH2,plot2d)
      plot = self.getResoGraph(sample, fullname, name,newplot)
      return (plot,newplot)

    def scaleProfList(self, sample,listofTH2bkgs,listofTH2,met,plot2d):
      newplot = self.addHistWithBkgsList(sample,listofTH2bkgs,listofTH2,plot2d)
      plot = newplot.ProfileX(sample+'_'+met+'_pfx')
      return (plot,newplot)

    def legendsDraw(self,sample,leg,plot,samples,name,met):
        newsamples = []
        if 'Wev' in sample:
          newsamples = ['Wev_data']
          if any("361100" in sample for sample in samples):
               i = 361100
               newsamples.append('Wev_%s' % str(i))
          if any("361800" in sample for sample in samples):
               i = 361800
               newsamples.append('Wev_%s' % str(i))
          if any("361520" in sample for sample in samples):
               i = 361520
               newsamples.append('Wev_%s' % str(i))
          if any("361300" in sample for sample in samples):
               i = 361300
               newsamples.append('Wev_%s' % str(i))
          if any("363460" in sample for sample in samples):
               i = 363460
               newsamples.append('Wev_%s' % str(i))
          samparray = ['361102','361106','361084','410000','410013']
          for samp in samparray:
              newsamples.append('Wev_%s' % samp)
        if 'Zee' in sample:
          newsamples = ['Zee_data']
          if any("361106" in sample for sample in samples):
               i = 361106
               newsamples.append('Zee_%s' % str(i))
          if any("361372" in sample for sample in samples):
           newsamples.append('Zee_361372')
          if any("363388" in sample for sample in samples):
           newsamples.append('Zee_363388')
          if any("361700" in sample for sample in samples):
           newsamples.append('Zee_361700')
          if any("361500" in sample for sample in samples):
           newsamples.append('Zee_361500')
          samparray = ['361108','361084','410000','410013']
          for samp in samparray:
              newsamples.append('Zee_%s' % samp)
        if 'Zmm' in sample:
          newsamples = ['Zmm_data']
          if any("361107" in sample for sample in samples):
               i = 361107
               newsamples.append('Zmm_%s' % str(i))
          if any("361396" in sample for sample in samples):
           newsamples.append('Zmm_361396')
          if any("363364" in sample for sample in samples):
           newsamples.append('Zmm_363364')
          if any("361710" in sample for sample in samples):
           newsamples.append('Zmm_361710')
          if any("361505" in sample for sample in samples):
           newsamples.append('Zmm_361505')
          samparray = ['361108','361084','410000','410013']
          for samp in samparray:
              newsamples.append('Zmm_%s' % samp)
        #newsamples.sort(key = lambda sample1: self.get_legend_pos(sample1), reverse = True )
        for sample1 in newsamples :
          if self.compare_samples:
            if sample1 in names:
              if 'data' in sample1:
                plot = self.plots[name][met][self.data_sample]
                leg.AddEntry(plot,names[sample1],'P')
              else:
                plot = self.plots[name][met][sample1]
                colour = colours[sample1] if sample1 in colours.keys() else ROOT.kBlack
                style  = styles[sample1] if sample1 in styles.keys() else ROOT.kFullCircle
                plot.SetFillColor(colour)
                plot.SetMarkerStyle(style)
                leg.AddEntry(plot,names[sample1],'F')
        del newsamples[:]
        leg.Draw()


    def basicFormatting(self,plot,colour,style):
        plot.SetLineColor(colour)
        plot.SetLineWidth(2)
        plot.SetMarkerColor(colour)
        plot.SetMarkerStyle(style)
        #plot.SetMarkerStyle( 20 )
        plot.SetMarkerSize( 1.0 )

    def stackFormatting(self,plot,colour):
        #plot.SetLineColor(colour)
        plot.SetLineColor(ROOT.kBlack)
        plot.SetLineWidth(2)
        plot.SetFillColor(colour)
        #plot.SetFillStyle(3001)
        plot.SetMarkerStyle( 20 )
        plot.SetMarkerSize(0)

    def axisFormatting(self,plot,xtitle,ytitle,xrange,yrange):
        plot.GetXaxis().SetLabelSize(0.037)
        plot.GetYaxis().SetLabelSize(0.037)
        plot.GetXaxis().SetTitleSize(0.042)
        plot.GetYaxis().SetTitleSize(0.042)
        plot.GetXaxis().SetTitleOffset(1.3)
        plot.GetYaxis().SetTitleOffset(1.3)
        plot.SetXTitle(xtitle)
        plot.SetYTitle(ytitle)
        #print '{xtit} ({xlo},{xhi}) vs {ytit} ({ylo},{yhi})'.format(xtit=xtitle,xlo=xrange[0],xhi=xrange[1],
#                                                                    ytit=ytitle,ylo=yrange[0],yhi=yrange[1])
        plot.GetXaxis().SetRangeUser(xrange[0],xrange[1])
        plot.GetYaxis().SetRangeUser(yrange[0],yrange[1])

    def getXrange(self,plot):
        xlow = plot.GetXaxis().GetBinLowEdge(1)
        xhigh = plot.GetXaxis().GetBinUpEdge(plot.GetNbinsX())
        return (xlow,xhigh)

    def getYrange(self,plot):
        ymin = plot.GetMinimum()
        ylow = 1.3*ymin if ymin<0 else 0.
        yhigh = 1.3*plot.GetMaximum()
        return (ylow,yhigh)

    def equalizeHistograms(self, hists):
      '''
      Scale histograms such that they have equal sum of weights.
      hist2 will be scaled to match hist1.
      '''
      if len(hists) == 0: return
      sow1 = hist[0].GetSumOfWeights()
      for hist in hists:
        sow = hist.GetSumOfWeights()
        scale_factor = sow1 / sow
        hist.Scale(scale_factor)
      return

    def addToStack(self,name,met,sample,option='hist'):
        plot = self.plots[name][met][sample]
        print name, met, sample
        if not self.total_mc:
          self.total_mc = plot.Clone('total_mc')
        else: 
	  self.total_mc.Add(plot)
          self.total_mc_sample = sample
        if self.compare_samples:
          colour = colours[sample] if sample in colours.keys() else ROOT.kBlack
        else: # compare met definitions
          colour = colours[met] if met in colours.keys() else ROOT.kBlack
        self.stackFormatting(plot,colour)
        self.stack.Add(plot, option)

    def drawStatBandPlot(self,name,met,sample,total_mc,canvas,option='hist',logy=False):
        plot = total_mc.Clone()
        ROOT.SetOwnership(plot,0)
        canvas.cd(1)
        colour = ROOT.kBlack
        style  = ROOT.kFullCircle
        self.basicFormatting(plot,colour,style)
        plot.SetMarkerSize( 2.5 )
        plot.SetFillColor(ROOT.kBlack)
        plot.SetFillStyle( 3245 )
        plot.SetLineColor(ROOT.kWhite)
        plot.SetMarkerStyle( 1 )
        plot.Draw('same E2')
        ROOT.gPad.SetLogy(logy)

    def drawSinglePlot(self,name,met,sample,canvas,option='hist',logy=False):
        plot = self.plots[name][met][sample]
        if do_ratio_plots:
          canvas.cd(1)
        else: canvas.cd()
        if self.compare_samples:
          colour = colours[sample] if sample in colours.keys() else ROOT.kBlack
          style  = styles[sample] if sample in styles.keys() else ROOT.kFullCircle
        else: # compare met definitions
          colour = colours[met] if met in colours.keys() else ROOT.kBlack
          style  = styles[met] if met in styles.keys() else ROOT.kFullCircle
        #xtitle = xtitles[name] if name in xtitles.keys() else ''
        #ytitle = ytitles[name] if name in ytitles.keys() else 'Events'
        #xrange = xranges[name] if name in xranges.keys() else self.getXrange(plot)
        #yrange = yranges[name] if name in yranges.keys() else self.getYrange(plot)
        #if logy: yrange = (0.1,yrange[1]*20/1.3)
        self.basicFormatting(plot,colour,style)#,xtitle,ytitle,xrange,yrange)
        plot.Draw(option)
        ROOT.gPad.SetLogy(logy)
        ROOT.gPad.RedrawAxis()
        ROOT.gPad.Update()

    def drawBasicPlots(self,metlist,plotnames,samples=['Zmm'],outdir='',is0j=False):
      to_plot_list = []
      for sample in samples:
        for met in metlist:
          to_plot_list.append((sample, met))
      mycanvas = {}
      for name in plotnames:
        if is0j and (name in jet_bins): continue
        self.plots[name] = {}
        canvas_name_stem = '_'.join(samples) + ('_0j' if is0j else '')
        mycanvas[name] = ROOT.TCanvas(canvas_name_stem+'_'+name,name)
        mycanvas[name].SetRightMargin(0.1)
        if do_ratio_plots:
          pad1 = ROOT.TPad("pad1","pad1",0,0.3,1,1)
          pad1.SetTopMargin( 0.06 )
          pad1.SetBottomMargin(0.05)
          pad1.SetFillStyle( 4000 )
          pad1.SetFillColor( 0 )
          pad1.SetFrameFillStyle( 4000 )
          pad1.SetFrameFillColor( 0 )
          pad1.Draw()
          #pad1.SetGrid()
          pad1.cd()

          mycanvas[name].cd()

          pad2 = ROOT.TPad("pad2","pad2",0,0,1,0.3)
          pad2.SetTopMargin(0)
          pad2.SetBottomMargin( 0.35 )
          pad2.SetFillStyle( 4000 )
          pad2.SetFillColor( 0 )
          pad2.SetFrameFillStyle( 4000 )
          pad2.SetFrameFillColor( 0 )
          pad2.SetGrid()
          pad2.Draw()
          pad2.cd()

          mycanvas[name].cd()
          pad1.cd()

        logy = False
        if name in logylist: logy = True
        frame = None
        if name in basic_legend_coords:
          leg_coords = basic_legend_coords[name]
        else:
          #leg_coords = (0.5,0.65,0.85,0.9)
          leg_coords = (0.66,0.5,0.76,0.9)
        leg = ROOT.TLegend(*leg_coords)
        #leg.SetNColumns( 2 )
        leg.SetBorderSize(0)
        leg.SetFillStyle(0)
        leg.SetTextFont( 43 )
        leg.SetTextSize( 17 )

        listofTH2bkgs = []
        listofTH2 = []

        for idx,to_plot in enumerate(to_plot_list):
            met = to_plot[1]
            sample = to_plot[0]
            evttype = sample.split('_')[0]
            plotdir = evttype + ('_0j' if is0j else '')
            fullname = '{pdir}/{metsuf}_{shortname}{njet}'.format(pdir=plotdir,
                                                                  metsuf=met,
                                                                  shortname=name,
                                                                  njet='_0jet' if is0j else ''
                                                                  )
            print 'In file', sample, ', draw', fullname
            plot = self.infiles[sample].Get(fullname)
            if self.normalizeHists:
              if idx==0:
                self.sum_of_weights = plot.GetSumOfWeights()
              else:
                this_sow = plot.GetSumOfWeights()
                scale_factor = self.sum_of_weights / this_sow
                plot.Scale(scale_factor)

            if self.normalize_to_lumi and not 'data' in sample:
              scale_factor = self.get_mc_scale(sample)
              plot.Scale(scale_factor)

            if met not in self.plots[name] : self.plots[name][met]={}
            if name in rebinlist:
              if rebinlist[name] == 'variable':
                from array import array
                nbins = len(variableRebin[name])-1
                bin_array = array('d', variableRebin[name])
                print "Variable rebin!"
                plot = plot.Rebin(nbins, sample+'_'+met+'_rebin', bin_array)
              else:
                print "Rebin!"
                plot.Rebin(rebinlist[name])
            self.plots[name][met][sample] = plot
            if idx==0:
                self.total_mc = False
                self.diboson_mc = False
                self.singlet_mc = False
                if is0j and name in xranges_jetveto:
                  xrange = xranges_jetveto[name]
                else:
                  xrange = xranges[name] if name in xranges.keys() else self.getXrange(plot)
                if is0j and name in yranges_jetveto:
                  yrange = yranges_jetveto[name]
                elif name in yranges:
                  yrange = yranges[name]
                else:
                  yrange = self.getYrange(plot)
                  if logy: yrange = (0.1,yrange[1]*20/1.3)
                frame = ROOT.TH2F('','',1,xrange[0],xrange[1],1,yrange[0],yrange[1])
                xtitle = xtitles[name] if name in xtitles.keys() else ''
                bin_width = plot.GetXaxis().GetBinWidth(0)
                print bin_width
                ytitle = ytitles[name] if name in ytitles.keys() else 'Events / '+str(int(bin_width))+' GeV'
                frame.SetXTitle(xtitle)
                frame.SetYTitle(ytitle)
                frame.GetYaxis().SetTitleSize(20)
                frame.GetYaxis().SetTitleFont(43)
                frame.GetYaxis().SetTitleOffset(1.5)
                frame.GetYaxis().SetLabelFont(43)
                frame.GetYaxis().SetLabelSize(20)
                frame.Draw('')
                frame.GetXaxis().SetTickLength(0.03);
                frame.GetXaxis().SetTickSize(0.04);
                frame.GetYaxis().SetTickSize(0.03);
                frame.GetXaxis().SetLabelOffset(999);
                frame.GetXaxis().SetTitleOffset(999);

                if self.stack_mc:
                  self.stack = ROOT.THStack()
            #drawopt = 'histsame'

            if 'data' not in sample:
	      #print listofTH2_bkgs
	      #print listofTH2_powheg
              if sample in listofTH2_bkgs:
                listofTH2bkgs.append(plot)
                if '410013' in sample or '361084' in sample or '410000' in sample or '361106' in sample or '361102' in sample:
                  newplot = self.addHistList(sample,listofTH2bkgs,plot)
                  self.plots[name][met][sample] = newplot
                else: continue
              elif sample in listofTH2_powheg:
                listofTH2.append(plot)
                if 'Zmm_361107' in sample or 'Zee_361106' in sample or 'Wev_361100' in sample:
                  newplot = self.addHistList(sample,listofTH2,plot)
                  self.plots[name][met][sample] = newplot
                else: continue
              elif sample in listofTH2_sherpa:
                listofTH2.append(plot)
                if 'Zmm_363364' in sample or 'Zee_363388' in sample or 'Wev_363460' in sample:
                  newplot = self.addHistList(sample,listofTH2,plot)
                  self.plots[name][met][sample] = newplot
                else: continue
              elif sample in listofTH2_oldsherpa:
                listofTH2.append(plot)
                if 'Zmm_361396' in sample or 'Zee_361372' in sample or 'Wev_361300' in sample:
                  newplot = self.addHistList(sample,listofTH2,plot)
                  self.plots[name][met][sample] = newplot
                else: continue
              elif sample in listofTH2_madgraph:
                listofTH2.append(plot)
                if 'Zmm_361505' in sample or 'Zee_361500' in sample or 'Wev_361520' in sample:
                  newplot = self.addHistList(sample,listofTH2,plot)
                  self.plots[name][met][sample] = newplot
                else: continue
              elif sample in listofTH2_alpgen:
                listofTH2.append(plot)
                if 'Zmm_361710' in sample or 'Zee_361700' in sample or 'Wev_361800' in sample:
                  newplot = self.addHistList(sample,listofTH2,plot)
                  self.plots[name][met][sample] = newplot
                else: continue

            drawopt = 'same'
            print 'stacking mc'
            if self.stack_mc:
              print sample
              if 'data' in sample:
                self.drawSinglePlot(name,met,sample,mycanvas[name],drawopt,logy)
                self.data_sample = sample
              else:
                self.addToStack(name,met,sample,drawopt)
            else:
              self.drawSinglePlot(name,met,sample,mycanvas[name],drawopt,logy)

        if self.stack_mc:
          self.stack.Draw('histf same')
          self.drawSinglePlot(name,met,self.data_sample,mycanvas[name],drawopt,logy)

        self.drawStatBandPlot(name,met,self.total_mc_sample,self.total_mc,mycanvas[name],drawopt,logy)
        if name in basic_atlas_coords:
          atlas_coords = basic_atlas_coords[name]
        else:
          #atlas_coords = (0.38,0.82,"Internal")
          atlas_coords = (0.35,0.84)
        #ROOT.ATLASLabel(*atlas_coords)
        self.ATLAS_LABEL(*atlas_coords)
        if is0j and name in basic_textbox_coords_jet_veto:
          textcoords = basic_textbox_coords_jet_veto[name]
        elif name in basic_textbox_coords:
          textcoords = basic_textbox_coords[name]
        else:
          #textcoords = (0.19,0.65,0.39,0.85,'brNDC')
          textcoords = (0.33,0.6,0.63,0.79,'brNDC')
          #textcoords = (0.20,0.65,0.45,0.90,'brNDC')
        textbox = ROOT.TPaveText(*textcoords)
        textbox.SetTextAlign(11)
        textbox.SetTextSize(0.065)
        textbox.SetFillStyle(0)
        textbox.SetBorderSize(0)
        textbox.AddText("Data 2015, #sqrt{s} = 13 TeV")
        if 'Zmm' in sample:
          textbox.AddText('Z #rightarrow #mu#mu,  '+str(round(self.data_lumi/1000., 1))+' fb^{-1}')
        if 'Zee' in sample:
          textbox.AddText('Z #rightarrow ee,  '+str(round(self.data_lumi/1000., 1))+' fb^{-1}')
        elif 'Wev' in sample:
          textbox.AddText('W #rightarrow e#nu,  '+str(round(self.data_lumi/1000., 1))+' fb^{-1}')
        elif 'Wmv' in sample:
          textbox.AddText('W #rightarrow #mu#nu,  '+str(round(self.data_lumi/1000., 1))+' fb^{-1}')
        elif 'ttbar' in sample:
          textbox.AddText('ttbar,  '+str(round(self.data_lumi/1000., 1))+' fb^{-1}')
        if '0j' in name or is0j:
          textbox.AddText('0 jets, p_{T} > 20 GeV')
        elif '1j' in name:
          textbox.AddText('1 jet, p_{T} > 20 GeV')
        elif '2j' in name:
          textbox.AddText('#geq2 jets, p_{T} > 20 GeV')
        else:
          textbox.AddText('all jets')
        textbox.Draw('same')
        latex = ROOT.TLatex()
        latex.SetNDC()
        latex.SetTextFont( 42 )
        latex.SetTextSize( 0.035 )
        latex.SetTextColor( 1 )
        latex.SetTextAlign( 12 )
        latex.SetTextAngle( 90 )
        latex.SetTextColor( ROOT.kGray + 2 )
        latex.DrawLatex( 0.97, 0.02, 'Under-/Over-flow Contents for Data (#it{MC}): %d/%d (#it{%.3f/%.3f})' % ( int( self.plots[name][met][self.data_sample].GetBinContent( 0 ) ), int( self.plots[name][met][self.data_sample].GetBinContent( self.plots[name][met][self.data_sample].GetNbinsX() + 1 ) ), self.total_mc.GetBinContent( 0 ), self.total_mc.GetBinContent( self.total_mc.GetNbinsX() + 1 ) ) )

        self.legendsDraw(sample,leg,plot,samples,name,met)

        if do_ratio_plots:
          pad2.cd()
          data_hist = self.plots[name][met][self.data_sample]
          ratio_hist = data_hist.Clone()

          ratio_hist.GetYaxis().SetNdivisions(505);

          ratio_hist.Divide(self.total_mc)
          ratio_hist.SetTitle("")

          ratio_hist.GetYaxis().SetNdivisions(505)
          ratio_hist.GetYaxis().SetTitleSize(20)
          ratio_hist.GetYaxis().SetTitleFont(43)
          ratio_hist.GetYaxis().SetTitle('Data/MC')
          ratio_hist.GetYaxis().SetTitleOffset(1.5)
          ratio_hist.GetYaxis().SetLabelFont(43)
          ratio_hist.GetYaxis().SetLabelSize(20)

          ratio_hist.GetXaxis().SetTitleSize(20)
          ratio_hist.GetXaxis().SetTitleFont(43)
          ratio_hist.GetXaxis().SetTitleOffset(3.5)
          ratio_hist.GetXaxis().SetLabelFont(43)
          ratio_hist.GetXaxis().SetLabelSize(20)
          xtitle = xtitles[name] if name in xtitles.keys() else ''
          ratio_hist.GetXaxis().SetTitle(xtitle)

          ratio_hist.GetYaxis().SetRangeUser(0.,2.)
          xrange = xranges[name] if name in xranges.keys() else self.getXrange(plot)
          ratio_hist.GetXaxis().SetRangeUser(xrange[0],xrange[1])

          ratio_hist.Draw()

          min_x = ratio_hist.GetXaxis().GetBinLowEdge(1)
          #max_x = ratio_hist.GetXaxis().GetBinLowEdge(ratio_hist.GetNbinsX())
	  max_x = xrange[1]
          line = ROOT.TLine(min_x, 1., max_x, 1.)
          line.SetLineColor( ROOT.kRed + 1 )
          line.SetLineWidth( 4 )
          line.Draw('same')
          ratio_hist.Draw( 'same' )

        #samples_string = '_'.join(samples)
        if any("Zmm" in sample for sample in samples):
           samples_string = 'Zmm'
        elif any("Zee" in sample for sample in samples):
           samples_string = 'Zee'
        elif any("Wev" in sample for sample in samples):
           samples_string = 'Wev'
        elif any("Wmv" in sample for sample in samples):
           samples_string = 'Wmv'
        elif any("ttbar" in sample for sample in samples):
           samples_string = 'ttbar'
        else:
           samples_string = ''

	outfile_format = ['pdf','png','eps','C']

	for filetype in outfile_format :

           outfile_name = '{outdir}/{etype}/{shortname}{njet}.{filetype}'.format(outdir=outdir,
									         etype=samples_string,
                                                                                 shortname=name,
                                                                                 njet='_0jet' if is0j else '',
									         filetype=filetype
                                                                               )
           mycanvas[name].SaveAs(outfile_name)

    def drawScalePlots(self,metlist,plotnames,samples=['Zmm'],outdir='',is0j=False):
        to_plot_list = []
        for sample in samples:
          for met in metlist:
            to_plot_list.append((sample, met))

        c = {}
        for name in plotnames:
            if is0j and (name in jet_bins): continue
            self.plots[name] = {}
            canvas_name_stem = '_'.join(samples) + ('_0j' if is0j else '')
            c[name] = ROOT.TCanvas(canvas_name_stem+'_'+name,name)
            c[name].SetRightMargin(0.1)
            if do_ratio_plots:
              pad1 = ROOT.TPad("pad1","pad1",0,0.3,1,1)
              pad1.SetTopMargin( 0.06 )
              pad1.SetBottomMargin(0.05)
              pad1.SetFillStyle( 4000 )
              pad1.SetFillColor( 0 )
              pad1.SetFrameFillStyle( 4000 )
              pad1.SetFrameFillColor( 0 )
              pad1.Draw()
              pad1.cd()

              c[name].cd()

              pad2 = ROOT.TPad("pad2","pad2",0,0,1,0.3)
              pad2.SetTopMargin(0)
              pad2.SetBottomMargin( 0.35 )
              pad2.SetFillStyle( 4000 )
              pad2.SetFillColor( 0 )
              pad2.SetFrameFillStyle( 4000 )
              pad2.SetFrameFillColor( 0 )
              pad2.SetGrid()
              pad2.Draw()
              pad2.cd()

              c[name].cd()
              pad1.cd()

            frame = None
            if name in scale_legend_coords:
              leg_coords = scale_legend_coords[name]
            else:
              #leg_coords = (0.20,0.25,0.45,0.50)
              leg_coords = (0.60,0.65,0.85,0.9)
            if name == 'residL_ptz' and zoomedDiagnostic:
              leg_coords = (0.60,0.65,0.85,0.90)
            leg = ROOT.TLegend(*leg_coords)
            leg.SetBorderSize(0)
            leg.SetFillStyle(0)
            leg.SetTextSize(0.04)

            line = ROOT.TLine(0.,0.,1.,1.)
            line.SetLineColor(ROOT.kGray+2)
            line.SetLineWidth(2)
            line.SetLineStyle(ROOT.kDashed)

            listofTH2bkgs = []
            listofTH2 = []
            plotFrame = True 
            for idx,to_plot in enumerate(to_plot_list):
                met = to_plot[1]
                sample = to_plot[0]
                evttype = sample.split('_')[0]
                plotdir = evttype + ('_0j' if is0j else '')
                fullname = '{pdir}/{metsuf}_{shortname}{njet}'.format(pdir=plotdir,
                                                                      metsuf=met,
                                                                      shortname=name,
                                                                      njet='_0jet' if is0j else ''
                                                                      )
                print 'In file', sample, ', draw', fullname
                plot2d = self.infiles[sample].Get(fullname)
                if self.normalize_to_lumi and not 'data' in sample:
                  scale_factor = self.get_mc_scale(sample)
                  plot2d.Scale(scale_factor)
		plot = None
                #plot = plot2d.ProfileX(sample+'_'+met+'_pfx')
                #plot = self.getGausFitMean(plot2d,sample,name)
                if 'data' in sample:
		  self.data_sample = sample
                if 'data' not in sample:
                  if sample in listofTH2_bkgs:
                    listofTH2bkgs.append(plot2d)
                    continue
                  if sample in listofTH2_powheg:
                    listofTH2.append(plot2d)
                    if 'Zmm_361107' in sample or 'Zee_361106' in sample or 'Wev_361100' in sample:
                      plot,powplot = self.scaleProfList(sample,listofTH2bkgs,listofTH2,met,plot2d)
                      self.powplot = powplot
                    else: continue
                  if sample in listofTH2_oldsherpa:
                    listofTH2.append(plot2d)
                    if 'Zmm_361396' in sample or 'Zee_361372' in sample or 'Wev_361300' in sample:
                      plot,sher1plot = self.scaleProfList(sample,listofTH2bkgs,listofTH2,met,plot2d)
                      self.sher1plot = sher1plot
                    else: continue
                  if sample in listofTH2_sherpa:
                    listofTH2.append(plot2d)
                    if 'Zmm_363364' in sample or 'Zee_363388' in sample or 'Wev_363460' in sample:
                      plot,sherplot = self.scaleProfList(sample,listofTH2bkgs,listofTH2,met,plot2d)
                      self.sherplot = sherplot
                    else: continue
                  if sample in listofTH2_madgraph:
                    listofTH2.append(plot2d)
                    if 'Zmm_361505' in sample or 'Zee_361500' in sample or 'Wev_361520' in sample:
                      plot,madplot = self.scaleProfList(sample,listofTH2bkgs,listofTH2,met,plot2d)
                      self.madplot = madplot
                    else: continue
                  if sample in listofTH2_alpgen:
                    listofTH2.append(plot2d)
                    if 'Zmm_361710' in sample or 'Zee_361700' in sample or 'Wev_361800' in sample:
                      plot,alpplot = self.scaleProfList(sample,listofTH2bkgs,listofTH2,met,plot2d)
                      self.alpplot = alpplot
                    else: continue
                else:
                   self.dataplot = plot2d
                   plot = plot2d.ProfileX(sample+'_'+met+'_pfx')

                if name in rebinlist:
                  if rebinlist[name] == 'variable':
                    from array import array
                    nbins = len(variableRebin[name])-1
                    bin_array = array('d', variableRebin[name])
                    print "Variable rebin!"
                    plot = plot.Rebin(nbins, sample+'_'+met+'_rebin', bin_array)
                  else:
                    print "Rebin!"
                    plot.Rebin(rebinlist[name])
                if met not in self.plots[name] : self.plots[name][met]={}
                self.plots[name][met][sample] = plot
                if plotFrame:
                #if idx==0:
		    plotFrame=False
                    if is0j and name in xranges_jetveto:
                      xrange = xranges_jetveto[name]
                    else:
                      xrange = xranges[name] if name in xranges.keys() else self.getXrange(plot)
                    if is0j and name in yranges_jetveto:
                      yrange = yranges_jetveto[name]
                    else:
                      yrange = yranges[name] if name in yranges.keys() else self.getYrange(plot)
                    if name == 'lin_truth' and sample in ['Wev','Wmv']:
                      if '0j' in name or is0j:
                        yrange = (-.5,1.)
                      else:
                        yrange = (-1.,1.)
                    frame = ROOT.TH2F('','',1,xrange[0],xrange[1],1,yrange[0],yrange[1])
                    xtitle = xtitles[name] if name in xtitles.keys() else ''
                    ytitle = ytitles[name] if name in ytitles.keys() else 'Events'
                    #frame.SetXTitle(xtitle)
                    #frame.SetYTitle(ytitle)
                    #frame.Draw('')
                    frame.GetXaxis().SetTitleSize(20)
                    frame.GetXaxis().SetTitleFont(43)
                    frame.GetXaxis().SetTitleOffset(3.5)
                    frame.GetXaxis().SetLabelFont(43)
                    frame.GetXaxis().SetLabelSize(20)
                    frame.GetXaxis().SetTitle(xtitle)
                    frame.GetYaxis().SetNdivisions(505)
                    frame.GetYaxis().SetTitleSize(20)
                    frame.GetYaxis().SetTitleFont(43)
                    frame.GetYaxis().SetTitleOffset(1.5)
                    #frame.SetYTitle(ytitle)
                    frame.GetYaxis().SetTitle(ytitle)
                    frame.GetYaxis().SetLabelFont(43)
                    frame.GetYaxis().SetLabelSize(20)
                    frame.Draw('')
                    #frame.GetXaxis().SetTickLength(0.03);
                    #frame.GetXaxis().SetTickSize(0.04);
                    #frame.GetYaxis().SetTickSize(0.03);
                    frame.GetXaxis().SetLabelOffset(999);
                    frame.GetXaxis().SetTitleOffset(999);

                    if 'scale' in name:
                      line.DrawLine(xrange[0],1.,xrange[1],1.)
                      pass
                    elif 'resid' in name:
                      pass
                        # #line.DrawLine(xrange[0],0.,xrange[1],0.)
                        # x2 = xrange[1]
                        # y2 = -1*xrange[1]
                        # if -1*yrange[0]<xrange[1]:
                        #     x2 = -1*yrange[0]
                        #     y2 = yrange[0]
                        #line.DrawLine(0.,0.,x2,y2)
                    elif 'bias' in name or 'lin' in name:
                      line.DrawLine(xrange[0],0.,xrange[1],0.)
                      pass
                self.plots[name][met][sample] = plot
                drawopt = 'pesame'
                self.drawSinglePlot(name,met,sample,c[name],drawopt)
                if self.compare_samples:
                  if sample in names:
                    leg.AddEntry(plot,names[sample],'lp')
                  else:
                    pass
                    #leg.AddEntry(plot,sample,'lp')
                else:
                  leg.AddEntry(plot,names[met],'p')
            #if is0j: 
            # self.ProjFit(sample, fullname, name,powplot,sherplot,madplot,alpplot,self.dataplot,'0jet')
            #else:
            # self.ProjFit(sample, fullname, name,powplot,sherplot,madplot,alpplot,self.dataplot,'')
            if is0j and name in scale_atlas_coords_jet_veto:
              atlas_coords = scale_atlas_coords_jet_veto[name]
            elif name in scale_atlas_coords:
              atlas_coords = scale_atlas_coords[name]
            else:
              atlas_coords = (0.20,0.57,1,0.05)
              #atlas_coords = (0.20,0.87,"Internal")
            self.ATLAS_LABEL(*atlas_coords)
            #ROOT.ATLASLabel(*atlas_coords)
            leg.Draw()
            if is0j and name in scale_textbox_coords_jet_veto:
              textcoords = scale_textbox_coords_jet_veto[name]
            elif name in scale_textbox_coords:
              textcoords = scale_textbox_coords[name]
            else:
              textcoords = (0.20,0.20,0.45,0.45,'brNDC')
            #if name == 'residL_ptz' and zoomedDiagnostic:
            #  textcoords = (0.2,0.65,0.45,0.90,'brNDC')
            textbox = ROOT.TPaveText(*textcoords)
            textbox.SetTextAlign(11)
            textbox.SetTextSize(0.05)
            textbox.SetFillStyle(0)
            textbox.SetBorderSize(0)
            textbox.AddText("Data 2015, #sqrt{s} = 13 TeV")
            if 'Zmm' in sample:
              textbox.AddText('Z #rightarrow #mu#mu  '+str(round(self.data_lumi/1000., 2))+' fb^{-1}')
            if 'Zee' in sample:
              textbox.AddText('Z #rightarrow ee  '+str(round(self.data_lumi/1000., 2))+' fb^{-1}')
            elif 'Wev' in sample:
              textbox.AddText('W #rightarrow e#nu  '+str(round(self.data_lumi/1000., 2))+' fb^{-1}')
            elif 'Wmv' in sample:
              textbox.AddText('W #rightarrow #mu#nu  '+str(round(self.data_lumi/1000., 2))+' fb^{-1}')
            elif 'ttbar' in sample:
              textbox.AddText('ttbar')
            if sample == 'ttbar':
              textbox.AddText('1lepton+jets')
            else:
              if '0j' in name or is0j:
                textbox.AddText('0 jets p_{T} > 20 GeV')
              elif '1j' in name:
                textbox.AddText('1 jet p_{T} > 20 GeV')
              elif '2j' in name:
                textbox.AddText('#geq2 jets p_{T} > 20 GeV')
              else:
                textbox.AddText('all jets')
            textbox.Draw('same')
            #samples_string = '_'.join(samples)
            if any("Zmm" in sample for sample in samples):
               samples_string = 'Zmm'
            elif any("Zee" in sample for sample in samples):
               samples_string = 'Zee'
            elif any("Wev" in sample for sample in samples):
               samples_string = 'Wev'
            elif any("Wmv" in sample for sample in samples):
               samples_string = 'Wmv'
            elif any("ttbar" in sample for sample in samples):
               samples_string = 'ttbar'
            else:
               samples_string = ''

            if do_ratio_plots:
              pad2.cd()
              data_hist = self.plots[name][met][self.data_sample]
              #plot = self.getResoGraph(sample, fullname, name,data_hist)
              ratio_hist = ROOT.TGraphErrors(data_hist)
              ratio_hist.Clear()
              (ratio_hist1,ratio_hist2,ratio_hist3,ratio_hist4) = self.GetRatioScaleHistogram(sample,fullname,name,met, self.dataplot,powplot,sherplot,alpplot,madplot)
              #(ratio_hist1,ratio_hist2) = self.GetRatioScaleHistogram(sample,fullname,name,met, self.dataplot,sher1plot,sherplot)
              ratio_hist.SetTitle("")

              ratio_hist.GetYaxis().SetNdivisions(505)
              ratio_hist.GetYaxis().SetTitleSize(20)
              ratio_hist.GetYaxis().SetTitleFont(43)
              ratio_hist.GetYaxis().SetTitle('Data/MC')
              ratio_hist.GetYaxis().SetTitleOffset(1.5)
              ratio_hist.GetYaxis().SetLabelFont(43)
              ratio_hist.GetYaxis().SetLabelSize(20)

              ratio_hist.GetXaxis().SetTitleSize(20)
              ratio_hist.GetXaxis().SetTitleFont(43)
              ratio_hist.GetXaxis().SetTitleOffset(3.5)
              ratio_hist.GetXaxis().SetLabelFont(43)
              ratio_hist.GetXaxis().SetLabelSize(20)
              ratio_hist.GetXaxis().SetTitle(xtitle)

              ratio_hist.GetYaxis().SetRangeUser(0.6,1.4)

              ratio_hist1.SetMarkerColor(ROOT.kBlue+2)
              ratio_hist1.SetMarkerStyle(ROOT.kOpenTriangleDown)
              ratio_hist2.SetMarkerColor(ROOT.kRed+2)
              ratio_hist2.SetMarkerStyle(ROOT.kOpenCircle)
              ratio_hist3.SetMarkerColor(ROOT.kMagenta+2)
              ratio_hist3.SetMarkerStyle(ROOT.kOpenSquare)
              ratio_hist4.SetMarkerColor(ROOT.kGreen+2)
              ratio_hist4.SetMarkerStyle(ROOT.kOpenTriangleUp)

              if is0j and name in xranges_jetveto:
                xrange = xranges_jetveto[name]
              else:
                xrange = xranges[name] if name in xranges.keys() else self.getXrange(plot)
              if is0j and name in yranges_jetveto:
                yrange = yranges_jetveto[name]
              else:
                yrange = yranges[name] if name in yranges.keys() else self.getYrange(plot)
              if name == 'lin_truth' and sample in ['Wev','Wmv']:
                if '0j' in name or is0j:
                  yrange = (-.5,1.)
                else:
                  yrange = (-1.,1.)
              frame1 = ROOT.TH2F(sample+'_'+name+'_'+met,'',1,xrange[0],xrange[1],1,0.6,1.4)
              #frame.GetYaxis().SetRangeUser(0.6,1.4)
              xtitle = xtitles[name] if name in xtitles.keys() else ''
              #ytitle = ytitlesreso[name] if name in ytitlesreso.keys() else 'Events'
              #frame.SetXTitle(xtitle)
              frame1.GetXaxis().SetTitleSize(20)
              frame1.GetXaxis().SetTitleFont(43)
              frame1.GetXaxis().SetTitleOffset(3.5)
              frame1.GetXaxis().SetLabelFont(43)
              frame1.GetXaxis().SetLabelSize(20)
              frame1.GetXaxis().SetTitle(xtitle)
              frame1.GetYaxis().SetNdivisions(505)
              frame1.GetYaxis().SetTitleSize(20)
              frame1.GetYaxis().SetTitleFont(43)
              frame1.GetYaxis().SetTitle('Data/MC')
              frame1.GetYaxis().SetTitleOffset(1.5)
              frame1.GetYaxis().SetLabelFont(43)
              frame1.GetYaxis().SetLabelSize(20)

              #frame.SetYTitle(ytitle)
              frame1.Draw('')
              min_x = int(xrange[0])
              max_x = int(xrange[1])
              #print min_x
              #print max_x
              #print ratio_hist.GetXaxis().GetXmin()
              #print ratio_hist.GetXaxis().GetXmax()
              #ratio_hist.GetXaxis().SetRangeUser(min_x,max_x)
              #print ratio_hist.GetXaxis().GetXmin()
              #print ratio_hist.GetXaxis().GetXmax()
              #ratio_hist.Draw('same')
              line = ROOT.TLine(min_x, 1., max_x, 1.)
              line.SetLineColor( ROOT.kRed + 1 )
              line.SetLineWidth( 4 )
              line.Draw('same')
              ratio_hist1.Draw("pesame")
              ratio_hist2.Draw("pesame")
              ratio_hist3.Draw("pesame")
              ratio_hist4.Draw("pesame")

              c[name].Update()


	    outfile_format = ['pdf','png','eps','C']

	    for filetype in outfile_format :

               outfile_name = '{outdir}/{etype}/{shortname}{njet}.{filetype}'.format(outdir=outdir,
									         etype=samples_string,
                                                                                 shortname=name.replace('resid','diag'),
                                                                                 njet='_0jet' if is0j else '',
									         filetype=filetype
                                                                               )
               c[name].SaveAs(outfile_name)

    def getGausFitMean(self,plot2d,sample,name):
        #plot = ROOT.TGraphErrors(plot2d.GetNbinsX())
        plot = plot2d.ProjectionX(plot2d.GetName()+"_proj",0,-1)
        gaus = ROOT.TF1('gaus','gaus',-100,100)
        for bin in range(plot2d.GetNbinsX()):
            fitplot = plot2d.ProjectionY(plot2d.GetName()+'_xbin'+str(bin+1),bin+1,bin+1)
            if fitplot.GetEntries()<=50 or fitplot.Integral()<=50: continue
            mean = fitplot.GetMean()
            rms = fitplot.GetRMS()
            rmserr = fitplot.GetRMSError()
            fitresult = fitplot.Fit(gaus,'QNRS','',mean-rms,mean+rms)
            #print "Filling with", fitresult.Parameter(1)
            plot.SetBinContent(bin+1,fitresult.Parameter(1))
            plot.SetBinError(bin+1,fitresult.ParError(1))
            #plot.SetBinContent(bin+1,fitresult.Parameter(2))
            #plot.SetBinError(bin+1,fitresult.ParError(2))
            #plot.SetBinContent(bin+1,fitplot.GetRMS())
            #plot.SetBinError(bin+1,fitplot.GetRMSError())
        return plot

    def getResoGraph(self,sample,plotname,name,plot2ds):
        #plot2d = self.infiles[sample].Get(plotname)
        plot2d = plot2ds 
        points = []
        for bin in range(plot2d.GetNbinsX()):
            fitplot = plot2d.ProjectionY(plotname+'_xbin'+str(bin+1),bin+1,bin+1)
            xpos = plot2d.GetXaxis().GetBinCenter(bin+1)
            xerr = plot2d.GetXaxis().GetBinUpEdge(bin+1) - xpos
            if fitplot.GetEntries()<=50 or fitplot.Integral()<=50: continue
            mean = fitplot.GetMean()
            rms = fitplot.GetRMS()
            rmserr = fitplot.GetRMSError()
            points.append((xpos,rms,xerr,rmserr))
        plot = ROOT.TGraphErrors(len(points))
        for bin, point in enumerate(points):
          xpos,rms,xerr,rmserr = point
          plot.SetPoint(bin,xpos,rms)
          plot.SetPointError(bin,xerr,rmserr)
        return plot

    def GetRatioScaleHistogram(self,sample,plotname,name,met, Data, MonteCarloPow,MonteCarloSher,MonteCarloAlp,MonteCarloMad):
    #def GetRatioScaleHistogram(self,sample,plotname,name,met, Data, MonteCarloSher,MonteCarloAlp):
        Result = Data.Clone( 'RatioHistogramFor' + plotname )
        #if name in rebinlist:
        #  if rebinlist[name] == 'variable':
        #    from array import array
        #    nbins = len(variableRebin[name])-1
        #    bin_array = array('d', variableRebin[name])
        #    print "Variable rebin!"
        #    Result = Result.RebinX(nbins, sample+'_'+met+'_data_rebin', bin_array)
        #    MonteCarloPow = MonteCarloPow.RebinX(nbins, sample+'_'+met+'_pow_rebin', bin_array)
        #    MonteCarloSher = MonteCarloSher.RebinX(nbins, sample+'_'+met+'_sher_rebin', bin_array)
        #    MonteCarloAlp = MonteCarloAlp.RebinX(nbins, sample+'_'+met+'_alp_rebin', bin_array)
        #    MonteCarloMad = MonteCarloMad.RebinX(nbins, sample+'_'+met+'_mad_rebin', bin_array)
        points_pow = []
        points_mad = []
        points_alp = []
        points_sher = []
        for bin in range(Result.GetNbinsX()):
            fitplot_data = Result.ProjectionY(plotname+'_data_xbin'+str(bin+1),bin+1,bin+1)
            fitplot_MCPow = MonteCarloPow.ProjectionY(plotname+'_pow_xbin'+str(bin+1),bin+1,bin+1)
            fitplot_MCSher = MonteCarloSher.ProjectionY(plotname+'_sher_xbin'+str(bin+1),bin+1,bin+1)
            fitplot_MCAlp = MonteCarloAlp.ProjectionY(plotname+'_alp_xbin'+str(bin+1),bin+1,bin+1)
            fitplot_MCMad = MonteCarloMad.ProjectionY(plotname+'_mad_xbin'+str(bin+1),bin+1,bin+1)
            xpos = Result.GetXaxis().GetBinCenter(bin+1)
            xerr = Result.GetXaxis().GetBinUpEdge(bin+1) - xpos
            if fitplot_data.GetEntries()<=50 or fitplot_MCPow.Integral()<=50 or fitplot_MCSher.Integral()<=50 or fitplot_MCAlp.Integral()<=50 or fitplot_MCMad.Integral()<=50: continue
            #if fitplot_data.GetEntries()<=50 or fitplot_MCSher.Integral()<=50 or fitplot_MCAlp.Integral()<=50 : continue
            mean_data = fitplot_data.GetMean()
            rms_data = fitplot_data.GetMean()
            rmserr_data = fitplot_data.GetMeanError()
            mean_mcpow = fitplot_MCPow.GetMean()
            rms_mcpow = fitplot_MCPow.GetMean()
            rmserr_mcpow = fitplot_MCPow.GetMeanError()
            mean_mcsher = fitplot_MCSher.GetMean()
            rms_mcsher = fitplot_MCSher.GetMean()
            rmserr_mcsher = fitplot_MCSher.GetMeanError()
            mean_mcalp = fitplot_MCAlp.GetMean()
            rms_mcalp = fitplot_MCAlp.GetMean()
            rmserr_mcalp = fitplot_MCAlp.GetMeanError()
            mean_mcmad = fitplot_MCMad.GetMean()
            rms_mcmad = fitplot_MCMad.GetMean()
            rmserr_mcmad = fitplot_MCMad.GetMeanError()
            rms_pow = 0 
            error_prop_pow = 0 
            rms_sher = 0 
            error_prop_sher = 0 
            rms_alp = 0 
            error_prop_alp = 0 
            rms_mad = 0 
            error_prop_mad = 0 
 	    if rms_mcpow == 0: 
	      pass 
 	    elif rms_mcalp == 0: 
	      pass 
 	    elif rms_mcmad == 0: 
	      pass 
 	    elif rms_mcsher == 0: 
	      pass 
  	    else:
              rms_pow = rms_data / rms_mcpow 
              rms_mad = rms_data / rms_mcmad 
              rms_sher = rms_data / rms_mcsher 
              rms_alp = rms_data / rms_mcalp 
	      error_prop_pow = math.sqrt( math.pow( rmserr_data / rms_mcpow, 2 ) + math.pow( rms_data * rmserr_mcpow / math.pow( rms_mcpow, 2 ), 2 ) ) 
	      error_prop_mad = math.sqrt( math.pow( rmserr_data / rms_mcmad, 2 ) + math.pow( rms_data * rmserr_mcmad / math.pow( rms_mcmad, 2 ), 2 ) ) 
	      error_prop_sher = math.sqrt( math.pow( rmserr_data / rms_mcsher, 2 ) + math.pow( rms_data * rmserr_mcsher / math.pow( rms_mcsher, 2 ), 2 ) ) 
	      error_prop_alp = math.sqrt( math.pow( rmserr_data / rms_mcalp, 2 ) + math.pow( rms_data * rmserr_mcalp / math.pow( rms_mcalp, 2 ), 2 ) ) 
              #print rms_pow
              #print error_prop_pow
              #print rms_mad
              #print error_prop_mad
              #print rms_sher
              #print error_prop_sher
              #print rms_alp
              #print error_prop_alp
            points_pow.append((xpos,rms_pow,xerr,error_prop_pow))
            points_sher.append((xpos,rms_sher,xerr,error_prop_sher))
            points_alp.append((xpos,rms_alp,xerr,error_prop_alp))
            points_mad.append((xpos,rms_mad,xerr,error_prop_mad))
        plot_pow = ROOT.TGraphErrors(len(points_pow))
        plot_sher = ROOT.TGraphErrors(len(points_sher))
        plot_alp = ROOT.TGraphErrors(len(points_alp))
        plot_mad = ROOT.TGraphErrors(len(points_mad))
        for bin, point in enumerate(points_pow):
          xpos,rms,xerr,rmserr = point
          plot_pow.SetPoint(bin,xpos,rms)
          plot_pow.SetPointError(bin,xerr,rmserr)
        for bin, point in enumerate(points_sher):
          xpos,rms,xerr,rmserr = point
          plot_sher.SetPoint(bin,xpos,rms)
          plot_sher.SetPointError(bin,xerr,rmserr)
        for bin, point in enumerate(points_alp):
          xpos,rms,xerr,rmserr = point
          plot_alp.SetPoint(bin,xpos,rms)
          plot_alp.SetPointError(bin,xerr,rmserr)
        for bin, point in enumerate(points_mad):
          xpos,rms,xerr,rmserr = point
          plot_mad.SetPoint(bin,xpos,rms)
          plot_mad.SetPointError(bin,xerr,rmserr)
        return (plot_pow,plot_sher,plot_alp,plot_mad)
        #return (plot_sher,plot_alp)

    def GetRatioHistogram(self,sample,plotname, Data, MonteCarloPow,MonteCarloSher,MonteCarloAlp,MonteCarloMad):
    #def GetRatioHistogram(self,sample,plotname, Data, MonteCarloSher,MonteCarloAlp):
        Result = Data.Clone( 'RatioHistogramFor' + plotname )
        points_pow = []
        points_mad = []
        points_alp = []
        points_sher = []
        for bin in range(Result.GetNbinsX()):
            fitplot_data = Result.ProjectionY(plotname+'_data_xbin'+str(bin+1),bin+1,bin+1)
            fitplot_MCPow = MonteCarloPow.ProjectionY(plotname+'_pow_xbin'+str(bin+1),bin+1,bin+1)
            fitplot_MCSher = MonteCarloSher.ProjectionY(plotname+'_sher_xbin'+str(bin+1),bin+1,bin+1)
            fitplot_MCAlp = MonteCarloAlp.ProjectionY(plotname+'_alp_xbin'+str(bin+1),bin+1,bin+1)
            fitplot_MCMad = MonteCarloMad.ProjectionY(plotname+'_mad_xbin'+str(bin+1),bin+1,bin+1)
            xpos = Result.GetXaxis().GetBinCenter(bin+1)
            xerr = Result.GetXaxis().GetBinUpEdge(bin+1) - xpos
            if fitplot_data.GetEntries()<=50 or fitplot_MCPow.Integral()<=50 or fitplot_MCSher.Integral()<=50 or fitplot_MCAlp.Integral()<=50 or fitplot_MCAlp.Integral()<=50: continue
            #if fitplot_data.GetEntries()<=50 or fitplot_MCSher.Integral()<=50 or fitplot_MCAlp.Integral()<=50 : continue
            mean_data = fitplot_data.GetMean()
            rms_data = fitplot_data.GetRMS()
            rmserr_data = fitplot_data.GetRMSError()
            mean_mcpow = fitplot_MCPow.GetMean()
            rms_mcpow = fitplot_MCPow.GetRMS()
            rmserr_mcpow = fitplot_MCPow.GetRMSError()
            mean_mcsher = fitplot_MCSher.GetMean()
            rms_mcsher = fitplot_MCSher.GetRMS()
            rmserr_mcsher = fitplot_MCSher.GetRMSError()
            mean_mcalp = fitplot_MCAlp.GetMean()
            rms_mcalp = fitplot_MCAlp.GetRMS()
            rmserr_mcalp = fitplot_MCAlp.GetRMSError()
            mean_mcmad = fitplot_MCMad.GetMean()
            rms_mcmad = fitplot_MCMad.GetRMS()
            rmserr_mcmad = fitplot_MCMad.GetRMSError()
            rms_pow = 0 
            error_prop_pow = 0 
            rms_sher = 0 
            error_prop_sher = 0 
            rms_alp = 0 
            error_prop_alp = 0 
            rms_mad = 0 
            error_prop_mad = 0 
 	    if rms_mcpow == 0: 
	      pass 
 	    elif rms_mcalp == 0: 
	      pass 
 	    elif rms_mcmad == 0: 
	      pass 
 	    elif rms_mcsher == 0: 
	      pass 
  	    else:
              rms_pow = rms_data / rms_mcpow 
              rms_mad = rms_data / rms_mcmad 
              rms_sher = rms_data / rms_mcsher 
              rms_alp = rms_data / rms_mcalp 
	      error_prop_pow = math.sqrt( math.pow( rmserr_data / rms_mcpow, 2 ) + math.pow( rms_data * rmserr_mcpow / math.pow( rms_mcpow, 2 ), 2 ) ) 
	      error_prop_mad = math.sqrt( math.pow( rmserr_data / rms_mcmad, 2 ) + math.pow( rms_data * rmserr_mcmad / math.pow( rms_mcmad, 2 ), 2 ) ) 
	      error_prop_sher = math.sqrt( math.pow( rmserr_data / rms_mcsher, 2 ) + math.pow( rms_data * rmserr_mcsher / math.pow( rms_mcsher, 2 ), 2 ) ) 
	      error_prop_alp = math.sqrt( math.pow( rmserr_data / rms_mcalp, 2 ) + math.pow( rms_data * rmserr_mcalp / math.pow( rms_mcalp, 2 ), 2 ) ) 
              #print rms_pow
              #print error_prop_pow
              #print rms_mad
              #print error_prop_mad
              #print rms_sher
              #print error_prop_sher
              #print rms_alp
              #print error_prop_alp
            points_pow.append((xpos,rms_pow,xerr,error_prop_pow))
            points_sher.append((xpos,rms_sher,xerr,error_prop_sher))
            points_alp.append((xpos,rms_alp,xerr,error_prop_alp))
            points_mad.append((xpos,rms_mad,xerr,error_prop_mad))
        plot_pow = ROOT.TGraphErrors(len(points_pow))
        plot_sher = ROOT.TGraphErrors(len(points_sher))
        plot_alp = ROOT.TGraphErrors(len(points_alp))
        plot_mad = ROOT.TGraphErrors(len(points_mad))
        for bin, point in enumerate(points_pow):
          xpos,rms,xerr,rmserr = point
          plot_pow.SetPoint(bin,xpos,rms)
          plot_pow.SetPointError(bin,xerr,rmserr)
        for bin, point in enumerate(points_sher):
          xpos,rms,xerr,rmserr = point
          plot_sher.SetPoint(bin,xpos,rms)
          plot_sher.SetPointError(bin,xerr,rmserr)
        for bin, point in enumerate(points_alp):
          xpos,rms,xerr,rmserr = point
          plot_alp.SetPoint(bin,xpos,rms)
          plot_alp.SetPointError(bin,xerr,rmserr)
        for bin, point in enumerate(points_mad):
          xpos,rms,xerr,rmserr = point
          plot_mad.SetPoint(bin,xpos,rms)
          plot_mad.SetPointError(bin,xerr,rmserr)
        return (plot_pow,plot_sher,plot_alp,plot_mad)
        #return (plot_sher,plot_alp)

    def ProjFit(self,sample,plotname,name,plot2dpow,plot2dsher,plot2dmad,plot2dalp,dataplot,jetbin):
        print sample,plotname
        plot2d_data = dataplot
        if 'Zmm' in sample:
                evt = 'Zmm'
                event = 'Zmm'
        elif 'Zmm' in sample and '0jet' in jetbin:
                evt = 'Zmm'
                event = 'Zmm_0j'
        if 'Zee' in sample:
                evt = 'Zee'
                event = 'Zee'
        elif 'Zee' in sample and '0jet' in jetbin:
                evt = 'Zee'
                event = 'Zee_0j'
        plot2d_pow = plot2dpow
        plot2d_sher = plot2dsher
        plot2d_mad = plot2dmad
        plot2d_alp = plot2dalp
        gaus = ROOT.TF1('gaus','gaus',-100,100)
        points = []
        c = ROOT.TCanvas("canvas","Projection_Fit",800,600)
        c.SetRightMargin(0.1)
        pad1 = ROOT.TPad("pad1","pad1",0,0.3,1,1)
        pad1.SetTopMargin( 0.06 )
        pad1.SetBottomMargin(0.05)
        pad1.SetFillStyle( 4000 )
        pad1.SetFillColor( 0 )
        pad1.SetFrameFillStyle( 4000 )
        pad1.SetFrameFillColor( 0 )
        pad1.Draw()
        pad1.cd()

        c.cd()

        pad2 = ROOT.TPad("pad2","pad2",0,0,1,0.3)
        pad2.SetTopMargin(0)
        pad2.SetBottomMargin( 0.35 )
        pad2.SetFillStyle( 4000 )
        pad2.SetFillColor( 0 )
        pad2.SetFrameFillStyle( 4000 )
        pad2.SetFrameFillColor( 0 )
        pad2.SetGrid()
        pad2.Draw()
        pad2.cd()

        c.cd()
        pad1.cd()

        leg_coords = (0.60,0.5,0.85,0.8)
        leg = ROOT.TLegend(*leg_coords)
        leg.SetBorderSize(0)
        leg.SetFillStyle(0)
        c.Print("%s/" % (evt) +"%s_slices%s.pdf[" % (name,jetbin))
        num = 0
        frame = None
        for bin in range(plot2d_Zmm_data.GetNbinsX()):
            fitplot_data = plot2d_Zmm_data.ProjectionY("data+"+plotname+'_xbin'+str(bin+1),bin+1,bin+1)
            fitplot_mc = plot2d_Zmm_pow.ProjectionY("pow_"+plotname+'_xbin'+str(bin+1),bin+1,bin+1)
            fitplot_mc1 = plot2d_Zmm_sher.ProjectionY("sher_"+plotname+'_xbin'+str(bin+1),bin+1,bin+1)
            fitplot_mc2 = plot2d_Zmm_mad.ProjectionY("mad_"+plotname+'_xbin'+str(bin+1),bin+1,bin+1)
            fitplot_mc3 = plot2d_Zmm_alp.ProjectionY("alp_"+plotname+'_xbin'+str(bin+1),bin+1,bin+1)
            data = plot2d_data.Integral()
            mc = plot2d_pow.Integral()
            mc1 = plot2d_sher.Integral()
            mc2 = plot2d_mad.Integral()
            mc3 = plot2d_alp.Integral()
            scale_factor = data/mc
            scale_factor1 = data/mc1
            scale_factor2 = data/mc2
            scale_factor3 = data/mc3
            fitplot_mc.Scale(scale_factor)
            fitplot_mc1.Scale(scale_factor1)
            fitplot_mc2.Scale(scale_factor2)
            fitplot_mc3.Scale(scale_factor3)
            xpos = plot2d_data.GetXaxis().GetBinCenter(bin+1)
            xerr = plot2d_data.GetXaxis().GetBinUpEdge(bin+1) - xpos
            xerr = plot2d_Zmm_data.GetXaxis().GetBinUpEdge(bin+1) - xpos
            if fitplot_data.GetEntries()<=50 or fitplot_data.Integral()<=50: continue
            elif fitplot_mc.GetEntries()<=50 or fitplot_mc.Integral()<=50: continue
            elif fitplot_mc1.GetEntries()<=50 or fitplot_mc1.Integral()<=50: continue
            elif fitplot_mc2.GetEntries()<=50 or fitplot_mc2.Integral()<=50: continue
            elif fitplot_mc3.GetEntries()<=50 or fitplot_mc3.Integral()<=50: continue
            mean = fitplot_data.GetMean()
            rms = fitplot_data.GetRMS()
            rmserr = fitplot_data.GetRMSError()
            mean1 = fitplot_mc.GetMean()
            rms1 = fitplot_mc.GetRMS()
            mean2 = fitplot_mc1.GetMean()
            rms2 = fitplot_mc1.GetRMS()
            mean3 = fitplot_mc2.GetMean()
            rms3 = fitplot_mc2.GetRMS()
            mean4 = fitplot_mc3.GetMean()
            rms4 = fitplot_mc3.GetRMS()
            rmserr1 = fitplot_mc.GetRMSError()
            fitplot_data.Fit("gaus","O")
            fitplot_mc.Fit("gaus","O")
            fit = fitplot_data.GetFunction("gaus")
            fit1 = fitplot_mc.GetFunction("gaus")
            low = plot2d_Zmm_data.GetXaxis().GetBinLowEdge(bin+1)
            up = plot2d_Zmm_data.GetXaxis().GetBinUpEdge(bin+1)
            txt = ROOT.TPaveText(0.2, 0.6, 0.5, 0.9, "NDC")
            txt.SetBorderSize(0);
            txt.SetFillColor(0);
            txt.SetTextSize(0.03);
            if 'data' in sample:
              txt.AddText("Data 2015, #sqrt{s} = 13 TeV")
            else: txt.AddText("MC15, #sqrt{s} = 13 TeV")
            if 'Zmm' in sample:
              txt.AddText('Z #rightarrow #mu#mu,  '+str(round(self.data_lumi/1000., 2))+' fb^{-1}')
            if 'Zee' in sample:
              txt.AddText('Z #rightarrow ee,  '+str(round(self.data_lumi/1000., 2))+' fb^{-1}')
            if '0j' in name and '0jet' in jetbin:
              textbox.AddText('0 jets p_{T} > 20 GeV')
            xtitle = xtitles[name] if name in xtitles.keys() else ''
            txt.AddText('{low1} < {name1} < {up1}'.format(low1=low,name1=xtitle,up1=up))
            chisq = fit.GetChisquare()
            ndof = fit.GetNDF()
            chisq1 = fit1.GetChisquare()
            ndof1 = fit1.GetNDF()
            fitmean = fit.GetParameter(1)
            fitrms = fit.GetParameter(2)
            fitmean1 = fit1.GetParameter(1)
            fitrms1 = fit1.GetParameter(2)
            self.ATLAS_LABEL(0.5,0.7)
            if ndof != 0 :
              txt.AddText('Data, '+'#chi^{2}/N_{dof} = ' + '{chi}/{ndof} = {div}'.format(chi=round(chisq,2),ndof=round(ndof,2),div=round(chisq/ndof,2)))
            if ndof1 != 0 :
              txt.AddText('Sim, '+'#chi^{2}/N_{dof} = ' + '{chi}/{ndof} = {div}'.format(chi=round(chisq1,2),ndof=round(ndof1,2),div=round(chisq1/ndof1,2)))
            txt.AddText('Data, Mean, RMS = '+'{mean}, {rms}'.format(mean=round(mean,2),rms=round(rms,2)))
            txt.AddText('Pow, Mean, RMS = '+'{mean}, {rms}'.format(mean=round(mean1,2),rms=round(rms1,2)))
            txt.AddText('Sher, Mean, RMS = '+'{mean}, {rms}'.format(mean=round(mean2,2),rms=round(rms2,2)))
            txt.AddText('Mad, Mean, RMS = '+'{mean}, {rms}'.format(mean=round(mean3,2),rms=round(rms3,2)))
            txt.AddText('Alp, Mean, RMS = '+'{mean}, {rms}'.format(mean=round(mean4,2),rms=round(rms4,2)))
            ytitle = ytitles[name] if name in ytitles.keys() else ''
            fitplot_data.GetXaxis().SetTitle(ytitle)
            fitplot_mc.GetXaxis().SetTitle(ytitle)
            bin_width = fitplot_data.GetXaxis().GetBinWidth(0)
            fitplot_data.GetYaxis().SetTitle('Events / '+str(int(bin_width))+' GeV')
            fit.SetLineColor(ROOT.kRed)
            fit1.SetLineColor(ROOT.kBlue)
            fit.SetLineWidth(0)
            fit1.SetLineWidth(0)
            fitplot_data.GetXaxis().SetTickLength(0.03);
            fitplot_data.GetXaxis().SetTickSize(0.04);
            fitplot_data.GetYaxis().SetTickSize(0.03);
            fitplot_data.Draw('')
            fitplot_mc.Draw('same')
            fitplot_mc1.Draw('same')
            fitplot_mc2.Draw('same')
            fitplot_mc3.Draw('same')
            txt.Draw("same")
            if ('data' in sample) and (num == 0) :
             leg.AddEntry(fitplot_data,"Data",'lp')
             leg.AddEntry(fitplot_mc,"Powheg+bkgs",'lp')
             leg.AddEntry(fitplot_mc1,"Sherpa+bkgs",'lp')
             leg.AddEntry(fitplot_mc2,"Madgraph+bkgs",'lp')
             leg.AddEntry(fitplot_mc3,"Alpgen+bkgs",'lp')
             num = num + 1
            pad2.cd()
            ratio_hist = fitplot_data.Clone()
            ratio_hist1 = fitplot_data.Clone()
            ratio_hist2 = fitplot_data.Clone()
            ratio_hist3 = fitplot_data.Clone()

            ratio_hist.GetYaxis().SetNdivisions(505);
            ratio_hist1.GetYaxis().SetNdivisions(505);
            ratio_hist2.GetYaxis().SetNdivisions(505);
            ratio_hist3.GetYaxis().SetNdivisions(505);

            ratio_hist.Divide(fitplot_mc)
            ratio_hist1.Divide(fitplot_mc1)
            ratio_hist2.Divide(fitplot_mc2)
            ratio_hist3.Divide(fitplot_mc3)
            ratio_hist.SetTitle("")

            ratio_hist.SetMarkerColor(ROOT.kGreen)
            ratio_hist.SetMarkerStyle(ROOT.kOpenTriangleUp)
            ratio_hist1.SetMarkerColor(ROOT.kBlue)
            ratio_hist1.SetMarkerStyle(ROOT.kOpenTriangleDown)
            ratio_hist2.SetMarkerColor(ROOT.kRed)
            ratio_hist2.SetMarkerStyle(ROOT.kOpenCircle)
            ratio_hist3.SetMarkerColor(ROOT.kMagenta)
            ratio_hist3.SetMarkerStyle(ROOT.kOpenSquare)

            ratio_hist.GetYaxis().SetNdivisions(505)
            ratio_hist.GetYaxis().SetTitleSize(20)
            ratio_hist.GetYaxis().SetTitleFont(43)
            ratio_hist.GetYaxis().SetTitle('Data/MC')
            ratio_hist.GetYaxis().SetTitleOffset(1.5)
            ratio_hist.GetYaxis().SetLabelFont(43)
            ratio_hist.GetYaxis().SetLabelSize(20)

            ratio_hist.GetXaxis().SetTitleSize(20)
            ratio_hist.GetXaxis().SetTitleFont(43)
            ratio_hist.GetXaxis().SetTitleOffset(3.5)
            ratio_hist.GetXaxis().SetLabelFont(43)
            ratio_hist.GetXaxis().SetLabelSize(20)
            ratio_hist.GetXaxis().SetTitle(ytitle)

            ratio_hist.GetYaxis().SetRangeUser(0.6,1.4)

            ratio_hist.Draw()
            ratio_hist1.Draw("same")
            ratio_hist2.Draw("same")
            ratio_hist3.Draw("same")

            min_x = ratio_hist.GetXaxis().GetBinLowEdge(1)
            max_x = ratio_hist.GetXaxis().GetBinLowEdge(ratio_hist.GetNbinsX())
            line = ROOT.TLine(min_x, 1., max_x, 1.)
            line.SetLineColor( ROOT.kRed + 1 )
            line.SetLineWidth( 4 )
            line.Draw('same')
            ratio_hist.Draw( 'same' )
            c.Update()
            pad1.cd()
            txt.Draw("same")
            leg.Draw("same")
            ratio_hist.GetXaxis().SetTitle(ytitle)
            c.Update()
            c.Print("%s/" % (evt) +"%s_slices%s.pdf" % (name,jetbin))
        c.Print("%s/" % (evt) +"%s_slices%s.pdf]" % (name,jetbin))
        del c

    def drawResoPlots(self,metlist,plotnames,samples=['Zmm'],outdir='',is0j=False):
        to_plot_list = []
        for sample in samples:
          for met in metlist:
            to_plot_list.append((sample, met))

        c = {}
        for name in plotnames:
            if is0j and (name in jet_bins): continue
            self.plots[name] = {}
            canvas_name_stem = '_'.join(samples) + ('_0j' if is0j else '')
            c[name] = ROOT.TCanvas(canvas_name_stem+'_'+name,name)
            c[name].SetRightMargin(0.1)
            if do_ratio_plots:
              pad1 = ROOT.TPad("pad1","pad1",0,0.3,1,1)
              pad1.SetTopMargin( 0.06 )
              pad1.SetBottomMargin(0.05)
              pad1.SetFillStyle( 4000 )
              pad1.SetFillColor( 0 )
              pad1.SetFrameFillStyle( 4000 )
              pad1.SetFrameFillColor( 0 )
              pad1.Draw()
              pad1.cd()

              c[name].cd()

              pad2 = ROOT.TPad("pad2","pad2",0,0,1,0.3)
              pad2.SetTopMargin(0)
              pad2.SetBottomMargin( 0.35 )
              pad2.SetFillStyle( 4000 )
              pad2.SetFillColor( 0 )
              pad2.SetFrameFillStyle( 4000 )
              pad2.SetFrameFillColor( 0 )
              pad2.SetGrid()
              pad2.Draw()
              pad2.cd()

              c[name].cd()
              pad1.cd()

            frame = None

            if name in reso_legend_coords:
              leg_coords = reso_legend_coords[name]
            else:
              leg_coords = (0.45,0.68,0.70,0.92)
            if name == 'residxy_npv' and zoomedDiagnostic:
              #leg_coords = (0.60,0.65,0.85,0.90)
              leg_coords = (0.60,0.2,0.85,0.5)
            if is0j:
              leg_coords = (0.60,0.65,0.85,0.90)
              #leg_coords = (0.60,0.2,0.85,0.5)
            leg = ROOT.TLegend(*leg_coords)
            leg.SetBorderSize(0)
            leg.SetFillStyle(0)
            #self.total_mc = ROOT.TMultiGraph()
            listofTH2bkgs = []
            listofTH2 = []
	    plotFrame = True
            for idx,to_plot in enumerate(to_plot_list):
                met = to_plot[1]
                sample = to_plot[0]
                evttype = sample.split('_')[0]
                plotdir = evttype + ('_0j' if is0j else '')
                fullname = '{pdir}/{metsuf}_{shortname}{njet}'.format(pdir=plotdir,
                                                                      metsuf=met,
                                                                      shortname=name,
                                                                      njet='_0jet' if is0j else ''
                                                                      )
                print 'In file', sample, ', draw', fullname
        	plot2d = self.infiles[sample].Get(fullname)
                #plot = self.getGausFitMean(plot2d,sample,name)
		### if you need slices of distributions uncomment the line below
                #self.ProjFit(sample, fullname, name)
                #plot = self.getResoGraph(sample, fullname, name)
                if self.normalize_to_lumi and not 'data' in sample:
                  scale_factor = self.get_mc_scale(sample)
                  plot2d.Scale(scale_factor)
                if 'data' in sample:
		  self.data_sample = sample
                plot = None
                if 'data' not in sample:
                  if sample in listofTH2_bkgs:
                    listofTH2bkgs.append(plot2d)
                    continue
                  if sample in listofTH2_powheg:
                    listofTH2.append(plot2d)
                    if 'Zmm_361107' in sample or 'Zee_361106' in sample or 'Wev_361100' in sample:
                      plot,powplot = self.resoGraphList(sample,listofTH2bkgs,listofTH2,fullname,name,plot2d)
                      self.powplot = powplot
                    else: continue
                  if sample in listofTH2_oldsherpa:
                    listofTH2.append(plot2d)
                    if 'Zmm_361396' in sample or 'Zee_361372' in sample or 'Wev_361300' in sample:
                      plot,sher1plot = self.resoGraphList(sample,listofTH2bkgs,listofTH2,fullname,name,plot2d)
                      self.sher1plot = sher1plot
                    else: continue
                  if sample in listofTH2_sherpa:
                    listofTH2.append(plot2d)
                    if 'Zmm_363364' in sample or 'Zee_363388' in sample or 'Wev_363460' in sample:
                      plot,sherplot = self.resoGraphList(sample,listofTH2bkgs,listofTH2,fullname,name,plot2d)
                      self.sherplot = sherplot
                    else: continue
                  if sample in listofTH2_madgraph:
                    listofTH2.append(plot2d)
                    if 'Zmm_361505' in sample or 'Zee_361500' in sample or 'Wev_361520' in sample:
                      plot,madplot = self.resoGraphList(sample,listofTH2bkgs,listofTH2,fullname,name,plot2d)
                      self.madplot = madplot
                    else: continue
                  if sample in listofTH2_alpgen:
                    listofTH2.append(plot2d)
                    if 'Zmm_361710' in sample or 'Zee_361700' in sample or 'Wev_361800' in sample:
                      plot,alpplot = self.resoGraphList(sample,listofTH2bkgs,listofTH2,fullname,name,plot2d)
                      self.alpplot = alpplot
                    else: continue
                else:
                  self.dataplot = plot2d
                  plot = self.getResoGraph(sample, fullname, name,plot2d)

                if met not in self.plots[name] : self.plots[name][met]={}
                self.plots[name][met][sample] = plot
                #if idx==0:
                if plotFrame:
		  plotFrame = False
                  xrange = xrangesreso[name] if name in xrangesreso.keys() else (0.,1000.)
                  if sample == 'ttbar' and name in yrangesresottbar:
                    yrange = yrangesresottbar[name]
                  elif name in yrangesreso:
                    yrange = yrangesreso[name]
                  else:
                    yrange = (0.,40)
                  frame = ROOT.TH2F('','',1,xrange[0],xrange[1],1,yrange[0],yrange[1])
                  xtitle = xtitles[name] if name in xtitles.keys() else ''
                  ytitle = ytitlesreso[name] if name in ytitlesreso.keys() else 'Events'
                  #frame.SetXTitle(xtitle)
                  frame.GetXaxis().SetTitleSize(20)
                  frame.GetXaxis().SetTitleFont(43)
                  frame.GetXaxis().SetTitleOffset(3.5)
                  frame.GetXaxis().SetLabelFont(43)
                  frame.GetXaxis().SetLabelSize(20)
                  frame.GetXaxis().SetTitle(xtitle)
                  frame.GetYaxis().SetNdivisions(505)
                  frame.GetYaxis().SetTitleSize(20)
                  frame.GetYaxis().SetTitleFont(43)
                  frame.GetYaxis().SetTitleOffset(1.5)
                  #frame.SetYTitle(ytitle)
                  frame.GetYaxis().SetTitle(ytitle)
                  frame.GetYaxis().SetLabelFont(43)
                  frame.GetYaxis().SetLabelSize(20)
                  frame.Draw('')
                  #frame.GetXaxis().SetTickLength(0.03);
                  #frame.GetXaxis().SetTickSize(0.04);
                  #frame.GetYaxis().SetTickSize(0.03);
                  frame.GetXaxis().SetLabelOffset(999);
                  frame.GetXaxis().SetTitleOffset(999);

                drawopt = 'pesame'
                self.drawSinglePlot(name,met,sample,c[name],drawopt)
                if self.compare_samples:
                  if sample in names:
                    leg.AddEntry(plot,names[sample],'lp')
                  else:
                    pass
                    #leg.AddEntry(plot,sample,'lp')
                else:
                  leg.AddEntry(plot,names[met],'p')
            #if is0j: 
            # self.ProjFit(sample, fullname, name,powplot,sherplot,madplot,alpplot,self.dataplot,'0jet')
            #else:
            # self.ProjFit(sample, fullname, name,powplot,sherplot,madplot,alpplot,self.dataplot,'')
            if name == 'residxy_npv' and zoomedDiagnostic:
              #ROOT.ATLASLabel(0.20,0.88,"Internal")
              atlas_coords = (0.20,0.88,1,0.05)
            else:
              #ROOT.ATLASLabel(0.47,0.20,"Internal")
              #ROOT.ATLASLabel(0.20,0.88,"Internal")
              atlas_coords = (0.20,0.88,1,0.05)
            self.ATLAS_LABEL(*atlas_coords)
            if name in reso_textbox_coords:
              textcoords = reso_textbox_coords[name]
            else:
              textcoords = (0.19,0.55,0.44,0.85,'brNDC')
            textbox = ROOT.TPaveText(*textcoords)
            textbox.SetTextAlign(11)
            textbox.SetTextSize(0.05)
            textbox.SetFillStyle(0)
            textbox.SetBorderSize(0)
            textbox.AddText("Data 2015, #sqrt{s} = 13 TeV")
            if 'Zmm' in sample:
              textbox.AddText('Z #rightarrow #mu#mu,  '+str(round(self.data_lumi/1000., 2))+' fb^{-1}')
            elif 'Zee' in sample:
              textbox.AddText('Z #rightarrow ee,  '+str(round(self.data_lumi/1000., 2))+' fb^{-1}')
            elif 'Wmv' in sample:
              textbox.AddText('W #rightarrow #mu#nu,  '+str(round(self.data_lumi/1000., 2))+' fb^{-1}')
            elif 'ttbar' in sample:
              textbox.AddText('ttbar')
            if sample == 'ttbar':
              textbox.AddText('1lepton+jets')
            else:
              if '0j' in name or is0j:
                textbox.AddText('0 jets p_{T} > 20 GeV')
              elif '1j' in name:
                textbox.AddText('1 jet p_{T} > 20 GeV')
              elif '2j' in name:
                textbox.AddText('#geq 2 jet p_{T} > 20 GeV')
              else:
                textbox.AddText('all jets')
            textbox.Draw('same')
            leg.Draw()
            #samples_string = '_'.join(samples)
            if any("Zmm" in sample for sample in samples):
               samples_string = 'Zmm'
            elif any("Zee" in sample for sample in samples):
               samples_string = 'Zee'
            elif any("Wev" in sample for sample in samples):
               samples_string = 'Wev'
            elif any("Wmv" in sample for sample in samples):
               samples_string = 'Wmv'
            elif any("ttbar" in sample for sample in samples):
               samples_string = 'ttbar'
            else:
               samples_string = ''

            if do_ratio_plots:
              pad2.cd()
              data_hist = self.plots[name][met][self.data_sample]
              #plot = self.getResoGraph(sample, fullname, name,data_hist)
              ratio_hist = ROOT.TGraphErrors(data_hist) 
	      ratio_hist.Clear()
              (ratio_hist1,ratio_hist2,ratio_hist3,ratio_hist4) = self.GetRatioHistogram(sample, fullname, self.dataplot,powplot,sherplot,alpplot,madplot)
              #(ratio_hist1,ratio_hist2) = self.GetRatioHistogram(sample, fullname, self.dataplot,sher1plot,sherplot)
              ratio_hist.SetTitle("")

              ratio_hist.GetYaxis().SetNdivisions(505)
              ratio_hist.GetYaxis().SetTitleSize(20)
              ratio_hist.GetYaxis().SetTitleFont(43)
              ratio_hist.GetYaxis().SetTitle('Data/MC')
              ratio_hist.GetYaxis().SetTitleOffset(1.5)
              ratio_hist.GetYaxis().SetLabelFont(43)
              ratio_hist.GetYaxis().SetLabelSize(20)

              ratio_hist.GetXaxis().SetTitleSize(20)
              ratio_hist.GetXaxis().SetTitleFont(43)
              ratio_hist.GetXaxis().SetTitleOffset(3.5)
              ratio_hist.GetXaxis().SetLabelFont(43)
              ratio_hist.GetXaxis().SetLabelSize(20)
              ratio_hist.GetXaxis().SetTitle(xtitle)

              ratio_hist.GetYaxis().SetRangeUser(0.6,1.4)

              ratio_hist1.SetMarkerColor(ROOT.kBlue+2)
              ratio_hist1.SetMarkerStyle(ROOT.kOpenTriangleDown)
              ratio_hist2.SetMarkerColor(ROOT.kRed+2)
              ratio_hist2.SetMarkerStyle(ROOT.kOpenCircle)
              ratio_hist3.SetMarkerColor(ROOT.kMagenta+2)
              ratio_hist3.SetMarkerStyle(ROOT.kOpenSquare)
              ratio_hist4.SetMarkerColor(ROOT.kGreen+2)
              ratio_hist4.SetMarkerStyle(ROOT.kOpenTriangleUp)

              #xrange = xrangesreso[name] if name in xrangesreso.keys() else (0.,1000.)
              #min_x = ratio_hist.GetXaxis().GetBinLowEdge(int(xrange[0]))
              #max_x = ratio_hist.GetXaxis().GetBinLowEdge(data_hist.GetN()+1)
              #max_x = ratio_hist.GetXaxis().GetBinUpEdge(data_hist.GetN()+1)
              #max_x = ratio_hist.GetXaxis().GetBinLowEdge(int(xrange[1]))
              #print xrange
              xrange = xrangesreso[name] if name in xrangesreso.keys() else (0.,1000.)
              if sample == 'ttbar' and name in yrangesresottbar:
                yrange = yrangesresottbar[name]
              elif name in yrangesreso:
                yrange = yrangesreso[name]
              else:
                yrange = (0.,40)
              frame1 = ROOT.TH2F(sample+'_'+name+'_'+met,'',1,xrange[0],xrange[1],1,0.6,1.4)
              #frame.GetYaxis().SetRangeUser(0.6,1.4)
              xtitle = xtitles[name] if name in xtitles.keys() else ''
              #ytitle = ytitlesreso[name] if name in ytitlesreso.keys() else 'Events'
              #frame.SetXTitle(xtitle)
              frame1.GetXaxis().SetTitleSize(20)
              frame1.GetXaxis().SetTitleFont(43)
              frame1.GetXaxis().SetTitleOffset(3.5)
              frame1.GetXaxis().SetLabelFont(43)
              frame1.GetXaxis().SetLabelSize(20)
              frame1.GetXaxis().SetTitle(xtitle)
              frame1.GetYaxis().SetNdivisions(505)
              frame1.GetYaxis().SetTitleSize(20)
              frame1.GetYaxis().SetTitleFont(43)
              frame1.GetYaxis().SetTitle('Data/MC')
              frame1.GetYaxis().SetTitleOffset(1.5)
              frame1.GetYaxis().SetLabelFont(43)
              frame1.GetYaxis().SetLabelSize(20)

              #frame.SetYTitle(ytitle)
              frame1.Draw('')
              min_x = int(xrange[0])
              max_x = int(xrange[1])
              #print min_x
              #print max_x
              #print ratio_hist.GetXaxis().GetXmin()
              #print ratio_hist.GetXaxis().GetXmax()
              #ratio_hist.GetXaxis().SetRangeUser(min_x,max_x)
              #print ratio_hist.GetXaxis().GetXmin()
              #print ratio_hist.GetXaxis().GetXmax()
              #ratio_hist.Draw('same')
              line = ROOT.TLine(min_x, 1., max_x, 1.)
              line.SetLineColor( ROOT.kRed + 1 )
              line.SetLineWidth( 4 )
              line.Draw('same')
              ratio_hist1.Draw("pesame")
              ratio_hist2.Draw("pesame")
              ratio_hist3.Draw("pesame")
              ratio_hist4.Draw("pesame")

              c[name].Update()

	    outfile_format = ['pdf','png','eps','C']

	    for filetype in outfile_format :

               outfile_name = '{outdir}/{etype}/{shortname}{njet}.{filetype}'.format(outdir=outdir,
                                                                                 etype=samples_string,
                                                                                 shortname=name.replace('resid','reso'),
                                                                                 njet='_0jet' if is0j else '',
                                                                                 filetype=filetype
                                                                               )
               c[name].SaveAs(outfile_name)

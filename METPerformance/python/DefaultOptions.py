# @Author: Marco Valente <valentem>
# @Date:   1970-01-01T01:00:00+01:00
# @Email:  marco.valente@cern.ch
# @Last modified by:   valentem
# @Last modified time: 2016-11-03T20:34:37+01:00

# Description:  This file incorporates the definition of the default options for
#               MakeMETPerfSlims.py. If you want to add your personal default
#               options, please add them to the 'default_options' dictionary
#               by specifying your AFS username.

default_options = { 'default': {
                                 'EvtMax':              -1,
                                 'Year':                '2015+2016', #Sets GRL, PRWFile and LUMIFile for that year. Year must correspond to mc generation (i.e. 2017 for mc16d).
                                 'JetSelection':        'Tight',
                                 'SystematicTerm':      'Nominal', #Nominal is the default one, for CST: MET_SoftCalo_ScaleUp, MET_SoftCalo_ScaleDown, MET_SoftCalo_Reso
                                 'runOnGrid':           True,
                                 'overrideIsData':      False,
                                 'trees':               False, # Flag to produce flat ntuples
                                 'plots':               True,  # Flag to produce histograms
                                 'xaodslim':            False,  # Flag to produce slimmed xAOD
                                 'doCST':               False,
                                 'doTST':               True,
                                 'doTrack':             True,
                                 'DoEverySelection':    False,  # if set to True, does every Event selection. Useful if plots is False
                                 'doTracksPlot':        True,  # if true, does ip distributions
                                 'EvtType':             'ttbar', # if plots is set to false it doesn't matter what you put here as EvtType
                                 'JetColl':             'EMPFlow', #EMTopo or EMPFlow
                                 #'fJVTWP':              'None', #Loose, Tight, NEW, Loose120, Loose90, None
                                 'fJVTWP':              'Loose', #Loose, Tight, NEW, Loose120, Loose90, None
                                 'JetEtaMax':           4.5,
                                 'CustomJetEtaForw':    2.4,
                                 'fJVTCenPt':            -1, # central pT threshold for fJVT. currently set to infinity. could try to set this to other options
                                 'doFwdJetVeto':        False, #Skip events containing fwd jets
                                 'doTruthMaps':         False,
                                 'doPFlowJVTScan':      False,
                                 'UseGhostMuons':       True,
                                 'doJVTScan' :          False, #Currently (23/10/2018) memory intensive when true.
                                 'doCutBK':             True,
                                 'metCut':              -10.0,
                                 'doGoodRunCleaning':   False,  # added by Matteo to deal with data with no GRL available, if False skip GR selection
                                 'lowRN':               -1,    # if positive, set low RunNumber veto
                                 'highRN':              -1,    # if positive, set high RunNumber veto
                                 'doJetCleaning':       True,
                                 'VeryVerbose':         False,
                                 'Verbose':             False,
                                 'doPileupReweighting': True,  # claire added flag for mc-only studies without prw
                                 'doMuonElossPlots':    False, # flag for plots of muon eloss
                                 'RequireTrigger':      True,  # flag to require a trigger. Set to false to skip trigger decision
                                 'JetMinEFrac':             0.0,
                                 'JetMinWeightedPt':             20.0e3,
                                 'doEleJetOlapScan':    True, #flag to scan jet energy frac and pt diff with ele for olap study
                                 'doTriggerMatching':   True,  # flag to provide trigger matching in Zll events
                                 'writeTracks':         False, # switch on to prevent writing tracks to xAOD slim
                                 'writeMaps':           False, # switch on to prevent writing MET maps to xAOD slim
                                 'buildaltAODmap':      False, # to build alternative association map in AOD
                                 'skipTauTruthMatch':   True, # to run on AODs
                                 'applyPFlowJVT':       True, # to enable JVT for PFlow
                                 'doPerfMon':           False, # to run PerfMon for monitoring code speed
                                 'doValgrind':          False, # to run Valgrind for code optimisation
                                 'inputdir':            '',

                               },
                    'valentem': {
                                 'EvtType':  'Zee',
                                 'inputdir': '/atlas/data1/userdata/valentem/PFlow/DAODs/mc15_13TeV.361106.PowhegPythia8EvtGen_AZNLOCTEQ6L1_Zee.merge.DAOD_JETM3.e3601_s2576_s2132_r7725_r7676_p2666',
                                },
                    'schae':    {'inputdir':'/tmp/schae/data17_13TeV.00334487.physics_Main.deriv.DAOD_JETM3.f859_m1860_p3319/'},
                    'hp341':    {'inputdir':'/r03/atlas/hpacey/r21_TSTSystematics_samples/Zee/Sherpa/mc16_13TeV'},
                    'etolley': {'EvtType':  'Zee',
                                 'doCST':    False,
                                 'doTST':    True,
                                 'doTrack':  True,
                                 'doPileupReweighting': True,
                                 #'PRWFiles': ['METPerformance/mc16c_powheg_sherpa_ttbar.root'],
                                 'JetColl':  'EMTopo',
                                 #'PRWFiles': ['METPerformance/mc16c.sherpa.prw.root'],
                                },
                  }

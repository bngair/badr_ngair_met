#! /bin/bash
###################
# get Grid samples#
###################

#################################################################
# To get grid data text file with run in GRL                    #
# in shell do nohup ./fill_grid_ds.sh > grid_samples_data.txt & #
#################################################################

dq2-ls data15_13TeV.*JETM3*_p2425/ | sort -n > datalist.txt
export INPUT=datalist.txt
export inDS=($(< ${INPUT}))
export GRL=data15_13TeV.periodAllYear_DetStatus-v71-pro19-05_DQDefects-00-01-02_PHYS_StandardGRL_All_Good_25ns.xml
for names in "${inDS[@]}"
do
	export var=$(echo ${names} | awk -F"." '{print $1,$2,$3,$4}')
	set -- $var
	export RUN=$2
	read_dom () {
	    	local IFS=\>
	    	read -d \< ENTITY CONTENT
	}
		
	while read_dom; do
	    	if [[ $ENTITY = "Run" ]] ; then
			if [[ "${RUN}" == *"${CONTENT}"* ]] 
			then 
				dq2-ls data15_13TeV.*${RUN}*DAOD_JETM3*_p2425/		
			fi
		fi
	done < ${GRL}
done

#dq2-ls mc15_13TeV.3615*MadGraphPythia8EvtGen_A14NNPDF23LO_*_Np*merge.DAOD_JETM3*p2425/ | sort -n > grid_samples_mc_madgraph.txt
dq2-ls mc15_13TeV.*JETM3*_p2425/ | sort -n > grid_samples_mc.txt

############
# get Grid jobs
############

#dq2-ls user.${USER}.*_StreamAOD/ | sort -n > grid_streamAOD.txt
#dq2-ls user.${USER}.*_histo_EvWeights/ | sort -n > grid_histo_EvWeights.txt
#dq2-ls user.${USER}.*_MetHistoStream/ > grid_histo_MetHistoStream.txt

This directory contains lists of grid samples. Each list is made up of either
data, or MC selected for a particular final state.
A quick run-through of derivations used:
JETM3: dilepton. First choice for Zll studies, but some samples may not be present.
JETM7: ttbar selection. First choice for ttbar, but very few backgrounds available.
SUSY5: quite loose single-lepton selection. Appropriate for ttbar (or for Zll, but probably tighter derivations available).
SUSY2: 2l filter.


#! /bin/bash

export comm=()
for i in "$@"
do
        comm+=("${i}")
done
for comma in "${comm[@]}"
do
        if [[ "${comma}" == *".txt"* ]]
        then
                export INPUT=${comma}
        fi
        if [[ "${comma}" == *".py"* ]]
        then
                export JO=${comma}
        fi
        if [[ "${comma}" == *"Zee"* ]]
        then
                export EVT=${comma}
        fi
        if [[ "${comma}" == *"Zmm"* ]]
        then
                export EVT=${comma}
        fi
        if [[ "${comma}" == *"Wmv"* ]]
        then
                export EVT=${comma}
        fi
        if [[ "${comma}" == *"Wev"* ]]
        then
                export EVT=${comma}
        fi
        if [[ "${comma}" == *"ttbar"* ]]
        then
                export EVT=${comma}
        fi
        if [[ "${comma}" == *"plots_True"* ]]
        then
                export plots=True
        fi
        if [[ "${comma}" == *"plots_False"* ]]
        then
                export plots=False
        fi
        if [[ "${comma}" == *".xml"* ]]
        then
                export GRL=${comma}
        fi
        if [[ "${comma}" == *"prw"* ]]
        then
                export PRW=${comma}
        fi
        if [[ "${comma}" == *"ilumicalc"* ]]
        then
                export LUMI=${comma}
        fi

done
export REL=2.3.30
export DATE=$(date +"%b%d")
export MAKER=METPerformance
export ver=1.1
export EXC=ANALY_MPPMU,ANALY_NSC,ANALY_SiGNET,
#export EXC=ANALY_MPPMU,ANALY_NSC,ANALY_SiGNET,ANALY_FZK,ANALY_FZK_SHORT,ANALY_FZK_GLEXEC
#export EXC=ANALY_MPPMU,ANALY_NSC,ANALY_SiGNET,ANALY_BNL_LONG,ANALY_BNL_SHORT,ANALY_BNL_CLOUD,
#cd $WORKDIR
if [ ! -f ${INPUT} ]; then
    echo "File not found!"
    exit /b
fi
##################
###setupATLAS
##################
source /cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase/user/atlasLocalSetup.sh
##############
###setup Grid
##############
#localSetupDQ2Client --skipConfirm
#localSetupPandaClient --noAthenaCheck
#voms-proxy-init -voms atlas -valid 96:00:00
#export DQ2_LOCAL_SITE_ID=UKI-SOUTHGRID-OX-HEP_SCRATCHDISK #enter your DQ2_LOCAL_SITE_ID
#################
###setup Athena
#################
source /cvmfs/atlas.cern.ch/repo/sw/software/AthAnalysisBase/x86_64-slc6-gcc48-opt/${REL}/AtlasSetup/scripts/asetup.sh ${REL},AthAnalysisBase,here,gcc48
export inDS=($(< ${INPUT}))

for names in "${inDS[@]}"
do
	export var=$(echo ${names} | awk -F"." '{print $1,$2,$3,$4,$5,$6,$7}')
	set -- $var
	export MS=$1
	export RUN=$2
	export DS=$3
	export DER=$5
	#export MS=$3
	#export RUN=$4
	#export DS=$6
	#export DER=$7
	if [[ "${names}" =~ ^# ]] 
	then 
		continue
	fi
	if [[ "${names}" == *"data15_13TeV"* ]] 
	then 
		export ismc=False
		pathena -c "PRWFile='./${PRW}';GRLFile='./${GRL}';LUMIFile='./${LUMI}';plots=${plots};isMC=${ismc};EvtType='${EVT}'" ${MAKER}/${JO} --inDS ${names} --outDS user.${USER}.${MS}.${RUN}.${DS}.${DER}_${DATE}_${REL}.v${ver} --nGBPerJob=10 --excludedSite=${EXC} --extFile=./${GRL},./${PRW},./${LUMI} 			
	fi
	if [[ "${names}" == *"mc15_13TeV"* ]] 
	then 
		export ismc=True
		pathena -c "PRWFile='./${PRW}';GRLFile='./${GRL}';LUMIFile='./${LUMI}';plots=${plots};isMC=${ismc};EvtType='${EVT}'" ${MAKER}/${JO} --inDS ${names} --outDS user.${USER}.${MS}.${RUN}.${DS}.${DER}_${DATE}_${REL}.v${ver} --nGBPerJob=10 --excludedSite=${EXC} --extFile=./${GRL},./${PRW},./${LUMI} 			
	fi

	sleep 1

done


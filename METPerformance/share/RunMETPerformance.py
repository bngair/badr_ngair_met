# *************************
# Application configuration
# *************************

### example commands, running locally:
# athena.py -c "SystematicTerm='MET_SoftCalo_ScaleUp';EvtMax=10" METPerformance/MakeMETPerfSlims.py

from METPerformance import utils

# Default options now defined in python/DefaultOptions.py. Please, modify this
# file for your personal options :)
options=locals()
utils.setupDefaultOptions(options)

# This uses POOL::TEvent and is much faster
# than ReadAthenaPool
import AthenaRootComps.ReadAthenaxAODHybrid
svcMgr.ProxyProviderSvc.ProviderNames += [ "MetaDataSvc" ]
#import AthenaPoolCnvSvc.ReadAthenaPool
#import AthenaPoolCnvSvc.ReadAthenaPool
from AthenaCommon.AthenaCommonFlags import athenaCommonFlags

from glob import glob

### to run over multiple files in directory
#filelist = []
#for name in glob(inputdir+'/*.root*'):
#  filelist += [name]

### You must make sure filelist points to an actual file locally before submitting to the grid, it is important as it checks the macro locally and will throw exception at line 57
#filelist = ['/eos/atlas/atlascerngroupdisk/penn-ww/MET/mc16_13TeV.361107.PowhegPythia8EvtGen_AZNLOCTEQ6L1_Zmumu.deriv.DAOD_JETM3.e3601_s3126_r9781_r9778_p3371/DAOD_JETM3.12648304._000897.pool.root.1']
#ServiceMgr.EventSelector.InputCollections = filelist

from AthenaCommon.AlgSequence import AlgSequence
topSequence = AlgSequence()

verbosity = INFO
if VeryVerbose or Verbose:
	verbosity = VERBOSE
if VeryVerbose: ServiceMgr.MessageSvc.OutputLevel=verbosity

#####################################################################
# Configure GRL and LumiCalc files based on year
#####################################################################

# #if using GRL and LUMI from data directory, use the syntax METPerformance/[filename]
# #If taking GRL and LUMI from GroupData
# # http://atlas.web.cern.ch/Atlas/GROUPS/DATABASE/GroupData/
if Year =='2016':
    options.setdefault('GRLFiles',['GoodRunsLists/data16_13TeV/20180129/data16_13TeV.periodAllYear_DetStatus-v89-pro21-01_DQDefects-00-02-04_PHYS_StandardGRL_All_Good_25ns.xml'])
    options.setdefault('LUMIFiles',['GoodRunsLists/data16_13TeV/20180129/PHYS_StandardGRL_All_Good_25ns_297730-311481_OflLumi-13TeV-009.root'])
    options.setdefault('PRWFiles',['/cvmfs/atlas.cern.ch/repo/sw/database/GroupData/dev/SUSYTools/merged_prw_mc16a_Aug08.root'])

elif Year =='2015':
    options.setdefault('GRLFiles',['GoodRunsLists/data15_13TeV/20170619/data15_13TeV.periodAllYear_DetStatus-v89-pro21-02_Unknown_PHYS_StandardGRL_All_Good_25ns.xml'])
    options.setdefault('LUMIFiles',['GoodRunsLists/data15_13TeV/20170619/PHYS_StandardGRL_All_Good_25ns_276262-284484_OflLumi-13TeV-008.root'])
    options.setdefault('PRWFiles',['/cvmfs/atlas.cern.ch/repo/sw/database/GroupData/dev/SUSYTools/merged_prw_mc16a_Aug08.root'])

elif Year =='2015+2016':
    options.setdefault('GRLFiles',['GoodRunsLists/data15_13TeV/20170619/data15_13TeV.periodAllYear_DetStatus-v89-pro21-02_Unknown_PHYS_StandardGRL_All_Good_25ns.xml', 'GoodRunsLists/data16_13TeV/20180129/data16_13TeV.periodAllYear_DetStatus-v89-pro21-01_DQDefects-00-02-04_PHYS_StandardGRL_All_Good_25ns.xml'])
    options.setdefault('LUMIFiles',['GoodRunsLists/data15_13TeV/20170619/PHYS_StandardGRL_All_Good_25ns_276262-284484_OflLumi-13TeV-008.root', 'GoodRunsLists/data16_13TeV/20180129/PHYS_StandardGRL_All_Good_25ns_297730-311481_OflLumi-13TeV-009.root'])
    options.setdefault('PRWFiles',['/cvmfs/atlas.cern.ch/repo/sw/database/GroupData/dev/SUSYTools/merged_prw_mc16a_Aug08.root'])

elif Year =='2017':
    options.setdefault('GRLFiles',['GoodRunsLists/data17_13TeV/20180619/data17_13TeV.periodAllYear_DetStatus-v99-pro22-01_Unknown_PHYS_StandardGRL_All_Good_25ns_Triggerno17e33prim.xml'])
    options.setdefault('LUMIFiles',['GoodRunsLists/data17_13TeV/20180619/physics_25ns_Triggerno17e33prim.lumicalc.OflLumi-13TeV-010.root'])
    options.setdefault('PRWFiles',['GoodRunsLists/data17_13TeV/20180619/physics_25ns_Triggerno17e33prim.actualMu.OflLumi-13TeV-010.root'])

elif Year =='2018': 
    options.setdefault('GRLFiles',['GoodRunsLists/data18_13TeV/20180906/data18_13TeV.periodAllYear_DetStatus-v102-pro22-03_Unknown_PHYS_StandardGRL_All_Good_25ns_Triggerno17e33prim.xml'])
    options.setdefault('LUMIFiles',['GoodRunsLists/data18_13TeV/20180906/physics_25ns_Triggerno17e33prim.lumicalc.OflLumi-13TeV-001.root'])
    options.setdefault('PRWFiles',['GoodRunsLists/data18_13TeV/20180906/physics_25ns_Triggerno17e33prim.actualMu.OflLumi-13TeV-001.root'])
#####################################################################
# Adjust settings for MC or data
#####################################################################
from PyUtils import AthFile
if runOnGrid:
  # TODO: make these more intelligent
  isMC = not overrideIsData
  isAF2 = False
else:
  af = AthFile.fopen(svcMgr.EventSelector.InputCollections[0])
  isMC = 'IS_SIMULATION' in af.fileinfos['evt_type']
  isAF2 = isMC and af.fileinfos['metadata']['/Simulation/Parameters']['SimulationFlavour']!='default'

from METPerformance import TriggerDef
singleElectronTriggers = TriggerDef.trigger_def[Year]['singleElectronTriggers']
singleMuonTriggers     = TriggerDef.trigger_def[Year]['singleMuonTriggers']
singleLeptonTriggers   = TriggerDef.trigger_def[Year]['singleLeptonTriggers']
dijetTriggers   = TriggerDef.trigger_def[Year]['dijetTriggers']
gjetTriggers   = TriggerDef.trigger_def[Year]['gjetTriggers']

if   EvtType == 'Zmm':   triggers    = singleMuonTriggers
elif EvtType == 'Wmv':   triggers    = singleMuonTriggers
elif EvtType == 'Wev':   triggers    = singleElectronTriggers
elif EvtType == 'Zee':   triggers    = singleElectronTriggers
elif EvtType == 'dijet': triggers    = dijetTriggers
elif EvtType == 'gjet':  triggers    = gjetTriggers
else : triggers                    = singleLeptonTriggers

# Needed for trigger SFs in METPerfAlg and METSlim_AnAlg:
# No SF for dijet selection provided
triggerSF_to_apply = ''
if triggers == singleMuonTriggers:
  triggerSF_to_apply = 'Muon'
elif triggers == singleElectronTriggers:
  triggerSF_to_apply = 'Electron'
elif triggers == singleLeptonTriggers:
  triggerSF_to_apply = 'Muon'

############################

# Set up the preparation of the event
# Retrieve objects via SUSYTools
from METPerformance.METPerfSlimConfig import setupSlimAlg
# This needs to switch over to passing a single options construct
slimAlg = setupSlimAlg(JetSelection,JetColl,JetMinWeightedPt, JetMinEFrac,
                       doCST, doTST, doTrack, SystematicTerm, doEleJetOlapScan,
                       doPileupReweighting, doTriggerMatching,RequireTrigger, PRWFiles, LUMIFiles,
                       doGoodRunCleaning, GRLFiles, doCutBK,
                       EvtType, singleElectronTriggers, singleMuonTriggers, gjetTriggers,
                       doTruthMaps, isMC, Year, fJVTWP,doPFlowJVTScan, doJVTScan, JetEtaMax,CustomJetEtaForw,
		       fJVTCenPt,metCut,UseGhostMuons,
                       verbosity
                       )
topSequence += slimAlg

# If you set 'plots' to True, you will have as output only the histograms
# (otherwise put out by MakeMETPerfPlots.py)
# If you set 'plots' to False, output is to mini-xAOD. Use MakeMETPerfPlots.py
# to generate performance plots from this.

if trees:
  from METPerformance.METPerfTreeConfig import setupTreeAlgs
  setupTreeAlgs(topSequence,doTST,doCST,EvtType,
                verbosity,doPileupReweighting)

if plots:
  from METPerformance.METPerfPlotConfig import setupHistogramAlgs
  setupHistogramAlgs(topSequence,EvtType,JetColl,
                     doCST,doTST,doTrack, 
                     SystematicTerm,
                     doTruthMaps,doMuonElossPlots,doTracksPlot,
                     doPileupReweighting, doEleJetOlapScan,doFwdJetVeto,FwdJetPtCut,
                     doPFlowJVTScan,doJVTScan,
                     verbosity)

if xaodslim:
  from METPerformance.METPerfSlimConfig import setupOutputStreamXAOD
  setupOutputStreamXAOD(JetColl,SystematicTerm,
                        writeMaps,writeTracks)

if doPerfMon:
  from PerfMonComps.PerfMonFlags import jobproperties as pmon_properties
  pmon_properties.PerfMonFlags.doSemiDetailedMonitoring=True

if doValgrind:
  from Valkyrie.JobOptCfg import ValgrindSvc
  svcMgr += ValgrindSvc( OutputLevel = DEBUG,
                         ProfiledAlgs = ['METSlimming'],
                         ProfiledIntervals = ['METSlimming.execute'])

theApp.EvtMax = EvtMax
ServiceMgr.EventSelector.SkipEvents = 0
ServiceMgr.MessageSvc.defaultLimit = 10000 if (Verbose or VeryVerbose) else 100
svcMgr += CfgMgr.AthenaEventLoopMgr(EventPrintoutInterval=10 if (Verbose or VeryVerbose) else (int(float(EvtMax) / 100) if EvtMax>0 else 1000) )

#!/bin/bash
#Local run
#athena.py -c 'EvtType="None";EvtMax=-1' MakeMETPerfSlims.py 
#athena.py -c 'EvtType="ttbar";EvtMax=-1' MakeMETPerfSlims.py 
#athena.py -c 'EvtType="dijet";EvtMax=-1' MakeMETPerfSlims.py 
#athena.py -c 'EvtType="Wev";EvtMax=-1' MakeMETPerfSlims.py 
#athena.py -c 'EvtType="Wmv";EvtMax=-1' MakeMETPerfSlims.py 
#athena.py -c 'EvtType="Zmm";EvtMax=-1' MakeMETPerfSlims.py 
athena.py -c 'EvtType="Zee";EvtMax=-1' MakeMETPerfSlims.py 
#Run over grid
#nohup python ../python/gridjobSlim.py --user mpetrov --version v01 --eventType None  > grid_submission_None.txt &
#nohup python ../python/gridjobSlim.py --user mpetrov --version v01 --eventType ttbar  > grid_submission_ttbar.txt &
#nohup python ../python/gridjobSlim.py --user mpetrov --version v01 --eventType dijet  > grid_submission_dijet.txt &
#nohup python ../python/gridjobSlim.py --user mpetrov --version v01 --eventType Wmv  > grid_submission_Wmv.txt &
#nohup python ../python/gridjobSlim.py --user mpetrov --version v01 --eventType Wev  > grid_submission_Wev.txt &
#nohup python ../python/gridjobSlim.py --user mpetrov --version v01 --eventType Zmm  > grid_submission_Zmm.txt &
#nohup python ../python/gridjobSlim.py --user mpetrov --version v01 --eventType Zee  > grid_submission_Zee.txt &
#nohup python ../python/gridjobSlim.py --user mpetrov --version v01 --noTruthPtHard --eventType Wmv  > grid_submission_Wmv.txt &
#nohup python ../python/gridjobSlim.py --user mpetrov --version v01 --noTruthPtHard --eventType Wev  > grid_submission_Wev.txt &
#nohup python ../python/gridjobSlim.py --user mpetrov --version v01 --noTruthPtHard --eventType ttbar  > grid_submission_ttbar.txt &
#nohup python ../python/gridjobSlim.py --user mpetrov --version v01 --jetSelection Tight --eventType Zmm  > grid_submission_Zmm_tight.txt &
#for mc15b samples
#athena.py -c 'EvtType="Zmm";EvtMax=-1;PRWFile="METPerformance/data/mc15b.prw.ttbar.root"' MakeMETPerfSlims.py 

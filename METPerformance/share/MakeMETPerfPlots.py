# *************************
# Application configuration
# *************************

options=locals()
options.setdefault('EvtMax',50)
options.setdefault('EvtType','Zmm')
options.setdefault('doTruthMaps',False)
options.setdefault('Verbose', False)
options.setdefault('doPileupReweighting',True)
options.setdefault('Year',2016) #Flag to switch from GRL and Triggers for 2015 and 2016
options.setdefault('doMuonElossPlots',False) # flag for plots of muon eloss
options.setdefault('inputdir','/atlas/data1/userdata/valentem/PFlow/SlimxAODs/2016-09-26/user.valentem.361107.PFlowOldCalib.SlimxAOD.e3601_s2576_s2132_r7725_r7676_p2666_2016-09-26_StreamAOD/') # flag for plots of muon eloss

import AthenaPoolCnvSvc.ReadAthenaPool
from AthenaCommon.AthenaCommonFlags import athenaCommonFlags
from AthenaCommon.AppMgr import ServiceMgr, ToolSvc
from AthenaCommon import CfgMgr

from glob import glob
filelist=[]
if inputdir != '':
    if inputdir[-1] != '/': inputdir += '/'
    filelist = glob(inputdir+'*.root*')
else:
    print 'Please, specify input directory with \'inputdir\' option'
ServiceMgr.EventSelector.InputCollections = filelist

streamName = 'MetHistoStream'
fileName   = 'metPerfxAOD_test.'+EvtType+'.root'

from AthenaCommon.AlgSequence import AlgSequence
topSequence = AlgSequence()

verbosity = INFO
if Verbose:
	verbosity = VERBOSE
ServiceMgr.MessageSvc.OutputLevel=verbosity

from PyUtils import AthFile
af = AthFile.fopen(svcMgr.EventSelector.InputCollections[0])
isMC = 'IS_SIMULATION' in af.fileinfos['evt_type']
isAF2 = isMC and af.fileinfos['metadata']['/Simulation/Parameters']['SimulationFlavour']!='default'


from METPerformance import TriggerDef
singleElectronTriggers = TriggerDef.trigger_def[Year]['singleElectronTriggers']
singleMuonTriggers     = TriggerDef.trigger_def[Year]['singleMuonTriggers']
singleLeptonTriggers   = TriggerDef.trigger_def[Year]['singleLeptonTriggers']
dijetTriggers          = TriggerDef.trigger_def[Year]['dijetTriggers']

if   EvtType == 'Zmm': triggers    = singleMuonTriggers
elif EvtType == 'Wmv': triggers    = singleMuonTriggers
elif EvtType == 'Wev': triggers    = singleElectronTriggers
elif EvtType == 'Zee': triggers    = singleElectronTriggers
elif EvtType == 'dijet': triggers  = dijetTriggers
else : triggers                    = singleLeptonTriggers

# Needed for trigger SFs in METPerfAlg and METSlim_AnAlg:
# No SF for dijet selection provided
triggerSF_to_apply = ''
if triggers == singleMuonTriggers:
  triggerSF_to_apply = 'Muon'
elif triggers == singleElectronTriggers:
  triggerSF_to_apply = 'Electron'
elif triggers == singleLeptonTriggers:
  triggerSF_to_apply = 'Muon'


######################################################################
# Configure METPerfTool and METPerfAlg

evtTypeEnum = {
    'None':0,
    'Zmm':1,
    'Zee':2,
    'Wmv':3,
    'Wev':4,
    'ttbar':5,
    }

metPerfTST = CfgMgr.met__METPerfAlg('METPerfTST',
                                    Suffix='RefTST',
                                    JetTerm='RefJet',
                                    SoftTerm='PVSoftTrk' if not doTruthMaps else 'SoftTruthChargedCentral',
                                    RootStreamName=streamName,
                                    RootDirName='/'+EvtType,
                                    EvtType=evtTypeEnum[EvtType],
                                    JetBins=True,
                                    DoTauGammaPlots = True,
                                    DoMuonElossPlots = doMuonElossPlots,
                                    #WriteTree=True,
                                    OutputLevel=verbosity,
                                    DoPRW=doPileupReweighting
                                    )

metPerfCST = CfgMgr.met__METPerfAlg('METPerfCST',
                                    Suffix='RefCST',
                                    JetTerm='RefJet',
                                    SoftTerm='SoftClus' if not doTruthMaps else 'SoftTruthAll',
                                    RootStreamName=streamName,
                                    RootDirName='/'+EvtType,
                                    EvtType=evtTypeEnum[EvtType],
                                    JetBins=True,
                                    DoTauGammaPlots = True,
                                    DoMuonElossPlots = doMuonElossPlots,
                                    #WriteTree=True,
                                    OutputLevel=verbosity,
                                    DoPRW=doPileupReweighting
                                    )

# metPerfTrk = CfgMgr.met__METPerfAlg('METPerfTrack',
#                             	    Suffix='RefTrk',
#                             	    JetTerm='RefJet',
#                             	    SoftTerm='PVSoftTrk',
#                             	    RootStreamName=streamName,
#                             	    RootDirName='/'+EvtType,
#                             	    EvtType=evtTypeEnum[EvtType],
#                             	    JetBins=True,
#                             	    DoTauGammaPlots = False,
#                             	    #WriteTree=True,
#                                   OutputLevel=verbosity,
#                             	    )

topSequence += metPerfTST
topSequence += metPerfCST
#topSequence += metPerfTrk

######################################################################
# jet veto plots

metPerfTST_0j = CfgMgr.met__METPerfAlg('METPerfTST_0j',
                                       Suffix='RefTST',
                                       JetTerm='RefJet',
                                       SoftTerm='PVSoftTrk' if not doTruthMaps else 'SoftTruthChargedCentral',
                                       RootStreamName=streamName,
                                       RootDirName='/'+EvtType+'_0j',
                                       EvtType=evtTypeEnum[EvtType],
                                       DoTauGammaPlots = True,
                                       DoMuonElossPlots = doMuonElossPlots,
                                       DoJetVeto= True,
                                       OutputLevel=verbosity,
                                       DoPRW=doPileupReweighting
                                       )

metPerfCST_0j = CfgMgr.met__METPerfAlg('METPerfCST_0j',
                                       Suffix='RefCST',
                                       JetTerm='RefJet',
                                       SoftTerm='SoftClus' if not doTruthMaps else 'SoftTruthAll',
                                       RootStreamName=streamName,
                                       RootDirName='/'+EvtType+'_0j',
                                       EvtType=evtTypeEnum[EvtType],
                                       DoTauGammaPlots = True,
                                       DoMuonElossPlots = doMuonElossPlots,
                                       DoJetVeto= True,
                                       OutputLevel=verbosity,
                                       DoPRW=doPileupReweighting
                                       )

# metPerfTrk_0j = CfgMgr.met__METPerfAlg('METPerfTrack_0j',
#                                	       Suffix='RefTrk',
#                                	       JetTerm='RefJet',
#                                	       SoftTerm='PVSoftTrk',
#                                	       RootStreamName=streamName,
#                                	       RootDirName='/'+EvtType+'_0j',
#                                	       EvtType=evtTypeEnum[EvtType],
#                                	       DoTauGammaPlots = False,
#                                	       DoJetVeto= True,
#                                              OutputLevel=verbosity,
#                               	       )

topSequence += metPerfTST_0j
topSequence += metPerfCST_0j
#topSequence += metPerfTrk_0j

#from Valkyrie.JobOptCfg import ValgrindSvc
#svcMgr += ValgrindSvc( OutputLevel = DEBUG,
#                       ProfiledAlgs = ["METPerfTST"],
#                       ProfiledIntervals = ["METPerfTST.execute"])
#
#from PerfMonComps.PerfMonFlags import jobproperties as pmon_properties
#pmon_properties.PerfMonFlags.doSemiDetailedMonitoring=True

from OutputStreamAthenaPool.MultipleStreamManager import MSMgr
MyFirstHistoXAODStream = MSMgr.NewRootStream( streamName, fileName )

theApp.EvtMax = EvtMax
ServiceMgr.EventSelector.SkipEvents = 0
ServiceMgr.MessageSvc.defaultLimit = 100

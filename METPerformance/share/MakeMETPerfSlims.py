 # *************************
# Application configuration
# *************************

### example commands, running locally:
# athena.py -c "SystematicTerm='MET_SoftCalo_ScaleUp';EvtMax=10" METPerformance/MakeMETPerfSlims.py

from METPerformance import utils

# Default options now defined in python/DefaultOptions.py. Please, modify this
# file for your personal options :)
options=locals()
utils.setupDefaultOptions(options)

# #if using GRL and LUMI from data directory, use the syntax METPerformance/[filename]
# #If taking GRL and LUMI from GroupData
# # http://atlas.web.cern.ch/Atlas/GROUPS/DATABASE/GroupData/
if Year =='2016':
    options.setdefault('GRLFile','GoodRunsLists/data16_13TeV/20161101/data16_13TeV.periodAllYear_DetStatus-v83-pro20-15_DQDefects-00-02-04_PHYS_StandardGRL_All_Good_25ns.xml')
    options.setdefault('LUMIFile','GoodRunsLists/data16_13TeV/20161101/physics_25ns_20.7.lumicalc.OflLumi-13TeV-005.root')
elif Year =='2015':
    options.setdefault('GRLFile','GoodRunsLists/data15_13TeV/20160720/data15_13TeV.periodAllYear_DetStatus-v79-repro20-02_DQDefects-00-02-02_PHYS_StandardGRL_All_Good_25ns.xml')
    options.setdefault('LUMIFile','GoodRunsLists/data15_13TeV/20160720/physics_25ns_20.7.lumicalc.OflLumi-13TeV-005.root')
elif Year =='2015+2016':
    options.setdefault('GRLFile','GoodRunsLists/data15_13TeV/20160720/data15_13TeV.periodAllYear_DetStatus-v79-repro20-02_DQDefects-00-02-02_PHYS_StandardGRL_All_Good_25ns.xml,GoodRunsLists/data16_13TeV/20161101/data16_13TeV.periodAllYear_DetStatus-v83-pro20-15_DQDefects-00-02-04_PHYS_StandardGRL_All_Good_25ns.xml','METPerformance/data17_13TeV.periodAllYear_DetStatus-v92-pro21-05_Unknown_PHYS_StandardGRL_All_Good_25ns_Triggerno17e33prim.xml')
    options.setdefault('LUMIFile','GoodRunsLists/data15_13TeV/20160720/physics_25ns_20.7.lumicalc.OflLumi-13TeV-005.root,GoodRunsLists/data16_13TeV/20161101/physics_25ns_20.7.lumicalc.OflLumi-13TeV-005.root','METPerformance/physics_25ns_Triggerno17e33prim.lumicalc.OflLumi-13TeV-001.root')

# This uses POOL::TEvent and is much faster
# than ReadAthenaPool
import AthenaRootComps.ReadAthenaxAODHybrid
svcMgr.ProxyProviderSvc.ProviderNames += [ "MetaDataSvc" ]
#import AthenaPoolCnvSvc.ReadAthenaPool
from AthenaCommon.AthenaCommonFlags import athenaCommonFlags
from AthenaCommon.AppMgr import ServiceMgr, ToolSvc
from AthenaCommon import CfgMgr

from glob import glob

### to run over multiple files in directory
filelist = []
inputdir = utils.AddLastSlash(inputdir)
for name in glob(inputdir+'*.root*'):
  filelist += [name]

### You must make sure filelist points to an actual file locally before submitting to the grid, it is important as it checks the macro locally and will throw exception at line 57
#filelist = ['AOD.pool.root']
ServiceMgr.EventSelector.InputCollections = filelist

from AthenaCommon.AlgSequence import AlgSequence
topSequence = AlgSequence()

verbosity = INFO
if VeryVerbose or Verbose:
	verbosity = VERBOSE
if VeryVerbose: ServiceMgr.MessageSvc.OutputLevel=verbosity

from PyUtils import AthFile
af = AthFile.fopen(svcMgr.EventSelector.InputCollections[0])
isMC = 'IS_SIMULATION' in af.fileinfos['evt_type']
isAF2 = isMC and af.fileinfos['metadata']['/Simulation/Parameters']['SimulationFlavour']!='default'

#####################################################################
# Adjust settings for MC or data
#####################################################################
#doPFlow  = JetColl == 'EMPFlow'

doPFlow=False
if 'PFlow' in JetSelection or 'PFlow' in JetColl:
  doPFlow      = True
  JetColl      = 'EMPFlow'
  JetSelection = 'PFlow'

jetColl    = 'STCalibAntiKt4'+JetColl+'Jets'
muonColl   = 'STCalibMuons'
eleColl    = 'STCalibElectrons'
tauColl    = 'STCalibTauJets'
photonColl = 'STCalibPhotons'

if SystematicTerm != 'Nominal':
    jetColl    += SystematicTerm
    muonColl   += SystematicTerm
    eleColl    += SystematicTerm
    tauColl    += SystematicTerm
    photonColl += SystematicTerm

year = Year

from METPerformance import TriggerDef
singleElectronTriggers = TriggerDef.trigger_def[year]['singleElectronTriggers']
singleMuonTriggers     = TriggerDef.trigger_def[year]['singleMuonTriggers']
singleLeptonTriggers   = TriggerDef.trigger_def[year]['singleLeptonTriggers']
dijetTriggers          = TriggerDef.trigger_def[year]['dijetTriggers']

if   EvtType == 'Zmm': triggers    = singleMuonTriggers
elif EvtType == 'Wmv': triggers    = singleMuonTriggers
elif EvtType == 'Wev': triggers    = singleElectronTriggers
elif EvtType == 'Zee': triggers    = singleElectronTriggers
elif EvtType == 'dijet': triggers    = dijetTriggers
else : triggers                    = singleLeptonTriggers

# Needed for trigger SFs in METPerfAlg and METSlim_AnAlg:
# No SF for dijet selection provided
triggerSF_to_apply = ''
if triggers == singleMuonTriggers:
  triggerSF_to_apply = 'Muon'
elif triggers == singleElectronTriggers:
  triggerSF_to_apply = 'Electron'
elif triggers == singleLeptonTriggers:
  triggerSF_to_apply = 'Muon'

############################

# Set up SUSYTools. Largely auto-config from config file.

susytools = CfgMgr.ST__SUSYObjDef_xAOD("SUSYTools")
susytools.OutputLevel = verbosity

fjvtLoose = CfgMgr.JetForwardJvtTool("fjvtLoose",OutputDec="passFJVTLoose");
fjvtTight = CfgMgr.JetForwardJvtTool("fjvtTight",OutputDec="passFJVTTight",UseTightOP=True);
ToolSvc += fjvtLoose
ToolSvc += fjvtTight

GRLFiles=GRLFile.split(',')
PRWFiles=PRWFile.split(',')
PRWLumiCalcFiles=LUMIFile.split(',')

print 'GRL files:      ', GRLFiles
print 'PRW files:      ', PRWFiles
print 'Lumicalc files: ', PRWLumiCalcFiles

if JetSelection == 'Default':
  if JetColl == 'EMTopo':
     susytools.ConfigFile= 'METPerformance/SUSYTools_METPerformance.EMTopo.conf'
  elif JetColl == 'LCTopo':
     susytools.ConfigFile= 'METPerformance/SUSYTools_METPerformance.LCTopo.conf'
elif JetSelection == 'Tight':
  susytools.ConfigFile= 'METPerformance/SUSYTools_METPerformance.Tight_JetSel.conf'
elif JetSelection == 'Loose':
  susytools.ConfigFile= 'METPerformance/SUSYTools_METPerformance.Loose_JetSel.conf'
elif 'PFlow' in JetSelection:
  susytools.ConfigFile = "METPerformance/SUSYTools_METPerformance.EMPFlow.conf"


susytools.DebugMode=VeryVerbose

if doPileupReweighting:
    #susytools.PRWDefaultChannel= 410000
    susytools.PRWConfigFiles=PRWFiles
    susytools.PRWLumiCalcFiles=PRWLumiCalcFiles
else:
    susytools.PRWConfigFiles=[]
    susytools.PRWLumiCalcFiles=[]

ToolSvc += susytools


############################
# Good Run List
grlTool= CfgMgr.GoodRunsListSelectionTool('GRLTool',
                                          GoodRunsListVec=GRLFiles,
                                          PassThrough=False,
                                          )
ToolSvc+=grlTool


######################################################################
# Configure object and event selection
# TileTrips currently broken?
#include ( "TileTripReader/AthTileTripReader_jobOptions.py" )


# METPerformance object selection

if 'PFlow' in jetColl: doJetCleaning = False

metObjSel = CfgMgr.met__METObjSel('METObjSel',
                                  JetColl=jetColl,
                                  MuonColl=muonColl,
                                  EleColl=eleColl,
                                  TauColl=tauColl,
                                  PhotonColl=photonColl,
                                  SUSYTools = susytools,
                                  OutputLevel=verbosity,
                                  doJetCleaning=doJetCleaning
                                  )
ToolSvc += metObjSel

evtTypeEnum = {
              'None':0,
              'Zmm':1,
              'Zee':2,
              'Wmv':3,
              'Wev':4,
              'ttbar':5,
              'dijet':6,
              }

metEvtSel = CfgMgr.met__METEvtSel('METEvtSel',
                                  SUSYTools = susytools,
                                  TriggersRequired=triggers,
                                  doTriggerDecision=RequireTrigger,
                                  OutputLevel=verbosity,
                                  doTriggerMatch=doTriggerMatching,
                                  doJetCleaning=doJetCleaning,
                                  EvtType=evtTypeEnum[EvtType],
                                  doEverySelection=DoEverySelection
                                  )
#metEvtSel.AthTileTripReader = ToolSvc.AthTileTripReader
ToolSvc += metEvtSel

######################################################################
# Configure METPerfTool and METPerfAlg

# setup for R20 MET Calculation
# order: electron, muon, jet, photon, tau, MET
containers_cst   = ['GoodElectrons', 'GoodMuons', jetColl, 'GoodPhotons', 'GoodTauJets', 'MET_RefCST']
containers_tst   = ['GoodElectrons', 'GoodMuons', jetColl, 'GoodPhotons', 'GoodTauJets', 'MET_RefTST']
containers_track = ['GoodElectrons', 'GoodMuons', jetColl, '', '', 'MET_RefTrk']

map_prefix = 'METAssoc_AntiKt4'
core_prefix = 'MET_Core_AntiKt4'
if doTruthMaps:
	map_prefix = 'METAssoc_Truth_AntiKt4'
	core_prefix = 'MET_Core_Truth_AntiKt4'
map_name=map_prefix+JetColl
core_name=core_prefix+JetColl
if buildaltAODmap:
        from METPerformance.METMap import *
        BuildMETCustomAssociationMap(topSequence)
        assoc_suffix="_d0lt2"
        map_name+=assoc_suffix
        core_name+=assoc_suffix
from METPerformance import METPerformanceJobHelper
csttool=METPerformanceJobHelper.getMETCalculator('CST', containers_cst,
                                                 map_name,
                                                 core_name,
                                                 SystematicTerm,
                                                 JetSelection = JetSelection,
                                                 DoMuonEloss=True,
                                                 DoIsolMuonEloss=True,
						 DoPFlow=doPFlow,
                                                 OutputLevel=verbosity,
                                                 )
csttool.DoJVT = False

fjvtcuts = {
           'None':'',
           'Loose':'passFJVTLoose',
           'Tight':'passFJVTTight',
           }

tsttool=METPerformanceJobHelper.getMETCalculator('TST', containers_tst,
                                                 map_name,
                                                 core_name,
                                                 SystematicTerm,
                                                 JetSelection = JetSelection,
                                                 FJVTName=fjvtcuts[fJVTWP],
						 DoPFlow=doPFlow,
                                                 OutputLevel=verbosity,
                                                 )
tsttool.DoJVT = True
if not applyPFlowJVT and doPFlow: tsttool.DoJVT = False
# trktool=METPerformanceJobHelper.getMETCalculator('Track', containers_track,
#                                                  map_name,
#                                                  core_name,
#                                                  JetSelection = JetSelection,
#                                                  SystematicTerm,
# 						 DoPFlow=doPFlow,
#                                                  OutputLevel=verbosity,
#                                                  )
# trktool.DoJVT = False if doPFlow else True

METCalculatorTools=[]
METCalculatorTools.append(csttool)
METCalculatorTools.append(tsttool)
# METCalculatorTools.append(trktool)

weightStreamName = "histo_EvWeight"
weightFileName = "weights.{0}.root".format(SystematicTerm)
from OutputStreamAthenaPool.MultipleStreamManager import MSMgr
MSMgr.NewRootStream( weightStreamName, weightFileName )

metPerfTool = CfgMgr.met__METPerfTool('METPerfTool',
                                      EleColl='Electrons',
                                      GammaColl='Photons',
                                      TauColl='TauJets',
                                      JetColl = 'AntiKt4'+JetColl+'Jets',
                                      SUSYTools = susytools,
                                      FJVTTool1 = fjvtLoose,
                                      FJVTTool2 = fjvtTight,
                                      SystematicTag=SystematicTerm,
                                      IsSimulation=isMC,
                                      OutputLevel=verbosity
                                      )
ToolSvc += metPerfTool

metSlim_AnAlg = CfgMgr.met__METSlimAlg_AnAlg('METSlimming',
                                             DoCalib=True,
                                             DoSelection=True,
                                             DoCutBK=doCutBK,
                                             DoPRW=doPileupReweighting, ##claire
                                             ObjSelTool=metObjSel,
                                             EvtSelTool=metEvtSel,
                                             PerfTool=metPerfTool,
                                             SUSYTools = susytools,
                                             METCalculators=METCalculatorTools,
                                             GRLTool=grlTool,
                                             DoGRL=doGoodRunCleaning, ##Matteo
                                             LowRN=lowRN,
                                             HighRN=highRN,
                                             IsSimulation = isMC, ## bool added to swich on the lines to get the sum of weight from metadata
                                             Triggers = triggers,
                                             TriggerScaleFactor = triggerSF_to_apply,
                                             OutputLevel=verbosity,
                                             is20152016=Year=='2015+2016',
                                             RootStreamName=weightStreamName,
                                             RootDirName='/'+weightStreamName
                                             )

topSequence += metSlim_AnAlg

# If you set 'plots' to True, you will have as output only the histograms
# (otherwise put out by MakeMETPerfPlots.py)
# If you set 'plots' to False, output is to mini-xAOD. Use MakeMETPerfPlots.py
# to generate performance plots from this.

if trees:

    streamName = 'MetHistoStream'
    fileName   = 'tree.metPerfDxAOD.'+EvtType+'.'+JetColl+'.%s.root'%(SystematicTerm)
    stream = MSMgr.NewRootStream( streamName, fileName )

    evtTypeEnum = {
        'None':0,
        'Zmm':1,
        'Zee':2,
        'Wmv':3,
        'Wev':4,
        'ttbar':5,
        'dijet':6,
        }
#GetDataScaleFactor()
    metPerfTST = CfgMgr.met__METTreeAlg('METPerfTST',
                                        Suffix='RefTST',
                                        JetTerm='RefJet',
                                        SoftTerm='PVSoftTrk' if not doTruthMaps else 'SoftTruthChargedCentral',
                                        RootStreamName=streamName,
                                        RootDirName='/'+EvtType,
                                        EvtType=evtTypeEnum[EvtType],
                                        JetBins=True,
                                        DoTauGammaPlots = True,
                                        DoMuonElossPlots = doMuonElossPlots,
                                        WriteTree=True,
                                        WriteHist=False,
                                        SysName=SystematicTerm,
                                        OutputLevel=verbosity,
                                        doPRW=doPileupReweighting
                                        )

    topSequence += metPerfTST

if plots:

    streamName = 'MetHistoStream'
    fileName   = 'metPerfDxAOD.'+EvtType+'.'+JetColl+'.'+SystematicTerm+'.root'
    stream = MSMgr.NewRootStream( streamName, fileName )


    evtTypeEnum = {
        'None':0,
        'Zmm':1,
        'Zee':2,
        'Wmv':3,
        'Wev':4,
        'ttbar':5,
        'dijet':6,
        }

    metPerfTST = CfgMgr.met__METPerfAlg('METPerfTST',
                                        Suffix='RefTST',
                                        JetTerm='RefJet',
                                        SoftTerm='PVSoftTrk' if not doTruthMaps else 'SoftTruthChargedCentral',
                                        RootStreamName=streamName,
                                        RootDirName='/{0}/incljet/{1}/'.format(EvtType,'RefTST'),
                                        EvtType=evtTypeEnum[EvtType],
                                        JetBins=True,
                                        DoTauGammaPlots = True,
                                        DoMuonElossPlots = doMuonElossPlots,
                                        #WriteTree=True,
                                        OutputLevel=verbosity,
                                        DoPRW=doPileupReweighting,
                                        DoPlotTracks=doTracksPlot,
                                        DoFwdJetVeto=doFwdJetVeto,
                                        )

    metPerfCST = CfgMgr.met__METPerfAlg('METPerfCST',
                                        Suffix='RefCST',
                                        JetTerm='RefJet',
                                        SoftTerm='SoftClus' if not doTruthMaps else 'SoftTruthAll',
                                        RootStreamName=streamName,
                                        RootDirName='/{0}/incljet/{1}/'.format(EvtType,'RefCST'),
                                        EvtType=evtTypeEnum[EvtType],
                                        JetBins=True,
                                        DoTauGammaPlots = True,
                                        DoMuonElossPlots = doMuonElossPlots,
                                        #WriteTree=True,
                                        OutputLevel=verbosity,
                                        DoPRW=doPileupReweighting,
                                        DoPlotTracks=doTracksPlot,
                                        DoFwdJetVeto=doFwdJetVeto,
                                        )

    # metPerfTrk = CfgMgr.met__METPerfAlg('METPerfTrack',
    #                             	    Suffix='RefTrk',
    #                             	    JetTerm='RefJet',
    #                             	    SoftTerm='PVSoftTrk',
    #                             	    RootStreamName=streamName,
    #                             	    RootDirName='/'+EvtType,
    #                             	    EvtType=evtTypeEnum[EvtType],
    #                             	    JetBins=True,
    #                             	    DoTauGammaPlots = False,
    #                             	    #WriteTree=True,
    #                                   OutputLevel=verbosity,
    #                             	    )

    topSequence += metPerfTST
    topSequence += metPerfCST
    # topSequence += metPerfTrk

    metPerfTST_0j = CfgMgr.met__METPerfAlg('METPerfTST_0j',
                                           Suffix='RefTST',
                                           JetTerm='RefJet',
                                           SoftTerm='PVSoftTrk' if not doTruthMaps else 'SoftTruthChargedCentral',
                                           RootStreamName=streamName,
                                           RootDirName='/{0}/0j/{1}/'.format(EvtType,'RefTST'),
                                           EvtType=evtTypeEnum[EvtType],
                                           DoTauGammaPlots = True,
                                           DoMuonElossPlots = doMuonElossPlots,
                                           DoJetVeto= True,
                                           OutputLevel=verbosity,
                                           DoPRW=doPileupReweighting,
                                           DoPlotTracks=doTracksPlot,
                                           DoFwdJetVeto=doFwdJetVeto,
                                           )

    metPerfCST_0j = CfgMgr.met__METPerfAlg('METPerfCST_0j',
                                           Suffix='RefCST',
                                           JetTerm='RefJet',
                                           SoftTerm='SoftClus' if not doTruthMaps else 'SoftTruthAll',
                                           RootStreamName=streamName,
                                           RootDirName='/{0}/0j/{1}/'.format(EvtType,'RefCST'),
                                           EvtType=evtTypeEnum[EvtType],
                                           DoTauGammaPlots = True,
                                           DoMuonElossPlots = doMuonElossPlots,
                                           DoJetVeto= True,
                                           OutputLevel=verbosity,
                                           DoPRW=doPileupReweighting,
                                           DoPlotTracks=doTracksPlot,
                                           DoFwdJetVeto=doFwdJetVeto,
                                           )

    # metPerfTrk_0j = CfgMgr.met__METPerfAlg('METPerfTrack_0j',
    #                                	       Suffix='RefTrk',
    #                                	       JetTerm='RefJet',
    #                                	       SoftTerm='PVSoftTrk',
    #                                	       RootStreamName=streamName,
    #                                	       RootDirName='/'+EvtType+'_0j',
    #                                	       EvtType=evtTypeEnum[EvtType],
    #                                	       DoTauGammaPlots = False,
    #                                	       DoJetVeto= True,
    #                                              OutputLevel=verbosity,
    #                                	       )

    topSequence += metPerfTST_0j
    topSequence += metPerfCST_0j
    # topSequence += metPerfTrk_0j

else:
    outputfile='xAOD.METslim.%s.pool.root'%(SystematicTerm)


    truth_jet_variables = ['pt', 'eta', 'phi', 'm']
   # jet_variables = ['pt', 'eta', 'phi', 'm', 'bjet', 'Jvt', 'bad', 'baseline', 'signal', 'passOR']
    lep_variables = ['pt', 'eta', 'phi', 'm', 'charge', 'baseline', 'signal', 'passOR']
    track_variables = ['d0', 'z0', 'vz', 'theta', 'definingParametersCovMatrix']

    from OutputStreamAthenaPool.MultipleStreamManager import MSMgr
    xaodStream = MSMgr.NewPoolRootStream( 'StreamAOD', outputfile )
    xaodStream.AddItem('xAOD::EventInfo#EventInfo')
    xaodStream.AddItem('xAOD::EventAuxInfo#EventInfoAux.')
    xaodStream.AddItem('xAOD::JetContainer#AntiKt4TruthJets')
    xaodStream.AddItem('xAOD::JetAuxContainer#AntiKt4TruthJetsAux.%s' % '.'.join(truth_jet_variables))
    xaodStream.AddItem('xAOD::JetContainer#GoodJets')
    xaodStream.AddItem('xAOD::JetAuxContainer#GoodJetsAux.%s' % '.'.join(jet_variables))
    #xaodStream.AddItem('xAOD::ElectronContainer#Electrons')
    #xaodStream.AddItem('xAOD::ElectronAuxContainer#ElectronsAux.')
    xaodStream.AddItem('xAOD::ElectronContainer#GoodElectrons')
    xaodStream.AddItem('xAOD::AuxContainerBase#GoodElectronsAux.%s' % '.'.join(lep_variables))
    #xaodStream.AddItem('xAOD::PhotonContainer#Photons')
    #xaodStream.AddItem('xAOD::PhotonAuxContainer#PhotonsAux.')
    xaodStream.AddItem('xAOD::PhotonContainer#GoodPhotons')
    xaodStream.AddItem('xAOD::AuxContainerBase#GoodPhotonsAux.')
    #xaodStream.AddItem('xAOD::TauJetContainer#TauJets')
    #xaodStream.AddItem('xAOD::TauJetAuxContainer#TauJetsAux.')
    xaodStream.AddItem('xAOD::TauJetContainer#GoodTauJets')
    xaodStream.AddItem('xAOD::AuxContainerBase#GoodTauJetsAux.')
    #xaodStream.AddItem('xAOD::MuonContainer#Muons')
    #xaodStream.AddItem('xAOD::MuonAuxContainer#MuonsAux.')
    xaodStream.AddItem('xAOD::MuonContainer#GoodMuons')
    xaodStream.AddItem('xAOD::AuxContainerBase#GoodMuonsAux.%s' % '.'.join(lep_variables))
    xaodStream.AddItem('xAOD::ElectronContainer#BestZee')
    xaodStream.AddItem('xAOD::AuxContainerBase#BestZeeAux.%s' % '.'.join(lep_variables))
    xaodStream.AddItem('xAOD::MuonContainer#BestZmm')
    xaodStream.AddItem('xAOD::AuxContainerBase#BestZmmAux.%s' % '.'.join(lep_variables))
    xaodStream.AddItem('xAOD::ElectronContainer#eleFromW')
    xaodStream.AddItem('xAOD::AuxContainerBase#eleFromWAux.%s' % '.'.join(lep_variables))
    xaodStream.AddItem('xAOD::MuonContainer#muonFromW')
    xaodStream.AddItem('xAOD::AuxContainerBase#muonFromWAux.%s' % '.'.join(lep_variables))
    xaodStream.AddItem('xAOD::MissingETContainer#MET_LocHadTopo')
    xaodStream.AddItem('xAOD::AuxContainerBase#MET_LocHadTopoAux.')
    xaodStream.AddItem('xAOD::MissingETContainer#MET_Truth')
    xaodStream.AddItem('xAOD::AuxContainerBase#MET_TruthAux.')
    xaodStream.AddItem('xAOD::MissingETContainer#MET_RefTST')
    xaodStream.AddItem('xAOD::MissingETAuxContainer#MET_RefTSTAux.')
    xaodStream.AddItem('xAOD::MissingETContainer#MET_Track')
    xaodStream.AddItem('xAOD::AuxContainerBase#MET_TrackAux.')
    xaodStream.AddItem('xAOD::MissingETContainer#MET_RefCST')
    xaodStream.AddItem('xAOD::MissingETAuxContainer#MET_RefCSTAux.')
    xaodStream.AddItem('xAOD::MissingETContainer#MET_RefTrk')
    xaodStream.AddItem('xAOD::MissingETAuxContainer#MET_RefTrkAux.')
    xaodStream.AddItem('xAOD::MissingETContainer#MET_Core_AntiKt4'+JetColl)
    xaodStream.AddItem('xAOD::MissingETAuxContainer#MET_Core_AntiKt4'+JetColl+'Aux.')

    if writeMaps:
      xaodStream.AddItem('xAOD::MissingETAssociationMap#*')
      xaodStream.AddItem('xAOD::MissingETAuxAssociationMap#*')

    #xaodStream.AddItem('xAOD::CaloClusterContainer#egammaClusters');
    #xaodStream.AddItem('xAOD::AuxContainerBase#egammaClustersAux.');
    #xaodStream.AddItem('xAOD::CaloClusterContainer#CaloCalTopoCluster');
    #xaodStream.AddItem('xAOD::AuxContainerBase#CaloCalTopoClusterAux.');
    #xaodStream.AddItem('xAOD::CaloClusterContainer#*');
    #xaodStream.AddItem('xAOD::CaloClusterAuxContainer#*');

    if writeTracks:
      xaodStream.AddItem('xAOD::TrackParticleContainer#CombinedMuonTrackParticles')
      xaodStream.AddItem('xAOD::AuxContainerBase#CombinedMuonTrackParticlesAux.%s' % '.'.join(track_variables))
      xaodStream.AddItem('xAOD::TrackParticleContainer#InDetTrackParticles')
      #xaodStream.AddItem('xAOD::TrackParticleAuxContainer#InDetTrackParticlesAux.')
      xaodStream.AddItem('xAOD::AuxContainerBase#InDetTrackParticlesAux.%s' % '.'.join(track_variables))
      xaodStream.AddItem('xAOD::TrackParticleContainer#MuonSpectrometerTrackParticles')
      xaodStream.AddItem('xAOD::AuxContainerBase#MuonSpectrometerTrackParticlesAux.%s' % '.'.join(track_variables))
    xaodStream.AddItem('xAOD::VertexContainer#PrimaryVertices')
    xaodStream.AddItem('xAOD::AuxContainerBase#PrimaryVerticesAux.')
    xaodStream.AcceptAlgs(['METSlimming'])

#from OutputStreamAthenaPool.MultipleStreamManager import MSMgr
#MyFirstHistoXAODStream = MSMgr.NewRootStream( streamName, fileName )

#from Valkyrie.JobOptCfg import ValgrindSvc
#svcMgr += ValgrindSvc( OutputLevel = DEBUG,
#                       ProfiledAlgs = ['METSlimming'],
#                       ProfiledIntervals = ['METSlimming.execute'])
#
#from PerfMonComps.PerfMonFlags import jobproperties as pmon_properties
#pmon_properties.PerfMonFlags.doSemiDetailedMonitoring=True

theApp.EvtMax = EvtMax
ServiceMgr.EventSelector.SkipEvents = 0
ServiceMgr.MessageSvc.defaultLimit = 10000 if (Verbose or VeryVerbose) else 100
svcMgr += CfgMgr.AthenaEventLoopMgr(EventPrintoutInterval=10 if (Verbose or VeryVerbose) else (int(float(EvtMax) / 100) if EvtMax>0 else 1000) )

# *************************
# Application configuration
# *************************

options=locals()
options.setdefault('EvtMax',50)

import AthenaPoolCnvSvc.ReadAthenaPool
from AthenaCommon.AthenaCommonFlags import athenaCommonFlags
from AthenaCommon.AppMgr import ServiceMgr, ToolSvc
from AthenaCommon import CfgMgr

from glob import glob
filelist = ['/r03/atlas/brunt/METtesting/mc15_13TeV.410000.PowhegPythiaEvtGen_P2012_ttbar_hdamp172p5_nonallhad.merge.DAOD_JETM7.e3698_s2608_s2183_r6765_r6282_p2425/DAOD_JETM7.06575657._000026.pool.root.1']

ServiceMgr.EventSelector.InputCollections = filelist

streamName = 'TreeStream'
fileName   = 'check_for_duplicates.root'

from AthenaCommon.AlgSequence import AlgSequence
topSequence = AlgSequence()

check_dup_alg = CfgMgr.CheckForDuplicatesAlg('CheckForDuplicatesAlg',
                                             StreamName=streamName,
                                            )
topSequence += check_dup_alg

svcMgr += CfgMgr.THistSvc()
svcMgr.THistSvc.Output += [' '.join([streamName, 'DATAFILE='+"'"+fileName+"'", "OPT='RECREATE'"])]

theApp.EvtMax = EvtMax
#ServiceMgr.EventSelector.SkipEvents = 0

///////////////////////// -*- C++ -*- /////////////////////////////
// METCalculator.cxx
// Implementation file for class METCalculator
// Author: T.J.Khoo<khoo@cern.ch>
///////////////////////////////////////////////////////////////////

// METObjSel includes
#include "METPerformance/METCalculator.h"
#include "METPerformance/METPerfHelper.h"
badr
// EDM includes
#include "xAODCore/ShallowCopy.h"
#include "xAODJet/JetContainer.h"
#include "xAODMuon/MuonContainer.h"
#include "xAODMuon/MuonAuxContainer.h"
#include "xAODEgamma/ElectronContainer.h"
#include "xAODEgamma/PhotonContainer.h"
#include "xAODTau/TauJetContainer.h"
#include "xAODMissingET/MissingETContainer.h"
#include "xAODMissingET/MissingETAuxContainer.h"
#include "xAODMissingET/MissingETComponentMap.h"
#include "xAODMissingET/MissingETAssociationMap.h"

#include "PATInterfaces/ISystematicsTool.h"
#include "PATInterfaces/SystematicCode.h"
#include "PATInterfaces/SystematicSet.h"
#include "PATInterfaces/SystematicVariation.h"

#include "EventPrimitives/EventPrimitivesHelpers.h"
#include "FourMomUtils/xAODP4Helpers.h"

namespace met {


  static const SG::AuxElement::Decorator< std::vector<float> > dec_constitObjWeights("ConstitObjectWeights");
  static const SG::AuxElement::Decorator< float > dec_jetDescription("METWeight");

  using std::vector;
  using namespace xAOD;

  ///////////////////////////////////////////////////////////////////
  // Public methods:
  ///////////////////////////////////////////////////////////////////

  // Constructors
  ////////////////
  METCalculator::METCalculator(const std::string& name) :
    AsgTool(name)
  {
    //
    // Property declaration
    //
    declareProperty( "METMakerTool",         m_metMakerTool );
    declareProperty( "METSystematicTool",    m_metSystTool);
    declareProperty( "METSystematicTerm", m_metsysTerm);
    declareProperty( "ElectronContainerName",  m_elContName="");
    declareProperty( "JetContainerName",  m_jetContName="");
    declareProperty( "MuonContainerName",  m_muContName="");
    declareProperty( "PhotonContainerName",  m_phContName="");
    declareProperty( "TauContainerName",  m_tauContName="");
    declareProperty( "METContainerName",  m_metContName="");
    declareProperty( "METMapName",  m_metMapName);
    declareProperty( "METCoreName",  m_metCoreName);
    declareProperty( "DoJVT" ,        m_doJVT=false);
    declareProperty( "DoTruth" ,      m_doTruth=false);
    declareProperty( "METType" ,   m_mettype="CST"); // CST, TST, Track
    declareProperty( "doEleJetOlapScan" ,   m_doEleJetOlapScan=false);
    declareProperty( "doPFlowJVTScan" ,  m_doPFlowJVTScan=false); 
    declareProperty( "doJVTScan" ,       m_doJVTScan=false); 
  }

  // Destructor
  ///////////////
  METCalculator::~METCalculator()
  {}

  // Athena algtool's Hooks
  ////////////////////////////
  StatusCode METCalculator::initialize()
  {
    ATH_MSG_INFO ("Initializing " << name() << "...");

    ATH_CHECK(m_metMakerTool.retrieve());
    ATH_CHECK(m_metSystTool.retrieve());

    if(m_mettype=="CST") m_mettype_enum = CST;
    else if(m_mettype=="TST") m_mettype_enum = TST;
    else if(m_mettype=="Track") m_mettype_enum = Track;
    else if(m_doEleJetOlapScan){
         if(m_mettype=="TST_frac00pt00") m_mettype_enum = TST;
         else if(m_mettype=="TST_frac00pt10") m_mettype_enum = TST;
         else if(m_mettype=="TST_frac00pt20") m_mettype_enum = TST;
         else if(m_mettype=="TST_frac00pt30") m_mettype_enum = TST;
         else if(m_mettype=="TST_frac02pt00") m_mettype_enum = TST;
         else if(m_mettype=="TST_frac02pt10") m_mettype_enum = TST;
         else if(m_mettype=="TST_frac02pt20") m_mettype_enum = TST;
         else if(m_mettype=="TST_frac02pt30") m_mettype_enum = TST;
         else if(m_mettype=="TST_frac05pt00") m_mettype_enum = TST;
         else if(m_mettype=="TST_frac05pt10") m_mettype_enum = TST;
         else if(m_mettype=="TST_frac05pt20") m_mettype_enum = TST;
         else if(m_mettype=="TST_frac05pt30") m_mettype_enum = TST;
    }
    else if(m_doPFlowJVTScan){ 
	ATH_MSG_INFO("Doing JVT Scan on pflow");
        m_mettype_enum = TST;
        }
    else if(m_doJVTScan){ 
	ATH_MSG_INFO("Doing JVT Scan");
        m_mettype_enum = TST;
        }
    else {
      ATH_MSG_ERROR("Invalid MET type requested -- quitting!");
      return StatusCode::FAILURE;
    }

    return StatusCode::SUCCESS;
  }

  StatusCode METCalculator::finalize()
  {
    ATH_MSG_INFO ("Finalizing " << name() << "...");
    ATH_CHECK(m_metMakerTool.release());
    ATH_CHECK(m_metSystTool.release());


    return StatusCode::SUCCESS;
  }

  StatusCode METCalculator::execute()
  {
    ATH_MSG_VERBOSE ("Executing" << name() << "...");
    const xAOD::MissingETAssociationMap *metMap=0;
    //ATH_CHECK(evtStore()->retrieve(metMap, "METAssoc_AntiKt4LCTopo"));
    ATH_CHECK(evtStore()->retrieve(metMap, m_metMapName));
    metMap->resetObjSelectionFlags();
    const xAOD::MissingETContainer *coreMet=0;
    //ATH_CHECK(evtStore()->retrieve(coreMet, "MET_Core_AntiKt4LCTopo"));
    ATH_CHECK(evtStore()->retrieve(coreMet, m_metCoreName));
    xAOD::MissingETContainer *outmetCont=new xAOD::MissingETContainer();
    xAOD::MissingETAuxContainer *outmetAuxCont=new xAOD::MissingETAuxContainer();

    if(coreMet){
      const MissingET* met_soft   = (*coreMet)["PVSoftTrkCore"];
      ATH_MSG_VERBOSE("Core: " << met_soft->met() << " sumET: " << met_soft->sumet());
    }

    std::string metauxname=m_metContName+"Aux.";
    outmetCont->setStore(outmetAuxCont);
    ATH_CHECK(evtStore()->record(outmetCont, m_metContName));
    ATH_CHECK(evtStore()->record(outmetAuxCont, metauxname));
    const xAOD::ElectronContainer *elCont=0;
    const xAOD::MuonContainer *muCont=0;
    const xAOD::JetContainer*jetCont=0;
    const xAOD::TauJetContainer *tauCont=0;
    std::string taucontName="TauRecContainer";
    const xAOD::PhotonContainer *phCont=0;
    std::string phcontName="PhotonCollection";

    //  check the exitence of the containers and get the nominal one if the container not exist
    ATH_CHECK(evtStore()->retrieve(tauCont, "TauJets"));
    ATH_CHECK(evtStore()->retrieve(phCont, "Photons"));
    if(metMap->empty()) return StatusCode::SUCCESS; //Marco: adding this line to avoid problems with 0 PV events.

    if(m_mettype_enum==CST || m_mettype_enum==TST)
    {
      if(m_elContName!="")
      {
        ATH_CHECK(evtStore()->retrieve(elCont, m_elContName));
        ATH_CHECK(m_metMakerTool->rebuildMET("RefEle",xAOD::Type::Electron, outmetCont, elCont, metMap));
      }
      if(m_phContName!="")
      {
        ATH_CHECK(evtStore()->retrieve(phCont, m_phContName));
        ATH_CHECK(m_metMakerTool->rebuildMET("RefGamma",xAOD::Type::Photon, outmetCont, phCont, metMap));
      }

      if(m_tauContName!="")
      {
        ATH_CHECK(evtStore()->retrieve(tauCont, m_tauContName));
        ATH_CHECK(m_metMakerTool->rebuildMET("RefTau",xAOD::Type::Tau, outmetCont, tauCont, metMap));
      }
      if(m_muContName!="")
      {
        ATH_CHECK(evtStore()->retrieve(muCont, m_muContName));
        ATH_CHECK(m_metMakerTool->rebuildMET("Muons",xAOD::Type::Muon, outmetCont, muCont, metMap));
      }
      if(m_jetContName!="")
      {
        ATH_CHECK(evtStore()->retrieve(jetCont, m_jetContName));
	bool doJVT = m_doJVT;
	if(jetCont->size()>0) {
          if( jetCont->front()->isAvailable< int >("InputType") ){ // need to check if branch exists
	    // intrinsically pileup-suppressed
	    //if(jetCont->front()->getInputType()==xAOD::JetInput::EMPFlow) doJVT=false;//Need to use JVT now on PFlow jets

	  }
	}
	
	if(m_doTruth) {
	  ATH_CHECK(m_metMakerTool->rebuildJetMET("RefJet", "SoftTruthAll", "SoftTruthChargedCentral", outmetCont, jetCont, coreMet, metMap, doJVT));
	} else {
	  ATH_CHECK(m_metMakerTool->rebuildJetMET("RefJet", "SoftClus", "PVSoftTrk", outmetCont, jetCont, coreMet, metMap, doJVT));
	  // label the jets with how much of the jet pT is used in the MET
	  
	  MissingET* met_jetterm = (*outmetCont)["RefJet"];
	  std::vector<float>& uniqueWeights = dec_constitObjWeights(*met_jetterm);
	  unsigned iJet = 0;
	  for(const auto &jet : *jetCont){
	    //std::cout << "jet: " << jet->pt() << " eta:" << jet->eta() << " jetCont: " << uniqueWeights[iJet] << std::endl; 
	    //jet
	    dec_jetDescription(*jet) = uniqueWeights[iJet];
	    ++iJet;
	  }
	}
      }
      if(jetCont->size()>0) {
	if( jetCont->front()->isAvailable< int >("InputType") ){ // need to check if branch exists
	  if(jetCont->front()->getInputType()==xAOD::JetInput::EMPFlow) {
	    ATH_MSG_DEBUG("Starting removal of bad PFlow tracks...");
	    const xAOD::PFOContainer * tracks = 0;
	    if ( !evtStore()->retrieve(tracks,"JetETMissChargedParticleFlowObjects").isSuccess() ) {
	      ATH_MSG_ERROR("Failed to retrieve charged PFO collection. Exiting." );
	      return StatusCode::SUCCESS;
	    }
	    const xAOD::CaloClusterContainer * clusters = 0;
	    if ( !evtStore()->retrieve(clusters,"CaloCalTopoClusters").isSuccess() ) {
	      ATH_MSG_ERROR("Failed to retrieve calo cluster collection. Exiting." );
	      return StatusCode::SUCCESS;
	    }
	    const xAOD::VertexContainer * vertices = 0;
	    if ( !evtStore()->retrieve(vertices,"PrimaryVertices").isSuccess() ) {
	      ATH_MSG_ERROR("Failed to retrieve calo cluster collection. Exiting." );
	      return StatusCode::SUCCESS;
	    }
	    const xAOD::Vertex* pv(0);
	    for(const auto& vx : *vertices) {
	      if(vx->vertexType()==xAOD::VxType::PriVtx) {
		pv = vx;
		break;
	      }
	    }
    if(!pv) {
      ATH_MSG_ERROR("No primary vertex found!");
      return StatusCode::SUCCESS;
    }
        for ( const auto& pfo : *tracks ) {
      const xAOD::TrackParticle* ptrk = pfo->track(0);
      if ( ptrk == 0 ) {
        ATH_MSG_WARNING("Skipping charged PFO with null track pointer.");
        continue;
      }
      float Rerr = (float)Amg::error(ptrk->definingParametersCovMatrix(),4)/fabs(ptrk->qOverP());
      //float totalE=0.;
      /*for ( const auto& iclus : *clusters ) {
        //if (iclus->rawE()>0 && xAOD::P4Helpers::isInDeltaR(*pfo, *iclus, 0.1)) {
        if (xAOD::P4Helpers::isInDeltaR(*pfo, *iclus, 0.1)) {
          totalE+=iclus->pt();
        }
      }
      float EoPr01 = totalE/pfo->pt();
       */
      //bool isBad = 0;
      //if (pfo->pt()>20000. && (Rerr>0.4 || (EoPr01<0.65 && ((EoPr01>0.1 && Rerr>0.05) || Rerr>0.1)))
      //    && ((ptrk->z0() + ptrk->vz() - pv->z())*sin(ptrk->theta())<2) ) isBad=1;
      
      //if (pfo->pt()>20000. &&  Rerr>0.1
      //    && ((ptrk->z0() + ptrk->vz() - pv->z())*sin(ptrk->theta())<2) ) isBad=1;  
      // Check OR vs all objects used by the MET hard terms
      // bool passOR=true;
      //const static SG::AuxElement::ConstAccessor<std::vector<ElementLink<IParticleContainer> > > cacc_constObjLinks("ConstitObjectLinks");
      //for( const auto& metterm : *outmetCont ) {
      //  if(metterm->source()==(MissingETBase::Source::Muon|MissingETBase::Source::Calo)) continue; // ignore Eloss term
      //  if(metterm->source()==0x100000) continue; // ignore invisible
      //  if(MissingETBase::Source::isSoftTerm(metterm->source())) continue;
      //  for(const auto& objel : cacc_constObjLinks(*metterm) ) {
      //    const xAOD::IParticle* obj = *objel;
      //    float deltaRcut = obj->type()==xAOD::Type::Jet ? 0.4 : 0.1;
      //    if(xAOD::P4Helpers::isInDeltaR(*obj,*pfo,deltaRcut)) {
      //  passOR = false;
      //    }
      //  }
      //  if (isBad && passOR) {
      //    *(*outmetCont)["PVSoftTrk"] -= pfo;
      //  }
      //}
        }
      }
  }
}
      if(m_metsysTerm!="Nominal") {
        xAOD::MissingET *softMET=0;

        CP::SystematicVariation systVar(m_metsysTerm);
        CP::SystematicSet systSet(m_metsysTerm);

        if(m_metsysTerm.find("SoftCalo")!=std::string::npos && m_mettype_enum==CST ) {
          softMET=(*outmetCont)["SoftClus"];
          if(m_metSystTool->isAffectedBySystematic(systVar)) {
            if(m_metSystTool->applySystematicVariation(systSet)==CP::SystematicCode::Ok) {
              ATH_MSG_DEBUG("Successfully Apply Variation");
              if(m_metSystTool->applyCorrection(*softMET)==CP::CorrectionCode::Ok) {
                ATH_MSG_DEBUG("Successfully Apply Correction");
              }
            }
          }
        }
        ATH_MSG_DEBUG("MET before applying variation:" << (*outmetCont)["PVSoftTrk"]->met() );
        if(m_metsysTerm.find("SoftTrk")!=std::string::npos && m_mettype_enum==TST ) {
          softMET=(*outmetCont)["PVSoftTrk"];
          if(m_metSystTool->isAffectedBySystematic(systVar)) {
            if(m_metSystTool->applySystematicVariation(systSet)==CP::SystematicCode::Ok) {
              ATH_MSG_DEBUG("Successfully Apply Variation");
              if(m_metSystTool->applyCorrection(*softMET)==CP::CorrectionCode::Ok) {
                ATH_MSG_DEBUG("Successfully Apply correction");
              }
            }
          }
        }
        ATH_MSG_DEBUG("MET after applying variation:" << (*outmetCont)["PVSoftTrk"]->met() );

      }

      if(m_mettype_enum==CST )
      {
        ATH_CHECK(m_metMakerTool->buildMETSum("Final", outmetCont, MissingETBase::Source::LCTopo));
      }
 
      if(m_mettype_enum==TST )
      {
        //  ATH_CHECK(m_metMakerTool->rebuildTrackMET("RefJetTrk", "PVSoftTrk", outmetCont, jetCont, coreMet, metMap, doJVF));
        ATH_CHECK(m_metMakerTool->buildMETSum("Final", outmetCont, MissingETBase::Source::Track));
      }

    }
    else if(m_mettype_enum==Track)
    {
      if(m_muContName!="")
      {
        ATH_CHECK(evtStore()->retrieve(muCont, m_muContName));
        ATH_CHECK(m_metMakerTool->rebuildMET("Muons",xAOD::Type::Muon, outmetCont, muCont, metMap));
      }
      if(m_elContName!="")
      {
        ATH_CHECK(evtStore()->retrieve(elCont, m_elContName));
        ATH_CHECK(m_metMakerTool->rebuildMET("RefEle",xAOD::Type::Electron, outmetCont, elCont, metMap));
      }

      if(m_jetContName!="")
      {
        ATH_CHECK(evtStore()->retrieve(jetCont, m_jetContName));
        ATH_CHECK(m_metMakerTool->rebuildTrackMET("RefJet", "PVSoftTrk", outmetCont, jetCont, coreMet, metMap, m_doJVT));
      }

      if(m_metsysTerm!="Nominal") {
        xAOD::MissingET *softMET=0;
        xAOD::MissingET *jetTrkMET=0;

        CP::SystematicVariation systVar(m_metsysTerm);
        CP::SystematicSet systSet(m_metsysTerm);
        if(m_metsysTerm.find("SoftTrk")!=std::string::npos ) {
          softMET=(*outmetCont)["PVSoftTrk"];
          if(m_metSystTool->isAffectedBySystematic(systVar)) {
            if(m_metSystTool->applySystematicVariation(systSet)==CP::SystematicCode::Ok) {
              ATH_MSG_DEBUG("Successfully Apply Variation");
              if(m_metSystTool->applyCorrection(*softMET)==CP::CorrectionCode::Ok) {
                ATH_MSG_DEBUG("Successfully Apply Correction");
              }
            }
          }
        }
        else if(m_metsysTerm.find("JetTrk")!=std::string::npos) {
          jetTrkMET=(*outmetCont)["RefJet"];
          if(m_metSystTool->isAffectedBySystematic(systVar)) {
            if(m_metSystTool->applySystematicVariation(systSet)==CP::SystematicCode::Ok) {
              ATH_MSG_DEBUG("Successfully Apply Variation");
              if(m_metSystTool->applyCorrection(*jetTrkMET, metMap)==CP::CorrectionCode::Ok) {
                ATH_MSG_DEBUG("Successfully Apply Correction");
              }
            }
          }
        }
      }
      ATH_CHECK(m_metMakerTool->buildMETSum("Final", outmetCont, MissingETBase::Source::Track));
    }

    return StatusCode::SUCCESS;
  }

} //> end namespace met

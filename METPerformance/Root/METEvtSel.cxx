///////////////////////// -*- C++ -*- /////////////////////////////
// METEvtSel.cxx
// Implementation file for class METEvtSel
// Author: T.J.Khoo<khoo@cern.ch>
///////////////////////////////////////////////////////////////////

// METEvtSel includes
#include "METPerformance/METEvtSel.h"
#include "METPerformance/METPerfHelper.h"
#include "METPerformance/JetID.h"

// MET EDM
#include "xAODMissingET/MissingETContainer.h"

// EDM includes
#include "xAODCore/ShallowCopy.h"
#include "xAODEventInfo/EventInfo.h"
#include "xAODJet/JetContainer.h"
#include "xAODMuon/MuonContainer.h"
#include "xAODMuon/MuonAuxContainer.h"
#include "xAODEgamma/ElectronContainer.h"
#include "xAODEgamma/ElectronAuxContainer.h"
#include "xAODEgamma/PhotonContainer.h"

   // look here !
   // here you find the CP prereccomandations for Physics Analysis (rel 20)
   // https://twiki.cern.ch/twiki/bin/view/AtlasProtected/PhysicsAnalysisWorkBookRel20CPRec
   //

namespace met {

  const static double Zmass = 91.2e3;
  static SG::AuxElement::ConstAccessor<char> dec_passOR("passOR");
//  static SG::AuxElement::ConstAccessor<char> dec_bjet("bjet");

  using std::vector;
  using namespace xAOD;

  ///////////////////////////////////////////////////////////////////
  // Public methods:
  ///////////////////////////////////////////////////////////////////

  // Constructors
  ////////////////
  // https://twiki.cern.ch/twiki/bin/view/AtlasComputing/FaqCompileTimeWarnings#In_constructor_will_be_initializ
  METEvtSel::METEvtSel(const std::string& name) :
    AsgTool(name),
    m_isPVDec("isPV"),
    m_evtCleanDec("eventClean"),
    m_jetCleanDec("jetClean"),
    m_isZmmDec("isZmm"),
    m_isZeeDec("isZee"),
    m_isWmvDec("isWmv"),
    m_isWevDec("isWev"),
    m_isttbarDec("isttbar"),
    m_isdijetDec("isdijet"),
    m_isgjetDec("isgjet"),
    m_trigDec("trigDec")
   // m_isBjetDec("isBjet")
  {
    //
    // Property declaration
    //
    declareProperty( "EleColl",     m_eleColl     = "GoodElectrons"       );
    declareProperty( "JetColl",     m_jetColl     = "GoodJets"            );
    declareProperty( "BadJetColl",  m_badjetColl  = "BadJets"             );
    declareProperty( "MuonColl",    m_muonColl    = "GoodMuons"           );
    declareProperty( "OutputPhotons",m_photonColl = "GoodPhotons"         );
    declareProperty( "MetColl",     m_metColl     = "RefTST"              );

    declareProperty( "SUSYTools",       m_SUSYTools                      );

    declareProperty( "TriggersRequired",   m_reqTriggers                 );
    declareProperty( "doTriggerDecision",  m_doTriggerDecision = true    );
    declareProperty( "doTriggerMatch",     m_reqTriggerMatch = true      );
    declareProperty( "doJetCleaning",      m_reqJetCleaning  = true      );
    declareProperty( "metCut",             m_metCut  = -10.0             );
    declareProperty("EvtType",             m_evttype     = None          );
    declareProperty("doEverySelection",    m_doEverySelection     = true );
    // Tile trips currently broken?
    //declareProperty("AthTileTripReader",m_trips);
  }

  // Destructor
  ///////////////
  METEvtSel::~METEvtSel()
  {}

  // Athena algtool's Hooks
  ////////////////////////////
  StatusCode METEvtSel::initialize()
  {
    ATH_MSG_INFO ("Initializing " << name() << "...");

    ATH_MSG_INFO("Attempting to retrieve SUSYTools." );
    if (m_SUSYTools.retrieve().isFailure()){
      ATH_MSG_ERROR("Failed to retrieve tool: " << m_SUSYTools.name());
      return StatusCode::FAILURE;
    }
    ATH_MSG_INFO("Retrieved SUSYTools: " << m_SUSYTools.name());

    // Tile trips currently broken?
    //StatusCode sc = m_trips.retrieve();
    //if ( sc.isFailure() ) {
    //  ATH_MSG_ERROR("Can't get handle on trip reader tool");
    //  return sc;
    //}

   // triggersRequired.push_back("HLT_mu10");

    return StatusCode::SUCCESS;
  }

  StatusCode METEvtSel::finalize()
  {
    ATH_MSG_INFO ("Finalizing " << name() << "...");
    ATH_CHECK(m_SUSYTools.release());

    return StatusCode::SUCCESS;
  }

  StatusCode METEvtSel::execute()
  {
    ATH_MSG_DEBUG ( name() << " in execute...");


    const EventInfo* eventinfo = 0;
    ATH_CHECK( evtStore()->retrieve( eventinfo, "EventInfo" ) );

    MuonContainer* bestZmm = new MuonContainer;
    ATH_CHECK( evtStore()->record(bestZmm,"BestZmm") );
    MuonAuxContainer* bestZmmAux = new MuonAuxContainer;
    ATH_CHECK( evtStore()->record(bestZmmAux,"BestZmmAux.") );
    bestZmm->setStore(bestZmmAux);

    ElectronContainer* bestZee = new ElectronContainer;
    ATH_CHECK( evtStore()->record(bestZee,"BestZee") );
    ElectronAuxContainer* bestZeeAux = new ElectronAuxContainer;
    ATH_CHECK( evtStore()->record(bestZeeAux,"BestZeeAux.") );
    bestZee->setStore(bestZeeAux);

   // to produce the plots with W plots (in METPerfAlg.cxx):
   // store the lepton coming from the W
   // save it in the slimmed output xAOD
    ElectronContainer* ele_fromW = new ElectronContainer;
    ATH_CHECK( evtStore()->record(ele_fromW,"eleFromW") );
    ElectronAuxContainer* ele_fromWAux = new ElectronAuxContainer;
    ATH_CHECK( evtStore()->record(ele_fromWAux,"eleFromWAux.") );
    ele_fromW->setStore(ele_fromWAux);

    MuonContainer* muon_fromW = new MuonContainer;
    ATH_CHECK( evtStore()->record(muon_fromW,"muonFromW") );
    MuonAuxContainer* muon_fromWAux = new MuonAuxContainer;
    ATH_CHECK( evtStore()->record(muon_fromWAux,"muonFromWAux.") );
    muon_fromW->setStore(muon_fromWAux);

    m_isPVDec(*eventinfo) = doPrimaryVertex();
    m_evtCleanDec(*eventinfo) = doEventCleaning();
    m_jetCleanDec(*eventinfo) = m_reqJetCleaning ? doJetCleaning() : true;


    if(m_doEverySelection){
       m_isZmmDec(*eventinfo) = selectZmumu();
       m_isZeeDec(*eventinfo) = selectZee();
       m_isWmvDec(*eventinfo) = selectWmunu();
       m_isWevDec(*eventinfo) = selectWenu();
       m_isttbarDec(*eventinfo) = selectttbar();
       m_isdijetDec(*eventinfo) = selectdijet();
       m_isgjetDec(*eventinfo) = selectgjet();
    }
    else{
       m_isZmmDec(*eventinfo) = false;
       m_isZeeDec(*eventinfo) = false;
       m_isWmvDec(*eventinfo) = false;
       m_isWevDec(*eventinfo) = false;
       m_isttbarDec(*eventinfo) = false;
       m_isdijetDec(*eventinfo) = false;
       m_isgjetDec(*eventinfo) = false;
       EvtType selected = static_cast<EvtType>(m_evttype);
       bool has_passed_selection = selectEvent(selected);
       switch(selected) {
          case Zmm:   m_isZmmDec(*eventinfo)   = has_passed_selection;
                      break; 
          case Zee:   m_isZeeDec(*eventinfo)   = has_passed_selection;
                      break; 
          case Wmv:   m_isWmvDec(*eventinfo)   = has_passed_selection;
                      break; 
          case Wev:   m_isWevDec(*eventinfo)   = has_passed_selection;
                      break; 
          case ttbar: m_isttbarDec(*eventinfo) = has_passed_selection;
                      break; 
          case dijet: m_isdijetDec(*eventinfo) = has_passed_selection;
                      break;
          case gjet: m_isgjetDec(*eventinfo) = has_passed_selection;
                      break;
          case None: break;
       }
    }

    if(m_doTriggerDecision)
      m_trigDec(*eventinfo) = selectTrigger(m_reqTriggers);
    else
      m_trigDec(*eventinfo) = true;

//    m_muEffSF(*eventinfo) = musf;

    ATH_MSG_DEBUG( "Pass PV selection? "   << ( m_isPVDec(*eventinfo) ? "Yes" : "No" ) );
    ATH_MSG_DEBUG( "Pass event cleaning? " << ( m_evtCleanDec(*eventinfo) ? "Yes" : "No" ) );
    ATH_MSG_DEBUG( "Pass jet cleaning? "   << ( m_jetCleanDec(*eventinfo) ? "Yes" : "No" ) );
    ATH_MSG_DEBUG( "Pass Zmm selection? "  << ( m_isZmmDec(*eventinfo) ? "Yes" : "No" ) );
    ATH_MSG_DEBUG( "Pass Zee selection? "  << ( m_isZeeDec(*eventinfo) ? "Yes" : "No" ) );
    ATH_MSG_DEBUG( "Pass Wmv selection? "  << ( m_isWmvDec(*eventinfo) ? "Yes" : "No" ) );
    ATH_MSG_DEBUG( "Pass Wev selection? "  << ( m_isWevDec(*eventinfo) ? "Yes" : "No" ) );
    ATH_MSG_DEBUG( "Pass ttbar selection? " << ( m_isttbarDec(*eventinfo) ? "Yes" : "No" ) );
    ATH_MSG_DEBUG( "Pass dijet selection? " << ( m_isdijetDec(*eventinfo) ? "Yes" : "No" ) );
    ATH_MSG_DEBUG( "Pass gjet selection?  " << ( m_isgjetDec(*eventinfo) ? "Yes" : "No" ) );
    ATH_MSG_DEBUG( "Pass trigger? "         << ( m_trigDec(*eventinfo) ? "Yes" : "No" ) );


    return StatusCode::SUCCESS;
  }

  bool METEvtSel::doPrimaryVertex( ) {

    bool foundPV = false;
    const VertexContainer* pvertices = 0;
    ATH_CHECK( evtStore()->retrieve( pvertices, "PrimaryVertices" ) );
    for(const auto& vx : *pvertices) {
      if( vx->vertexType() == xAOD::VxType::PriVtx ) {
        foundPV = true;
        break;
      }
    }

    if(!foundPV) return false;
    else return true;
  }

  bool METEvtSel::doEventCleaning()
  {
    // https://twiki.cern.ch/twiki/bin/view/AtlasProtected/PhysicsAnalysisWorkBookRel20CPRec#Event_cleaning
    const EventInfo* eventinfo = 0;
    ATH_CHECK( evtStore()->retrieve(eventinfo, "EventInfo") );

    // for data only :
    if(!eventinfo->eventType(EventInfo::IS_SIMULATION)) {
         // Remove bad events due to problems in TileCal :
        if(eventinfo->errorState(EventInfo::Tile)==EventInfo::Error) return false;
        // Remove bad events due to problems in LAr :
        if(eventinfo->errorState(EventInfo::LAr)==EventInfo::Error) return false;

        // Additional corrupted events from TileCal for which the flag was not set ?

        //if(eventinfo->eventFlags(EventInfo::Core) & 0x40000) return false;//no more useful?
        if (eventinfo->isEventFlagBitSet(EventInfo::Core, 18)) return false; //added by me

        //ATH_MSG_DEBUG("Check tile trips");
        //const Root::TAccept& check=m_trips->accept(0);
        //bool inTrip=check.getCutResult("InTrip");
        //bool badEvent=check.getCutResult("BadEvent");
        //if(inTrip || badEvent) return false;
    }

    // for MC only :
    //if(eventinfo->eventType(EventInfo::IS_SIMULATION)){
     //There were problems in the production which lead to the same physics event being written out more than once
     // how to get rid of this?
    //}

    return true;
  }

  bool METEvtSel::doJetCleaning()
  {
    const JetContainer* badjets = 0;
    if(evtStore()->retrieve(badjets, m_badjetColl).isFailure()) {
      ATH_MSG_WARNING("Failed to retrieve bad jet collection");
      return false;
    }
    return badjets->size()==0;
  }

  bool METEvtSel::selectTrigger(std::vector<std::string>& triggers)
  {
    bool pass_trigger(false);
    for (const auto& trigger : triggers) {
      if (m_SUSYTools->IsTrigPassed(trigger)) pass_trigger = true;
    }
    return pass_trigger;
  }

  bool METEvtSel::selectEvent(EvtType type)
  {
    switch(type) {
    case None:  return true;
    case Zmm:   return selectZmumu();
    case Zee:   return selectZee();
    case Wmv:   return selectWmunu();
    case Wev:   return selectWenu();
    case ttbar: return selectttbar();
    case dijet: return selectdijet();
    case gjet: return selectgjet();
    default:    return false;
    }
  }

  bool METEvtSel::selectZmumu()
  {
    const ElectronContainer* electrons = 0;
    ATH_CHECK( evtStore()->retrieve(electrons,m_eleColl) );
    const MuonContainer* muons = 0;
    ATH_CHECK( evtStore()->retrieve(muons,m_muonColl) );
    // Require exactly two good muons
    int nMuons(0), nElectrons(0);
    ATH_MSG_VERBOSE( "Event has " << electrons->size() << " electrons before OR." );
    ATH_MSG_VERBOSE( "Event has " << muons->size()     << " muons before OR." );
    for( const auto& el : *electrons ) {
      if( ( *el ).isAvailable< char >("passOR") ){
        if (!dec_passOR(*el)) continue;
        ++nElectrons;
      }
    }
    for( const auto& mu : *muons ) {
        if( ( *mu ).isAvailable< char >("passOR") ){
          if (!dec_passOR(*mu)) continue;
          ++nMuons;
        }
    }
    if (nMuons!=2) return false;
    IParticle::FourMom_t Zboson;
    double best_Zmass = 0;
    MuonContainer* bestZmm = 0;
    ATH_CHECK( evtStore()->retrieve(bestZmm,"BestZmm") );

    // Require opposite charge
    if (muons->at(0)->charge() * muons->at(1)->charge() > 0) {
      ATH_MSG_VERBOSE( "Muon pair has same sign" );
      return false;
    }
    // Require dilepton mass consistent with mZ
    Zboson = muons->at(0)->p4() + muons->at(1)->p4();
    ATH_MSG_VERBOSE( "Dimuon mass = " << Zboson.M() );
    xAOD::Muon* mu1 = new xAOD::Muon();
    xAOD::Muon* mu2 = new xAOD::Muon();
    bestZmm->push_back(mu1);
    bestZmm->push_back(mu2);
    *mu1 = *muons->at(0);
    *mu2 = *muons->at(1);
    best_Zmass = Zboson.M();

    // Require a trigger match
    if(m_reqTriggerMatch){
      bool match_found = false;
      for( const auto& mu : *muons ) {
        for (const auto& trigger : m_reqTriggers) {
          if (m_SUSYTools->IsTrigMatched(mu, trigger)) match_found = true;
        }
        if (match_found) break;
      }
      if (!match_found) {
        ATH_MSG_INFO( "No trigger match found.");
        return false;
      }
    }  

    if(fabs(best_Zmass-Zmass)>25e3) return false;
    if(m_metCut>0.0){
      const MissingETContainer* met = 0;
      ATH_CHECK( evtStore()->retrieve(met,"MET_"+m_metColl) );
      const MissingET* pMET = (*met)["Final"];
      if(pMET->met()<m_metCut) return false;
    }
    return true;
  }

  bool METEvtSel::selectZee()
  {
    const ElectronContainer* electrons = 0;
    ATH_CHECK( evtStore()->retrieve(electrons,m_eleColl) );
    const MuonContainer* muons = 0;
    ATH_CHECK( evtStore()->retrieve(muons,m_muonColl) );
    // Require exactly two good electrons
    int nMuons(0), nElectrons(0);
    ATH_MSG_VERBOSE( "Event has " << electrons->size() << " electrons before OR." );
    ATH_MSG_VERBOSE( "Event has " << muons->size()     << " muons before OR." );
    for( const auto& el : *electrons ) {
      if( ( *el ).isAvailable< char >("passOR") ){ // need to check if decoration exists, passOR is not set if PV is not found
        if (!dec_passOR(*el)) continue;
        ++nElectrons;
      }
    }
    for( const auto& mu : *muons ) {
      if( ( *mu ).isAvailable< char >("passOR") ){
        if (!dec_passOR(*mu)) continue;
        ++nMuons;
      }
    }
    if ((nMuons!=0) || (nElectrons!=2)) return false;
    IParticle::FourMom_t Zboson;
    double best_Zmass = 0;
    ElectronContainer* bestZee = 0;
    ATH_CHECK( evtStore()->retrieve(bestZee,"BestZee") );
    // Require opposite charge
    if (electrons->at(0)->charge() * electrons->at(1)->charge() > 0) {
      ATH_MSG_VERBOSE( "Electron pair has same sign" );
      return false;
    }
    // Require dilepton mass consistent with mZ
    Zboson = electrons->at(0)->p4() + electrons->at(1)->p4();
    ATH_MSG_VERBOSE( "Di-electron mass = " << Zboson.M() );
    Electron* el1 = new Electron();
    Electron* el2 = new Electron();
    bestZee->push_back(el1);
    bestZee->push_back(el2);
    *el1 = *electrons->at(0);
    *el2 = *electrons->at(1);
    best_Zmass = Zboson.M();

    // Require a trigger match
    if(m_reqTriggerMatch){
      bool match_found = false;
      for( const auto& el : *electrons ) {
        for (const auto& trigger : m_reqTriggers) {
          if (m_SUSYTools->IsTrigMatched(el, trigger)) match_found = true;
        }
        if (match_found) break;
      }
      if (!match_found) {
        ATH_MSG_INFO( "No trigger match found." );
        return false;
      }
    }

    if(fabs(best_Zmass-Zmass)>25e3) return false;
    
    if(m_metCut>0.0){
      const MissingETContainer* met = 0;
      ATH_CHECK( evtStore()->retrieve(met,"MET_"+m_metColl) );
      const MissingET* pMET = (*met)["Final"];
      if(pMET->met()<m_metCut) return false;

    }
    return true;
  }

  bool METEvtSel::selectWmunu()
  {
    const MuonContainer* muons = 0;
    ATH_CHECK( evtStore()->retrieve(muons,m_muonColl) );
    const ElectronContainer* electrons = 0;
    ATH_CHECK( evtStore()->retrieve(electrons,m_eleColl) );
    const MissingETContainer* met = 0;
    ATH_CHECK( evtStore()->retrieve(met,"MET_"+m_metColl) );

    // store the lepton coming from the W decay
    MuonContainer* muon_fromW = 0;
    ATH_CHECK( evtStore()->retrieve(muon_fromW,"muonFromW") );
    //
    if(muons->size()!=1) return false;
    if(electrons->size()>0) return false;
    const xAOD::Muon* pMu = muons->front();
    xAOD::Muon* muW = new xAOD::Muon();
    muon_fromW->push_back(muW);
    *muW = *pMu;

 //   float sfmu=(pMu)->auxdata<float>("EfficiencyScaleFactor");
 //   musf=sfmu;

    // MET cut is now imposed on histogramming
    const MissingET* pMET = (*met)["Final"];
    //if(pMET->met()<25e3) return false;
    IParticle::FourMom_t Wboson;

    IParticle::FourMom_t nu;
    nu.SetPtEtaPhiM(pMET->met(),0, pMET->phi(),0);
    Wboson=pMu->p4()+nu;

    double WmT=Wboson.Mt();
//    double WmT = sqrt(2*pMu->pt()*pMET->met()*(1-cos(pMu->phi()-pMET->phi())));

    if(WmT<40e3) return false;
    return true;
  }

  bool METEvtSel::selectWenu()
  {
    const MuonContainer* muons = 0;
    ATH_CHECK( evtStore()->retrieve(muons,m_muonColl) );
    const ElectronContainer* electrons = 0;
    ATH_CHECK( evtStore()->retrieve(electrons,m_eleColl) );
    const MissingETContainer* met = 0;
    ATH_CHECK( evtStore()->retrieve(met,"MET_"+m_metColl) );

    // store the lepton coming from the W decay
    ElectronContainer* ele_fromW = 0;
    ATH_CHECK( evtStore()->retrieve(ele_fromW,"eleFromW") );
    //
    if(electrons->size()!=1) return false;
    if(muons->size()>0) return false;
    const Electron* pEl = electrons->front();
    Electron* elW = new Electron();
    ele_fromW->push_back(elW);
    *elW = *pEl;


    // MET cut is now imposed on histogramming
    const MissingET* pMET = (*met)["Final"];
    //if(pMET->met()<25e3) return false;

    IParticle::FourMom_t Wboson;

    IParticle::FourMom_t nu;
    nu.SetPtEtaPhiM(pMET->met(),0, pMET->phi(),0);
    Wboson=pEl->p4()+nu;

    double WmT=Wboson.Mt();
//    double WmT = sqrt(2*pEl->pt()*pMET->met()*(1-cos(pEl->phi()-pMET->phi())));

    if(WmT<40e3) return false;
    return true;
  }

  // select semilep ttbar events
  // require exactly 1 ele or exactly 1 muon
  bool METEvtSel::selectttbar() //marta
  {
    const MuonContainer* muons = 0;
    ATH_CHECK( evtStore()->retrieve(muons,m_muonColl) );
    const ElectronContainer* electrons = 0;
    ATH_CHECK( evtStore()->retrieve(electrons,m_eleColl) );
    ATH_MSG_VERBOSE (  " ttbar selection; num of good ele = "<< electrons->size());
    ATH_MSG_VERBOSE (  " ttbar selection; num of good muons = "<< muons->size());
    if(   (electrons->size() + muons->size() )  != 1 ) return false; // HACK
    //if(   (electrons->size() + muons->size() )  != 1  || (electrons->size() + muons->size() )  != 2) return false;

    const JetContainer* jets = 0;
    ATH_CHECK( evtStore()->retrieve(jets,m_jetColl) );
    ATH_MSG_VERBOSE (  " ttbar selection; num of good jets = "<< jets->size());
    if(jets->size() < 4 ) return false; //HACK

    int countBJets = 0;
//    for(const auto& iJet : *jets) {
  //    if (dec_bjet(*iJet)) ++countBJets;
   // }
   // ATH_MSG_VERBOSE (  " ttbar selection; num of b-jets = "<< countBJets);
   // if (countBJets == 0) return false;// HACK
    
    return true;
  }

  bool METEvtSel::selectdijet() //Matteo
  {
    //require at least 2 jets
    const JetContainer* jets = 0;
    ATH_CHECK( evtStore()->retrieve(jets,m_jetColl) );
    ATH_MSG_VERBOSE (  " dijet selection; num of good jets = "<< jets->size());
    if(jets->size() < 2 ) return false;
    //Trigger must be fired; dijet system must be over a trigger-dependent threshold
    //fill vector of pair containing thresholds
    std::vector< std::pair < std::string, float > > threshold_vec;
    std::pair < std::string, float > temp_pair;
    float GeV=1000;
    temp_pair.first="j15"; temp_pair.second=25*GeV; threshold_vec.push_back(temp_pair);
    temp_pair.first="j25"; temp_pair.second=40*GeV; threshold_vec.push_back(temp_pair);
    temp_pair.first="j35"; temp_pair.second=55*GeV; threshold_vec.push_back(temp_pair);
    temp_pair.first="j45"; temp_pair.second=65*GeV; threshold_vec.push_back(temp_pair);
    temp_pair.first="j60"; temp_pair.second=85*GeV; threshold_vec.push_back(temp_pair);
    temp_pair.first="j110"; temp_pair.second=175*GeV; threshold_vec.push_back(temp_pair);
    temp_pair.first="j175"; temp_pair.second=220*GeV; threshold_vec.push_back(temp_pair);
    temp_pair.first="j260"; temp_pair.second=330*GeV; threshold_vec.push_back(temp_pair);
    temp_pair.first="j360"; temp_pair.second=400    *GeV; threshold_vec.push_back(temp_pair);
    //Looking for fired trigger
    bool match_found = false;
    float threshold=-1.;
    for (const auto& trigger : m_reqTriggers){
        if( m_SUSYTools->IsTrigPassed(trigger) ){
            for (unsigned int i=0; i<threshold_vec.size(); ++i){
          if(trigger.find( threshold_vec.at(i).first )!=std::string::npos){
        threshold=threshold_vec.at(i).second;
        break;
          }
            }
            if(threshold<0){
          ATH_MSG_VERBOSE("Dijet trigger threshold not anticipated, check METEvtSel::selectdijet() method! Returning false");
          return false;
            }
            //Trigger is fired, threshold is set, vetoing over dijet system pt
            float pt_dijet=( jets->at(0)->pt() + jets->at(1)->pt() ) / 2.;
            if ( pt_dijet - threshold > 0.) match_found = true;
        }
    }
    if(!match_found) return false;
    //Require back to back jets
    float pi=3.14159265358979323846264338327950288;
    float d_phi=fabs( jets->at(0)->phi() - jets->at(1)->phi() ); 
    if (d_phi>pi)
        d_phi -= pi;
    if (d_phi - 2.5 < 0) return false;
    int num_loop=2;
    if (jets->size()>2){
       num_loop=3;
       float third_jet_cut = ( jets->at(0)->pt() + jets->at(1)->pt() ) * 0.2;
       if ( jets->at(2)->pt() - third_jet_cut < 0) return false;
    }
    //JVT requirement on central jets
    static SG::AuxElement::Accessor<float>  acc_jvt("Jvt");
    for(int i=0; i<num_loop; ++i){
       if( (fabs( jets->at(i)->eta()) ) < 2.1 )
          if( (jets->at(i)->pt()) < 50*GeV )
             if( acc_jvt( * (jets->at(i)) ) < 0.64 ) return false;
    }
    return true;
  }

  // select gamma+jet events
  // require at least 1 photon
  bool METEvtSel::selectgjet() //doug
  {

    const xAOD::PhotonContainer* photons = 0;
    ATH_CHECK( evtStore()->retrieve(photons, m_photonColl) );
    if(!photons) return false;
    int nGoodPh=0;
    for( const auto& ph : *photons ) {
      ATH_MSG_VERBOSE("Photon Pt in selection: " << ph->pt() );
	//if( ( *ph ).isAvailable< char >("passOR") ){ // OR is not currently applied
        //if (!dec_passOR(*ph)) continue;
	//std::cout << "     ph pass OR: " << ph->pt() << std::endl;
	++nGoodPh;
	//}
    }
    // require at least 1 photon
    if(nGoodPh==0) return false;

    return true;
  }


} //> end namespace met

///////////////////////// -*- C++ -*- /////////////////////////////
// METPerfTool.cxx
// Implementation file for class METPerfTool
// Author: T.J.Khoo<khoo@cern.ch>
///////////////////////////////////////////////////////////////////

// METPerfTool includes
#include "METPerformance/METPerfTool.h"

// MET EDM
#include "xAODMissingET/MissingETContainer.h"
#include "xAODMissingET/MissingETComposition.h"
#include "xAODMissingET/MissingETAuxContainer.h"
#include "xAODMissingET/MissingETComponentMap.h"

// EDM includes
#include "xAODCore/ShallowCopy.h"
#include "xAODJet/JetContainer.h"
#include "xAODJet/JetAuxContainer.h"
#include "xAODMuon/MuonContainer.h"
#include "xAODMuon/MuonAuxContainer.h"
#include "xAODEgamma/ElectronContainer.h"
#include "xAODEgamma/ElectronAuxContainer.h"
#include "xAODEgamma/PhotonContainer.h"
#include "xAODTau/TauJetContainer.h"
#include "xAODBase/IParticleHelpers.h"
#include "xAODEgamma/EgammaDefs.h"

#include "METUtilities/METHelpers.h"

namespace met {

  using std::vector;

  using namespace xAOD;
  using namespace CP;

  ///////////////////////////////////////////////////////////////////
  // Public methods:
  ///////////////////////////////////////////////////////////////////

  // Constructors
  ////////////////
  METPerfTool::METPerfTool(const std::string& name) :
    AsgTool(name),
    m_origLinkAcc("originalObjectLink")

  {
    //
    // Property declaration
    //
    declareProperty( "EleColl",         m_eleColl    = "ElectronCollection" );
    declareProperty( "GammaColl",       m_gammaColl  = "PhotonCollection"   );
    declareProperty( "TauColl",         m_tauColl    = "TauRecContainer"    );
    declareProperty( "JetColl",         m_jetColl    = "AntiKt4LCTopoJets"  );
    declareProperty( "MuonColl",        m_muonColl   = "Muons"              );
    declareProperty( "METColl",         m_metColl    = "MET_RefTST"         );
    //
    // declareProperty( "EleTerm",         m_eleTerm    = "RefEle"             );
    // declareProperty( "GammaTerm",       m_gammaTerm  = "RefGamma"           );
    // declareProperty( "TauTerm",         m_tauTerm    = "RefTau"             );
    // declareProperty( "JetTerm",         m_jetTerm    = "RefJet"             );
    // declareProperty( "MuonTerm",        m_muonTerm   = "Muons"              );
    // declareProperty( "SoftTerm",        m_softTerm   = "PVSoftTrk"          );
    //
    declareProperty( "SUSYTools",       m_SUSYTools                         );
    declareProperty( "FJVTTool1",       m_fjvtTool1                         );
    declareProperty( "FJVTTool2",       m_fjvtTool2                         );
    declareProperty( "ApplyFJVT",       m_fjvtName =""                      );
    declareProperty("CustomJetEtaForw", m_JetEtaForw         = 2.4          );
    // Tool recognize tag for systematic: avaibable list, JES(m_jesProv), MuonSmear(m_mucalib), MuonEff(m_sf_Tool), ElSmear(m_elcalib), JER(m_jersmearTool)
    declareProperty( "IsSimulation",    m_IsMC = false        );
    declareProperty( "SystematicTag",    m_systematicTag= "Nominal"  );   // Systematic name, nominal as Default

  }

  // Destructor
  ///////////////
  METPerfTool::~METPerfTool()
  {}

  // Athena algtool's Hooks
  ////////////////////////////
  StatusCode METPerfTool::initialize()
  {
    ATH_MSG_INFO ("Initializing " << name() << "...");

    ATH_MSG_INFO("Attempting to retrieve SUSYTools." );
    if (m_SUSYTools.retrieve().isFailure()){
      ATH_MSG_ERROR("Failed to retrieve tool: " << m_SUSYTools.name());
      return StatusCode::FAILURE;
    }
    ATH_MSG_INFO("Retrieved SUSYTools: " << m_SUSYTools.name());

    return StatusCode::SUCCESS;
  }

  StatusCode METPerfTool::finalize()
  {
    ATH_MSG_INFO ("Finalizing " << name() << "...");

    return StatusCode::SUCCESS;
  }

  StatusCode METPerfTool::execute()
  {
    ATH_MSG_DEBUG ( name() << " in execute...");

    // Apply Systematics

    if(m_IsMC && m_systematicTag!="Nominal"){
        CP::SystematicSet shiftSet;
        shiftSet.insert(CP::SystematicVariation(m_systematicTag));
        if(m_SUSYTools->applySystematicVariation(shiftSet) != CP::SystematicCode::Ok){
          ATH_MSG_WARNING("Problem applying systematic");
        }
    }

    // SUSYTools Jets
    xAOD::JetContainer* ST_jets(0);
    xAOD::ShallowAuxContainer* ST_jetsaux(0);
    ATH_CHECK( m_SUSYTools->GetJets(ST_jets,ST_jetsaux, true) );

    m_fjvtTool1->modify(*ST_jets);
    m_fjvtTool2->modify(*ST_jets);

    // add in JVT working point names
    for(const auto &jet : *ST_jets){
      float jvt = -100.0;
      float pt = jet->pt();
      bool gotJVT = jet->getAttribute<float>("Jvt",jvt);
      jet->auxdata<char>("passJVT40T60M120L")=1;
      jet->auxdata<char>("passJVT20T60M120L")=1;
      jet->auxdata<char>("passJVT30T60M120L")=1;
      jet->auxdata<char>("passJVT20T80M120L")=1;
      jet->auxdata<char>("passJVT20T60M90L")=1;
      jet->auxdata<char>("passJVT20T60M150L")=1;
      jet->auxdata<char>("passJVT20T60M120VL")=1;
      jet->auxdata<char>("passJVT20T60M120VLEta25")=1;
      jet->auxdata<char>("passJVT20T60M120VLEta27")=1;
      if(gotJVT){
	if(fabs(jet->eta())<m_JetEtaForw){ // loose 0.11, 0.59, 0.91
	  if((pt<40.0e3 && jvt<0.91) || (pt<60.0e3 && jvt<0.59) || (pt<120.0e3 && jvt<0.11)) jet->auxdata<char>("passJVT40T60M120L")=0; // this is fail
	  //if((pt<60.0e3 && jvt<0.59)) jet->auxdata<char>("passJVT40T60M120L")=0; // this is fail
	  if((pt<20.0e3 && jvt<0.91) || (pt<60.0e3 && jvt<0.59) || (pt<120.0e3 && jvt<0.11)) jet->auxdata<char>("passJVT20T60M120L")=0; // this is fail
	  if((pt<30.0e3 && jvt<0.91) || (pt<60.0e3 && jvt<0.59) || (pt<120.0e3 && jvt<0.11)) jet->auxdata<char>("passJVT30T60M120L")=0; // this is fail
	  if((pt<20.0e3 && jvt<0.91) || (pt<80.0e3 && jvt<0.59) || (pt<120.0e3 && jvt<0.11)) jet->auxdata<char>("passJVT20T80M120L")=0; // this is fail
	  if((pt<20.0e3 && jvt<0.91) || (pt<60.0e3 && jvt<0.59) || (pt<90.0e3 && jvt<0.11)) jet->auxdata<char>("passJVT20T60M90L")=0; // this is fail
	  if((pt<20.0e3 && jvt<0.91) || (pt<60.0e3 && jvt<0.59) || (pt<150.0e3 && jvt<0.11)) jet->auxdata<char>("passJVT20T60M150L")=0; // this is fail
	  if((pt<20.0e3 && jvt<0.91) || (pt<60.0e3 && jvt<0.59) || (pt<120.0e3 && jvt<0.05)){
	    jet->auxdata<char>("passJVT20T60M120VL")=0; // this is fail
	    jet->auxdata<char>("passJVT20T60M120VLEta25")=0; // this is fail
	    jet->auxdata<char>("passJVT20T60M120VLEta27")=0; // this is fail
	  }
	}
	else if(fabs(jet->eta())<2.5){
	  if((pt<120.0e3 && fabs(jvt)<0.05)) jet->auxdata<char>("passJVT20T60M120VLEta25")=0;
	  if((pt<120.0e3 && fabs(jvt)<0.05)) jet->auxdata<char>("passJVT20T60M120VLEta27")=0;
	}
	else if(fabs(jet->eta())<2.7){
	  if((pt<120.0e3 && fabs(jvt)<0.05)) jet->auxdata<char>("passJVT20T60M120VLEta27")=0;
	}
      }
      // applying FJVT
      if(m_fjvtName!=""){
	char passFJVT=1;
	bool gotFJVT = jet->getAttribute<char>(m_fjvtName,passFJVT);
	//std::cout << "Eta: " << jet->eta() << " gotFJVT: " << gotFJVT << " f-jvt " << (passFJVT==1) << std::endl;
	if(gotFJVT && (passFJVT!=1)){
	  jet->auxdata<char>("passJVT40T60M120L")=0;
	  jet->auxdata<char>("passJVT20T60M120L")=0;
	  jet->auxdata<char>("passJVT30T60M120L")=0;
	  jet->auxdata<char>("passJVT20T80M120L")=0;
	  jet->auxdata<char>("passJVT20T60M90L") =0;
	  jet->auxdata<char>("passJVT20T60M150L")=0;
	  jet->auxdata<char>("passJVT20T60M120VL")=0;
	  if(fabs(jet->eta())>2.5) jet->auxdata<char>("passJVT20T60M120VLEta25")=0;
	  if(fabs(jet->eta())>2.7) jet->auxdata<char>("passJVT20T60M120VLEta27")=0;
	}
      }
    }

    if(!m_muonColl.empty()) {
      // SUSYTools muons
      xAOD::MuonContainer* ST_muons(0);
      xAOD::ShallowAuxContainer* ST_muonsaux(0);
      ATH_CHECK( m_SUSYTools->GetMuons(ST_muons,ST_muonsaux, true) );
      addGhostMuonsToJets(*ST_muons, *ST_jets);
    }

    if(!m_eleColl.empty()) {
      // SUSYTools Electrons
      xAOD::ElectronContainer* ST_electrons(0);
      xAOD::ShallowAuxContainer* ST_electronsaux(0);
      ATH_CHECK( m_SUSYTools->GetElectrons(ST_electrons,ST_electronsaux, true) );
    }

    if(!m_gammaColl.empty()) {
      // SUSYTools Photons
      xAOD::PhotonContainer* ST_photons(0);
      xAOD::ShallowAuxContainer* ST_photonsaux(0);
      ATH_CHECK( m_SUSYTools->GetPhotons(ST_photons,ST_photonsaux, true) );
    }

    if(!m_tauColl.empty()) {
      // SUSYTools Taus
      xAOD::TauJetContainer* ST_taujets(0);
      xAOD::ShallowAuxContainer* ST_taujetsaux(0);
      ATH_CHECK( m_SUSYTools->GetTaus(ST_taujets,ST_taujetsaux, true) );
    }

    return StatusCode::SUCCESS;
  }

} //> end namespace met

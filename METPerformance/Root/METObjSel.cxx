///////////////////////// -*- C++ -*- /////////////////////////////
// METObjSel.cxx
// Implementation file for class METObjSel
// Author: T.J.Khoo<khoo@cern.ch>
///////////////////////////////////////////////////////////////////

// METObjSel includes
#include "METPerformance/METObjSel.h"
#include "METPerformance/METPerfHelper.h"
#include "METPerformance/JetID.h"

// EDM includes
#include "xAODCore/ShallowCopy.h"
#include "xAODCore/AuxContainerBase.h"
#include "xAODJet/JetContainer.h"
#include "xAODJet/JetAuxContainer.h"
#include "xAODMuon/MuonContainer.h"
#include "xAODEgamma/ElectronContainer.h"
#include "xAODTau/TauJetContainer.h"
#include "xAODEgamma/PhotonContainer.h"
#include "xAODEgamma/EgammaDefs.h"

namespace met {

  static SG::AuxElement::ConstAccessor<char> dec_baseline("baseline");
  static SG::AuxElement::ConstAccessor<char> dec_isol("isol");
  static SG::AuxElement::ConstAccessor<char> dec_signal("signal");
  static SG::AuxElement::ConstAccessor<char> dec_bad("bad"); // bad jet

  using std::vector;
  using namespace xAOD;

  ///////////////////////////////////////////////////////////////////
  // Public methods:
  ///////////////////////////////////////////////////////////////////

  // Constructors
  ////////////////
  METObjSel::METObjSel(const std::string& name) :
    AsgTool(name)
  {
    //
    // Property declaration
    //
    declareProperty( "EleColl",         m_eleColl     = "STCalibElectrons" );
    declareProperty( "JetColl",         m_jetColl     = "STCalibAntiKt4EMTopoJets"      );
    declareProperty( "MuonColl",        m_muonColl    = "STCalibMuons"     );
    declareProperty( "TauColl",         m_tauColl     = "STCalibTauJets"      );
    declareProperty( "PhotonColl",      m_photonColl  = "STCalibPhotons"   );
    declareProperty( "OutputMuons",     m_outputMuons = "GoodMuons"      );
    declareProperty( "OutputTaus",      m_outputTaus  = "GoodTauJets"    );
    declareProperty( "OutputPhotons",   m_outputPhotons  = "GoodPhotons" );

    declareProperty( "doJetCleaning",   m_doJetCleaning = true );

    declareProperty( "SUSYTools",       m_SUSYTools                         );

  }

  // Destructor
  ///////////////
  METObjSel::~METObjSel()
  {}

  // Athena algtool's Hooks
  ////////////////////////////
  StatusCode METObjSel::initialize()
  {
    ATH_MSG_INFO ("Initializing " << name() << "...");

    ATH_MSG_INFO("Attempting to retrieve SUSYTools." );
    if (m_SUSYTools.retrieve().isFailure()){
      ATH_MSG_ERROR("Failed to retrieve tool: " << m_SUSYTools.name());
      return StatusCode::FAILURE;
    }
    ATH_MSG_INFO("Retrieved SUSYTools: " << m_SUSYTools.name());

    return StatusCode::SUCCESS;
  }

  StatusCode METObjSel::finalize()
  {
    ATH_MSG_INFO ("Finalizing " << name() << "...");
    ATH_CHECK(m_SUSYTools.release());
    return StatusCode::SUCCESS;
  }

  StatusCode METObjSel::execute()
  {
    ATH_MSG_DEBUG ( name() << " in execute...");

    // We need to check for PV before OR
    bool foundPV = false;
    const VertexContainer* pvertices = 0;
    ATH_CHECK( evtStore()->retrieve( pvertices, "PrimaryVertices" ) );
    for(const auto& vx : *pvertices) {
      if( vx->vertexType() == xAOD::VxType::PriVtx ) {
        foundPV = true;
        break;
      }
    }

    const JetContainer* jets = 0;
    ATH_CHECK( evtStore()->retrieve(jets, m_jetColl) );

    const MuonContainer* muons = 0;
    ATH_CHECK( evtStore()->retrieve(muons, m_muonColl) );

    const ElectronContainer* electrons = 0;
    ATH_CHECK( evtStore()->retrieve(electrons, m_eleColl) );

    //Retrieve jets
    JetContainer* goodjets = new JetContainer();
    JetAuxContainer* goodjetsAux = new JetAuxContainer;
    ATH_CHECK( evtStore()->record(goodjets, "GoodJets") );
    ATH_CHECK( evtStore()->record(goodjetsAux, "GoodJetsAux.") );
    goodjets->setStore(goodjetsAux);

    //Retrieve muons
    MuonContainer* goodmuons = new MuonContainer();
    AuxContainerBase* goodmuonsAux = new AuxContainerBase;
    ATH_CHECK( evtStore()->record(goodmuons, m_outputMuons) );
    ATH_CHECK( evtStore()->record(goodmuonsAux, m_outputMuons+"Aux.") );
    goodmuons->setStore(goodmuonsAux);

    // Retrieve tau
    const TauJetContainer* taus = 0;
    ATH_CHECK( evtStore()->retrieve(taus, m_tauColl) );
    ATH_MSG_DEBUG( "Number of taus: " << taus->size() );
    TauJetContainer* goodtaus = new TauJetContainer();
    AuxContainerBase* goodtausAux = new AuxContainerBase;
    ATH_CHECK( evtStore()->record(goodtaus, m_outputTaus) );
    ATH_CHECK( evtStore()->record(goodtausAux, m_outputTaus+"Aux.") );
    goodtaus->setStore(goodtausAux);

    //Retrieve photons
    const xAOD::PhotonContainer* photons = 0;
    ATH_CHECK( evtStore()->retrieve(photons, m_photonColl) );
    PhotonContainer* goodphotons = new PhotonContainer;
    AuxContainerBase* goodphotonsAux = new AuxContainerBase;
    ATH_CHECK( evtStore()->record(goodphotons, m_outputPhotons) );
    ATH_CHECK( evtStore()->record(goodphotonsAux, m_outputPhotons+"Aux.") );
    goodphotons->setStore(goodphotonsAux);

    //Retrieve electrons
    ElectronContainer* goodelectrons = new ElectronContainer();
    AuxContainerBase* goodelectronsAux = new AuxContainerBase;
    ATH_CHECK( evtStore()->record(goodelectrons, "GoodElectrons") );
    ATH_CHECK( evtStore()->record(goodelectronsAux, "GoodElectronsAux.") );
    goodelectrons->setStore(goodelectronsAux);

    if(!foundPV) return StatusCode::SUCCESS;

    // SUSYTools overlap removal between jets, electrons, muons
    // This decorates objects with dec_passOR
    ATH_MSG_DEBUG("Doing overlap removal through SUSYTools.");
    ATH_CHECK(m_SUSYTools->OverlapRemoval(electrons, muons, jets ));

    //Do jets
    ConstJetContainer* badjets = new ConstJetContainer(SG::VIEW_ELEMENTS);
    ATH_CHECK( evtStore()->record(badjets,  "BadJets" ) );
    for (const auto& jet : *jets) {
      if(m_doJetCleaning) {
          if      (dec_bad(*jet))    badjets->push_back(jet);
          else if (dec_signal(*jet)) {
            Jet* newJet = new Jet;
            goodjets->push_back(newJet);
            *newJet = *jet;
          }
      }
      else {
          Jet* newJet = new Jet;
          goodjets->push_back(newJet);
          *newJet = *jet;
      }
    }

    // Do muons
    for (const auto& mu : *muons) {
      ATH_MSG_DEBUG("Muon pT: " << mu->pt());
      ATH_MSG_DEBUG("Muon eta: " << mu->eta());
      if (!dec_baseline(*mu)) {
        ATH_MSG_DEBUG("Muon fails baseline selections.");
        continue;
      }
      if (!dec_isol(*mu)) {
        ATH_MSG_DEBUG("Muon fails isolation selection tool.");
      }
      if (dec_signal(*mu)) {
        xAOD::Muon* newMu = new xAOD::Muon;
        goodmuons->push_back(newMu);
        *newMu = *mu;
      }
    }
    ATH_MSG_DEBUG("Selected " << goodmuons->size() << " muons");

    //Do tau
    for (const auto& tau : *taus) {
      if (dec_baseline(*tau)) {
        TauJet* newTau = new TauJet;
        goodtaus->push_back(newTau);
        *newTau = *tau;
      }
    }
    ATH_MSG_DEBUG("Selected " << goodtaus->size() << " taus");

    //Do photon
    for (const auto& ph : *photons) {
      if (dec_signal(*ph)) {
        Photon* newPh = new Photon;
        goodphotons->push_back(newPh);
        *newPh = *ph;
      }
    }
    ATH_MSG_DEBUG("Selected " << goodphotons->size() << " photons");

    // Do electrons
    for (const auto& el : *electrons) {
      ATH_MSG_DEBUG("Electron pT: " << el->pt());
      if (!dec_baseline(*el)) {
        ATH_MSG_DEBUG("Electron fails baseline selections.");
        continue;
      }
      if (!dec_isol(*el)) {
        ATH_MSG_DEBUG("Electron fails isolation selection tool.");
      }
      if (dec_signal(*el)) {
        Electron* newEl = new Electron;
        goodelectrons->push_back(newEl);
        *newEl = *el;
      }
    }
    ATH_MSG_DEBUG("Selected " << goodelectrons->size() << " electrons");


    return StatusCode::SUCCESS;
  }

} //> end namespace met

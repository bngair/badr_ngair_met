///////////////////////// -*- C++ -*- /////////////////////////////
// METPerfTool.h
// Header file for class METPerfTool
//
// This runs the calibrations, MET rebuilding, and any other
// preprocessing needed before the analysis alg makes histograms
//
//  * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
//
// Author: TJ Khoo
///////////////////////////////////////////////////////////////////
#ifndef METPERFORMANCE_METPERFTOOL_H
#define METPERFORMANCE_METPERFTOOL_H 1

// STL includes
#include <string>

// FrameWork includes
#include "AsgTools/ToolHandle.h"
#include "AsgTools/AsgTool.h"
#include "METPerformance/IMETPerfTool.h"
#include "JetInterface/IJetModifier.h"

// EDM includes
//#include "xAODMissingET/MissingETContainer.h"

// Tool includes
#include "SUSYTools/ISUSYObjDef_xAODTool.h"

namespace met{

  class METPerfTool
    : virtual public asg::AsgTool,
      virtual public IMETPerfTool
  {
    // This macro defines the constructor with the interface declaration
    ASG_TOOL_CLASS(METPerfTool, IMETPerfTool)


    ///////////////////////////////////////////////////////////////////
    // Public methods:
    ///////////////////////////////////////////////////////////////////
    public:

    METPerfTool(const std::string& name);

    // AsgTool Hooks
    virtual StatusCode initialize();
    virtual StatusCode execute();
    virtual StatusCode finalize();

    ///////////////////////////////////////////////////////////////////
    // Const methods:
    ///////////////////////////////////////////////////////////////////

    ///////////////////////////////////////////////////////////////////
    // Non-const methods:
    ///////////////////////////////////////////////////////////////////

    ///////////////////////////////////////////////////////////////////
    // Private data:
    ///////////////////////////////////////////////////////////////////
  private:

    //virtual StatusCode decLLHElectron(const xAOD::Electron& el);

    /// Default constructor:
    METPerfTool();
    virtual ~METPerfTool();

    std::string m_eleColl;
    std::string m_gammaColl;
    std::string m_tauColl;
    std::string m_jetColl;
    std::string m_muonColl;
    std::string m_metColl;

    // std::string m_eleTerm;
    // std::string m_gammaTerm;
    // std::string m_tauTerm;
    // std::string m_jetTerm;
    // std::string m_muonTerm;
    // std::string m_softTerm;

    std::string m_systematicTag;
    float m_variation;
    bool m_IsMC;
    std::string m_sysToolTag; // identy tag for systematics

    SG::AuxElement::Accessor<ElementLink<xAOD::IParticleContainer> > m_origLinkAcc;

    ToolHandle<ST::ISUSYObjDef_xAODTool> m_SUSYTools;
    ToolHandle<IJetModifier> m_fjvtTool1;
    ToolHandle<IJetModifier> m_fjvtTool2;
    std::string m_fjvtName;
    double m_JetEtaForw;
  };

}


#endif //> !METPERFXAOD_METPERFTOOL_H

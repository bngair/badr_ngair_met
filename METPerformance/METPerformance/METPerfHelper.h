// -*- c++ -*-
// METPerfHelper.h

#ifndef METPerfHelper_H
#define METPerfHelper_H

#include "xAODMissingET/MissingET.h"
#include "AthContainers/ConstDataVector.h"
#include "xAODMuon/MuonContainer.h"
#include "xAODJet/JetContainer.h"
#include "xAODEgamma/ElectronContainer.h"
#include "xAODTau/TauJetContainer.h"
#include "xAODEgamma/PhotonContainer.h"
#include "TVector2.h"

namespace met {

  typedef ConstDataVector<xAOD::JetContainer> ConstJetContainer;
  typedef ConstDataVector<xAOD::MuonContainer> ConstMuonContainer;
  typedef ConstDataVector<xAOD::ElectronContainer> ConstElectronContainer;
  typedef ConstDataVector<xAOD::TauJetContainer> ConstTauJetContainer;
  typedef ConstDataVector<xAOD::PhotonContainer> ConstPhotonContainer;

  enum EvtType {
    None=0,
    Zmm,
    Zee,
    Wmv,
    Wev,
    ttbar,
    dijet,
    gjet
  };

  static inline xAOD::MissingET projectMET(const xAOD::MissingET& met, const xAOD::MissingET& axis) {
    double mag = axis.met();
    double projx = (met.mpx()*axis.mpx() + met.mpy()*axis.mpy()) / mag;
    double projy = (met.mpx()*axis.mpy() - met.mpy()*axis.mpx()) / mag;
    return xAOD::MissingET(projx, projy, 0.);
  }

}

#endif

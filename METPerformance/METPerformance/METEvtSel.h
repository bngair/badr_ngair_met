///////////////////////// -*- C++ -*- /////////////////////////////
// METEvtSel.h
// Header file for class METEvtSel
//
// This selects events for MET performance studies
//
//  * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
//
// Author: TJ Khoo
///////////////////////////////////////////////////////////////////
#ifndef METPERFORMANCE_METEVTSEL_H
#define METPERFORMANCE_METEVTSEL_H 1

// STL includes
#include <string>

// FrameWork includes
#include "AsgTools/ToolHandle.h"
#include "AsgTools/AsgTool.h"
#include "METPerformance/IMETEvtSel.h"

// EDM includes
//#include "xAODMissingET/MissingETContainer.h"

// Tool includes
#include "METInterface/IMETRebuilder.h"
#include "SUSYTools/ISUSYObjDef_xAODTool.h"

namespace met{

  class METEvtSel
    : virtual public asg::AsgTool,
      virtual public IMETEvtSel
  {
    // This macro defines the constructor with the interface declaration
    ASG_TOOL_CLASS(METEvtSel, IMETEvtSel)

    ///////////////////////////////////////////////////////////////////
    // Public methods:
    ///////////////////////////////////////////////////////////////////
    public:

    METEvtSel(const std::string& name);

    // AsgTool Hooks
    virtual StatusCode initialize();
    virtual StatusCode execute();
    virtual StatusCode finalize();

    virtual bool doPrimaryVertex();
    virtual bool doEventCleaning();
    virtual bool doJetCleaning();
    virtual bool selectTrigger(std::vector<std::string>& triggers);
    virtual bool selectEvent(EvtType type);

    virtual bool selectZmumu();
    virtual bool selectZee();
    virtual bool selectWmunu();
    virtual bool selectWenu();
    virtual bool selectttbar();
    virtual bool selectdijet();
    virtual bool selectgjet();

    ///////////////////////////////////////////////////////////////////
    // Private data:
    ///////////////////////////////////////////////////////////////////
  private:

    /// Default constructor:
    METEvtSel();
    virtual ~METEvtSel();

    //ToolHandle<IAthSelectorTool> m_trips;

    ToolHandle<ST::ISUSYObjDef_xAODTool> m_SUSYTools;

    std::string m_eleColl;
    std::string m_jetColl;
    std::string m_badjetColl;
    std::string m_muonColl;
    std::string m_photonColl;
    std::string m_metColl;

    std::vector<std::string> m_reqTriggers;
    bool                     m_doTriggerDecision;
    bool                     m_reqTriggerMatch;
    bool                     m_reqJetCleaning;
    float                    m_metCut;
    unsigned short m_evttype;
    bool m_doEverySelection;
//    double musf;

    SG::AuxElement::Decorator<bool> m_isPVDec;
    SG::AuxElement::Decorator<bool> m_evtCleanDec;
    SG::AuxElement::Decorator<bool> m_jetCleanDec;
    SG::AuxElement::Decorator<bool> m_isZmmDec;
    SG::AuxElement::Decorator<bool> m_isZeeDec;
    SG::AuxElement::Decorator<bool> m_isWmvDec;
    SG::AuxElement::Decorator<bool> m_isWevDec;
    SG::AuxElement::Decorator<bool> m_isttbarDec;
    SG::AuxElement::Decorator<bool> m_isdijetDec;
    SG::AuxElement::Decorator<bool> m_isgjetDec;
    SG::AuxElement::Decorator<bool> m_trigDec;

//    SG::AuxElement::Decorator<char> m_isBjetDec;

//    SG::AuxElement::Decorator<double> m_muEffSF;
//    SG::AuxElement::Decorator<double> m_elEffSF;

  };

}

// I/O operators
//////////////////////

///////////////////////////////////////////////////////////////////
// Inline methods:
///////////////////////////////////////////////////////////////////


#endif //> !METPERFXAOD_METPERFTOOL_H

///////////////////////// -*- C++ -*- /////////////////////////////
// IMETPerfTool.h
// Header file for interface IMETPerfTool
//
// This tool exists to set up all the collections needed to make
// MET performance histograms
//
//  * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
//
// Author: TJ Khoo
///////////////////////////////////////////////////////////////////

#ifndef METPERFXAOD_IMETPERFTOOL_H
#define METPERFXAOD_IMETPERFTOOL_H

#include "AsgTools/IAsgTool.h"
#include "xAODEgamma/Electron.h"

class IMETPerfTool : virtual public asg::IAsgTool {
  ASG_TOOL_INTERFACE(IMETPerfTool)

  public:

  virtual StatusCode execute() = 0;

  private:
  //virtual StatusCode decLLHElectron(const xAOD::Electron& el) = 0;

};

#endif

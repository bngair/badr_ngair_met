///////////////////////// -*- C++ -*- /////////////////////////////
// METObjSel.h
// Header file for class METObjSel
//
// This selects objects for MET performance studies
//
//  * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
//
// Author: P Loch, S Resconi, TJ Khoo
///////////////////////////////////////////////////////////////////
#ifndef METPERFORMANCE_METOBJSEL_H
#define METPERFORMANCE_METOBJSEL_H 1

// STL includes
#include <string>

// FrameWork includes
#include "AsgTools/ToolHandle.h"
#include "AsgTools/AsgTool.h"
#include "METPerformance/IMETObjSel.h"

// EDM includes

// Tool includes
#include "SUSYTools/ISUSYObjDef_xAODTool.h"

namespace met{

  class METObjSel
    : virtual public asg::AsgTool,
      virtual public IMETObjSel
  {
    // This macro defines the constructor with the interface declaration
    ASG_TOOL_CLASS(METObjSel, IMETObjSel)


    ///////////////////////////////////////////////////////////////////
    // Public methods:
    ///////////////////////////////////////////////////////////////////
    public:

    METObjSel(const std::string& name);

    // AsgTool Hooks
    virtual StatusCode initialize();
    virtual StatusCode execute();
    virtual StatusCode finalize();

  private:

    /// Default constructor:
    METObjSel();
    virtual ~METObjSel();

    ToolHandle<ST::ISUSYObjDef_xAODTool> m_SUSYTools;

    std::string m_eleColl;
    std::string m_jetColl;
    std::string m_muonColl;
    std::string m_tauColl;
    std::string m_photonColl;
    std::string m_outputMuons;
    std::string m_outputTaus;
    std::string m_outputPhotons;

    bool m_doJetCleaning;

  };

}

// I/O operators
//////////////////////

///////////////////////////////////////////////////////////////////
// Inline methods:
///////////////////////////////////////////////////////////////////


#endif //> !METPERFXAOD_METPERFTOOL_H

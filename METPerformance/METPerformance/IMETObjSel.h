///////////////////////// -*- C++ -*- /////////////////////////////
// IMETObjSel.h
// Header file for interface IMETObjSel
//
// This tool exists to set up all the collections needed to make
// MET performance histograms
//
//  * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
//
// Author: TJ Khoo
///////////////////////////////////////////////////////////////////

#ifndef METPERFORMANCE_IMETOBJSEL_H
#define METPERFORMANCE_IMETOBJSEL_H

#include "AsgTools/IAsgTool.h"
#include "xAODMuon/Muon.h"
#include "xAODJet/Jet.h"
#include "xAODEgamma/Electron.h"
#include "xAODEgamma/Photon.h"

class IMETObjSel : virtual public asg::IAsgTool {
  ASG_TOOL_INTERFACE(IMETObjSel)

  public:

  virtual StatusCode execute() = 0;

};

#endif

///////////////////////// -*- C++ -*- /////////////////////////////
// IMETCalculator.h
// Header file for interface IMETCalculator
//
// This tool exists to set up all the collections needed to make
// MET performance histograms
//
//  * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
//
// Author: TJ Khoo
///////////////////////////////////////////////////////////////////

#ifndef METPERFORMANCE_IMETCALCULATOR_H
#define METPERFORMANCE_IMETCALCULATOR_H

#include "AsgTools/IAsgTool.h"
//#include "xAODMuon/Muon.h"
//#include "xAODJet/Jet.h"
//#include "xAODEgamma/ElectronFwd.h"

namespace met{
class IMETCalculator: virtual public asg::IAsgTool {
  ASG_TOOL_INTERFACE(IMETCalculator)

  public:

  virtual StatusCode execute() = 0;

};

}
#endif

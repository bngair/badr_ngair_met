///////////////////////// -*- C++ -*- /////////////////////////////
// IMETEvtSel.h
// Header file for interface IMETEvtSel
//
// This tool exists to set up all the collections needed to make
// MET performance histograms
//
//  * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
//
// Author: TJ Khoo
///////////////////////////////////////////////////////////////////

#ifndef METPERFORMANCE_IMETEVTSEL_H
#define METPERFORMANCE_IMETEVTSEL_H

#include "AsgTools/IAsgTool.h"
#include "METPerformance/METPerfHelper.h"

class IMETEvtSel : virtual public asg::IAsgTool {
  ASG_TOOL_INTERFACE(IMETEvtSel)

  public:

  virtual StatusCode execute() = 0;

  virtual bool doEventCleaning() = 0;
  virtual bool doJetCleaning() = 0;
  virtual bool selectEvent(met::EvtType type) = 0;

};

#endif

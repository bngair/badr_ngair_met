///////////////////////// -*- C++ -*- /////////////////////////////
// METCalculator.h
// Header file for class METObjSel
//
// This MET calculation for MET performance studies
//
//  * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
//
// Author: P Loch, S Resconi, TJ Khoo
///////////////////////////////////////////////////////////////////
#ifndef METPERFORMANCE_METCALCULATOR_H
#define METPERFORMANCE_METCALCULATOR_H 1

// STL includes
#include <string>

// FrameWork includes
#include "AsgTools/ToolHandle.h"
#include "AsgTools/AsgTool.h"
#include "METPerformance/IMETCalculator.h"

// EDM includes
//#include "xAODMissingET/MissingETContainer.h"

// Tool includes
#include "METInterface/IMETMaker.h"
#include "METInterface/IMETSystematicsTool.h"

namespace met{

  enum METType {
    CST,
    TST,
    TST_frac00pt00,
    TST_frac00pt10,
    TST_frac00pt20,
    TST_frac00pt30,
    TST_frac02pt00,
    TST_frac02pt10,
    TST_frac02pt20,
    TST_frac02pt30,
    TST_frac05pt00,
    TST_frac05pt10,
    TST_frac05pt20,
    TST_frac05pt30,
    TST_0p3,
    TST_0p4,
    TST_0p5,
    TST_0p59,
    Track
  };

  class METCalculator
    : virtual public asg::AsgTool,
      virtual public IMETCalculator
  {
    // This macro defines the constructor with the interface declaration
    ASG_TOOL_CLASS(METCalculator, IMETCalculator)


    ///////////////////////////////////////////////////////////////////
    // Public methods:
    ///////////////////////////////////////////////////////////////////
    public:

    METCalculator(const std::string& name);
    ~METCalculator();

    // AsgTool Hooks
    virtual StatusCode initialize();
    virtual StatusCode execute();
    virtual StatusCode finalize();

    ///////////////////////////////////////////////////////////////////
    // Private data:
    ///////////////////////////////////////////////////////////////////
  private:

    ToolHandle<IMETMaker> m_metMakerTool;
    ToolHandle<IMETSystematicsTool> m_metSystTool;

    std::string m_muContName;
    std::string m_elContName;
    std::string m_jetContName;
    std::string m_phContName;
    std::string m_tauContName;

    std::string m_metContName;

    std::string m_metMapName;
    std::string m_metCoreName;
    /// Default constructor:

    bool m_doJVT;
    bool m_doTruth;
    bool m_doEleJetOlapScan;
    bool m_doPFlowJVTScan;
    bool m_doJVTScan;
    std::string m_mettype;
    METType m_mettype_enum;

    std::string m_metsysTerm;
  };

}

// I/O operators
//////////////////////

///////////////////////////////////////////////////////////////////
// Inline methods:
///////////////////////////////////////////////////////////////////


#endif //> !METPERFXAOD_METPERFTOOL_H

#ifndef CUTFLOW_DEF
#define CUTFLOW_DEF

#include <string>

namespace met{

  enum CutFlow {
    PassGRL = 0,
    PassPV,
    PassTrigDec,
    PassEvtCleaning,
    PassJetCleaning,
    PassZmmSel,
    PassZeeSel,
    PassWmvSel,
    PassWevSel,
    PassttbarSel,
    PassdijetSel,
    PassgjetSel,
    END_OF_CUTFLOW
  };

  const std::string cutflow_bin_name(int ibin) {
    return cutflow_bin_name(static_cast<CutFlow>(ibin));
  }

  const std::string cutflow_bin_name(CutFlow bin) {
    switch (bin) {
      case PassGRL:         return "PassGRL";
      case PassPV:          return "PassPV";
      case PassTrigDec:     return "PassTrigDec";
      case PassEvtCleaning: return "PassEvtCleaning";
      case PassJetCleaning: return "PassJetCleaning";
      case PassZmmSel:      return "PassZmmSel";
      case PassZeeSel:      return "PassZeeSel";
      case PassWmvSel:      return "PassWmvSel";
      case PassWevSel:      return "PassWevSel";
      case PassttbarSel:    return "PassttbarSel";
      case PassdijetSel:    return "PassdijetSel";
      case PassgjetSel:    return "PassgjetSel";
      default: return "Unknown";
    };
    return "Unknown";
  }
}

#endif

#ifndef METPERFORMANCE_METSLIMALG_ANALG_H
#define METPERFORMANCE_METSLIMALG_ANALG_H 1

#include "AthAnalysisBaseComps/AthAnalysisAlgorithm.h" // now METSlimAlg is defined as an AthAnalysisAlgorithm and not AthAlgorithm

#include "GaudiKernel/ToolHandle.h"
#include "AthContainers/AuxElement.h"
#include "AsgTools/ToolHandle.h"

#include "SUSYTools/ISUSYObjDef_xAODTool.h"

class TH1;

class IGoodRunsListSelectionTool;

class IMETPerfTool;
class IMETObjSel;
class IMETEvtSel;

namespace met {

class IMETCalculator;

class METSlimAlg_AnAlg: public ::AthAnalysisAlgorithm {
 public:

    /// Constructor with parameters:
  METSlimAlg_AnAlg( const std::string& name, ISvcLocator* pSvcLocator );

  virtual ~METSlimAlg_AnAlg();

    /// Athena algorithm's Hooks
  virtual StatusCode  initialize();
  virtual StatusCode  execute();
  virtual StatusCode  finalize();

  virtual StatusCode beginInputFile();

  private:
    std::string RemoveHLTPrefix(const std::string& triggerline);
    std::string GetElectronSFString();
    double getElectronTriggerScaleFactor();
    double getMuonTriggerScaleFactor();

    bool m_docalib;
    bool m_doselection;
    bool m_doskim;
    bool m_isSimulation;
    bool m_docutbk;
    bool m_doprw;
    bool m_dogrl;
    bool m_is2015plus2016;
    float m_LowRN;
    float m_HighRN;

    std::vector<std::string> m_triggers;
    std::string m_triggerScaleFactor;

    std::string m_metsuffix;
    std::string m_jetterm;
    std::string m_softterm;

    std::string m_jetcoll;
    std::string m_elecoll;
    std::string m_muoncoll;

    SG::AuxElement::Decorator<bool> m_passAllEvtSelections;
    SG::AuxElement::Decorator<bool> m_isPVDec;
    SG::AuxElement::Decorator<bool> m_evtCleanDec;
    SG::AuxElement::Decorator<bool> m_jetCleanDec;
    SG::AuxElement::Decorator<bool> m_isZmmDec;
    SG::AuxElement::Decorator<bool> m_isZeeDec;
    SG::AuxElement::Decorator<bool> m_isWmvDec;
    SG::AuxElement::Decorator<bool> m_isWevDec;
    SG::AuxElement::Decorator<bool> m_isttbarDec;
    SG::AuxElement::Decorator<bool> m_isdijetDec;
    SG::AuxElement::Decorator<bool> m_isgjetDec;
    SG::AuxElement::Decorator<bool> m_trigDec;

    ToolHandle<ST::ISUSYObjDef_xAODTool> m_SUSYTools;

    /// Athena configured tools
    ToolHandle<IMETPerfTool> m_perftool;
    ToolHandle<IMETObjSel> m_objsel;
    ToolHandle<IMETEvtSel> m_evtsel;

    ToolHandle<IGoodRunsListSelectionTool> m_grltool;
    ToolHandleArray<met::IMETCalculator> m_metCalTools;


 };

}
#endif //> !METPERFORMANCE_METSLIMALG_ANALG_H

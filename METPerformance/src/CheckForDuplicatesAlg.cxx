#include "CheckForDuplicatesAlg.h"

#include "GaudiKernel/ITHistSvc.h"
#include "xAODEventInfo/EventInfo.h"
#include "TTree.h"


CheckForDuplicatesAlg::CheckForDuplicatesAlg( const std::string& name, ISvcLocator* pSvcLocator )
  : AthAlgorithm( name, pSvcLocator ),
    m_eventTree(0) {
  declareProperty( "StreamName", m_streamName );

}


CheckForDuplicatesAlg::~CheckForDuplicatesAlg() {}


StatusCode CheckForDuplicatesAlg::initialize() {
  ATH_MSG_INFO ("Initializing " << name() << "...");

  ServiceHandle<ITHistSvc> histSvc("THistSvc",name());
  CHECK( histSvc.retrieve() );

  m_eventTree = new TTree("eventTree","eventTree");
  CHECK( histSvc->regTree("/"+m_streamName+"/eventTree",m_eventTree) );

  m_eventTree->Branch("dsid",        &m_dsid,        "dsid/I"       );
  m_eventTree->Branch("runNumber",   &m_runNumber,   "runNumber/I"  );
  m_eventTree->Branch("eventNumber", &m_eventNumber, "eventNumber/I");

  return StatusCode::SUCCESS;
}

StatusCode CheckForDuplicatesAlg::finalize() {
  ATH_MSG_INFO ("Finalizing " << name() << "...");

  return StatusCode::SUCCESS;
}

StatusCode CheckForDuplicatesAlg::execute() {
  ATH_MSG_DEBUG ("Executing " << name() << "...");

  m_dsid        = -1;
  m_runNumber   = -1;
  m_eventNumber = -1;

  const xAOD::EventInfo* eventinfo = 0;
  ATH_CHECK( evtStore()->retrieve( eventinfo, "EventInfo" ) );

  if(eventinfo->eventType(xAOD::EventInfo::IS_SIMULATION)) {
    m_dsid = eventinfo->mcChannelNumber();
  }
  else {
    m_dsid   = eventinfo->runNumber();
  }
  m_runNumber   = eventinfo->runNumber();
  m_eventNumber = eventinfo->eventNumber();

  m_eventTree->Fill();

  return StatusCode::SUCCESS;
}



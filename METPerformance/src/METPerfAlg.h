// -*- c++ -*-
// METPerfAlg.h

#ifndef METPerfAlg_H
#define METPerfAlg_H

#include "AthenaBaseComps/AthHistogramAlgorithm.h"
#include "xAODMissingET/MissingETContainer.h"
#include "METPerformance/METPerfHelper.h"

#include "METInterface/IMETSignificance.h"

//#include "GaudiKernel/ToolHandle.h"

//#include "AsgTools/ToolHandle.h"
#include "AsgTools/AnaToolHandle.h"

namespace met {

  typedef ElementLink<xAOD::IParticleContainer> iplink_t;
  typedef ElementLink<xAOD::IParticleContainer> obj_link_t;

  class METPerfAlg : public AthHistogramAlgorithm {

  public:

    /// Constructor with parameters:
    METPerfAlg(const std::string& name, ISvcLocator* pSvcLocator);

    /// Destructor:
    ~METPerfAlg();

    /// Athena algorithm's Hooks
    StatusCode  initialize();
    StatusCode  execute();
    StatusCode  finalize();

  private:

    /// Default constructor:
    METPerfAlg();

    // fill plots
    //StatusCode plotBasics(const xAOD::MissingETContainer* metperf, double weight);
    StatusCode plotBasics(const xAOD::MissingETContainer* metperf, double weight, double avgmu);
    //StatusCode plotTerms(const xAOD::MissingETContainer* met, double weight);
    StatusCode plotTerms(const xAOD::MissingETContainer* met, double weight, double avgmu);
    StatusCode plotPerformance(const xAOD::MissingETContainer* metperf,
    			       double avgmu, unsigned int npv, double weight); // use this method if running over SUSY5
    //StatusCode plotPerformance(const xAOD::MissingETContainer* metperf,
    //			       double lht_sumet, double avgmu, unsigned int npv, double weight);
    StatusCode plotPerfZ(const xAOD::MissingETContainer* metperf, double weight);
    StatusCode plotforChecks(const xAOD::MissingETContainer* met,const xAOD::MissingETContainer* metperf , double weight);
    std::string histname(const std::string& name);

    std::string m_metsuffix;
    std::string m_jetterm;
    std::string m_softterm;
    std::string m_eleterm;
    std::string m_muonterm;
    std::string m_gammaterm;
    std::string m_tauterm;
std::string m_jetConstitScaleMom;

    std::string m_jetcoll;
    std::string m_elecoll;
    std::string m_muoncoll;

    unsigned short m_evttype;
    bool m_dojetveto;
    bool m_dofwdjetveto;
    bool m_doTauGammaPlots;
    bool m_jetBins;
    bool m_writetree;

    double m_wlnmetcut;
    double m_jetMinPt;
    double m_fwdJetMinPt;

    bool m_doPileupReweighting; //claire

    bool m_noTruthPthard;

    bool m_doMuonElossPlots;
    bool m_applyTriggerSF;
    bool m_doEleJetOlapScan;
    bool m_doPlotTracks;

    SG::AuxElement::ConstAccessor<bool> m_passAllEvtSelections;
    SG::AuxElement::ConstAccessor<bool> m_isPVAcc;
    SG::AuxElement::ConstAccessor<bool> m_evtCleanAcc;
    SG::AuxElement::ConstAccessor<bool> m_jetCleanAcc;
    SG::AuxElement::ConstAccessor<bool> m_isZmmAcc;
    SG::AuxElement::ConstAccessor<bool> m_isZeeAcc;
    SG::AuxElement::ConstAccessor<bool> m_isWmvAcc;
    SG::AuxElement::ConstAccessor<bool> m_isWevAcc;
    SG::AuxElement::ConstAccessor<bool> m_isttbarAcc;
    SG::AuxElement::ConstAccessor<bool> m_isdijetAcc;
    SG::AuxElement::ConstAccessor<bool> m_isgjetAcc;
  //  SG::AuxElement::ConstAccessor<char> m_isBjetDec;
    SG::AuxElement::ConstAccessor<obj_link_t>  m_objLinkAcc;
    SG::AuxElement::Decorator< std::vector<iplink_t > > m_constitObjLinks;

    // Branches of the tree
    Double_t m_pthard;
    Double_t m_residt_pthard;
    Double_t m_residl_pthard;
    Double_t m_avgmu;
    Double_t m_weight;
    Double_t m_PRWdatasf;
    Int_t m_njets;

    // met significance
    asg::AnaToolHandle<IMETSignificance> m_metSignif;

  };

}


#endif

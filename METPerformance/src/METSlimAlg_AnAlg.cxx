// METPerformance includes
#include "METSlimAlg_AnAlg.h"
#include "METPerformance/cutflow_def.h"

#include "AsgAnalysisInterfaces/IGoodRunsListSelectionTool.h"

#include "METPerformance/IMETCalculator.h"
#include "METPerformance/IMETPerfTool.h"
#include "METPerformance/IMETObjSel.h"
#include "METPerformance/IMETEvtSel.h"

#include "xAODEventInfo/EventInfo.h"

// to produce an output histo:
#include <TH1.h>
#include "GaudiKernel/ITHistSvc.h"
#include "GaudiKernel/ServiceHandle.h"

// to get information from metadata
#include "xAODCutFlow/CutBookkeeper.h"
#include "xAODCutFlow/CutBookkeeperContainer.h"

// // to get ghost muon jet association
// #include "METUtilities/METHelpers.h"


namespace met {

  const static double GeV=1e3;

  using std::string;
  using namespace xAOD;

  static SG::AuxElement::Decorator<float> dec_avgMu("avgMu");
  static SG::AuxElement::Decorator<float> dec_avgMunocorrection("avgMunocorrection");
  static SG::AuxElement::Decorator<float> dec_pileupWeight("pileupWeight");

  static SG::AuxElement::Decorator<float> dec_triggerSF("triggerSF");
  static SG::AuxElement::Decorator<float> dec_muonRecoEffSF("muonRecoEffSF");
  static SG::AuxElement::Decorator<float> dec_muonIsoEffSF( "muonIsoEffSF" );
  static SG::AuxElement::Decorator<float> dec_eleRecoEffSF( "eleRecoEffSF" );
  static SG::AuxElement::Decorator<float> dec_eleIDEffSF(   "eleIDEffSF"   );
  static SG::AuxElement::Decorator<float> dec_eleIsoEffSF(  "eleIsoEffSF"  );
//  static SG::AuxElement::Decorator<float> dec_btagEffSF("btagEffSF");

  //**********************************************************************

METSlimAlg_AnAlg::METSlimAlg_AnAlg( const std::string& name, ISvcLocator* pSvcLocator ) : ::AthAnalysisAlgorithm( name, pSvcLocator ),

    m_passAllEvtSelections("AllEventSelections"),
    m_isPVDec("isPV"),
    m_evtCleanDec("eventClean"),
    m_jetCleanDec("jetClean"),
    m_isZmmDec("isZmm"),
    m_isZeeDec("isZee"),
    m_isWmvDec("isWmv"),
    m_isWevDec("isWev"),
    m_isttbarDec("isttbar"),
    m_isdijetDec("isdijet"),
    m_isgjetDec("isgjet"),
    m_trigDec("trigDec"){

  //declareProperty( "Property", m_nProperty ); //example property declaration
    declareProperty("SUSYTools",   m_SUSYTools                 );
    declareProperty("PerfTool",    m_perftool                  );
    declareProperty("ObjSelTool",  m_objsel                    );
    declareProperty("EvtSelTool",  m_evtsel                    );
    declareProperty("METCalculators", m_metCalTools            );
    declareProperty("DoCalib",     m_docalib     = true        );
    declareProperty("DoSelection", m_doselection = true        );
    declareProperty("DoSkim",      m_doskim      = true        );
    declareProperty("DoCutBK",     m_docutbk     = true        );
    declareProperty("DoPRW",       m_doprw       = true        );
    declareProperty("is20152016",  m_is2015plus2016 = false    );
    declareProperty("GRLTool",     m_grltool                   );
    declareProperty("DoGRL",       m_dogrl       = true        );
    declareProperty("LowRN",       m_LowRN       = -1        );
    declareProperty("HighRN",      m_HighRN       = -1        );
    declareProperty("IsSimulation", m_isSimulation = false     );
    declareProperty("JetColl",    m_jetcoll     = "GoodJets"        );
    declareProperty("EleColl",    m_elecoll     = "GoodElectrons"   );
    declareProperty("MuonColl",   m_muoncoll    = "GoodMuons"       );

    declareProperty("Triggers",     m_triggers ); // List of triggers - for scale factors
    declareProperty("TriggerScaleFactor", m_triggerScaleFactor = "" ); // "Electron" or "Muon"
}

  //**********************************************************************

METSlimAlg_AnAlg::~METSlimAlg_AnAlg() {}

  //**********************************************************************


StatusCode METSlimAlg_AnAlg::initialize()
{
  ATH_MSG_INFO ("Initializing " << name() << "...");

  //////////////////////////////////////////////////////////////////////
  //    Register some useful histograms
  //////////////////////////////////////////////////////////////////////

  ATH_MSG_INFO("Booking histograms");
  //number of processed events and sum of weights -  if it is a DxAOD this is not really so useful
  ATH_CHECK( book( TH1D("h_allWeights","h_allWeights",10,0,10) ) );

  TH1* h_allWeights = hist("h_allWeights");
  h_allWeights->GetXaxis()->SetBinLabel(1, "all events");
  h_allWeights->GetXaxis()->SetBinLabel(2, "sum of mc_weights");
  h_allWeights->GetXaxis()->SetBinLabel(3, "sum of weights (PU+mc_w)");
  // The following is a simple AOD event count
  h_allWeights->GetXaxis()->SetBinLabel(4, "number of events in AOD");
  // The following one is the most useful number you need to reweight!!
  h_allWeights->GetXaxis()->SetBinLabel(5, "sum of weights from xAOD (metaData)");//this is the number you need to reweight DxAOD

  //avg mu distribution before any cut
  ATH_CHECK( book( TH1D("h_muInitial","h_muInitial",50,0.,50.) ) );
  // just to have a non-weighted distribution
  ATH_CHECK( book( TH1D("h_muInitialNoWeight","h_muInitialNoWeight",50,0.,50.) ) );

  //not corrected avg mu distribution before any cut
  ATH_CHECK( book( TH1D("h_not_corrected_muInitial","h_not_corrected_muInitial",50,0.,50.) ) );
           // just to have a non-weighted distribution
  ATH_CHECK( book( TH1D("h_not_corrected_muInitialNoWeight","h_not_corrected_muInitialNoWeight",50,0.,50.) ) );

  // Register cutflow histogram
  ATH_CHECK( book( TH1D("cutflow", "cutflow", END_OF_CUTFLOW, 0., END_OF_CUTFLOW ) ) );
  ATH_CHECK( book( TH1D("cutflow_raw", "cutflow_raw", END_OF_CUTFLOW, 0., END_OF_CUTFLOW ) ) );
  // Give cutflow bins useful names
  for (int i=0; i<END_OF_CUTFLOW; ++i){
    std::string bin_name = cutflow_bin_name(static_cast<CutFlow>(i));
    hist("cutflow")->GetXaxis()->SetBinLabel(i+1, bin_name.c_str());
    hist("cutflow_raw")->GetXaxis()->SetBinLabel(i+1, bin_name.c_str());
  }

  ATH_MSG_INFO("Done booking histograms");

  //////////////////////////////////////////////////////////////////////
  //    End histograms -- now retrieve tools
  //////////////////////////////////////////////////////////////////////

  // We have to retrieve tools *AFTER* booking histograms
  // if we are using the ReadAthenaxAOD(Hybrid) options.
  // This is because the beginInputFile method triggers when
  // SUSYTools accesses the metadata.

  // Check the trigger scale factor type requested is valid.
  if (m_triggerScaleFactor != "") {
    if ((m_triggerScaleFactor == "Electron") || (m_triggerScaleFactor == "Muon"))
      ATH_MSG_VERBOSE("Trigger scale factor " << m_triggerScaleFactor << " is valid.");
    else {
      ATH_MSG_ERROR("Unrecognized trigger scale factor requested: " << m_triggerScaleFactor);
      return StatusCode::FAILURE;
    }
  }

  ATH_MSG_INFO("Retrieving tools...");

  ATH_MSG_INFO("Attempting to retrieve SUSYTools." );
  if (m_SUSYTools.retrieve().isFailure()){
    ATH_MSG_ERROR("Failed to retrieve tool: " << m_SUSYTools.name());
    return StatusCode::FAILURE;
  }
  ATH_MSG_INFO("Retrieved SUSYTools: " << m_SUSYTools.name());

  if(m_docalib) {
    if( m_perftool.retrieve().isFailure() ) {
      ATH_MSG_FATAL("Failed to retrieve MET performance tool: " << m_perftool->name());
      return StatusCode::FAILURE;
    }
  }

  if(m_doselection) {
    if( m_objsel.retrieve().isFailure() ) {
      ATH_MSG_FATAL("Failed to retrieve object selection tool: " << m_objsel->name());
      return StatusCode::FAILURE;
    }
    if( m_evtsel.retrieve().isFailure() ) {
      ATH_MSG_FATAL("Failed to retrieve event selection tool: " << m_evtsel->name());
      return StatusCode::FAILURE;
    }
  }

  ATH_CHECK(m_grltool.retrieve());

  for(auto itool=m_metCalTools.begin(); itool!=m_metCalTools.end(); ++itool) {
    ToolHandle<met::IMETCalculator> m_metcaltool=*itool;
    ATH_CHECK(m_metcaltool.retrieve());
  }

  return StatusCode::SUCCESS;
}

  //**********************************************************************

StatusCode METSlimAlg_AnAlg::finalize() {
  ATH_MSG_INFO ("Finalizing " << name() << "...");

  ATH_CHECK(m_SUSYTools.release());
  ATH_CHECK(m_perftool.release());
  ATH_CHECK(m_objsel.release());
  ATH_CHECK(m_evtsel.release());
  ATH_CHECK(m_grltool.release());
  //ATH_CHECK(m_metcaltool.release());

  return StatusCode::SUCCESS;
}

  //**********************************************************************

StatusCode METSlimAlg_AnAlg::execute() {
  ATH_MSG_DEBUG ("Executing " << name() << "...");

  const EventInfo* eventinfo = 0;
  ATH_CHECK( evtStore()->retrieve( eventinfo, "EventInfo" ) );
  bool isSim = eventinfo->eventType(EventInfo::IS_SIMULATION);
  if (m_doprw) { // Apply PileupReweighting
    if (m_SUSYTools->ApplyPRWTool()!= StatusCode::SUCCESS){
      ATH_MSG_ERROR("Failed to apply PRW tool: " << m_SUSYTools.name());
      return StatusCode::FAILURE;
    }
  }
  ////////////////////////////////////////////////////////
  /// compute the sum of weights
  /// before any selection and fill an histo
  ///////////////////////////////////////////////////////
  double weight = 1.;
  TH1* h_allWeights = hist("h_allWeights");
  h_allWeights->Fill(.5 + 0,1.); // all events
  if(isSim){
    double mcweight = eventinfo->mcEventWeight();
    ATH_MSG_VERBOSE(" mcweight =  " << mcweight );
    h_allWeights->Fill(.5 + 1,mcweight);
    float pileupweight(1.);
    if(m_doprw) {
      pileupweight = m_SUSYTools->GetPileupWeight();
      ATH_MSG_VERBOSE("PileupWeight for this event: " << pileupweight );
    }
    dec_pileupWeight(*eventinfo) = pileupweight;
    weight = mcweight * pileupweight;
    h_allWeights->Fill(.5 + 2,weight);
    if(!m_docutbk) {
      h_allWeights->Fill(.5 + 3,1);
      h_allWeights->Fill(.5 + 4,mcweight);
    }
  }
  float avgmu=1;
  float avgmunocorr=1;
  //this is the average mu not corrected for PRW
  avgmunocorr=eventinfo->averageInteractionsPerCrossing();
  if (m_doprw) avgmu = m_SUSYTools->GetCorrectedAverageInteractionsPerCrossing();
  else avgmu= -1;  //claire
  dec_avgMu(*eventinfo) = avgmu;
  dec_avgMunocorrection(*eventinfo) = avgmunocorr;
  hist("h_muInitial")->Fill(avgmu, weight);
  hist("h_muInitialNoWeight")->Fill(avgmu);

  hist("h_not_corrected_muInitial")->Fill(avgmunocorr, weight);
  hist("h_not_corrected_muInitialNoWeight")->Fill(avgmunocorr);

    if(m_docalib) {
      // Call perftool to apply calibrations to objects and generate
      // final MET containers
      ATH_CHECK( m_perftool->execute() );
      ATH_CHECK( m_objsel->execute() );
      // Now that object selection is done, calculate MET.
      for(auto itool=m_metCalTools.begin(); itool!=m_metCalTools.end(); ++itool) {
        ToolHandle<met::IMETCalculator> m_metcaltool=*itool;
        ATH_CHECK(m_metcaltool->execute());
      }
   }
    if(m_doselection) {
      // And lastly, event selection.
      ATH_CHECK( m_evtsel->execute() );
    }
    bool passGRL=true;
    if( (!isSim) && (m_dogrl) ) {
      passGRL=m_grltool->passRunLB(*eventinfo);
    }
    if( (!isSim) && (m_LowRN>=0) ) {
      if( (eventinfo->runNumber()<m_LowRN) && (passGRL) )
         passGRL=false;
    }
    if( (!isSim) && (m_HighRN>=0) ) {
      if( (eventinfo->runNumber()>m_HighRN) && (passGRL) )
         passGRL=false;
    }
    TH1* h_cutflow = hist("cutflow");
    TH1* h_cutflow_raw = hist("cutflow_raw");
    if (passGRL)                    { h_cutflow->Fill(.5 + PassGRL,         weight); h_cutflow_raw->Fill(.5 + PassGRL         ); }
    if (m_isPVDec(*eventinfo) )     { h_cutflow->Fill(.5 + PassPV,          weight); h_cutflow_raw->Fill(.5 + PassPV          ); }
    if (m_trigDec(*eventinfo)     ) { h_cutflow->Fill(.5 + PassTrigDec,     weight); h_cutflow_raw->Fill(.5 + PassTrigDec     ); }
    if (m_evtCleanDec(*eventinfo) ) { h_cutflow->Fill(.5 + PassEvtCleaning, weight); h_cutflow_raw->Fill(.5 + PassEvtCleaning ); }
    if (m_jetCleanDec(*eventinfo) ) { h_cutflow->Fill(.5 + PassJetCleaning, weight); h_cutflow_raw->Fill(.5 + PassJetCleaning ); }
    if (m_isZmmDec(*eventinfo)    ) { h_cutflow->Fill(.5 + PassZmmSel,      weight); h_cutflow_raw->Fill(.5 + PassZmmSel      ); }
    if (m_isZeeDec(*eventinfo)    ) { h_cutflow->Fill(.5 + PassZeeSel,      weight); h_cutflow_raw->Fill(.5 + PassZeeSel      ); }
    if (m_isWmvDec(*eventinfo)    ) { h_cutflow->Fill(.5 + PassWmvSel,      weight); h_cutflow_raw->Fill(.5 + PassWmvSel      ); }
    if (m_isWevDec(*eventinfo)    ) { h_cutflow->Fill(.5 + PassWevSel,      weight); h_cutflow_raw->Fill(.5 + PassWevSel      ); }
    if (m_isttbarDec(*eventinfo)  ) { h_cutflow->Fill(.5 + PassttbarSel,    weight); h_cutflow_raw->Fill(.5 + PassttbarSel    ); }
    if (m_isdijetDec(*eventinfo)  ) { h_cutflow->Fill(.5 + PassdijetSel,    weight); h_cutflow_raw->Fill(.5 + PassdijetSel    ); }
    if (m_isgjetDec(*eventinfo)  ) { h_cutflow->Fill(.5 + PassgjetSel,    weight); h_cutflow_raw->Fill(.5 + PassgjetSel    ); }

    bool acceptEvent = (passGRL && m_trigDec(*eventinfo)&& m_isPVDec(*eventinfo) && m_evtCleanDec(*eventinfo) && m_jetCleanDec(*eventinfo)
                         && ( m_isZmmDec(*eventinfo) || m_isZeeDec(*eventinfo)
                              || m_isWmvDec(*eventinfo) || m_isWevDec(*eventinfo) || m_isttbarDec(*eventinfo)  || m_isdijetDec(*eventinfo) || m_isgjetDec(*eventinfo))  );
    m_passAllEvtSelections(*eventinfo) = acceptEvent;
    if(m_doskim) {
        setFilterPassed(acceptEvent);
    }
    else m_passAllEvtSelections(*eventinfo) = true;
  //////////////////////////////////////////////////////////////////////////
  // Scale factors:
  // Calculate some scale factors and store as decorations to be used in
  // plotting later.
  //////////////////////////////////////////////////////////////////////////
  if(isSim) {
    // Trigger scale factor
    float triggerSF(1.);
    if (m_triggerScaleFactor != "") {
      if (m_triggerScaleFactor == "Electron") {
	triggerSF = getElectronTriggerScaleFactor();
	if (triggerSF == 0){
		ATH_MSG_ERROR("Trigger scale factor = 0. Setting to 1.");
		triggerSF = 1;
	}
      }
      else if (m_triggerScaleFactor == "Muon") {
	triggerSF = getMuonTriggerScaleFactor();
 	if (triggerSF == 0){
		ATH_MSG_ERROR("Trigger scale factor = 0. Setting to 1.");
		triggerSF = 1;
	}
      }
      else {
        ATH_MSG_ERROR("Unrecognized trigger scale factor requested: " << m_triggerScaleFactor);
        return StatusCode::FAILURE;
      }
    }

    dec_triggerSF(*eventinfo) = triggerSF;
    const xAOD::MuonContainer *goodmu=0;
    CHECK(evtStore()->retrieve(goodmu, m_muoncoll));
    float muonRecoEffSF = m_SUSYTools->GetTotalMuonSF(*goodmu,true,false,"");
    float muonIsoEffSF = m_SUSYTools->GetTotalMuonSF(*goodmu,false,true,"");
    ATH_MSG_VERBOSE("Decorating muon reco scale factor: " << muonRecoEffSF);
    dec_muonRecoEffSF(*eventinfo) = muonRecoEffSF;
    ATH_MSG_VERBOSE("Decorating muon iso scale factor: " << muonIsoEffSF);
    dec_muonIsoEffSF(*eventinfo) = muonIsoEffSF;
    const xAOD::ElectronContainer *goodelectrons=0;
    CHECK(evtStore()->retrieve(goodelectrons, m_elecoll));
    float eleRecoEffSF = m_SUSYTools->GetTotalElectronSF(*goodelectrons,true,false,false,false,"");
    float eleIDEffSF   = m_SUSYTools->GetTotalElectronSF(*goodelectrons,false,true,false,false,"");
    float eleIsoEffSF  = m_SUSYTools->GetTotalElectronSF(*goodelectrons,false,false,false,true,"");
    ATH_MSG_VERBOSE("Decorating total electron reco scale factor: " << eleRecoEffSF);
    dec_eleRecoEffSF(*eventinfo) = eleRecoEffSF;
    ATH_MSG_VERBOSE("Decorating total electron ID scale factor: "   << eleIDEffSF);
    dec_eleIDEffSF(*eventinfo) = eleIDEffSF;
    ATH_MSG_VERBOSE("Decorating total electron iso scale factor: "  << eleIsoEffSF);
    dec_eleIsoEffSF(*eventinfo) = eleIsoEffSF;
    const xAOD::JetContainer *jets=0;
    CHECK(evtStore()->retrieve(jets, m_jetcoll));
    //float btagSF = m_SUSYTools->BtagSF(jets);
   // ATH_MSG_DEBUG("Decorating b-tagging scale factor: " << btagSF);
 //   dec_btagEffSF(*eventinfo) = btagSF;
  } else { // !isSim
    dec_triggerSF(*eventinfo)     = 1.;
    dec_muonRecoEffSF(*eventinfo) = 1.;
    dec_muonIsoEffSF(*eventinfo)  = 1.;
    dec_eleRecoEffSF(*eventinfo)  = 1.;
    dec_eleIDEffSF(*eventinfo)    = 1.;
    dec_eleIsoEffSF(*eventinfo)   = 1.;
  }

  return StatusCode::SUCCESS;
}

StatusCode METSlimAlg_AnAlg::beginInputFile() {  // the beginInputFile is

  //example of metadata retrieval:
  //float beamEnergy(0); CHECK( retrieveMetadata("/TagInfo","beam_energy",beamEnergy) );
  //std::vector<float> bunchPattern; CHECK( retrieveMetadata("/Digitiation/Parameters","BeamIntensityPattern",bunchPattern) );

   // instructions are in this twiki
   // https://twiki.cern.ch/twiki/bin/view/AtlasProtected/AthAnalysisBase#How_to_access_EventBookkeeper_in

  // get the initial sum of weights from xAOD
  if(m_isSimulation && m_docutbk){
    int    cutbk_event_count    = 0;
    double cutbk_sum_of_weights = 0.;
    const xAOD::CutBookkeeperContainer* bks = 0;
    ATH_MSG_INFO( "Retrieving CBK" );
    CHECK( inputMetaStore()->retrieve(bks, "CutBookkeepers") );
    const xAOD::CutBookkeeper* all = 0;
    ATH_MSG_INFO( "Checking CBK" );
    int maxCycle = -1; //need to find the max cycle where input stream is StreamAOD and the name is AllExecutedEvents
    for(auto cbk : *bks) {
      if(cbk->inputStream()=="StreamAOD" && cbk->name()=="AllExecutedEvents" && cbk->cycle()>maxCycle) {
    maxCycle=cbk->cycle();
    all = cbk;
      }
    }
    cutbk_event_count    = all->nAcceptedEvents();
    cutbk_sum_of_weights = all->sumOfEventWeights();
    // also have all->nAcceptedEvents() which is simple event count,
    // and all->sumOfEventWeightsSquared()
    ATH_MSG_INFO( "About to fill allweights hist " << hist("h_allWeights") );
    hist("h_allWeights")->Fill(.5 + 3,cutbk_event_count);
    hist("h_allWeights")->Fill(.5 + 4,cutbk_sum_of_weights);
  }//is sim

  return StatusCode::SUCCESS;
}

  //**********************************************************************

std::string METSlimAlg_AnAlg::RemoveHLTPrefix(const std::string& triggerline) {
     return triggerline.substr(4, triggerline.size());
}

std::string METSlimAlg_AnAlg::GetElectronSFString() {
    //Get electron trigger scale factor only for the trigger matched electron.
    // Join together electron triggers into an ORed string
    std::ostringstream trigger_stream;
    // egamma trig SF tool demands we strip off the 'HLT_' prefix
    trigger_stream << RemoveHLTPrefix(m_triggers[0]);
    for (const auto& trigger : m_triggers) {
        if (&trigger != &m_triggers[0]) { // not the first trigger
            if(m_is2015plus2016 && &trigger == &m_triggers[3]) trigger_stream << "_2016_" << RemoveHLTPrefix(trigger);
            else trigger_stream << "_OR_" << RemoveHLTPrefix(trigger);
        }
    }
    return trigger_stream.str();
}


double METSlimAlg_AnAlg::getElectronTriggerScaleFactor()
  {
    double scale_factor(1.);
    std::string trigger_str = GetElectronSFString();
    ATH_MSG_VERBOSE("Trying to retrieve electron trigger SF for trigger string " << trigger_str);

    const xAOD::ElectronContainer *goodelectrons=0;
    CHECK(evtStore()->retrieve(goodelectrons, m_elecoll));
    if(goodelectrons)
      scale_factor = m_SUSYTools->GetTotalElectronSF(*goodelectrons,false,false,true,false,trigger_str, false);

    ATH_MSG_VERBOSE("Electron trigger scale factor: " << scale_factor);
    return scale_factor;
  }

double METSlimAlg_AnAlg::getMuonTriggerScaleFactor()
{
  double scale_factor(1.);
  if (m_doprw) {
    int random_run_no = m_SUSYTools->GetRandomRunNumber();
    ATH_MSG_VERBOSE("Random run number: " << random_run_no);
    if (random_run_no == 282420) {
      // This is a run number that PRW returns. It's from the ALFA run: no
      // muon trig SFs available.
      // For now, print a WARNING and use the default taken from MuonTrigSFTool.
      ATH_MSG_WARNING("No muon trigger SFs for this run number (ALFA Period I). Defaulting to run 267639." );
      random_run_no = 267639;
    }
    // PRW selects random run number from amongst those data events with the same mu as this MC.
    // If there are none to choose from, returns run_no = 0.
    // This is fine, as event will be weighted to zero by prw_weight, but
    // muon trigger SF will throw warnings.
    if (random_run_no == 0) {
      float pileupweight = m_SUSYTools->GetPileupWeight();
      if (pileupweight == 0.) {
        ATH_MSG_VERBOSE("Random run number is zero, but PRW weight also zero. Skip muon trig SF.");
      }
      else ATH_MSG_WARNING("Random run number is zero in an event with non-zero PRW weight.");
    }
    else {
      std::ostringstream trigger_stream;
      trigger_stream << m_triggers[0];
      for (const auto& trigger : m_triggers) {
        if (&trigger != &m_triggers[0]) { // not the first trigger
	  trigger_stream << "_OR_" << trigger;
       }
      }
      std::string trigger_str = trigger_stream.str();
      ATH_MSG_VERBOSE("Trying to retrieve muon trigger SF for trigger string " << trigger_str);

      const xAOD::MuonContainer *goodmu=0;
      CHECK(evtStore()->retrieve(goodmu, m_muoncoll));
      scale_factor = m_SUSYTools->GetTotalMuonSF(*goodmu,false,false,trigger_str);
      ATH_MSG_VERBOSE("Muon trigger scale factor: " << scale_factor);
    }
  } else { // not doing pileup reweighting
    ATH_MSG_WARNING("Pileup reweighting not available: muon trigger SF will be returned as unity." );
  }
  return scale_factor;
}

}

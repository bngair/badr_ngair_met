#include "GaudiKernel/DeclareFactoryEntries.h"

// Top level tool
#include "METPerformance/METPerfTool.h"
#include "METPerformance/METObjSel.h"
#include "METPerformance/METEvtSel.h"
#include "METPerformance/METCalculator.h"
// Algs
#include "../METTreeAlg.h"
#include "../METPerfAlg.h"
#include "../METSlimAlg_AnAlg.h"

using namespace met;

DECLARE_TOOL_FACTORY(METPerfTool)
DECLARE_TOOL_FACTORY(METObjSel)
DECLARE_TOOL_FACTORY(METEvtSel)
DECLARE_NAMESPACE_TOOL_FACTORY(met,METCalculator)
//
DECLARE_ALGORITHM_FACTORY(METPerfAlg)
DECLARE_ALGORITHM_FACTORY(METTreeAlg)
DECLARE_ALGORITHM_FACTORY(METSlimAlg_AnAlg)


#include "../CheckForDuplicatesAlg.h"
DECLARE_ALGORITHM_FACTORY( CheckForDuplicatesAlg )

DECLARE_FACTORY_ENTRIES(METPerformance) {
  DECLARE_ALGORITHM( METSlimAlg_AnAlg );
  DECLARE_ALGORITHM(METPerfAlg)
  DECLARE_ALGORITHM(METTreeAlg)
  DECLARE_ALGORITHM( CheckForDuplicatesAlg );
  DECLARE_TOOL(METPerfTool)
  DECLARE_TOOL(METObjSel)
  DECLARE_TOOL(METEvtSel)
  DECLARE_NAMESPACE_TOOL(met,METCalculator)
}

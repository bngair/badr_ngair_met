// -*- c++ -*-
// METTreeAlg.h

#ifndef METTreeAlg_H
#define METTreeAlg_H

#include "AthenaBaseComps/AthHistogramAlgorithm.h"
#include "xAODMissingET/MissingETContainer.h"
#include "METPerformance/METPerfHelper.h"

//#include "GaudiKernel/ToolHandle.h"



//#include "AsgTools/ToolHandle.h"





namespace met {

  typedef ElementLink<xAOD::IParticleContainer> obj_link_t;

  class METTreeAlg : public AthHistogramAlgorithm {

  public:

    /// Constructor with parameters:
    METTreeAlg(const std::string& name, ISvcLocator* pSvcLocator);

    /// Destructor:
    ~METTreeAlg();

    /// Athena algorithm's Hooks
    StatusCode  initialize();
    StatusCode  execute();
    StatusCode  finalize();

  private:

    /// Default constructor:
    METTreeAlg();

    // fill plots
    //StatusCode plotBasics(const xAOD::MissingETContainer* metperf, double weight);
    StatusCode fillTree(const xAOD::MissingETContainer* metperf, double weight);
    StatusCode setTreeZ(const xAOD::MissingETContainer* metperf, double weight);
    StatusCode plotBasics(const xAOD::MissingETContainer* metperf, double weight, double avgmu);
    //StatusCode plotTerms(const xAOD::MissingETContainer* met, double weight);
    StatusCode plotTerms(const xAOD::MissingETContainer* met, double weight, double avgmu);
    StatusCode plotPerformance(const xAOD::MissingETContainer* metperf,
    			       double avgmu, unsigned int npv, double weight); // use this method if running over SUSY5
    //StatusCode plotPerformance(const xAOD::MissingETContainer* metperf,
    //			       double lht_sumet, double avgmu, unsigned int npv, double weight);
    StatusCode plotPerfZ(const xAOD::MissingETContainer* metperf, double weight);

    const char* histname(std::string name);

    std::string m_metsuffix;
    std::string m_jetterm;
    std::string m_softterm;
    std::string m_eleterm;
    std::string m_muonterm;
    std::string m_gammaterm;
    std::string m_tauterm;

    std::string m_jetcoll;
    std::string m_elecoll;
    std::string m_muoncoll;
    std::string m_sysName;
    std::string m_treeName;

    unsigned short m_evttype;
    bool m_dojetveto;
    bool m_doTauGammaPlots;
    bool m_jetBins;
    bool m_writetree;
    bool m_writehist;

    double m_wlnmetcut;
    double m_jetMinPt;

    bool m_doPileupReweighting; //claire

    bool m_noTruthPthard;

    bool m_doMuonElossPlots;
    bool m_applyTriggerSF;





    SG::AuxElement::ConstAccessor<bool> m_passAllEvtSelections;
    SG::AuxElement::ConstAccessor<bool> m_isPVAcc;
    SG::AuxElement::ConstAccessor<bool> m_evtCleanAcc;
    SG::AuxElement::ConstAccessor<bool> m_jetCleanAcc;
    SG::AuxElement::ConstAccessor<bool> m_isZmmAcc;
    SG::AuxElement::ConstAccessor<bool> m_isZeeAcc;
    SG::AuxElement::ConstAccessor<bool> m_isWmvAcc;
    SG::AuxElement::ConstAccessor<bool> m_isWevAcc;
    SG::AuxElement::ConstAccessor<bool> m_isttbarAcc;
    SG::AuxElement::ConstAccessor<bool> m_isgjetAcc;
//    SG::AuxElement::ConstAccessor<char> m_isBjetDec;
    SG::AuxElement::ConstAccessor<obj_link_t>  m_objLinkAcc;

    // Branches of the tree
    Double_t m_pthard;
    Double_t m_residt_pthard;
    Double_t m_residl_pthard;
    Double_t m_avgmu;
    Double_t m_weight;
    Int_t m_njets;
    Int_t m_npv;

    Float_t m_met;
    Float_t m_phi;
    Float_t m_mpx;
    Float_t m_mpy;
    Float_t m_sumet;
    Float_t m_jetmet;
    Float_t m_jetphi;
    Float_t m_jetmpx;
    Float_t m_jetmpy;
    Float_t m_jetsumet;
    Float_t m_softmet;
    Float_t m_softphi;
    Float_t m_softmpx;
    Float_t m_softmpy;
    Float_t m_softsumet;
    Float_t m_elemet;
    Float_t m_elephi;
    Float_t m_elempx;
    Float_t m_elempy;
    Float_t m_elesumet;
    Float_t m_muonmet;
    Float_t m_muonphi;
    Float_t m_muonmpx;
    Float_t m_muonmpy;
    Float_t m_muonsumet;
    Float_t m_photonmet;
    Float_t m_photonphi;
    Float_t m_photonmpx;
    Float_t m_photonmpy;
    Float_t m_photonsumet;
    Float_t m_taumet;
    Float_t m_tauphi;
    Float_t m_taumpx;
    Float_t m_taumpy;
    Float_t m_tausumet;

    Float_t m_ptZ;
    Float_t m_mZ;
    Float_t m_pLPtZ;
    Float_t m_pTPtZ;

    Float_t m_d0;
    Float_t m_z0sinT;
    Float_t m_d0sig;
    Int_t m_Ntrack;

  };

}

inline const char* met::METTreeAlg::histname(std::string name) {
  if(m_dojetveto) name += "_0jet";
  return (m_metsuffix+"_"+name).c_str();
}


#endif

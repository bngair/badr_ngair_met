#ifndef METPERFORMANCE_CHECKFORDUPLICATESALG_H
#define METPERFORMANCE_CHECKFORDUPLICATESALG_H 1

#include "AthenaBaseComps/AthAlgorithm.h"
//#include "GaudiKernel/ToolHandle.h"

class TTree;

class CheckForDuplicatesAlg: public ::AthAlgorithm {
 public:
  CheckForDuplicatesAlg( const std::string& name, ISvcLocator* pSvcLocator );
  virtual ~CheckForDuplicatesAlg();

  virtual StatusCode  initialize();
  virtual StatusCode  execute();
  virtual StatusCode  finalize();

 private:

  std::string m_streamName;
  TTree* m_eventTree;

  int m_dsid;
  int m_runNumber;
  int m_eventNumber;

};

#endif //> !METPERFORMANCE_CHECKFORDUPLICATESALG_H

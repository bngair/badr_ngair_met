// METPerfAlg.cxx

#include "METPerfAlg.h"
#include "xAODMissingET/MissingETContainer.h"
#include "xAODMissingET/MissingETAssociationMap.h"

#include "xAODEventInfo/EventInfo.h"
#include "xAODJet/JetContainer.h"
#include "xAODMuon/MuonContainer.h"
#include "xAODEgamma/ElectronContainer.h"
#include "xAODMissingET/MissingETAuxContainer.h"
#include "xAODTruth/TruthParticleContainer.h"
#include "xAODMissingET/MissingETComposition.h"
#include "xAODEgamma/EgammaxAODHelpers.h"

#include "TH1.h"
#include "TGraphAsymmErrors.h"
#include "TGraph.h"
#include "TH2D.h"
#include "TH3D.h"
#include "TVector2.h"


namespace met {


using std::vector;

  using xAOD::MissingET;
  using xAOD::MissingETContainer;
  using xAOD::MissingETAssociation;
  using xAOD::MissingETAssociationMap;
  using xAOD::MissingETAuxContainer;
  using xAOD::MissingETComposition;



 using xAOD::IParticle;
using xAOD::IParticleContainer;
using xAOD::JetContainer;
using xAOD::JetConstituentVector;
 using xAOD::TrackParticle;


  const static double GeV=1e3;
typedef ElementLink<xAOD::IParticleContainer> iplink_t;
  static const SG::AuxElement::ConstAccessor< iplink_t  > acc_originalObject("originalObjectLink");

  static SG::AuxElement::ConstAccessor<float> dec_avgMu("avgMu");
  static SG::AuxElement::ConstAccessor<float> dec_avgMunocorrection("avgMunocorrection");
  static SG::AuxElement::ConstAccessor<float> dec_pileupWeight("pileupWeight");

  static SG::AuxElement::ConstAccessor<char>  dec_passOR(       "passOR");
  static SG::AuxElement::ConstAccessor<float> dec_triggerSF(    "triggerSF");
  static SG::AuxElement::ConstAccessor<float> dec_muonRecoEffSF("muonRecoEffSF");
  static SG::AuxElement::ConstAccessor<float> dec_muonIsoEffSF( "muonIsoEffSF" );
  static SG::AuxElement::ConstAccessor<float> dec_eleRecoEffSF( "eleRecoEffSF" );
  static SG::AuxElement::ConstAccessor<float> dec_eleIDEffSF(   "eleIDEffSF"   );
  static SG::AuxElement::ConstAccessor<float> dec_eleIsoEffSF(  "eleIsoEffSF"  );
 // static SG::AuxElement::ConstAccessor<float> dec_btagEffSF(    "btagEffSF");

  static SG::AuxElement::Accessor<float>  acc_jvt("Jvt");
  using std::string;
  using namespace xAOD;

  //**********************************************************************

  METPerfAlg::METPerfAlg(const std::string& name,
			 ISvcLocator* pSvcLocator )
    : ::AthHistogramAlgorithm( name, pSvcLocator ),
    m_passAllEvtSelections("AllEventSelections"),
    m_isPVAcc("isPV"),
    m_evtCleanAcc("eventClean"),
    m_jetCleanAcc("jetClean"),
    m_isZmmAcc("isZmm"),
    m_isZeeAcc("isZee"),
    m_isWmvAcc("isWmv"),
    m_isWevAcc("isWev"),
    m_isttbarAcc("isttbar"),
    m_isdijetAcc("isdijet"),
    m_isgjetAcc("isgjet"),
   // m_isBjetDec("isBjet"),
    m_objLinkAcc("originalObjectLink"),
    m_constitObjLinks("ConstitObjectLinks")
  {
    declareProperty("Suffix",     m_metsuffix   = "RefTST"          );
    declareProperty("JetTerm",    m_jetterm     = "RefJet"          );
    declareProperty("SoftTerm",   m_softterm    = "PVSoftTrk"       );
    declareProperty("EleTerm",    m_eleterm     = "RefEle"          );
    declareProperty("MuonTerm",   m_muonterm    = "Muons"           );
    declareProperty("GammaTerm",  m_gammaterm   = "RefGamma"        );
    declareProperty("TauTerm",    m_tauterm     = "RefTau"          );

    declareProperty("JetColl",    m_jetcoll     = "GoodJets"        );
    declareProperty("EleColl",    m_elecoll     = "GoodElectrons"   );
    declareProperty("MuonColl",   m_muoncoll    = "GoodMuons"       );

    declareProperty("EvtType",    m_evttype     = None              );
    declareProperty("DoJetVeto",  m_dojetveto   = false             );
    declareProperty("DoFwdJetVeto", m_dofwdjetveto = false          );
    declareProperty("DoTauGammaPlots", m_doTauGammaPlots   = false  );
    declareProperty("JetBins",    m_jetBins     = false             );
    declareProperty("WriteTree",  m_writetree   = false             );
  declareProperty("JetConstitScaleMom", m_jetConstitScaleMom = "JetConstitScaleMomentum");

    declareProperty("WlnMETcut",  m_wlnmetcut   = 25*GeV            );
    declareProperty("JetPtCut",   m_jetMinPt    = 20*GeV            );
    declareProperty("FwdJetPtCut",m_fwdJetMinPt = 20*GeV            );

    declareProperty("ApplyTriggerSF",   m_applyTriggerSF = true    ); 
    declareProperty("DoMuonElossPlots", m_doMuonElossPlots         );
    
    declareProperty("DoPRW",		 m_doPileupReweighting      ); //claire
    declareProperty("noTruthPthard",	 m_noTruthPthard = false    ); // Do not include true MET in pthard.

    declareProperty("DoPlotTracks",      m_doPlotTracks = false     );
    declareProperty("PRW_datasf",       m_PRWdatasf = 1.     );
    declareProperty("DoEleJetOlapScan", m_doEleJetOlapScan = false);
  }

  //**********************************************************************

  METPerfAlg::~METPerfAlg() { }

  //**********************************************************************

  StatusCode METPerfAlg::initialize() {
    ATH_MSG_INFO("Initializing " << name() << "...");

    m_metSignif.setTypeAndName("met::METSignificance/metSignif");
    ANA_CHECK( m_metSignif.setProperty("SoftTermParam", met::Random) );
    ANA_CHECK( m_metSignif.setProperty("TreatPUJets",   true) );
    ANA_CHECK( m_metSignif.setProperty("DoPhiReso",     true) );
    ANA_CHECK( m_metSignif.retrieve() );

    TH1::StatOverflows(); //Some of the results (e.g. hadronic recoil) are skewed quite significantly if we don't do this
    if (m_writetree) {
      ATH_CHECK (book(TTree("resid_pthard", "Total MET residuals vs pthard") ) );
      TTree* t = tree("resid_pthard");
      t->Branch("pthard", &m_pthard, "pthard/D");
      t->Branch("residT_pthard", &m_residt_pthard, "residT_pthard/D");
      t->Branch("residL_pthard", &m_residl_pthard, "residL_pthard/D");
      t->Branch("nJets", &m_njets, "nJets/I");
      t->Branch("avgMu", &m_avgmu, "avgMu/D");
      t->Branch("weight", &m_weight, "weight/D");
    }
ATH_CHECK( book( TH1D( "metTermdR04n",   "electron-jet differance", 500, 0, 1000 ) ) );
hist("metTermdR04n")->Sumw2();
ATH_CHECK( book( TH1D( "eleJetdRn",   "electron-jet differance", 500, 0, 50 ) ) );
hist("eleJetdRn")->Sumw2();

ATH_CHECK( book( TH2D( "metvsfrac", "pt vs fr", 1000, 0., 1000., 100, 0,1.2 ) ) );
hist2d("metvsfrac")->Sumw2();
ATH_CHECK( book( TH2D( "metvsdiff", "pt vs fr", 1000, 0., 1000., 100, 0,300  ) ) );
hist2d("metvsdiff")->Sumw2();

ATH_CHECK( book( TH2D( "metvsfracd", "pt vs fr", 1000, 0., 1000., 100, 0,1.2 ) ) );
hist2d("metvsfracd")->Sumw2();

ATH_CHECK( book( TH2D( "metvsdiffd", "pt vs fr", 1000, 0., 1000.,100,0,300  ) ) );
hist2d("metvsdiffd")->Sumw2();
ATH_CHECK( book( TH2D( "metvsfracn", "pt vs fr", 1000, 0., 1000., 100, 0,1.2 ) ) );
hist2d("metvsfracn")->Sumw2();

ATH_CHECK( book( TH2D( "metvsdiffn", "pt vs fr", 1000, 0., 1000.,100,0,300  ) ) );
hist2d("metvsdiffn")->Sumw2();

ATH_CHECK( book( TH2D( "metvsdr", "pt vs fr", 1000, 0., 1000., 100, 0,9 ) ));
hist2d("metvsdr")->Sumw2();
ATH_CHECK( book( TH2D( "diffvsfrac", "pt vs fr", 1000, 0,1 ,1000, 0., 100 ) ) );
hist2d("diffvsfrac")->Sumw2();
ATH_CHECK( book( TH2D( "diffvsfracm", "pt vs fr", 1000, 0,1 ,1000, 0., 100  ) ) );
hist2d("diffvsfracm")->Sumw2();

ATH_CHECK( book( TH1D( "jetmet",   "electron-jet differance", 500, 0, 1000 ) ) );
hist("jetmet")->Sumw2();
ATH_CHECK( book( TH1D( "elemet",   "electron-jet differance", 500, 0, 1000 ) ) );
hist("elemet")->Sumw2();
ATH_CHECK( book( TH1D( "softmet",   "electron-jet differance", 500, 0, 1000 ) ) );
hist("softmet")->Sumw2();
ATH_CHECK( book( TH1D( "muonmet",   "electron-jet differance", 500, 0, 1000 ) ) );
hist("muonmet")->Sumw2();

ATH_CHECK( book( TH1D( "metbadr11",   "electron-jet differance", 500, 0, 500 ) ) );
hist("metbadr11")->Sumw2();
ATH_CHECK( book( TH1D( "metbadr22",   "electron-jet differance", 500, 0, 500 ) ) );
hist("metbadr22")->Sumw2();
ATH_CHECK( book( TH1D( "metbadr33",   "electron-jet differance", 500, 0, 500 ) ) );
hist("metbadr33")->Sumw2();
ATH_CHECK( book( TH1D( "metbadr44",   "electron-jet differance", 500, 0, 500 ) ) );
hist("metbadr44")->Sumw2();

ATH_CHECK( book( TH1D( "fracm",   "electron-jet fraction", 1000,0, 1 ) ) );
hist("fracm")->Sumw2();
ATH_CHECK( book( TH1D( "diffm",   "electron-jet differance", 1000, 0, 100 ) ) );
hist("diffm")->Sumw2();
ATH_CHECK( book( TH1D( "frac",   "electron-jet fraction", 1000,0, 1 ) ) );
hist("frac")->Sumw2();
ATH_CHECK( book( TH1D( "diff",   "electron-jet differance", 1000, 0, 100 ) ) );
hist("diff")->Sumw2();
ATH_CHECK( book( TH1D( "fracdm",   "electron-jet fraction", 1000,0, 1 ) ) );
hist("fracdm")->Sumw2();
ATH_CHECK( book( TH1D( "diffdm",   "electron-jet differance", 1000, 0, 100 ) ) );
hist("diffdm")->Sumw2();

    ATH_CHECK( book( TH1D( "met",   "Magnitude of the total MET", 200, 0., 1000. ) ) );
 ATH_CHECK( book( TH1D( "met1a",   "Magnitude of the total MET", 200, 0., 1000. ) ) );
 ATH_CHECK( book( TH1D( "met2a",   "Magnitude of the total MET", 200, 0., 1000. ) ) );
 ATH_CHECK( book( TH1D( "met3a",   "Magnitude of the total MET", 200, 0., 1000. ) ) );
 ATH_CHECK( book( TH1D( "met1b",   "Magnitude of the total MET", 200, 0., 1000. ) ) );
 ATH_CHECK( book( TH1D( "met2b",   "Magnitude of the total MET", 200, 0., 1000. ) ) );
 ATH_CHECK( book( TH1D( "met3b",   "Magnitude of the total MET", 200, 0., 1000. ) ) );

 ATH_CHECK( book( TH1D( "metm",   "Magnitude of the total MET", 200, 0., 1000. ) ) );
 ATH_CHECK( book( TH1D( "metba",   "Magnitude of the total MET", 200, 0., 1000. ) ) );
  ATH_CHECK( book( TH1D( "metdm",   "Magnitude of the total MET", 200, 0., 1000. ) ) );

    ATH_CHECK( book( TH1D( "phi",   "Azimuthal direction of the total MET", 64, -3.2, 3.2 ) ) );
    ATH_CHECK( book( TH1D( "phi_met50",   "Azimuthal direction of the total MET", 64, -3.2, 3.2 ) ) );
    ATH_CHECK( book( TH1D( "phi_met100",   "Azimuthal direction of the total MET", 64, -3.2, 3.2 ) ) );
    hist("met")->Sumw2();
 hist("met1a")->Sumw2();
 hist("met2a")->Sumw2();
 hist("met3a")->Sumw2();
 hist("met1b")->Sumw2();
 hist("met2b")->Sumw2();
 hist("met3b")->Sumw2();

     hist("metba")->Sumw2();
    hist("phi")->Sumw2();
    hist("phi_met50")->Sumw2();
    hist("phi_met100")->Sumw2();
    ATH_CHECK( book( TH1D( "sumet", "Total Sum ET", 200, 0., 3000. ) ) );
    ATH_CHECK( book( TH1D( "mpx",   "X-component of the total MET", 100, -250., 250. ) ) );
    ATH_CHECK( book( TH1D( "mpy",   "Y-component of the total MET", 100, -250., 250. ) ) );
    hist("sumet")->Sumw2();
    hist("mpx")->Sumw2();
    hist("mpy")->Sumw2();
    ATH_CHECK( book( TH1D( "sig",   "Total MET Significance", 100, 0., 20. ) ) );
    hist("sig")->Sumw2();
    ATH_CHECK( book( TH1D( "sigHT",   "SUSY MET Significance", 100, 0., 20. ) ) );
    hist("sigHT")->Sumw2();
/*
    ATH_CHECK( book( TH1D( "mu_distribution", "Average pileup", 100, 0., 100. ) ) );
    hist("mu_distribution")->Sumw2();

    ATH_CHECK( book( TH1D( "mu_distribution_w", "Average pileup after weighting", 100, 0., 100. ) ) );
    hist("mu_distribution_w")->Sumw2();

    ATH_CHECK( book( TH1D( "mu_distribution_w_nosf", "Average pileup without SF", 100, 0., 100. ) ) );
    hist("mu_distribution_w_nosf")->Sumw2();
*/
    ATH_CHECK( book( TH1D( "goodjets_emfrac_eta1", "Jets emfrac", 100, 0., 1. ) ) );
    hist("goodjets_emfrac_eta1")->Sumw2();
    ATH_CHECK( book( TH1D( "goodjets_emfrac_LBA2930", "Jets emfrac", 100, 0., 1. ) ) );
    hist("goodjets_emfrac_LBA2930")->Sumw2();

    ATH_CHECK( book( TH1D( "goodjets_eta", "Jets eta", 100, -5., 5. ) ) );
    hist("goodjets_eta")->Sumw2();
    ATH_CHECK( book( TH2D( "goodjets_eta_vs_phi", "Jets eta vs phi", 100, -5., 5., 64, -3.2,3.2 ) ) );
    hist2d("goodjets_eta_vs_phi")->Sumw2();
    ATH_CHECK( book( TH1D( "goodjets_eta_mu50", "Jets eta", 100, -5., 5. ) ) );
    hist("goodjets_eta_mu50")->Sumw2();

    ATH_CHECK( book( TH2D( "njet_vs_mu", "NJet vs mu",
			   80, 0, 80, 10, 0., 10.  ) ) );
    hist("njet_vs_mu")->Sumw2();
    ATH_CHECK( book( TH2D( "nfwdjet_vs_mu", "NFwdJet vs mu",
			   80, 0, 80, 10, 0., 10.  ) ) );
    hist("nfwdjet_vs_mu")->Sumw2();


    ATH_CHECK( book( TH1D( "jet_met",   "Magnitude of the jet MET", 200, 0., 1000. ) ) );
    ATH_CHECK( book( TH1D( "jet_phi",   "Azimuthal direction of the jet MET", 64, -3.2, 3.2 ) ) );
    ATH_CHECK( book( TH1D( "jet_sumet", "Jet Sum ET", 200, 0., 3000. ) ) );
    ATH_CHECK( book( TH1D( "jet_mpx",   "X-component of the jet MET", 100, -250., 250. ) ) );
    ATH_CHECK( book( TH1D( "jet_mpy",   "Y-component of the jet MET", 100, -250., 250. ) ) );
    hist("jet_met")->Sumw2();
    hist("jet_phi")->Sumw2();
    hist("jet_sumet")->Sumw2();
    hist("jet_mpx")->Sumw2();
    hist("jet_mpy")->Sumw2();
/*
    ATH_CHECK( book( TH1D( "muon_met",   "Magnitude of the muon MET", 200, 0., 1000. ) ) );
    ATH_CHECK( book( TH1D( "muon_phi",   "Azimuthal direction of the muon MET", 64, -3.2, 3.2 ) ) );
    ATH_CHECK( book( TH1D( "muon_sumet", "Muon Sum ET", 100, 0., 2000. ) ) );
    ATH_CHECK( book( TH1D( "muon_mpx",   "X-component of the muon MET", 100, -250., 250. ) ) );
    ATH_CHECK( book( TH1D( "muon_mpy",   "Y-component of the muon MET", 100, -250., 250. ) ) );
    hist("muon_met")->Sumw2();
    hist("muon_phi")->Sumw2();
    hist("muon_sumet")->Sumw2();
    hist("muon_mpx")->Sumw2();
    hist("muon_mpy")->Sumw2();
*/
    //ele and jet closeby studies
    ATH_CHECK( book( TH1D( "eleJetdR",   "dR(e,j)", 100, 0, 5 ) ) );
    ATH_CHECK( book( TH1D( "metTermdR04",   "MET", 100, 0., 1000. ) ) );
    ATH_CHECK( book( TH1D( "sfotmetdR04", "SoftMET", 100, 0., 1000. ) ) );
    ATH_CHECK( book( TH1D( "jetmetdR04", "JetMET", 100, 0., 1000. ) ) );
    ATH_CHECK( book( TH1D( "elemetdR04", "EleMET", 100, 0., 1000. ) ) );
    ATH_CHECK( book( TH1D( "jetEnergySharing", "(ePt+jPt)/truthJet", 100, 0., 10. ) ) );
    hist("eleJetdR")->Sumw2();
    hist("metTermdR04")->Sumw2();
    hist("sfotmetdR04")->Sumw2();
    hist("jetmetdR04")->Sumw2();
    hist("elemetdR04")->Sumw2();
    hist("jetEnergySharing")->Sumw2();
    //for high MET checks
  /*
   ATH_CHECK( book( TH1D( "jet_met_checks_soft70",   "Magnitude of the jet MET", 100, 0., 1000. ) ) ); 
    ATH_CHECK( book( TH1D( "met_checks_soft70",   "Magnitude of the MET", 100, 0., 1000. ) ) ); 
    ATH_CHECK( book( TH1D( "muon_met_checks_soft70",   "Magnitude of the muon MET", 100, 0., 1000. ) ) ); 
    ATH_CHECK( book( TH1D( "softmet_MET150",   "Magnitude of the soft MET with MET>150", 100, 0., 1000. ) ) ); 
    ATH_CHECK( book( TH1D( "ele_met_checks_soft70", "Magnitude of the ele MET", 100, 0., 1000. ) ) );  
    hist("jet_met_checks_soft70")->Sumw2(); 
    hist("met_checks_soft70")->Sumw2(); 
    hist("muon_met_checks_soft70")->Sumw2(); 
    hist("softmet_MET150")->Sumw2();     
    hist("ele_met_checks_soft70")->Sumw2(); 
    ATH_CHECK( book( TH1D( "dphi_jet_metmu_checks_soft70",   "Azimuthal between jet and metmu", 64, -3.2, 3.2 ) ) ); 
    ATH_CHECK( book( TH1D( "dphi_jet_metjet_checks_soft70",   "Azimuthal between jet and metjet", 64, -3.2, 3.2 ) ) ); 
    ATH_CHECK( book( TH1D( "dphi_jet_soft_checks_soft70",   "Azimuthal between jet and metsoft", 64, -3.2, 3.2 ) ) ); 
    ATH_CHECK( book( TH1D( "dphi_jet_metele_checks_soft70",   "Azimuthal between jet and metele", 64, -3.2, 3.2 ) ) );
    ATH_CHECK( book( TH1D( "dphi_met_soft_checks_soft70",   "Azimuthal between met and metsoft", 64, -3.2, 3.2 ) ) );
    hist("dphi_jet_metmu_checks_soft70")->Sumw2(); 
    hist("dphi_jet_metjet_checks_soft70")->Sumw2(); 
    hist("dphi_jet_soft_checks_soft70")->Sumw2(); 
    hist("dphi_jet_metele_checks_soft70")->Sumw2(); 
    hist("dphi_met_soft_checks_soft70")->Sumw2(); 
*/  
  if(m_evttype==Zmm){ 
    ATH_CHECK( book( TH1D( "dphi_jet_mu_checks_soft70",   "Azimuthal between jet and mu", 64, -3.2, 3.2 ) ) ); 
    ATH_CHECK( book( TH1D( "dphi_mu_metmu_checks_soft70",   "Azimuthal between mu and metmu", 64, -3.2, 3.2 ) ) ); 
    ATH_CHECK( book( TH1D( "dphi_mu_metjet_checks_soft70",   "Azimuthal between mu and metjet", 64, -3.2, 3.2 ) ) ); 
    ATH_CHECK( book( TH1D( "dphi_mu_soft_checks_soft70",   "Azimuthal between mu and metsoft", 64, -3.2, 3.2 ) ) ); 
    ATH_CHECK( book( TH1D( "dphi_mu_met_checks_soft70",   "Azimuthal between mu and met", 64, -3.2, 3.2 ) ) ); 
    hist("dphi_jet_mu_checks_soft70")->Sumw2(); 
    hist("dphi_mu_metmu_checks_soft70")->Sumw2(); 
    hist("dphi_mu_metjet_checks_soft70")->Sumw2(); 
    hist("dphi_mu_soft_checks_soft70")->Sumw2(); 
    hist("dphi_mu_met_checks_soft70")->Sumw2(); 
    }
    if(m_evttype==Zee){ 
    ATH_CHECK( book( TH1D( "dphi_jet_ele_checks_soft70",   "Azimuthal between jet and ele", 64, -3.2, 3.2 ) ) ); 
    ATH_CHECK( book( TH1D( "dphi_ele_metele_checks_soft70",   "Azimuthal between ele and metele", 64, -3.2, 3.2 ) ) ); 
    ATH_CHECK( book( TH1D( "dphi_ele_metjet_checks_soft70",   "Azimuthal between ele and metjet", 64, -3.2, 3.2 ) ) ); 
    ATH_CHECK( book( TH1D( "dphi_ele_soft_checks_soft70",   "Azimuthal between ele and metsoft", 64, -3.2, 3.2 ) ) );     
    ATH_CHECK( book( TH1D( "dphi_ele_met_checks_soft70",   "Azimuthal between ele and met", 64, -3.2, 3.2 ) ) ); 
    hist("dphi_jet_ele_checks_soft70")->Sumw2(); 
    hist("dphi_ele_metele_checks_soft70")->Sumw2(); 
    hist("dphi_ele_metjet_checks_soft70")->Sumw2(); 
    hist("dphi_ele_soft_checks_soft70")->Sumw2();  
    hist("dphi_ele_met_checks_soft70")->Sumw2();    
    } 
    ATH_CHECK( book( TH1D( "jet_met_checks_met200",   "Magnitude of the jet MET", 100, 0., 1000. ) ) ); 
    ATH_CHECK( book( TH1D( "met_checks_met200",   "Magnitude of the MET", 100, 0., 1000. ) ) ); 
    ATH_CHECK( book( TH1D( "muon_met_checks_met200",   "Magnitude of the muon MET", 100, 0., 1000. ) ) );  
    ATH_CHECK( book( TH1D( "ele_met_checks_met200", "Magnitude of the ele MET", 100, 0., 1000. ) ) );  
    hist("jet_met_checks_met200")->Sumw2(); 
    hist("met_checks_met200")->Sumw2(); 
    hist("muon_met_checks_met200")->Sumw2();     
    hist("ele_met_checks_met200")->Sumw2(); 
    ATH_CHECK( book( TH1D( "dphi_jet_metmu_checks_met200",   "Azimuthal between jet and metmu", 64, -3.2, 3.2 ) ) ); 
    ATH_CHECK( book( TH1D( "dphi_jet_metjet_checks_met200",   "Azimuthal between jet and metjet", 64, -3.2, 3.2 ) ) ); 
    ATH_CHECK( book( TH1D( "dphi_jet_soft_checks_met200",   "Azimuthal between jet and metsoft", 64, -3.2, 3.2 ) ) ); 
    ATH_CHECK( book( TH1D( "dphi_jet_metele_checks_met200",   "Azimuthal between jet and metele", 64, -3.2, 3.2 ) ) );
    ATH_CHECK( book( TH1D( "dphi_met_soft_checks_met200",   "Azimuthal between met and metsoft", 64, -3.2, 3.2 ) ) );
    hist("dphi_jet_metmu_checks_met200")->Sumw2(); 
    hist("dphi_jet_metjet_checks_met200")->Sumw2(); 
    hist("dphi_jet_soft_checks_met200")->Sumw2(); 
    hist("dphi_jet_metele_checks_met200")->Sumw2(); 
    hist("dphi_met_soft_checks_met200")->Sumw2(); 
    if(m_evttype==Zmm){ 
    ATH_CHECK( book( TH1D( "dphi_jet_mu_checks_met200",   "Azimuthal between jet and mu", 64, -3.2, 3.2 ) ) ); 
    ATH_CHECK( book( TH1D( "dphi_mu_metmu_checks_met200",   "Azimuthal between mu and metmu", 64, -3.2, 3.2 ) ) ); 
    ATH_CHECK( book( TH1D( "dphi_mu_metjet_checks_met200",   "Azimuthal between mu and metjet", 64, -3.2, 3.2 ) ) ); 
    ATH_CHECK( book( TH1D( "dphi_mu_soft_checks_met200",   "Azimuthal between mu and metsoft", 64, -3.2, 3.2 ) ) ); 
    ATH_CHECK( book( TH1D( "dphi_mu_met_checks_met200",   "Azimuthal between mu and met", 64, -3.2, 3.2 ) ) ); 
    hist("dphi_jet_mu_checks_met200")->Sumw2(); 
    hist("dphi_mu_metmu_checks_met200")->Sumw2(); 
    hist("dphi_mu_metjet_checks_met200")->Sumw2(); 
    hist("dphi_mu_soft_checks_met200")->Sumw2(); 
    hist("dphi_mu_met_checks_met200")->Sumw2(); 
    }
    if(m_evttype==Zee){ 
    ATH_CHECK( book( TH1D( "dphi_jet_ele_checks_met200",   "Azimuthal between jet and ele", 64, -3.2, 3.2 ) ) ); 
    ATH_CHECK( book( TH1D( "dphi_ele_metele_checks_met200",   "Azimuthal between ele and metele", 64, -3.2, 3.2 ) ) ); 
    ATH_CHECK( book( TH1D( "dphi_ele_metjet_checks_met200",   "Azimuthal between ele and metjet", 64, -3.2, 3.2 ) ) ); 
    ATH_CHECK( book( TH1D( "dphi_ele_soft_checks_met200",   "Azimuthal between ele and metsoft", 64, -3.2, 3.2 ) ) );     
    ATH_CHECK( book( TH1D( "dphi_ele_met_checks_met200",   "Azimuthal between ele and met", 64, -3.2, 3.2 ) ) ); 
    hist("dphi_jet_ele_checks_met200")->Sumw2(); 
    hist("dphi_ele_metele_checks_met200")->Sumw2(); 
    hist("dphi_ele_metjet_checks_met200")->Sumw2(); 
    hist("dphi_ele_soft_checks_met200")->Sumw2();  
    hist("dphi_ele_met_checks_met200")->Sumw2();    
    } 
    ATH_CHECK( book( TH2D( "met_tst", "total MET vs TST",
               100, 0., 300., 100, 0., 1000.  ) ) );

    ATH_CHECK( book( TH1D( "ele_met",   "Magnitude of the electron MET", 200, 0., 1000. ) ) );
    ATH_CHECK( book( TH1D( "ele_phi",   "Azimuthal direction of the electron MET", 64, -3.2, 3.2 ) ) );
    ATH_CHECK( book( TH1D( "ele_sumet", "Electron Sum ET", 100, 0., 2000. ) ) );
    ATH_CHECK( book( TH1D( "ele_mpx",   "X-component of the electron MET", 100, -250., 250. ) ) );
    ATH_CHECK( book( TH1D( "ele_mpy",   "Y-component of the electron MET", 100, -250., 250. ) ) );
    hist("ele_met")->Sumw2();
    hist("ele_phi")->Sumw2();
    hist("ele_sumet")->Sumw2();
    hist("ele_mpx")->Sumw2();
    hist("ele_mpy")->Sumw2();

    ATH_CHECK( book( TH1D( "soft_met",   "Magnitude of the soft MET", 100, 0., 300. ) ) );
    ATH_CHECK( book( TH1D( "soft_phi",   "Azimuthal direction of the soft MET", 64, -3.2, 3.2 ) ) );
    ATH_CHECK( book( TH1D( "soft_sumet", "Soft Sum ET", 100, 0., 1000. ) ) );
    ATH_CHECK( book( TH1D( "soft_mpx",   "X-component of the soft MET", 100, -250., 250. ) ) );
    ATH_CHECK( book( TH1D( "soft_mpy",   "Y-component of the soft MET", 100, -250., 250. ) ) );
    hist("soft_met")->Sumw2();
    hist("soft_phi")->Sumw2();
    hist("soft_sumet")->Sumw2();
    hist("soft_mpx")->Sumw2();
    hist("soft_mpy")->Sumw2();

    ATH_CHECK( book( TH1D( "photon_met",   "Magnitude of the photon MET", 100, 0., 300. ) ) );
    ATH_CHECK( book( TH1D( "photon_phi",   "Azimuthal direction of the photon MET", 64, -3.2, 3.2 ) ) );
    ATH_CHECK( book( TH1D( "photon_sumet", "photon Sum ET", 100, 0., 1000. ) ) );
    ATH_CHECK( book( TH1D( "photon_mpx",   "X-component of the photon MET", 100, -250., 250. ) ) );
    ATH_CHECK( book( TH1D( "photon_mpy",   "Y-component of the photon MET", 100, -250., 250. ) ) );
    hist("photon_met")->Sumw2();
    hist("photon_phi")->Sumw2();
    hist("photon_sumet")->Sumw2();
    hist("photon_mpx")->Sumw2();
    hist("photon_mpy")->Sumw2();
/*
    ATH_CHECK( book( TH1D( "tau_met",   "Magnitude of the tau MET", 100, 0., 300. ) ) );
    ATH_CHECK( book( TH1D( "tau_phi",   "Azimuthal direction of the tau MET", 64, -3.2, 3.2 ) ) );
    ATH_CHECK( book( TH1D( "tau_sumet", "tau Sum ET", 100, 0., 1000. ) ) );
    ATH_CHECK( book( TH1D( "tau_mpx",   "X-component of the tau MET", 100, -250., 250. ) ) );
    ATH_CHECK( book( TH1D( "tau_mpy",   "Y-component of the tau MET", 100, -250., 250. ) ) );
    hist("tau_met")->Sumw2();
    hist("tau_phi")->Sumw2();
    hist("tau_sumet")->Sumw2();
    hist("tau_mpx")->Sumw2();
    hist("tau_mpy")->Sumw2();

    ATH_CHECK( book( TH1D( "pThard_distribution", "pT hard distribution", 100, 0., 1000. ) ) );
    hist("pThard_distribution")->Sumw2();

    ATH_CHECK( book( TH1D( "pL_pthard", "Long. projection on pthard of the total MET", 100, -250., 250. ) ) );
    ATH_CHECK( book( TH1D( "pT_pthard", "Trans. projection on pthard of the total MET", 100, -250., 250. ) ) );
    hist("pL_pthard")->Sumw2();
    hist("pT_pthard")->Sumw2();

    //Histograms for AtlasDerivation20.7 validation

    ATH_CHECK( book( TH1D( "track_d0", "tracks d0", 800, -3, 3 ) ) );
    hist("track_d0")->Sumw2();
    
    ATH_CHECK( book( TH1D( "zoom_track_d0", "zoomed tracks d0", 200, -0.1, 0.1 ) ) );
    hist("zoom_track_d0")->Sumw2();
    
    ATH_CHECK( book( TH2D( "phi_vs_d0", "Track phi vs track d0",
			   64, -3.2, 3.2, 800, -3., 3.  ) ) );
    hist("phi_vs_d0")->Sumw2();

    ATH_CHECK( book( TH2D( "phi_vs_zoomd0", "Track phi vs zoomed track d0",
			   64, -3.2, 3.2, 200, -0.1, 0.1  ) ) );
    hist("phi_vs_zoomd0")->Sumw2();

    ATH_CHECK( book( TH2D( "phi_vs_sigd0", "Track phi vs significance d0",
			   64, -3.2, 3.2, 100, -6., 6.  ) ) );
    hist("phi_vs_sigd0")->Sumw2();

    ATH_CHECK( book( TH1D( "track_Dz0sin", "tracks delta z0 sin theta", 1000, 0, 4 ) ) );
    hist("track_Dz0sin")->Sumw2();

    ATH_CHECK( book( TH1D( "sig_track_d0", "significance tracks d0", 100, -6., 6. ) ) );
    hist("sig_track_d0")->Sumw2();
    
    ATH_CHECK( book( TH1D( "abs_sig_track_d0", "absolute significance tracks d0", 50, 0., 6. ) ) );
    hist("abs_sig_track_d0")->Sumw2();
    
    ATH_CHECK( book( TH1D( "track_d0_mu015", "tracks d0 for mu<=15", 800, -3, 3 ) ) );
    hist("track_d0_mu015")->Sumw2();
    
    ATH_CHECK( book( TH1D( "track_Dz0sin_mu015", "tracks delta z0 sin theta for mu<=15", 1000, 0, 4 ) ) );
    hist("track_Dz0sin_mu015")->Sumw2();

    ATH_CHECK( book( TH1D( "sig_track_d0_mu015", "significance tracks d0 for mu<=15", 100, -6., 6. ) ) );
    hist("sig_track_d0_mu015")->Sumw2();
    
    ATH_CHECK( book( TH1D( "track_d0_mu1630", "tracks d0 for mu in 16-30", 800, -3, 3 ) ) );
    hist("track_d0_mu1630")->Sumw2();
    
    ATH_CHECK( book( TH1D( "track_Dz0sin_mu1630", "tracks delta z0 sin theta for mu in 16-30", 1000, 0, 4 ) ) );
    hist("track_Dz0sin_mu1630")->Sumw2();

    ATH_CHECK( book( TH1D( "sig_track_d0_mu1630", "significance tracks d0 for mu in 16-30", 100, -6., 6. ) ) );
    hist("sig_track_d0_mu1630")->Sumw2();
    
    ATH_CHECK( book( TH1D( "track_d0_muhigh", "tracks d0 for high mu events", 800, -3, 3 ) ) );
    hist("track_d0_muhigh")->Sumw2();
    
    ATH_CHECK( book( TH1D( "track_Dz0sin_muhigh", "tracks delta z0 sin theta for high mu events", 1000, 0, 4 ) ) );
    hist("track_Dz0sin_muhigh")->Sumw2();

    ATH_CHECK( book( TH1D( "sig_track_d0_muhigh", "significance tracks d0 for high mu events", 100, -6., 6. ) ) );
    hist("sig_track_d0_muhigh")->Sumw2();
    
    ATH_CHECK( book( TH1D( "n_track", "n_track", 150, 0, 150 ) ) );
    hist("n_track")->Sumw2();

    ATH_CHECK( book( TH1D( "jethard_distribution", "jet hard distribution", 100, 0., 1000. ) ) );
    hist("jethard_distribution")->Sumw2();
  */  
    ATH_CHECK( book( TH1D( "Reco_NonIntMET", "Reconstructed - Truth MET", 100, 0., 500. ) ) );
    hist("Reco_NonIntMET")->Sumw2();
   
    ATH_CHECK( book( TH1D( "absReco_NonIntMET", "|Reconstructed| - |Truth| MET", 100, 0., 500. ) ) );
    hist("absReco_NonIntMET")->Sumw2();
   
 ATH_CHECK( book( TH1D( "TSTtruth", "Reconstructed - Truth MET", 1000, -500., 500. ) ) );
    hist("TSTtruth")->Sumw2();
 ATH_CHECK( book( TH1D( "absbadr1", "Reconstructed - Truth MET", 1000, 0., 500. ) ) );
    hist("absbadr1")->Sumw2();
 ATH_CHECK( book( TH1D( "absbadr2", "Reconstructed - Truth MET", 1000, 0., 500. ) ) );
    hist("absbadr2")->Sumw2();
 ATH_CHECK( book( TH1D( "absbadr3", "Reconstructed - Truth MET", 1000, 0., 500. ) ) );
    hist("absbadr3")->Sumw2();

/*
    ATH_CHECK( book( TH1D( "npv_distribution", "number of primary vertices distribution", 200, 0., 200. ) ) );
    hist("npv_distribution")->Sumw2();
    
    ATH_CHECK( book( TH1D( "lowjetpt_distribution", "Jet term for low pt jet events distribution", 100, 0., 1000. ) ) );
    hist("lowjetpt_distribution")->Sumw2();

    ATH_CHECK( book( TH1D( "jetsize60", "Number of jets with pt>60", 100, 0., 100. ) ) );
    hist("jetsize60")->Sumw2();
    ATH_CHECK( book( TH1D( "jetsize40", "Number of jets with pt>40", 100, 0., 100. ) ) );
    hist("jetsize40")->Sumw2();
    ATH_CHECK( book( TH1D( "jetsize25", "Number of jets with pt>25", 100, 0., 100. ) ) );
    hist("jetsize25")->Sumw2();
*/
    /// MET and MET terms distributions in different jet multiplicity bins
    if (m_jetBins) {
      for (Int_t njets = 0; njets <= 2; njets++) {
        std::string s_njets = std::to_string(njets);
        ATH_CHECK( book( TH1D(( "met_"+s_njets+"j").c_str(), ("Magnitude of the total MET, "+s_njets+" jets").c_str(),
			      100, 0., 1000. ) ) );
	ATH_CHECK( book( TH1D( ("jet_met_"+s_njets+"j").c_str(),   ("Magnitude of the jet MET, "+s_njets+" jets").c_str(),
			       100, 0., 1000. ) ) );
        ATH_CHECK( book( TH1D( ("muon_met_"+s_njets+"j").c_str(),  ("Magnitude of the muon MET, "+s_njets+" jets").c_str(),
			       100, 0., 1000. ) ) );
        ATH_CHECK( book( TH1D( ("ele_met_"+s_njets+"j").c_str(),   ("Magnitude of the electron MET, "+s_njets+" jets").c_str(),
			       100, 0., 1000. ) ) );
        ATH_CHECK( book( TH1D( ("soft_met_"+s_njets+"j").c_str(),  ("Magnitude of the soft MET, "+s_njets+" jets").c_str(),
			       100, 0., 1000. ) ) );
        hist("met_"+s_njets+"j")->Sumw2();
        hist("jet_met_"+s_njets+"j")->Sumw2();
        hist("muon_met_"+s_njets+"j")->Sumw2();
        hist("ele_met_"+s_njets+"j")->Sumw2();
        hist("soft_met_"+s_njets+"j")->Sumw2();
      }
    }
/*
    // PU/HS jet histograms
    ATH_CHECK( book( TH2D( "jetHS_pT_vs_jvt_eta24", "HS Jet Pt vs jvt eta2.4",
			   20, 0., 200.0, 24, -0.2, 1.0 ) ) );
    ATH_CHECK( book( TH2D( "jetPU_pT_vs_jvt_eta24", "PU Jet Pt vs jvt eta2.4",
			   20, 0., 200.0, 24, -0.2, 1.0 ) ) );
    ATH_CHECK( book( TH2D( "jetHS_pT_vs_jvt_eta25", "HS Jet Pt vs jvt eta2.5",
			   20, 0., 200.0, 24, -0.2, 1.0 ) ) );
    ATH_CHECK( book( TH2D( "jetPU_pT_vs_jvt_eta25", "PU Jet Pt vs jvt eta2.5",
			   20, 0., 200.0, 24, -0.2, 1.0 ) ) );
    ATH_CHECK( book( TH2D( "jetHS_pT_vs_jvt_eta27", "HS Jet Pt vs jvt eta2.7",
			   20, 0., 200.0, 24, -0.2, 1.0 ) ) );
    ATH_CHECK( book( TH2D( "jetPU_pT_vs_jvt_eta27", "PU Jet Pt vs jvt eta2.7",
			   20, 0., 200.0, 24, -0.2, 1.0 ) ) );
    hist("jetHS_pT_vs_jvt_eta24")->Sumw2();
    hist("jetPU_pT_vs_jvt_eta24")->Sumw2();
    hist("jetHS_pT_vs_jvt_eta25")->Sumw2();
    hist("jetPU_pT_vs_jvt_eta25")->Sumw2();
    hist("jetHS_pT_vs_jvt_eta27")->Sumw2();
    hist("jetPU_pT_vs_jvt_eta27")->Sumw2();
*/
    // jet track sumpt^2
    ATH_CHECK( book( TH2D( "jetPt_vs_sumTrkPtRatio_eta03", "Jet Pt vs the sum Track pT/JetPt",
			   100, 0., 1000., 200, 0., 2.0 ) ) );
    ATH_CHECK( book( TH2D( "jetPt_vs_sumTrkPtRatio_eta08", "Jet Pt vs the sum Track pT/JetPt",
               100, 0., 1000., 200, 0., 2.0 ) ) );
    ATH_CHECK( book( TH2D( "jetPt_vs_sumTrkPtRatio_eta12", "Jet Pt vs the sum Track pT/JetPt",
			   100, 0., 1000., 200, 0., 2.0  ) ) );
    ATH_CHECK( book( TH2D( "jetPt_vs_sumTrkPtRatio_eta17", "Jet Pt vs the sum Track pT/JetPt",
			   100, 0., 1000., 200, 0., 2.0  ) ) );
    ATH_CHECK( book( TH2D( "jetPt_vs_sumTrkPtRatio_eta25", "Jet Pt vs the sum Track pT/JetPt",
			   100, 0., 1000., 200, 0., 2.0  ) ) );
    hist("jetPt_vs_sumTrkPtRatio_eta03")->Sumw2();
    hist("jetPt_vs_sumTrkPtRatio_eta08")->Sumw2();
    hist("jetPt_vs_sumTrkPtRatio_eta12")->Sumw2();
    hist("jetPt_vs_sumTrkPtRatio_eta17")->Sumw2();
    hist("jetPt_vs_sumTrkPtRatio_eta25")->Sumw2();

    ATH_CHECK( book( TH3D( "jetPt_vs_sumTrkPtRatio",
                     "sum Trk pT/JetPt for jet pT vs eta" ,100, 0., 1000.,25, 0.0, 2.5, 50, 0.0, 2.) ) );
    hist3d("jetPt_vs_sumTrkPtRatio")->Sumw2();
/*
    ATH_CHECK( book( TH1D( "met_significance", "MET Significance", 500, 0., 100. ) ) );
    hist("met_significance")->Sumw2();
    ATH_CHECK( book( TH1D( "met_significance_dir", "MET Significance Directional", 500, 0., 100. ) ) );
    hist("met_significance_dir")->Sumw2();
    ATH_CHECK( book( TH1D( "met_over_HT", "MET/HT", 500, 0., 100. ) ) );
    hist("met_over_HT")->Sumw2();
    ATH_CHECK( book( TH1D( "met_over_SumET", "MET/#sum{E_{T}}", 500, 0., 100. ) ) );
    hist("met_over_SumET")->Sumw2();

    ATH_CHECK( book( TH1D( "met_significance_rho", "MET Significance #rho", 50, 0., 1.0 ) ) );
    hist("met_significance_rho")->Sumw2();

    ATH_CHECK( book( TH1D( "met_significance_VarL", "MET Significance VarL [GeV^{2}]", 50, 0., 500. ) ) ); hist("met_significance_VarL")->Sumw2();
    ATH_CHECK( book( TH1D( "met_significance_VarT", "MET Significance VarT [GeV^{2}]", 50, 0., 500. ) ) ); hist("met_significance_VarT")->Sumw2();
    ATH_CHECK( book( TH1D( "met_significance_VarL_jet", "MET Significance Jet VarL [GeV^{2}]", 50, 0., 500. ) ) ); hist("met_significance_VarL_jet")->Sumw2();
    ATH_CHECK( book( TH1D( "met_significance_VarT_jet", "MET Significance Jet VarT [GeV^{2}]", 50, 0., 500. ) ) ); hist("met_significance_VarT_jet")->Sumw2();
    ATH_CHECK( book( TH1D( "met_significance_VarL_muo", "MET Significance Muon VarL [GeV^{2}]", 50, 0., 500. ) ) ); hist("met_significance_VarL_muo")->Sumw2();
    ATH_CHECK( book( TH1D( "met_significance_VarT_muo", "MET Significance Muon VarT [GeV^{2}]", 50, 0., 500. ) ) ); hist("met_significance_VarT_muo")->Sumw2();
    ATH_CHECK( book( TH1D( "met_significance_VarL_ele", "MET Significance Electron VarL [GeV^{2}]", 50, 0., 500. ) ) ); hist("met_significance_VarL_ele")->Sumw2();
    ATH_CHECK( book( TH1D( "met_significance_VarT_ele", "MET Significance Electron VarT [GeV^{2}]", 50, 0., 500. ) ) ); hist("met_significance_VarT_ele")->Sumw2();
    ATH_CHECK( book( TH1D( "met_significance_VarL_pho", "MET Significance Photon VarL [GeV^{2}]", 50, 0., 500. ) ) );  hist("met_significance_VarL_pho")->Sumw2();
    ATH_CHECK( book( TH1D( "met_significance_VarT_pho", "MET Significance Photon VarT [GeV^{2}]", 50, 0., 500. ) ) ); hist("met_significance_VarT_pho")->Sumw2();
    ATH_CHECK( book( TH1D( "met_significance_VarL_tau", "MET Significance Tau VarL [GeV^{2}]", 50, 0., 500. ) ) ); hist("met_significance_VarL_tau")->Sumw2();
    ATH_CHECK( book( TH1D( "met_significance_VarT_tau", "MET Significance Tau VarT [GeV^{2}]", 50, 0., 500. ) ) ); hist("met_significance_VarT_tau")->Sumw2();
    ATH_CHECK( book( TH1D( "met_significance_VarL_soft", "MET Significance Soft VarL [GeV^{2}]", 50, 0., 500. ) ) ); hist("met_significance_VarL_soft")->Sumw2();
    ATH_CHECK( book( TH1D( "met_significance_VarT_soft", "MET Significance Soft VarT [GeV^{2}]", 50, 0., 500. ) ) ); hist("met_significance_VarT_soft")->Sumw2();

    hist("met_significance")->Sumw2();
*/
    // MET and MET terms distributions in different <mu> regions
    std::string muRegion ;
    for (Int_t iMuRegion = 0; iMuRegion <= 6; iMuRegion++) {

      if (iMuRegion == 0)  muRegion = "_mu0_10" ;
      if (iMuRegion == 1 ) muRegion = "_mu10_20" ;
      if (iMuRegion == 2 ) muRegion = "_mu20_30" ;
      if (iMuRegion == 3 ) muRegion = "_mu30_40" ;
      if (iMuRegion == 4 ) muRegion = "_mu40_50" ;
      if (iMuRegion == 5 ) muRegion = "_mu50_60" ;
      if (iMuRegion == 6 ) muRegion = "_mu60_70" ;

      ATH_CHECK( book( TH1D( ("metsig"+muRegion).c_str(),   ("Magnitude of MET significance, "+muRegion).c_str(), 100, 0., 100. ) ) );
      ATH_CHECK( book( TH1D( ("met"+muRegion).c_str(),   ("Magnitude of the total MET, "+muRegion).c_str(), 100, 0., 1000. ) ) );
      ATH_CHECK( book( TH1D( ("jet_met"+muRegion).c_str(),   ("Magnitude of the jet MET, "+muRegion).c_str(), 100, 0., 1000. ) ) );
      ATH_CHECK( book( TH1D( ("soft_met"+muRegion).c_str(),   ("Magnitude of the soft MET, "+muRegion).c_str(), 100, 0., 1000. ) ) );
      ATH_CHECK( book( TH1D( ("sumet"+muRegion).c_str(), ("Total Sum ET, "+muRegion).c_str(), 100, 0., 1000. ) ) );
      ATH_CHECK( book( TH1D( ("pThard_distribution"+muRegion).c_str(), ("pT hard distribution, "+muRegion).c_str(), 200, 0., 500. ) ) );
      ATH_CHECK( book( TH1D( ("ele_met"+muRegion).c_str(),   ("Magnitude of the electron MET, "+muRegion).c_str(), 100, 0., 1000. ) ) );
      ATH_CHECK( book( TH1D( ("muon_met"+muRegion).c_str(),   ("Magnitude of the muon MET, "+muRegion).c_str(), 100, 0., 1000. ) ) );
      hist("metsig"+muRegion)->Sumw2();
      hist("met"+muRegion)->Sumw2();
      hist("jet_met"+muRegion)->Sumw2();
      hist("soft_met"+muRegion)->Sumw2();
      hist("sumet"+muRegion)->Sumw2();
      hist("pThard_distribution"+muRegion)->Sumw2();
      hist("ele_met"+muRegion)->Sumw2();
      hist("muon_met"+muRegion)->Sumw2();
      //
    }
/*
    ATH_CHECK( book( TH2D( "residxy_mu", "Total MET residuals vs mu",
			   80, 0., 80., 100, -100., 100.  ) ) );
    ATH_CHECK( book( TH2D( "metxy_mu", "Total MET  vs mu",
                           80, 0., 80., 100, -100., 1000.  ) ) );
    ATH_CHECK( book( TH2D( "residL_mu", "Total MET long. residuals vs mu",
			   80, 0., 80., 100, -100., 100.  ) ) );
    ATH_CHECK( book( TH2D( "residT_mu", "Total MET trans. residuals vs mu",
			   80, 0., 80., 100, -100., 100.  ) ) );
    hist("residxy_mu")->Sumw2();
    hist("metxy_mu")->Sumw2();
    hist("residL_mu")->Sumw2();
    hist("residT_mu")->Sumw2();
    //
    ATH_CHECK( book( TH2D( "residxy_npv", "Total MET residuals vs npv",
			   80, 0., 80., 100, -100., 100.  ) ) );
    ATH_CHECK( book( TH2D( "metxy_npv", "Total MET  vs npv",
                           80, 0., 80., 100, -100., 1000.  ) ) );
    ATH_CHECK( book( TH2D( "metxy_npv_muweight", "Total MET  vs npv",
                           80, 0., 80., 100, -100., 1000.  ) ) );
    ATH_CHECK( book( TH2D( "residL_npv", "Total MET long. residuals vs npv",
			   80, 0., 80., 100, -100., 100.  ) ) );
    ATH_CHECK( book( TH2D( "residT_npv", "Total MET trans. residuals vs npv",
			   80, 0., 80., 100, -100., 100.  ) ) );
    hist("residxy_npv")->Sumw2();
    hist("metxy_npv")->Sumw2();
    hist("metxy_npv_muweight")->Sumw2();
    hist("residL_npv")->Sumw2();
    hist("residT_npv")->Sumw2();
    //
    // adding to check data/mc npv agreement in mc16c
    ATH_CHECK( book( TH2D( "residxy_npv_muleq60", "Total MET residuals vs npv",
               50, 0., 50., 100, -100., 100.  ) ) );
    ATH_CHECK( book( TH2D( "metxy_npv_muleq60", "Total MET  vs npv",
                           50, 0., 50., 100, -100., 1000.  ) ) );
    hist("residxy_npv_muleq60")->Sumw2();
    hist("metxy_npv_muleq60")->Sumw2();
    //
    //
    ATH_CHECK( book( TH2D( "residxy_sumet", "Total MET residuals vs sumet",
			   50, 0., 1000., 100, -100., 100.  ) ) );
    ATH_CHECK( book( TH2D( "residL_sumet", "Total MET long. residuals vs sumet",
			   50, 0., 1000., 100, -100., 100.  ) ) );
    ATH_CHECK( book( TH2D( "residT_sumet", "Total MET trans. residuals vs sumet",
			   50, 0., 1000., 100, -100., 100.  ) ) );
    hist("residxy_sumet")->Sumw2();
    hist("residL_sumet")->Sumw2();
    hist("residT_sumet")->Sumw2();
*/  
  //
/*  
  ATH_CHECK( book( TH2D( "residxy_CSTsumet", "Total MET residuals vs CSTsumet",
                           50, 0., 1000., 100, -100., 100.  ) ) );
    ATH_CHECK( book( TH2D( "residL_CSTsumet", "Total MET long. residuals vs CSTsumet",
                           50, 0., 1000., 100, -100., 100.  ) ) );
    ATH_CHECK( book( TH2D( "residT_CSTsumet", "Total MET trans. residuals vs CSTsumet",
                           50, 0., 1000., 100, -100., 100.  ) ) );
    hist("residxy_CSTsumet")->Sumw2();
    hist("residL_CSTsumet")->Sumw2();
    hist("residT_CSTsumet")->Sumw2();
    //
    ATH_CHECK( book( TH2D( "residxy_TSTsumet", "Total MET residuals vs TSTsumet",
                           50, 0., 1000., 100, -100., 100.  ) ) );
    ATH_CHECK( book( TH2D( "residL_TSTsumet", "Total MET long. residuals vs TSTsumet",
                           50, 0., 1000., 100, -100., 100.  ) ) );
    ATH_CHECK( book( TH2D( "residT_TSTsumet", "Total MET trans. residuals vs TSTsumet",
                           50, 0., 1000., 100, -100., 100.  ) ) );
    hist("residxy_TSTsumet")->Sumw2();
    hist("residL_TSTsumet")->Sumw2();
    hist("residT_TSTsumet")->Sumw2();
*/
/*
    ATH_CHECK( book( TH2D( "residL_pthard", "Total MET long. residuals vs ptHard",
			   100, 0., 1000., 800, -200., 200.  ) ) );
    ATH_CHECK( book( TH2D( "residT_pthard", "Total MET trans. residuals vs ptHard",
			   100, 0., 1000., 400, -100., 100.  ) ) );
    //
    ATH_CHECK( book( TH2D( "scaleL_pthard", "Total MET long. scale vs ptHard",
			   100, 0., 1000., 50, 0., 5.  ) ) );
    hist("residL_pthard")->Sumw2();
    hist("residT_pthard")->Sumw2();
    hist("scaleL_pthard")->Sumw2();

    ATH_CHECK( book( TH2D( "softL_pthard", "MET soft term long. residuals vs ptHard",
			   200, 0., 1000., 800, -200., 200.   ) ) );
    ATH_CHECK( book( TH2D( "softT_pthard", "MET soft term trans. residuals vs ptHard",
			   200, 0., 1000.,  400, -100., 100.  ) ) );
    hist2d("softL_pthard")->Sumw2();
    hist2d("softT_pthard")->Sumw2();
    ATH_CHECK( book( TH2D( "met_vs_soft", "MET vs Soft Term",
			   100, 0., 400.,  100, 0., 400.  ) ) );
    hist2d("met_vs_soft")->Sumw2();

    // pLsoft_vsPthard_vsSumEt - for CST syst studies
    ATH_CHECK( book( TH3D( "pLsoft_vsPthard_vsSumet",
                     "soft term long proj (pL) on pthard vs pthard vs sumet" ,200, 0., 2000.,40, 0., 200.,1400, -80., 200. ) ) );//x:sumet, y:pThard, z:pLsoft
    hist3d("pLsoft_vsPthard_vsSumet")->Sumw2();
*/

    if (m_jetBins) {
      for (Int_t njets = 0; njets <= 2; njets++) {
        std::string s_njets = std::to_string(njets);
  /*      ATH_CHECK( book( TH2D( ("residL_pthard_"+s_njets+"j").c_str(),
                               ("Total MET long. residuals vs pthard, "+s_njets+" jets").c_str(),
                               100, 0., 1000., 800, -200., 200.  ) ) );
        ATH_CHECK( book( TH2D( ("residT_pthard_"+s_njets+"j").c_str(),
                               ("Total MET trans. residuals vs pthard, "+s_njets+" jets").c_str(),
                               100, 0., 1000., 400, -100., 100.  ) ) );
        hist2d("residL_pthard_"+s_njets+"j")->Sumw2();
        hist2d("residT_pthard_"+s_njets+"j")->Sumw2();

        ATH_CHECK( book( TH2D( ("softL_pthard_"+s_njets+"j").c_str(),
                               ("Total MET long. soft term vs pthard, "+s_njets+" jets").c_str(),
                               200, 0., 1000., 800, -200., 200.   ) ) );
        ATH_CHECK( book( TH2D( ("softT_pthard_"+s_njets+"j").c_str(),
                               ("Total MET trans. soft term vs pthard, "+s_njets+" jets").c_str(),
                               200, 0., 1000., 400, -100., 100.  ) ) );
        hist2d("softL_pthard_"+s_njets+"j")->Sumw2();
        hist2d("softT_pthard_"+s_njets+"j")->Sumw2();
*/  
    }
    }
    if( m_evttype==dijet){
      ATH_CHECK( book( TH1D( "dijet_pt", "dijet system pt", 100, 0., 1000. ) ) );
      hist("dijet_pt")->Sumw2();
      ATH_CHECK( book( TH1D( "leading_jet_pt", "leading jet pt", 100, 0., 1000. ) ) );
      hist("leading_jet_pt")->Sumw2();
      ATH_CHECK( book( TH1D( "subleading_jet_pt", "subleading jet pt", 100, 0., 1000. ) ) );
      hist("subleading_jet_pt")->Sumw2();
      ATH_CHECK( book( TH1D( "subsubleading_jet_pt", "subsubleading jet pt", 100, 0., 1000. ) ) );
      hist("subsubleading_jet_pt")->Sumw2();
    }
    if( m_evttype==Zmm || m_evttype==Zee ) {
      ATH_CHECK( book( TH1D( "mZ", "Z mass", 100, 50., 150. ) ) );
      ATH_CHECK( book( TH1D( "ptZ", "Z transverse momentum", 100, 0., 1000. ) ) );
      ATH_CHECK( book( TH1D( "pthard", "Hard objects vector sum", 100, 0., 1000. ) ) );
      hist("mZ")->Sumw2();
      hist("ptZ")->Sumw2();
      hist("pthard")->Sumw2();
      //
      ATH_CHECK( book( TH1D( "leadingLep_pt", "Z mass", 200, 0., 1000. ) ) );
      ATH_CHECK( book( TH1D( "subleadingLep_pt", "subleading lepton pT", 200, 0., 1000. ) ) );
      hist("leadingLep_pt")->Sumw2();
      hist("subleadingLep_pt")->Sumw2();
      //
      ATH_CHECK( book( TH1D( "pL_ptz", "Long. projection on ptZ of the total MET", 100, -250., 250. ) ) );
      ATH_CHECK( book( TH2D( "pL_ptz_ptz", "Long. projection on ptZ of the total MET vs ptZ", 40, 0., 200., 100, -250., 250. ) ) );
      ATH_CHECK( book( TH1D( "pT_ptz", "Trans. projection on ptZ of the total MET", 100, -250., 250. ) ) );
      hist("pL_ptz")->Sumw2();
      hist2d("pL_ptz_ptz")->Sumw2();
      hist("pT_ptz")->Sumw2();
      //
      ATH_CHECK( book( TH2D( "residL_ptz", "Total MET long. residuals vs ptZ",
			     100, 0., 1000., 200, -200., 200.  ) ) );
      ATH_CHECK( book( TH2D( "residT_ptz", "Total MET trans. residuals vs ptZ",
			     100, 0., 1000., 200, -100., 100.  ) ) );
      hist2d("residL_ptz")->Sumw2();
      hist2d("residT_ptz")->Sumw2();
      //
      ATH_CHECK( book( TH2D( "scaleL_ptz", "Total MET long. scale vs ptZ",
			     100, 0., 1000., 50, 0., 5. ) ) );
      hist2d("scaleL_ptz")->Sumw2();

      ATH_CHECK( book( TH2D( "ratio_jetpt_ptz", "Ratio of leading jet pT to ptZ vs ptZ",
			     40, 0., 200., 100, 0., 5. ) ) );
      hist2d("ratio_jetpt_ptz")->Sumw2();
      ATH_CHECK( book( TH2D( "ratio_jetpt_ptz_1j", "Ratio of leading jet pT to ptZ vs ptZ",
			     40, 0., 200., 100, 0., 5. ) ) );
      hist2d("ratio_jetpt_ptz_1j")->Sumw2();

      ATH_CHECK( book( TH2D( "ratio_residL_ptz", "1 - Ratio of total MET long. residuals to ptZ vs ptZ",
			     40, 0., 200., 200, -10., 10.  ) ) );
      hist2d("ratio_residL_ptz")->Sumw2();
      ATH_CHECK( book( TH2D( "ratio_residL_ptz_1j", "1 - Ratio of total MET long. residuals to ptZ vs ptZ",
			     40, 0., 200., 200, -10., 10.  ) ) );
      hist2d("ratio_residL_ptz_1j")->Sumw2();
      ATH_CHECK( book( TH2D( "ratio_residL_ptz_1j_etacut", "1 - Ratio of total MET long. residuals to ptZ vs ptZ",
			     40, 0., 200., 200, -10., 10.  ) ) );
      hist2d("ratio_residL_ptz_1j_etacut")->Sumw2();
      ATH_CHECK( book( TH1D( "njets", "NJets", 15, 0, 15 ) ) );
      hist("njets")->Sumw2();
      ATH_CHECK( book( TH2D( "sumet_ptz", "Jet sumet vs ptZ",
			     100, 0., 1000., 100, 0., 1000.  ) ) );
      hist2d("sumet_ptz")->Sumw2();
      ATH_CHECK( book( TH2D( "forw_cent", "Forward jet vs Central jet sums",
			     100, 0., 1000., 100, 0., 1000.  ) ) );
      hist2d("forw_cent")->Sumw2();
/*
      if (m_jetBins) {
        for (Int_t njets = 0; njets <= 2; njets++) {
          std::string s_njets = std::to_string(njets);
          ATH_CHECK( book( TH2D( ("residL_ptz"+s_njets+"j").c_str(),
                                 ("Total MET long. residuals vs ptZ, "+s_njets+" jets").c_str(),
                                 100, 0., 1000., 100, -100., 100.  ) ) );
          ATH_CHECK( book( TH2D( ("residT_ptz"+s_njets+"j").c_str(),
                                 ("Total MET trans. residuals vs ptZ, "+s_njets+" jets").c_str(),
                                 100, 0., 1000., 100, -100., 100.  ) ) );
          hist2d("residL_ptz"+s_njets+"j")->Sumw2();
          hist2d("residT_ptz"+s_njets+"j")->Sumw2();
        }
      }
*/  
  }

    ATH_CHECK( book( TH1D( "jetrecosums", "Sum of reco jets matched to truth", 100, 0., 1000. ) ) );
    hist("jetrecosums")->Sumw2();
    ATH_CHECK( book( TH2D( "forw_cent_truth", "Forward vs Central truth matched jet sums",
      		     100, 0., 1000., 100, 0., 1000.  ) ) );
    hist2d("forw_cent_truth")->Sumw2();

    if( m_evttype==Wmv || m_evttype==Wev || m_evttype==ttbar ) {
      ATH_CHECK( book( TH1D( "WmT", "W transverse mass", 100, 0., 150. ) ) );
      //
      ATH_CHECK( book( TH2D( "residxy_truth", "Deviation of MET magnitude vs truth MET",
                             100, 0., 1000., 100, -100., 100. ) ) );
      ATH_CHECK( book( TH2D( "lin_truth", "Deviation of MET magnitude vs truth MET",
			     100, 0., 1000., 240, -2., 10. ) ) );
      ATH_CHECK( book( TH2D( "residL_truth", "Total MET long. residuals vs truth MET",
			     100, 0., 1000., 100, -100., 100. ) ) );
      ATH_CHECK( book( TH2D( "residT_truth", "Total MET trans. residuals magnitude vs truth MET",
			     100, 0., 1000., 100, -100., 100. ) ) );
      ATH_CHECK( book( TH2D( "biasL_truth", "Deviation of projected MET magnitude vs truth MET",
			     100, 0., 1000., 50, -5., 5. ) ) );
      ATH_CHECK( book( TH2D( "reco_vs_truth", "Total reco MET vs Truth MET",
                             100, 0., 1000., 40, 0., 200. ) ) );
      //
      ATH_CHECK( book( TH2D( "residPhi_truth", "Total MET phi residuals vs truth MET",
			     100, 0., 1000., 64, -3.2, 3.2 ) ) );
      hist("WmT")->Sumw2();
      hist("residxy_truth")->Sumw2();
      hist("lin_truth")->Sumw2();
      hist("residL_truth")->Sumw2();
      hist("residT_truth")->Sumw2();
      hist("biasL_truth")->Sumw2();
      hist("reco_vs_truth")->Sumw2();
      hist("residPhi_truth")->Sumw2();
    }
/*
    double jetbins[21] = {10.,20., 20.*sqrt(2), 40., 40.*sqrt(2), 80., 80.*sqrt(2), 160., 160.*sqrt(2),
			  320., 320.*sqrt(2), 640., 640.*sqrt(2), 1280., 1280*sqrt(2),
			  2560., 2560.*sqrt(2), 5120., 5120.*sqrt(2), 10240., 10240.*sqrt(2)};
    ATH_CHECK( book( TH2D( "jetresponse",   "Ratio of reco to true jet pt", 20, jetbins, 100, 0., 2. ) ) );
    hist("jetresponse")->Sumw2();

    ATH_CHECK( book( TH1D( "jetsizeHisto", "jets size", 50, 0., 50.) ) );
    hist("jetsizeHisto")->Sumw2();
*/
    if(m_doMuonElossPlots){
/*
    	ATH_CHECK( book( TH1D( "elosstruth",   "Energy Loss Truth", 100, 0., 1000. ) ) );
    	ATH_CHECK( book( TH1D( "eloss_fit_truth",   "Energy Loss Fit minus Truth difference", 100, -50., 50. ) ) );
    	ATH_CHECK( book( TH1D( "elossfit",   "Energy Loss Fit", 100, 0., 1000. ) ) );
    	ATH_CHECK( book( TH1D( "elossmeas",   "Measured Energy Loss", 100, 0., 1000. ) ) );
    	ATH_CHECK( book( TH1D( "elossparam",   "Parametrized Energy Loss", 100, 0., 1000. ) ) );
    	ATH_CHECK( book( TH1D( "elossfact",   "Energy Loss Scale Factor", 100, 0., 1. ) ) );
    	ATH_CHECK( book( TH2D( "jetpt_eloss",   "Correlation plot of jet p_{T} vs Muon Eloss", 100, 0., 1000., 100, 0., 1000. ) ) );
    	ATH_CHECK( book( TH2D( "muonpt_met",   "Correlation plot of muon p_{T} vs MET", 100, 0., 1000., 100, 0., 1000. ) ) );
    	ATH_CHECK( book( TH2D( "muon_pt_ip_met",   "Correlation plot of muon p_{T,IP} vs MET", 100, 0., 1000., 100, 0., 1000. ) ) );
    	ATH_CHECK( book( TH2D( "muon_pt_ip_jet_met",   "Correlation plot of muon p_{T,IP} vs Jet MET", 100, 0., 1000., 100, 0., 1000. ) ) );
      hist("elosstruth")->Sumw2();
      hist("eloss_fit_truth")->Sumw2();
      hist("elossfit")->Sumw2();
      hist("elossmeas")->Sumw2();
      hist("elossparam")->Sumw2();
      hist("elossfact")->Sumw2();
      hist2d("muonpt_met")->Sumw2();
      hist2d("muon_pt_ip_met")->Sumw2();
      hist2d("muon_pt_ip_jet_met")->Sumw2();
*/  
  }

    return StatusCode::SUCCESS;
  }

  //**********************************************************************

  StatusCode METPerfAlg::finalize() {
    ATH_MSG_INFO ("Finalizing " << name() << "...");
    return StatusCode::SUCCESS;
  }

  //**********************************************************************

  StatusCode METPerfAlg::execute() {
    ATH_MSG_DEBUG("Executing " << name() << "...");
    const xAOD::EventInfo* eventinfo = 0;
    ATH_CHECK( evtStore()->retrieve( eventinfo, "EventInfo" ) );

    bool isSim = eventinfo->eventType(xAOD::EventInfo::IS_SIMULATION);
    double weight=1;


    ATH_MSG_DEBUG("Event");

    if(isSim) {
      double mcweight = eventinfo->mcEventWeight();
      float pileupweight(1.);
      if (m_doPileupReweighting) pileupweight = dec_pileupWeight(*eventinfo);
   
      ATH_MSG_VERBOSE("Pileup weight: " << pileupweight);
      //ATH_MSG_VERBOSE("Avg mu: "<<avgmu);

      weight = mcweight * pileupweight;

      // Muon scale factors
      const xAOD::MuonContainer *goodmu=0;
      CHECK(evtStore()->retrieve(goodmu, m_muoncoll));

      // Muon eff and iso scale factors
      float muonRecoEffSF = dec_muonRecoEffSF(*eventinfo);
      float muonIsoEffSF  = dec_muonIsoEffSF( *eventinfo);

      ATH_MSG_VERBOSE("Total muon reco scale factor: " << muonRecoEffSF);
      ATH_MSG_VERBOSE("Total muon iso scale factor: "  << muonIsoEffSF );

      weight *= muonRecoEffSF * muonIsoEffSF;

      // Electron efficiency scale factors
      float eleRecoEffSF = dec_eleRecoEffSF(*eventinfo);
      float eleIDEffSF   = dec_eleIDEffSF(  *eventinfo);
      float eleIsoEffSF  = dec_eleIsoEffSF( *eventinfo);

      ATH_MSG_VERBOSE("Total electron reco scale factor: " << eleRecoEffSF);
      ATH_MSG_VERBOSE("Total electron ID scale factor: "   << eleIDEffSF);
      ATH_MSG_VERBOSE("Total electron iso scale factor: "  << eleIsoEffSF);
      weight *= eleRecoEffSF * eleIDEffSF * eleIsoEffSF;
      // Trigger scale factors
      if (m_applyTriggerSF) {
        double triggerSF = dec_triggerSF(*eventinfo);
        ATH_MSG_VERBOSE("Applying trigger scale factor: " << triggerSF);
        weight *= triggerSF;
      }

      if (m_evttype == ttbar) {
        // If we are looking at ttbar events (which need a b-tag), apply the b-tagging SF.

//        float btagSF = dec_btagEffSF(*eventinfo);

  //      ATH_MSG_DEBUG("Retrieved b-tagging scale factor: " << btagSF);
    //    weight *= btagSF;
      }
    } // isSim
    const VertexContainer* pvertices = 0;
    unsigned int npv = 0;
    ATH_CHECK( evtStore()->retrieve( pvertices, "PrimaryVertices" ) );
    for(const auto& vx : *pvertices) {
      // Count primary or pileup vertices
      if ((vx->vertexType() == 1) || (vx->vertexType() == 3)) ++npv;
    }
    if(!m_isPVAcc(*eventinfo)) {
      ATH_MSG_DEBUG("Event has failed primary vertex selection.");
      return StatusCode::SUCCESS;
    }
    if(!m_evtCleanAcc(*eventinfo) || !m_jetCleanAcc(*eventinfo)) {
      ATH_MSG_DEBUG("Event has failed cleaning.");
      return StatusCode::SUCCESS;
    }
    if(!m_passAllEvtSelections(*eventinfo)) {
      ATH_MSG_DEBUG("Event has failed event selections.");
      return StatusCode::SUCCESS;
    }
    switch(m_evttype) {
    case None: break;
    case Zmm:
      if(!m_isZmmAcc(*eventinfo)) return StatusCode::SUCCESS;
      break;
    case Zee:
      if(!m_isZeeAcc(*eventinfo)) return StatusCode::SUCCESS;
      break;
    case Wmv:
      if(!m_isWmvAcc(*eventinfo)) return StatusCode::SUCCESS;
      break;
    case Wev:
      if(!m_isWevAcc(*eventinfo)) return StatusCode::SUCCESS;
      break;
    case ttbar:
      if(!m_isttbarAcc(*eventinfo)) return StatusCode::SUCCESS;
      break;
    case dijet:
      if(!m_isdijetAcc(*eventinfo)) return StatusCode::SUCCESS;
      break;
    case gjet:
      if(!m_isgjetAcc(*eventinfo)) return StatusCode::SUCCESS;
      break;
    default:
      return StatusCode::SUCCESS;
    }
    // retrieve MET containers
    ATH_MSG_DEBUG("Retrieve MET " << m_metsuffix);
    MissingETContainer* met = 0;
    MissingETAuxContainer* metaux = 0;
    ATH_CHECK( evtStore()->retrieve(met, "MET_"+m_metsuffix) );
    ATH_CHECK( evtStore()->retrieve(metaux, "MET_"+m_metsuffix+"Aux.") );
    met->setStore(metaux);
    // Do jet veto according to CST SumET
    //const MissingETContainer* met_cst = 0;
    // ATH_CHECK( evtStore()->retrieve(met_cst, "MET_RefCST") );
    // //if(m_dojetveto && (*met_cst)[m_jetterm]->met()>1e-9) return StatusCode::SUCCESS;
    // if(m_dojetveto && (*met_cst)[m_jetterm]->sumet()>1e-9) return StatusCode::SUCCESS;

    // Old jet veto
    //if(m_dojetveto && (*met)[m_jetterm]->met()>1e-9) return StatusCode::SUCCESS;
    // New jet veto following Peter's suggestion, for no jets require also no gamma and tau term

    const JetContainer* jetcheck = 0;
    ATH_CHECK( evtStore()->retrieve(jetcheck, m_jetcoll) );
   // hist("jetsizeHisto")->Fill(jetcheck->size(), weight);
    ATH_MSG_DEBUG( "In GoodJets there are N jets =  " << jetcheck->size()  );
    

    /*const xAOD::MissingETAssociationMap *metMap_JetCheck=0;
    ATH_CHECK(evtStore()->retrieve(metMap_JetCheck, "METAssoc_AntiKt4EMTopo"));

    for(const auto & jet : *jetcheck){
      //MissingETComposition::selectIfNoOverlaps(map,orig,p)
      const MissingETAssociation* assoc = MissingETComposition::getAssociation(metMap_JetCheck,jet);
      const IParticle* orig = *m_objLinkAcc(*jet);
      // fill the sumPT2
      if(MissingETComposition::objSelected(metMap_JetCheck,orig))
	hist("jetPt_vs_sumTrkPt")->Fill(jet->pt()/1.0e3, assoc->overlapTrkVec().sumpt()/1.0e3,weight);      
	}*/
    
    // forward jet veto
    if(m_dofwdjetveto){
	for(const auto & jet : *jetcheck){
	  if(fabs(jet->eta())>2.4 && jet->pt()>m_fwdJetMinPt) {
	        ATH_MSG_VERBOSE("Detected event with forward jet (pt,eta,phi) = (" << jet->pt()/GeV <<", "<< jet->eta() << ", " << jet->phi() << "). Event rejected from histogram filling.");
		return StatusCode::SUCCESS;
            }
	}
    }

    if(m_dojetveto && (
                      (*met)[m_jetterm]->met()>1e-9
                      || ((*met)[m_gammaterm] && (*met)[m_gammaterm]->met()>1e-9)
                      || ((*met)[m_tauterm]   && (*met)[m_tauterm]->met()>1e-9)
                      )
      )return StatusCode::SUCCESS;
    // // Apply cuts on MET and mT using TST MET
    if ((m_evttype == Wev) || (m_evttype == Wmv)) {
//      const MissingETContainer* met_tst = 0;
//      ATH_CHECK( evtStore()->retrieve(met_tst, "MET_RefTST") );
//      const MissingET* pMET = (*met_tst)["Final"];
//
      // Apply cut on MET
      // // ATH_MSG_DEBUG("Applying MET cut.");
      // if (pMET->met() < m_wlnmetcut) {
      if ((*met)["Final"]->met() < m_wlnmetcut) {
        ATH_MSG_DEBUG("Event fails Wln MET cut.");
        return StatusCode::SUCCESS;
      }
      // Following Marianna's suggestion try run I cuts, a cut of  WmT < 50 GeV, MET < 25 GeV
      if (m_evttype == Wmv) {
        ATH_MSG_DEBUG("Applying mT cut (Wmv).");
        const MuonContainer* muon_fromW = 0;
        ATH_CHECK( evtStore()->retrieve(muon_fromW,"muonFromW") );
        const xAOD::Muon* pMu = muon_fromW->front();
        // Apply cut on mT
        IParticle::FourMom_t Wboson;
        IParticle::FourMom_t nu;
        nu.SetPtEtaPhiM( (*met)["Final"]->met(),0, (*met)["Final"]->phi(),0);
        Wboson=pMu->p4()+nu;
        double WmT=Wboson.Mt();
        //if (WmT<40e3) {
        if (WmT<50e3) {
          ATH_MSG_DEBUG("Event fails Wln mT cut.");
          return StatusCode::SUCCESS;
        }
      }
      else if (m_evttype == Wev) {
        ATH_MSG_DEBUG("Applying mT cut (Wev).");
        const ElectronContainer* ele_fromW = 0;
        ATH_CHECK( evtStore()->retrieve(ele_fromW,"eleFromW") );
        const xAOD::Electron* pEl = ele_fromW->front();
        // Apply cut on mT
        IParticle::FourMom_t Wboson;
        IParticle::FourMom_t nu;
        nu.SetPtEtaPhiM( (*met)["Final"]->met(),0, (*met)["Final"]->phi(),0);
        Wboson=pEl->p4()+nu;
        double WmT=Wboson.Mt();
        //if (WmT<40e3) {
        if (WmT<50e3) {
          ATH_MSG_DEBUG("Event fails Wln mT cut.");
          return StatusCode::SUCCESS;
        }
      }

    }
    //retrieve the PRW corrected <mu> or the uncorrected one. NB: If slimmed xAOD was produced enabling PRW, you can remove its effect by disabling PRW at plotting stage
    float avgmu = dec_avgMu(*eventinfo);
    if(!(m_doPileupReweighting))
       avgmu = dec_avgMunocorrection(*eventinfo);

    // Fill histograms of mu distributions after event selection
  /*  hist("mu_distribution"  )->Fill(avgmu); // unweighted
    hist("mu_distribution_w")->Fill(avgmu, weight); // weighted
    hist("mu_distribution_w_nosf")->Fill(avgmu/m_PRWdatasf, weight); // weighted
*/
    float jet_px=0.0, jet_py=0.0, emfrac=-1.0;
    unsigned my_njet=0;
    unsigned my_nfwdjet=0;
    for(const auto & jet : *jetcheck){
      jet->getAttribute(xAOD::JetAttribute::EMFrac, emfrac);
      //std::cout << "emfrac: " << emfrac  << std::endl;
      if(fabs(jet->eta())<1.0) hist("goodjets_emfrac_eta1")->Fill(emfrac,weight);
      if(jet->eta()>0.0 && jet->eta()<1.0 && jet->phi()>2.7 && jet->phi()<2.9) hist("goodjets_emfrac_LBA2930")->Fill(emfrac,weight);
        hist("goodjets_eta")->Fill(jet->eta(),weight);
        hist2d("goodjets_eta_vs_phi")->Fill(jet->eta(),jet->phi(),weight);
        if(avgmu>50) hist("goodjets_eta_mu50")->Fill(jet->eta(),weight);
	ATH_MSG_VERBOSE("jet: " << jet->pt() << " " << jet->eta() << " " << jet->phi() << " jvt: " << acc_jvt(*jet));
	++my_njet;
	if(fabs(jet->eta())>2.4) ++my_nfwdjet;
	jet_px-=jet->px();
	jet_py-=jet->py();
    }
    hist2d("njet_vs_mu")   ->Fill(avgmu, my_njet,    weight);
    hist2d("nfwdjet_vs_mu")->Fill(avgmu, my_nfwdjet, weight);
    ATH_MSG_DEBUG("jet_px: " << jet_px << " " << jet_py);
    const ElectronContainer* eleselect = 0;
    ATH_CHECK( evtStore()->retrieve(eleselect, m_elecoll) );
    for(const auto& e : *eleselect) {
      //const xAOD::TrackParticle* idtrack = xAOD::EgammaHelpers::getOriginalTrackParticleFromGSF(e->trackParticle(0));
      ATH_MSG_DEBUG("elec: " << e->pt() << " " << e->eta() << " " << e->phi());
    }

    //ATH_MSG_DEBUG("Retrieve LocHadTopo");
    //const MissingETContainer* lht = 0;
    //ATH_CHECK( evtStore()->retrieve(lht, "MET_LocHadTopo") );

    //// use LocHadTopo + Muon sumet as common sumet reference
    //double lht_sumet = (*lht)["LocHadTopo"]->sumet() + (*met)["Muons"]->sumet();

    //ATH_MSG_DEBUG("LHT+mu sumet = " << lht_sumet);

    // get some MET objects for convenience
    MissingETContainer* metperf = new MissingETContainer();
    MissingETAuxContainer* metperfaux = new MissingETAuxContainer();
    metperf->setStore(metperfaux);
    ATH_CHECK( evtStore()->record(metperf,"MET_"+name()) );
    ATH_CHECK( evtStore()->record(metperfaux,"MET_"+name()+"Aux.") );

    MissingET* met_final = new MissingET();
    metperf->push_back(met_final);
    *met_final = *(*met)["Final"];

    MissingET* met_jetterm = new MissingET();
    metperf->push_back(met_jetterm);
    *met_jetterm = *(*met)[m_jetterm];

    MissingET* met_soft  = new MissingET();
    metperf->push_back(met_soft);
    *met_soft = *(*met)[m_softterm];

    MissingET* met_hard  = new MissingET();
    metperf->push_back(met_hard);
    *met_hard = *met_final - *met_soft;
    met_hard->setName("Hard");

    // fill plots for track MET systematics
    const xAOD::MissingETAssociationMap *metMap_JetCheck=0;
    ATH_CHECK(evtStore()->retrieve(metMap_JetCheck, "METAssoc_AntiKt4EMTopo"));
    //for(const auto& jetCheck : m_constitObjLinks(*met_jetterm)) { // need to properly correct the jet from the MET map
    for(const auto & jet : *jetcheck){ // coding is a little funny because we want to move to the above collection from the METMap.
      if(!(fabs(jet->eta())<2.5 && jet->pt()>m_jetMinPt)) continue;
      const IParticle* orig = *m_objLinkAcc(*jet);
      if(true){
        const xAOD::Jet* origj(static_cast<const xAOD::Jet*>(orig));
        const MissingETAssociation* assoc = MissingETComposition::getAssociation(metMap_JetCheck,origj);

        if(!assoc) continue;
        if(     fabs(jet->eta())<0.3) hist2d("jetPt_vs_sumTrkPtRatio_eta03")->Fill(jet->pt()/1.0e3, assoc->jettrksumpt()/jet->pt(),weight);
        else if(fabs(jet->eta())<0.8) hist2d("jetPt_vs_sumTrkPtRatio_eta08")->Fill(jet->pt()/1.0e3, assoc->jettrksumpt()/jet->pt(),weight);
        else if(fabs(jet->eta())<1.2) hist2d("jetPt_vs_sumTrkPtRatio_eta12")->Fill(jet->pt()/1.0e3, assoc->jettrksumpt()/jet->pt(),weight);
        else if(fabs(jet->eta())<1.7) hist2d("jetPt_vs_sumTrkPtRatio_eta17")->Fill(jet->pt()/1.0e3, assoc->jettrksumpt()/jet->pt(),weight);
        else if(fabs(jet->eta())<2.5) hist2d("jetPt_vs_sumTrkPtRatio_eta25")->Fill(jet->pt()/1.0e3, assoc->jettrksumpt()/jet->pt(),weight);
        hist3d("jetPt_vs_sumTrkPtRatio")->Fill(jet->pt()/1.0e3, fabs(jet->eta()), assoc->jettrksumpt()/jet->pt(),weight);
      }
    }
/*  
  ANA_CHECK( m_metSignif->varianceMET(met, avgmu, m_jetterm, m_softterm, "Final"));
    hist("met_significance")->Fill(m_metSignif->GetSignificance(),weight);
    hist("met_significance_VarL")->Fill(m_metSignif->GetVarL(),weight);
    hist("met_significance_VarT")->Fill(m_metSignif->GetVarT(),weight);
    hist("met_significance_rho")->Fill(m_metSignif->GetRho(),weight);
    hist("met_significance_dir")->Fill(m_metSignif->GetSigDirectional(),weight);
    hist("met_over_SumET")->Fill(m_metSignif->GetMETOverSqrtSumET(),weight);
    hist("met_over_HT")->Fill(m_metSignif->GetMETOverSqrtHT(),weight);
    hist("met_significance_VarL_jet")->Fill(m_metSignif->GetTermVarL(met::ResoJet),weight);
    hist("met_significance_VarT_jet")->Fill(m_metSignif->GetTermVarT(met::ResoJet),weight);
    hist("met_significance_VarL_muo")->Fill(m_metSignif->GetTermVarL(met::ResoMuo),weight);
    hist("met_significance_VarT_muo")->Fill(m_metSignif->GetTermVarT(met::ResoMuo),weight);
    hist("met_significance_VarL_ele")->Fill(m_metSignif->GetTermVarL(met::ResoEle),weight);
    hist("met_significance_VarT_ele")->Fill(m_metSignif->GetTermVarT(met::ResoEle),weight);
    hist("met_significance_VarL_pho")->Fill(m_metSignif->GetTermVarL(met::ResoPho),weight);
    hist("met_significance_VarT_pho")->Fill(m_metSignif->GetTermVarT(met::ResoPho),weight);
    hist("met_significance_VarL_tau")->Fill(m_metSignif->GetTermVarL(met::ResoTau),weight);
    hist("met_significance_VarT_tau")->Fill(m_metSignif->GetTermVarT(met::ResoTau),weight);
    hist("met_significance_VarL_soft")->Fill(m_metSignif->GetTermVarL(met::ResoSoft),weight);
    hist("met_significance_VarT_soft")->Fill(m_metSignif->GetTermVarT(met::ResoSoft),weight);
*/
    ATH_MSG_DEBUG("Total MET: " << met_final->met() 
		 << " soft: " << met_soft->met() <<","<<met_soft->phi()
		 << " sumet: " << met_soft->sumet()
		 << " jet: " << met_jetterm->met() <<","<<met_jetterm->phi()
		 << " hard: " << met_hard->met() <<","<<met_hard->phi());

    ATH_MSG_DEBUG("Retrieve Truth");
    const MissingETContainer* truth = 0;
    MissingET* met_truth = 0;
    if(isSim) {
      ATH_CHECK( evtStore()->retrieve(truth, "MET_Truth") );
      met_truth = new MissingET();
      metperf->push_back(met_truth);
      *met_truth = (*(*truth)["NonInt"]);
      met_truth->setName("Truth");
    } else {
      met_truth = new MissingET(0.,0.,0.);
      metperf->push_back(met_truth);
      met_truth->setName("Truth");
    }

    MissingET* met_resid = new MissingET();
    double abs_met_resid = 0;
    metperf->push_back(met_resid);
    if ((m_evttype == Wev) || (m_evttype == Wmv) || (m_evttype == ttbar)) {
      *met_resid = *met_final - *met_truth;
      abs_met_resid = fabs(met_final->met()) - fabs(met_truth->met());
    }
    else{
      *met_resid = *met_final;
      abs_met_resid = fabs(met_final->met());
    }
    met_resid->setName("Resid");
    //histogram for AtlasDerivation validation
    hist("Reco_NonIntMET"  )->Fill(met_resid->met()/GeV,weight);
    hist("absReco_NonIntMET"  )->Fill(abs_met_resid/GeV,weight);
    hist("TSTtruth"  )->Fill((met_final->met() - met_truth->met())/GeV,weight);
  if ((m_evttype == ttbar)) {
 hist("absbadr1"  )->Fill(fabs(met_final->met() - met_truth->met())/GeV,weight);
if(fabs(met_final->met() - met_truth->met())/GeV>150) {
              hist("absbadr2"  )->Fill(fabs(met_final->met() - met_truth->met())/GeV,weight);
 }
if(fabs(met_final->met() - met_truth->met())/GeV>200) {
              hist("absbadr3"  )->Fill(fabs(met_final->met() - met_truth->met())/GeV,weight);
 }

}
     

    MissingET* pthard  = new MissingET();
    metperf->push_back(pthard);
    // noTruthPthard is used for closer comparison with data.
    if (isSim && !m_noTruthPthard) {
      *pthard = *met_truth - *met_hard;
    }
    else { // This is data, or noTruthPthard flag is set.
      MissingET zero_vector(0.,0.,0.);
      *pthard = zero_vector - *met_hard;
    }
    pthard->setName("PtHard");

    MissingET* proj_pthard = new MissingET();
    metperf->push_back(proj_pthard);
    *proj_pthard = projectMET(*met_resid, *pthard);
    proj_pthard->setName("Proj_PtHard");

    // MET soft term projected onto pthard
    MissingET* proj_soft_pthard = new MissingET();
    metperf->push_back(proj_soft_pthard);
    *proj_soft_pthard = projectMET(*met_soft, *pthard);
    proj_soft_pthard->setName("ProjSoft_PtHard");

    ATH_MSG_DEBUG("Final event weight just before plotting: " << weight);
    ATH_CHECK( plotBasics(metperf, weight, avgmu) );
    ATH_CHECK( plotTerms(met, weight, avgmu) );
    //ATH_CHECK( plotforChecks(met,metperf,weight));
    ATH_CHECK( plotPerformance(metperf, avgmu, npv, weight) );
    
    if( m_evttype==dijet ){
       const JetContainer* jj(0);
       ATH_CHECK( evtStore()->retrieve(jj,m_jetcoll) );
       float dijetpt=( jj->at(0)->pt() + jj->at(1)->pt() )*0.5;
       hist("dijet_pt")->Fill(dijetpt/GeV, weight);
       hist("leading_jet_pt")->Fill(jj->at(0)->pt()/GeV, weight);
       hist("subleading_jet_pt")->Fill(jj->at(1)->pt()/GeV, weight);
       if(jj->size()>2)
          hist("subsubleading_jet_pt")->Fill(jj->at(2)->pt()/GeV, weight);
       
    }
    if( m_evttype==Zmm || m_evttype==Zee ) {
      IParticle::FourMom_t Zboson;
      IParticle::FourMom_t leadingLep;
      IParticle::FourMom_t subleadingLep;
      if(m_evttype==Zmm) {
        const MuonContainer* mupair = 0;
        ATH_CHECK( evtStore()->retrieve(mupair, "BestZmm") );
        Zboson = (*mupair)[0]->p4() + (*mupair)[1]->p4();
        if( (*mupair)[0]->pt()>(*mupair)[1]->pt() ){
       		leadingLep = (*mupair)[0]->p4() ;
          subleadingLep =  (*mupair)[1]->p4();
        }
        else{
          leadingLep = (*mupair)[1]->p4() ;
          subleadingLep =  (*mupair)[0]->p4();	/// need to be corrected!!!! redo the plots!
        }
      } else { // m_evttype==Zee
        const ElectronContainer* elpair = 0;
        ATH_CHECK( evtStore()->retrieve(elpair, "BestZee") );
        Zboson = (*elpair)[0]->p4() + (*elpair)[1]->p4();
        if( (*elpair)[0]->pt()>(*elpair)[1]->pt() ){
          leadingLep = (*elpair)[0]->p4() ;
          subleadingLep =  (*elpair)[1]->p4();
        }
        else{
          leadingLep = (*elpair)[1]->p4() ;
          subleadingLep =  (*elpair)[0]->p4();
        }
      }
      hist("mZ")->Fill(Zboson.M()/GeV, weight);
      hist("ptZ")->Fill(Zboson.Pt()/GeV, weight);
      hist("pthard")->Fill(met_hard->met()/GeV, weight);
      hist("leadingLep_pt")->Fill(leadingLep.Pt()/GeV, weight);
      hist("subleadingLep_pt")->Fill(subleadingLep.Pt()/GeV, weight);
      // MET constructor assumes you're adding a particle
      MissingET* ptZ = new MissingET(Zboson.Px(),Zboson.Py(),0.);
      ptZ->setName("PtZ");
      metperf->push_back(ptZ);
      MissingET* proj_ptZ = new MissingET();
      metperf->push_back(proj_ptZ);
      *proj_ptZ = projectMET(*met_resid,*ptZ);
      proj_ptZ->setName("Proj_PtZ");
      ATH_CHECK( plotPerfZ(metperf, weight) );
    }

    if( m_evttype==Wmv || m_evttype==Wev || m_evttype==ttbar ) {
      IParticle::FourMom_t Wboson;//marta
      IParticle::FourMom_t nu;
      nu.SetPtEtaPhiM(met_truth->met(),0, met_truth->phi(),0);
      double WmT = 0;
      if(m_evttype==Wmv) {
        const MuonContainer* muons = 0;
        ATH_CHECK( evtStore()->retrieve(muons,"muonFromW") );//m_muoncoll
        const xAOD::Muon* pMu = muons->front();
        //WmT = sqrt(pMu->pt()*met_final->met()*(1-cos(pMu->phi()-met_final->phi())));
        Wboson=pMu->p4()+nu;
        WmT = Wboson.Mt();
      }
      if(m_evttype==Wev) {
        const ElectronContainer* electrons = 0;
        ATH_CHECK( evtStore()->retrieve(electrons,"eleFromW") );//m_elecoll
        const xAOD::Electron* pEl = electrons->front();
        //WmT = sqrt(pEl->pt()*met_final->met()*(1-cos(pEl->phi()-met_final->phi())));
        Wboson=pEl->p4()+nu;
        WmT = Wboson.Mt();
      }
 //     hist("WmT")->Fill(WmT/GeV, weight);
   //   hist2d("lin_truth")->Fill(met_truth->met()/GeV, met_final->met()/met_truth->met()-1, weight);
      MissingET* proj_truth = new MissingET();
      metperf->push_back(proj_truth);
      *proj_truth = projectMET(*met_resid,*met_truth);
      proj_truth->setName("Proj_Truth");
/*     
 hist2d("residL_truth")->Fill(met_truth->met()/GeV, proj_truth->mpx()/GeV, weight);
      hist2d("residxy_truth")->Fill(met_truth->met()/GeV, met_resid->mpx()/GeV, weight);
      hist2d("residxy_truth")->Fill(met_truth->met()/GeV, met_resid->mpy()/GeV, weight);
      hist2d("residT_truth")->Fill(met_truth->met()/GeV, proj_truth->mpy()/GeV, weight);
      hist2d("biasL_truth")->Fill(met_truth->met()/GeV,  proj_truth->mpx()/met_truth->met(), weight);
      hist2d("reco_vs_truth")->Fill(met_truth->met()/GeV,  met_final->met()/GeV, weight);
      hist2d("residPhi_truth")->Fill(met_truth->met()/GeV, TVector2::Phi_mpi_pi(met_final->phi()-met_truth->phi()), weight);
*/  
  }


if(m_evttype== ttbar ) { 
const JetContainer* jets=0;
double met_corr=0;
double met_ref=0;
double jet_px=0;
double jet_py=0;

ATH_CHECK( evtStore()->retrieve(jets, m_jetcoll) );
bool originalInputs = jets->empty() ? false : !acc_originalObject.isAvailable(*jets->front());
const xAOD::MissingETAssociationMap *map=0;
ATH_CHECK(evtStore()->retrieve(map, "METAssoc_AntiKt4EMTopo"));

const PhotonContainer* phselect = 0;
 ATH_CHECK( evtStore()->retrieve(phselect, "GoodPhotons" ) );
 const JetContainer* truthjets(0);
  ATH_CHECK( evtStore()->retrieve(truthjets,"AntiKt4TruthJets") );

 //for(const auto& e : *eleselect) {
          for(const auto& jet : *jets) {
               const MissingETAssociation* assoc = 0;
      if(originalInputs)
      {
        assoc = MissingETComposition::getAssociation(map,jet);
      } else {
        const IParticle* orig = *acc_originalObject(*jet);
        assoc = MissingETComposition::getAssociation(map,static_cast<const xAOD::Jet*>(orig));
      }
      xAOD::JetFourMom_t constjet;
     double constSF(1);
      MissingETBase::Types::constvec_t calvec = assoc->overlapCalVec();
     if(m_jetConstitScaleMom.empty() && assoc->hasAlternateConstVec()){
      constjet = assoc->getAlternateConstVec();
        } else {
     constjet = jet->jetP4("JetConstitScaleMomentum");
       double denom = (assoc->hasAlternateConstVec() ? assoc->getAlternateConstVec() : jet->jetP4("JetConstitScaleMomentum")).E();
          constSF = denom>1e-9 ? constjet.E()/denom : 0.;
          ATH_MSG_VERBOSE("Scale const jet by factor " << constSF);
         calvec *=constSF;
       }
hist("frac"  )->Fill(((constjet.E())-calvec.ce())/(constjet.E()),weight);
hist("diff"  )->Fill(((constjet.pt())-calvec.cpt())/GeV,weight);






//for(const auto& ph : *phselect) {
//double df = jet->p4().DeltaR(e->p4());
/*if ( met_final->met()/GeV>100  &&  df < 0.4 ) {
if  ((((constjet.E())-calvec.ce())/(constjet.E()))==0) {
 hist("met1b")->Fill(met_final->met()/GeV, weight);
}

if  ((((constjet.E())-calvec.ce())/(constjet.E()))>0.5) {
 hist("met2b")->Fill(met_final->met()/GeV, weight);
}

if  ((((constjet.E())-calvec.ce())/(constjet.E()))>0 && (((constjet.E())-calvec.ce())/(constjet.E()))< 0.5  ) {
 hist("met3b")->Fill(met_final->met()/GeV, weight);
}
}
if ( met_final->met()/GeV>100   ) {
if  ((((constjet.E())-calvec.ce())/(constjet.E()))==0) {
 hist("met1a")->Fill(met_final->met()/GeV, weight);
}

if  ((((constjet.E())-calvec.ce())/(constjet.E()))>0.5) {
 hist("met2a")->Fill(met_final->met()/GeV, weight);
}

if  ((((constjet.E())-calvec.ce())/(constjet.E()))>0 && (((constjet.E())-calvec.ce())/(constjet.E()))< 0.5  ) {
 hist("met3a")->Fill(met_final->met()/GeV, weight);
}
*/
//}



for(const auto& ph : *phselect) {
//double df = jet->p4().DeltaR(e->p4());
for(const auto& tj : *truthjets) {
//       double dR = jet->p4().DeltaR(tj->p4());
 
std::cout<< abs(tj->pt() - ph->pt())/GeV << std::endl;
//if(df < 0.4 ){
if( abs(tj->pt() - ph->pt())/GeV > 15  ) {
              hist("fracm"  )->Fill(((constjet.E())-calvec.ce())/(constjet.E()),weight);
              hist("diffm"  )->Fill(((constjet.pt())-calvec.cpt())/GeV,weight);
              hist("metm")->Fill(met_final->met()/GeV, weight);
      hist2d("diffvsfracm")->Fill(((constjet.E())-calvec.ce())/(constjet.E()) ,((constjet.pt())-calvec.cpt())/GeV, weight);
         }else{
              hist2d("diffvsfrac")->Fill(((constjet.E())-calvec.ce())/(constjet.E()) ,((constjet.pt())-calvec.cpt())/GeV, weight);
              hist("fracdm"  )->Fill(((constjet.E())-calvec.ce())/(constjet.E()),weight);
              hist("diffdm"  )->Fill(((constjet.pt())-calvec.cpt())/GeV,weight);
              hist("metdm")->Fill(met_final->met()/GeV, weight);
       }
     }
//}
}
 

for(const auto& ph : *phselect) {
 double da = jet->p4().DeltaR(ph->p4());
hist2d("metvsdr")->Fill(met_final->met()/GeV,da, weight);
if( da < 0.4 ){
hist2d("metvsdiffd")->Fill(met_final->met()/GeV,((constjet.pt())-calvec.cpt())/GeV , weight);
hist2d("metvsfracd")->Fill( met_final->met()/GeV,((constjet.E())-calvec.ce())/(constjet.E()), weight);
hist("metba")->Fill(met_final->met()/GeV,weight);
}
else{
hist2d("metvsdiff")->Fill(met_final->met()/GeV,((constjet.pt())-calvec.cpt())/GeV , weight);
hist2d("metvsfrac")->Fill( met_final->met()/GeV,((constjet.E())-calvec.ce())/(constjet.E()), weight);
}


if(met_final->met()/GeV>50) {
if( da < 0.4 ){
}else {
hist("metbadr11")->Fill(((constjet.pt())-calvec.cpt())/GeV,weight);

}
}
if(met_final->met()/GeV>50 && met_final->met()/GeV<100) {
if( da < 0.4 ){
}else {
hist("metbadr22")->Fill(((constjet.pt())-calvec.cpt())/GeV,weight);

}
}
if(met_final->met()/GeV>100 && met_final->met()/GeV<200) {
if( da < 0.4 ){
}else {
hist("metbadr33")->Fill(((constjet.pt())-calvec.cpt())/GeV,weight);
}
}
if(met_final->met()/GeV>200) {
if( da < 0.4 ){
hist("metbadr44")->Fill(((constjet.pt())-calvec.cpt())/GeV,weight);
hist2d("metvsdiffn")->Fill(met_final->met()/GeV,((constjet.pt())-calvec.cpt())/GeV , weight);
hist2d("metvsfracn")->Fill( met_final->met()/GeV,((constjet.E())-calvec.ce())/(constjet.E()), weight);
}else{
}
}
}

}

if(met_final->met()/GeV>150) {
hist("jetmet")->Fill(met_jetterm->met()/GeV, weight);
//hist("elemet")->Fill(mele->met()/GeV, weight);
hist("softmet")->Fill(met_soft->met()/GeV, weight);
//hist("muonmet")->Fill(mmuon->met()/GeV, weight);
}

}



    const JetContainer* jets(0);
    ATH_CHECK( evtStore()->retrieve(jets,m_jetcoll) );
    const ElectronContainer* eles = 0;
    ATH_CHECK( evtStore()->retrieve(eles, m_elecoll) );
    if(isSim)
    {
      const JetContainer* truthjets(0);
      //ATH_CHECK( evtStore()->retrieve(truthjets,"AntiKt4TruthJets") );
      double recojets = 0.;
      double recojets_central = 0.;
      double recojets_forward = 0.;
      for(const auto& j : *jets) {
        if (!dec_passOR(*j)) continue;
        double dRmin=99.;
        double truthPt = 0.;
        double recoPt = j->pt();
	if(truthjets){
	  for(const auto& tj : *truthjets) {
	    double dR = j->p4().DeltaR(tj->p4());
	    if(dR < dRmin) {dRmin = dR; truthPt = tj->pt();}
	  }
	}
	//
/*
	if(fabs(j->eta())<2.4){
	  if(dRmin<0.4) hist2d("jetHS_pT_vs_jvt_eta24")->Fill(j->pt()/GeV, acc_jvt(*j), weight);
	  else hist2d("jetPU_pT_vs_jvt_eta24")->Fill(j->pt()/GeV, acc_jvt(*j), weight);
	}else if(fabs(j->eta())<2.5){
	  if(dRmin<0.4) hist2d("jetHS_pT_vs_jvt_eta25")->Fill(j->pt()/GeV, acc_jvt(*j), weight);
	  else hist2d("jetPU_pT_vs_jvt_eta25")->Fill(j->pt()/GeV, acc_jvt(*j), weight);	  
	}else if(fabs(j->eta())<2.7){
	  if(dRmin<0.4) hist2d("jetHS_pT_vs_jvt_eta27")->Fill(j->pt()/GeV, acc_jvt(*j), weight);
	  else hist2d("jetPU_pT_vs_jvt_eta27")->Fill(j->pt()/GeV, acc_jvt(*j), weight);	  
	}
*/ 
       ATH_MSG_VERBOSE("Best truth jet dR = " << dRmin << ", pt ratio = " << recoPt/truthPt);
        if(dRmin<0.1) { 
//hist2d("jetresponse")->Fill(truthPt/1e3, recoPt/truthPt, weight);
}
        if(dRmin<0.3 && truthPt > 10e3) {
		recojets += recoPt;
		if(TMath::Abs( j->eta() ) < 2.4){
			recojets_central += recoPt;
		}
		else{
			recojets_forward += recoPt;
		}
	}
      }
      for(const auto& j : *jets) {
       for(const auto& e : *eles) {
	 if(truthjets){
	   for(const auto& tj : *truthjets) {
//	     hist("jetEnergySharing")->Fill((j->pt()+e->pt())/tj->pt(), weight);
	   }
	 }
       }
      }
  //    hist("jetrecosums")->Fill(recojets/GeV, weight);
    //  hist2d("forw_cent_truth")->Fill(recojets_forward/GeV,recojets_central/GeV, weight);
    }

   //for AtlasDerivation Validation:
   double lowjetterm=0;//what if the jet term is reconstructed only from low jet pt events?
   bool islow=true;
   const JetContainer* jetsDer(0);
   ATH_CHECK( evtStore()->retrieve(jetsDer,m_jetcoll) );
   double leading_Jet = 0.;
     for(const auto& j : *jetsDer) {
        if (j->pt() > leading_Jet)
               leading_Jet = j->pt();
  
           //for AtlasDerivation Validation:
           if(islow){
              if(j->pt()<(200*GeV))
                 lowjetterm+=j->pt();
              else
                 islow=false;
           }

     }
        //for AtlasDerivation Validation:
        if(islow)
       //     hist("lowjetpt_distribution")->Fill(lowjetterm/GeV,weight);
     //    hist("jethard_distribution")->Fill(leading_Jet/GeV,weight);

/*
    const JetContainer* jetnumtemp = 0;
    ATH_CHECK( evtStore()->retrieve(jetnumtemp, m_jetcoll) );
    int jet25=0, jet40=0, jet60=0;
    for(const auto& j : *jetnumtemp) {
       if( j->pt()>(25*GeV) ){
           ++jet25;
           if( j->pt()>(40*GeV) ){
               ++jet40;
               if( j->pt()>(60*GeV) )
                   ++jet60;
           }
       }
    }
*/
/*  
  hist("jetsize60")->Fill(jet60, weight);
    hist("jetsize40")->Fill(jet40, weight);
    hist("jetsize25")->Fill(jet25, weight);

    //retrieve primary vertex of the event
    const VertexContainer* vert = 0;
    xAOD::Vertex *primvtx(0);
    ATH_CHECK( evtStore()->retrieve( vert, "PrimaryVertices" ) );
    for(const auto& vx : *pvertices) {
      //if type is 1, then is primary
      if( vx->vertexType() == 1 ) {
        primvtx=vx;
        break;
      }
    }
*/
    if(m_doPlotTracks) {
      const xAOD::TrackParticleContainer* trackParticles;
      ATH_CHECK( evtStore()->retrieve(trackParticles, "InDetTrackParticles" ) );
	   
      // Check the event contains tracks
      unsigned int nTracks = trackParticles->size();
  //    hist("n_track")->Fill(nTracks,weight);
      for (size_t i=0; i<trackParticles->size(); ++i){
    //    float Dz=fabs( trackParticles->at(i)->z0() + trackParticles->at(i)->vz() - primvtx->z() );
        float sintheta=sin( trackParticles->at(i)->theta() );
        float d0=trackParticles->at(i)->d0();
        float errd0=sqrt( trackParticles->at(i)->definingParametersCovMatrix()(0,0) );
        float track_phi=trackParticles->at(i)->phi();
/*
        hist("zoom_track_d0")->Fill(d0,weight);
        hist2d("phi_vs_d0")->Fill(track_phi,d0,weight);
        hist2d("phi_vs_zoomd0")->Fill(track_phi,d0,weight);
        hist2d("phi_vs_sigd0")->Fill(track_phi,d0/errd0,weight);
        hist("track_d0")->Fill( d0,weight);
        hist("abs_sig_track_d0")->Fill(fabs(d0),weight);
        hist("track_Dz0sin")->Fill( fabs( Dz*sintheta ),weight);
        hist("sig_track_d0")->Fill( d0/errd0,weight);
        //Fill histograms in different <mu> regions
*/
/*  
      if(avgmu<=15){
        hist("track_d0_mu015")->Fill( d0,weight);
        hist("track_Dz0sin_mu015")->Fill( fabs( Dz*sintheta ),weight);
        hist("sig_track_d0_mu015")->Fill( d0/errd0,weight);
        }
        else if(avgmu<=30){
        hist("track_d0_mu1630")->Fill( d0,weight);
        hist("track_Dz0sin_mu1630")->Fill( fabs( Dz*sintheta ),weight);
        hist("sig_track_d0_mu1630")->Fill( d0/errd0,weight);
        }
        else{
        hist("track_d0_muhigh")->Fill( d0,weight);
        hist("track_Dz0sin_muhigh")->Fill( fabs( Dz*sintheta ),weight);
        hist("sig_track_d0_muhigh")->Fill( d0/errd0,weight);
        }
*/
      }
  
  }

    if(m_doMuonElossPlots){

	//const xAOD::TruthParticleContainer* TruthMuons = evtStore()->tryRetrieve< xAOD::TruthParticleContainer >("MuonTruthParticles");
	//if (!TruthMuons) {
	//	ATH_MSG_VERBOSE ("Couldn't retrieve TruthMuons container with key: " << "MuonTruthParticles");
     	//	return StatusCode::SUCCESS;
   	//}

	//typedef ElementLink< xAOD::MuonContainer > MuonLink;
   	//ATH_MSG_VERBOSE("Retrieved truth muons " << TruthMuons->size());

	//float px,py,pz = 0;
	//float Px,Py,Pz = 0;
	//float trutheloss = 0;

        //for (const auto truthMu: *TruthMuons){
	//	MuonLink link;

	//	if( truthMu->isAvailable< MuonLink >("recoMuonLink") ) link = truthMu->auxdata< MuonLink >("recoMuonLink");

	//	if( link.isValid() ){

	//		 const xAOD::TrackParticle* tp  = (*link)->primaryTrackParticle();
        //		 const xAOD::Muon* muon = truthMu;

	//	if(tp) {
	//
	//		px = (*muon)->auxdata<float>("MuonEntryLayer_px");
	//		py = (*muon)->auxdata<float>("MuonEntryLayer_py");
	//		pz = (*muon)->auxdata<float>("MuonEntryLayer_pz");
	////		Px = (*muon)->auxdata<float>("CaloEntryLayer_px");
	////		Py = (*muon)->auxdata<float>("CaloEntryLayer_py");
	////		Pz = (*muon)->auxdata<float>("CaloEntryLayer_pz");
	//		Px = (*muon)->auxdata<float>("px");
	//		Py = (*muon)->auxdata<float>("py");
	//		Pz = (*muon)->auxdata<float>("pz");
	//		trutheloss = TMath::Abs(TMath::Sqrt(px*px+py*py+pz*pz) - TMath::Sqrt(Px*Px+Py*Py+Pz*Pz));
        //		float eloss = (*link)->floatParameter(xAOD::Muon::EnergyLoss);
        //		unsigned char ElossType = (*link)->auxdata<unsigned char>("energyLossType");
        //		if((unsigned int)ElossType == 3){ // ElossType=3 is TAIL,1 - Not Isolated, 2 - MOP, 0 - Parametrized
        //			ATH_MSG_DEBUG ("You have selected elossType" << (unsigned int)ElossType << "muons" );
        //		}
	//		hist("elosstruth")->Fill(trutheloss/GeV,weight);
	//		hist("eloss_fit_truth")->Fill((eloss-trutheloss)/GeV,weight);
	//	}

	//}


      const xAOD::MissingETAssociationMap *metMap=0;
      ATH_CHECK(evtStore()->retrieve(metMap, "METAssoc_AntiKt4EMTopo"));
      metMap->resetObjSelectionFlags();
      const xAOD::MuonContainer *goodmuons=0;
      ATH_CHECK(evtStore()->retrieve(goodmuons, "GoodMuons"));
      if (!goodmuons) {
        ATH_MSG_ERROR ("Couldn't retrieve Muons container with key: " << "GoodMuons");
        return StatusCode::FAILURE;
      }
      ATH_MSG_DEBUG("Retrieved muons " << goodmuons->size());

      bool originalInputs = !goodmuons->front()->isAvailable<ElementLink<IParticleContainer> >("originalObjectLink");
      float muonpx(0.), muonpy(0.), muonpt(0.);
      float muon_px(0.), muon_py(0.), muon_pt_ip(0.);
      for(const auto& iMu : *goodmuons) {
        const IParticle* orig = iMu;
	ATH_MSG_DEBUG("muon: " << iMu->pt() << " " << iMu->eta() << " " << iMu->phi());
        if(!originalInputs) { orig = *m_objLinkAcc(*iMu); }
        const xAOD::Muon* muon = iMu;

        bool isolated = MissingETComposition::getRefJet(metMap, orig)==0; // use flag to test if muons is isolated from jet

        float px,py,pz = 0;
        float Px,Py,Pz = 0;
        float trutheloss = 0;

        float eloss = muon->floatParameter(xAOD::Muon::EnergyLoss);
        float elossMeas = muon->floatParameter(xAOD::Muon::MeasEnergyLoss);
        float elossParam = muon->floatParameter(xAOD::Muon::ParamEnergyLoss);
        float eloss_corr = 1. - eloss / muon->e();

        ElementLink< xAOD::TruthParticleContainer > truthLink;

        if( muon->isAvailable< ElementLink< xAOD::TruthParticleContainer > > ("truthParticleLink") ) truthLink = muon->auxdata< ElementLink< xAOD::TruthParticleContainer >  >("truthParticleLink");


        if(truthLink.isValid()) {
          px = (*truthLink)->auxdata<float>("MuonEntryLayer_px");
          py = (*truthLink)->auxdata<float>("MuonEntryLayer_py");
          pz = (*truthLink)->auxdata<float>("MuonEntryLayer_pz");
          //Px = (*truthLink)->auxdata<float>("CaloEntryLayer_px");
          //Py = (*truthLink)->auxdata<float>("CaloEntryLayer_py");
          //Pz = (*truthLink)->auxdata<float>("CaloEntryLayer_pz");
          Px = (*truthLink)->p4().Px();
          Py = (*truthLink)->p4().Py();
          Pz = (*truthLink)->p4().Pz();
          trutheloss = TMath::Abs(TMath::Sqrt(px*px+py*py+pz*pz) - TMath::Sqrt(Px*Px+Py*Py+Pz*Pz));
          //		unsigned char ElossType = (*muon)->auxdata<unsigned char>("energyLossType");
          //		if((unsigned int)ElossType == 3){ // ElossType=3 is TAIL,1 - Not Isolated, 2 - MOP, 0 - Parametrized
          //			ATH_MSG_DEBUG ("You have selected elossType" << (unsigned int)ElossType << "muons" );
          //		}
  //        hist("elosstruth")->Fill(trutheloss/GeV,weight);
    //      hist("eloss_fit_truth")->Fill((eloss-trutheloss)/GeV,weight);
        }

        if( eloss>muon->e()){
          ATH_MSG_WARNING("Muon Eloss " << eloss << " > muon E " << muon->e() << "!");
        }
      /*  else{
          if (isolated){
            hist("elossfit")->Fill(eloss/GeV,weight);
            hist("elossfact")->Fill(eloss_corr,weight);
            hist("elossmeas")->Fill(elossMeas/GeV,weight);
            hist("elossparam")->Fill(elossParam/GeV,weight);
            muonpx += muon->p4().Px();
            muonpy += muon->p4().Py();
            muon_px += eloss_corr*muon->p4().Px();
            muon_py += eloss_corr*muon->p4().Py();
            const JetContainer* jets(0);
            ATH_CHECK( evtStore()->retrieve(jets,m_jetcoll) );
            //In METPerformance 2.X was different, check if needed!
            //ATH_CHECK( evtStore()->retrieve(jets,m_jetcoll_ovRem) );//before was m_jetcoll
            double leading_recoJetPt = 0.;
            for(const auto& j : *jets) {
              if (!dec_passOR(*j)) continue;
              if (j->pt() > leading_recoJetPt) {
                leading_recoJetPt = j->pt();
              }
            }

            hist2d("jetpt_eloss")->Fill(leading_recoJetPt/GeV,eloss/GeV,weight);
          }
        }
*/
      }
  /*    muonpt = TMath::Sqrt(muonpx*muonpx + muonpy*muonpy);
      muon_pt_ip = TMath::Sqrt(muon_px*muon_px + muon_py*muon_py);
      hist2d("muonpt_met")->Fill(muonpt/GeV,(*met)["Final"]->met()/GeV,weight);
      hist2d("muon_pt_ip_met")->Fill(muon_pt_ip/GeV,(*met)["Final"]->met()/GeV,weight);
      hist2d("muon_pt_ip_jet_met")->Fill(muon_pt_ip/GeV,(*met)[m_jetterm]->met()/GeV,weight);
*/
    }

    return StatusCode::SUCCESS;
  }

  StatusCode METPerfAlg::plotBasics(const MissingETContainer* metperf, double weight, double avgmu) {
    ATH_MSG_DEBUG(name() << " -- plotting basic MET plots");
    const MissingET* met = (*metperf)["Final"];
    const MissingET* proj = (*metperf)["Proj_PtHard"];
    const MissingET* pthard = (*metperf)["PtHard"];
    const MissingET* jetterm = (*metperf)[m_jetterm];
    if(!met) {
      ATH_MSG_ERROR("Didn't find total MET");
      return StatusCode::FAILURE;
    }
    if(!proj) {
      ATH_MSG_ERROR("Didn't find MET projected on pthard");
      return StatusCode::FAILURE;
    }
    if(!pthard) {
      ATH_MSG_ERROR("Didn't find pthard");
      return StatusCode::FAILURE;
    }
    if(!jetterm) {
      ATH_MSG_ERROR("Didn't find jet term");
      return StatusCode::FAILURE;
    }
    //
    hist("met")->Fill(met->met()/GeV, weight);
    hist("phi")->Fill(met->phi(), weight);
    if(met->met()>50.0e3) hist("phi_met50")->Fill(met->phi(), weight);
    if(met->met()>80.0e3) hist("phi_met100")->Fill(met->phi(), weight);
    hist("mpx")->Fill(met->mpx()/GeV, weight);
    hist("mpy")->Fill(met->mpy()/GeV, weight);
    hist("sumet")->Fill(met->sumet()/GeV, weight);
    ATH_MSG_DEBUG("met: " << met->met() << " " << met->phi());
    //
  //  hist("sig")->Fill(met->met()/TMath::Sqrt(met->sumet() * GeV), weight);
   // hist("sigHT")->Fill(met->met()/TMath::Sqrt(jetterm->sumet() * GeV),weight);
    //
  //  hist("pThard_distribution")->Fill(pthard->met()/GeV,weight);
    //
   // hist("pL_pthard")->Fill(proj->mpx()/GeV,weight);
   // hist("pT_pthard")->Fill(proj->mpy()/GeV,weight);

    ATH_MSG_DEBUG( "MET hist now with " << hist("met")->GetEntries() << " entries" );

    // Fill histos in different jet multiplicity bins
    Int_t njets(0);
    if (m_jetBins) {
      const JetContainer* jets(0);
      ATH_CHECK( evtStore()->retrieve(jets,m_jetcoll) );
      // Number of jets with pT > 20 GeV and passing overlap removal.
      for(const auto& j : *jets) {
        if ((j->pt() > 20000) && (dec_passOR(*j))) njets++;
      }
      std::string jetlabel;
      switch (njets) {
        case 0: jetlabel = "0"; break;
        case 1: jetlabel = "1"; break;
        default: jetlabel = "2";
      }
      hist("met_"+jetlabel+"j")->Fill(met->met()/GeV, weight);
    }

    // Fill histos in different mu regions
    Int_t muBin(0);
    std::string muRegionLabel;
/*
    if (avgmu <10)               muBin = 0;
    if (avgmu >=10 && avgmu<20 ) muBin = 1;
    if (avgmu >=20 && avgmu<30 ) muBin = 2;
    if (avgmu >=30 && avgmu<40 ) muBin = 3;
    if (avgmu >=40 && avgmu<50 ) muBin = 4;
    if (avgmu >=50 && avgmu<60 ) muBin = 5;
    if (avgmu >=60 && avgmu<70 ) muBin = 6;

    switch (muBin) {
      case 0: muRegionLabel = "_mu0_10";    break;
      case 1: muRegionLabel = "_mu10_20"; break;
      case 2: muRegionLabel = "_mu20_30"; break;
      case 3: muRegionLabel = "_mu30_40"; break;
      case 4: muRegionLabel = "_mu40_50"; break;
      case 5: muRegionLabel = "_mu50_60"; break;
      case 6: muRegionLabel = "_mu60_70"; break;
    }

    hist("met"+muRegionLabel)->Fill(met->met()/GeV, weight);
    hist("metsig"+muRegionLabel)->Fill(m_metSignif->GetSignificance(), weight);
    hist("sumet"+muRegionLabel)->Fill(met->sumet()/GeV, weight);
    hist("pThard_distribution"+muRegionLabel)->Fill(pthard->met()/GeV,weight);
*/
    return StatusCode::SUCCESS;
  }

  StatusCode METPerfAlg::plotTerms(const MissingETContainer* met, double weight, double avgmu) {
    const MissingET* refjet = (*met)[m_jetterm];
    hist("jet_met")->Fill(refjet->met()/GeV,weight);
    hist("jet_phi")->Fill(refjet->phi(),weight);
    hist("jet_sumet")->Fill(refjet->sumet()/GeV,weight);
    hist("jet_mpx")->Fill(refjet->mpx()/GeV,weight);
    hist("jet_mpy")->Fill(refjet->mpy()/GeV,weight);
    //
    const MissingET* muons = (*met)[m_muonterm];
  /*  hist("muon_met")->Fill(muons->met()/GeV,weight);
    hist("muon_phi")->Fill(muons->phi(),weight);
    hist("muon_sumet")->Fill(muons->sumet()/GeV,weight);
    hist("muon_mpx")->Fill(muons->mpx()/GeV,weight);
    hist("muon_mpy")->Fill(muons->mpy()/GeV,weight);
    ATH_MSG_DEBUG("   met_muon:" << muons->met() << " " << muons->phi());
*/    
//
    const MissingET* refele = (*met)[m_eleterm];
    hist("ele_met")->Fill(refele->met()/GeV,weight);
    hist("ele_phi")->Fill(refele->phi(),weight);
    hist("ele_sumet")->Fill(refele->sumet()/GeV,weight);
    hist("ele_mpx")->Fill(refele->mpx()/GeV,weight);
    hist("ele_mpy")->Fill(refele->mpy()/GeV,weight);
    ATH_MSG_DEBUG("   met_ele:" << refele->met() << " " << refele->phi());
    //
    const MissingET* softterm = (*met)[m_softterm];
    hist("soft_met")->Fill(softterm->met()/GeV,weight);
    hist("soft_phi")->Fill(softterm->phi(),weight);
    hist("soft_sumet")->Fill(softterm->sumet()/GeV,weight);
    hist("soft_mpx")->Fill(softterm->mpx()/GeV,weight);
    hist("soft_mpy")->Fill(softterm->mpy()/GeV,weight);
    ATH_MSG_DEBUG("   softterm:" << softterm->met() << " " << softterm->phi());

    if (!m_gammaterm.empty()) {
      const MissingET* gammaterm = (*met)[m_gammaterm];
      if (gammaterm){
        hist("photon_met")->Fill(  gammaterm->met()/GeV,weight);
        hist("photon_phi")->Fill(  gammaterm->phi(),weight);
        hist("photon_sumet")->Fill(gammaterm->sumet()/GeV,weight);
        hist("photon_mpx")->Fill(  gammaterm->mpx()/GeV,weight);
        hist("photon_mpy")->Fill(  gammaterm->mpy()/GeV,weight);
        ATH_MSG_DEBUG("   photon_met:" << gammaterm->met() << " " << gammaterm->phi());
      }
    }

    if (!m_tauterm.empty()) {
      const MissingET* tauterm = (*met)[m_tauterm];
      if (tauterm){
  /*      hist("tau_met")->Fill(  tauterm->met()/GeV,weight);
        hist("tau_phi")->Fill(  tauterm->phi(),weight);
        hist("tau_sumet")->Fill(tauterm->sumet()/GeV,weight);
        hist("tau_mpx")->Fill(  tauterm->mpx()/GeV,weight);
        hist("tau_mpy")->Fill(  tauterm->mpy()/GeV,weight);
        ATH_MSG_DEBUG("   tau_met:" << tauterm->met() << " " << tauterm->phi());
*/    
  }
    }

      const MissingET* elossterm = (*met)["MuonEloss"];
      ATH_MSG_DEBUG("   eloss_met:" << elossterm->met() << " " << elossterm->phi());

    //FYing studies
    const MissingET* metselect = (*met)["Final"];
    const JetContainer* jetselect = 0;
    ATH_CHECK( evtStore()->retrieve(jetselect, m_jetcoll) );
    const PhotonContainer* eleselect = 0;
        ATH_CHECK( evtStore()->retrieve(eleselect, "GoodPhotons") );
    for(const auto& j : *jetselect) {
      for(const auto& e : *eleselect) {
        double dR = j->p4().DeltaR(e->p4());
        hist("eleJetdR")->Fill(dR, weight);
        if( dR < 0.4 ){
           hist("metTermdR04")->Fill(metselect->met()/GeV, weight);
           hist("sfotmetdR04")->Fill(softterm->met()/GeV,weight);
           hist("jetmetdR04")->Fill(refjet->met()/GeV,weight);
           hist("elemetdR04")->Fill(refele->met()/GeV,weight);
           }
       }
    }

    // Fill histos in different jet multiplicity bins
    Int_t njets(0);
    if (m_jetBins) {
      const JetContainer* jets(0);
      ATH_CHECK( evtStore()->retrieve(jets,m_jetcoll) );
      // Number of jets with pT > 20 GeV and passing overlap removal.
      for(const auto& j : *jets) {
        if ((j->pt() > 20000) && (dec_passOR(*j))) njets++;
      }
      std::string jetlabel;
      switch (njets) {
        case 0: jetlabel = "0"; break;
        case 1: jetlabel = "1"; break;
        default: jetlabel = "2";
      }
      hist("jet_met_"+jetlabel+"j")->Fill(refjet->met()/GeV, weight);
  //    hist("muon_met_"+jetlabel+"j")->Fill(muons->met()/GeV, weight);
      hist("ele_met_"+jetlabel+"j")->Fill(refele->met()/GeV, weight);
      hist("soft_met_"+jetlabel+"j")->Fill(softterm->met()/GeV, weight);
    }

    // Fill histos in different mu regions
    Int_t muBin(0);
    std::string muRegionLabel;

    if (avgmu <10)               muBin = 0;
    if (avgmu >=10 && avgmu<20 ) muBin = 1;
    if (avgmu >=20 && avgmu<30 ) muBin = 2;
    if (avgmu >=30 && avgmu<40 ) muBin = 3;
    if (avgmu >=40 && avgmu<50 ) muBin = 4;
    if (avgmu >=50 && avgmu<60 ) muBin = 5;
    if (avgmu >=60 && avgmu<70 ) muBin = 6;

    switch (muBin) {
      case 0: muRegionLabel = "_mu0_10";    break;
      case 1: muRegionLabel = "_mu10_20"; break;
      case 2: muRegionLabel = "_mu20_30"; break;
      case 3: muRegionLabel = "_mu30_40"; break;
      case 4: muRegionLabel = "_mu40_50"; break;
      case 5: muRegionLabel = "_mu50_60"; break;
      case 6: muRegionLabel = "_mu60_70"; break;
    }

    hist("soft_met"+muRegionLabel)->Fill(softterm->met()/GeV,weight);
    hist("jet_met"+muRegionLabel)->Fill(refjet->met()/GeV,weight);
    hist("ele_met"+muRegionLabel)->Fill(refele->met()/GeV,weight);
   // hist("muon_met"+muRegionLabel)->Fill(muons->met()/GeV,weight);

    return StatusCode::SUCCESS;
  }

  StatusCode METPerfAlg::plotPerformance(const MissingETContainer* metperf, double avgmu, unsigned int npv, double weight) {
    ATH_MSG_DEBUG(name() << " -- plotting MET performance plots");
    const MissingET* met = (*metperf)["Final"];
    const MissingET* resid = (*metperf)["Resid"];
    const MissingET* pthard = (*metperf)["PtHard"];
    const MissingET* proj = (*metperf)["Proj_PtHard"];
    const MissingET* proj_soft = (*metperf)["ProjSoft_PtHard"];

    //======
    // Removed by Emma -- these should be filled by the relevant algorithms
    //const MissingETContainer* tst_met = 0;
    //ATH_CHECK( evtStore()->retrieve(tst_met, "MET_RefTST") );
    //double tst_sumet = (*tst_met)["Final"]->sumet();

    //const MissingETContainer* cst_met = 0;
    //ATH_CHECK( evtStore()->retrieve(cst_met, "MET_RefCST") );
    //double cst_sumet = (*cst_met)["Final"]->sumet();

    if(!met) {
      ATH_MSG_ERROR("Didn't find total MET");
      return StatusCode::FAILURE;
    }
    if(!resid) {
      ATH_MSG_ERROR("Didn't find MET residuals");
      return StatusCode::FAILURE;
    }
    if(!pthard) {
      ATH_MSG_ERROR("Didn't find PtHard");
      return StatusCode::FAILURE;
    }
    if(!proj) {
      ATH_MSG_ERROR("Didn't find MET projected on pthard");
      return StatusCode::FAILURE;
    }
    if(!proj_soft) {
      ATH_MSG_ERROR("Didn't find MET soft term projected on pthard");
      return StatusCode::FAILURE;
    }
   
 //
/*    hist2d("residxy_mu")->Fill(avgmu, resid->mpx()/GeV, weight);
    hist2d("residxy_mu")->Fill(avgmu, resid->mpy()/GeV, weight);
    //
    hist2d("metxy_mu")->Fill(avgmu, met->mpx()/GeV, weight);
    hist2d("metxy_mu")->Fill(avgmu, met->mpy()/GeV, weight);
    //
    hist2d("residL_mu")->Fill(avgmu, proj->mpx()/GeV, weight);
    hist2d("residT_mu")->Fill(avgmu, proj->mpy()/GeV, weight);
    //
    hist2d("residxy_npv")->Fill(npv, resid->mpx()/GeV, weight);
    hist2d("residxy_npv")->Fill(npv, resid->mpy()/GeV, weight);
    //
    hist2d("metxy_npv")->Fill(npv, met->mpx()/GeV, weight);
    hist2d("metxy_npv")->Fill(npv, met->mpy()/GeV, weight);
    //
    if (avgmu < 60){
      hist2d("residxy_npv_muleq60")->Fill(npv, resid->mpx()/GeV, weight);
      hist2d("residxy_npv_muleq60")->Fill(npv, resid->mpy()/GeV, weight);
      hist2d("metxy_npv_muleq60")->Fill(  npv, met->mpx()/GeV,   weight);
      hist2d("metxy_npv_muleq60")->Fill(  npv, met->mpy()/GeV,   weight);
    }
*/
    //
    double mu_test_weight=1.0;
    if(avgmu>54.0) mu_test_weight = 0.3;
  /*  hist2d("metxy_npv_muweight")->Fill(npv, met->mpx()/GeV, weight*mu_test_weight);
    hist2d("metxy_npv_muweight")->Fill(npv, met->mpy()/GeV, weight*mu_test_weight);
    //
    hist2d("residL_npv")->Fill(npv, proj->mpx()/GeV, weight);
    hist2d("residT_npv")->Fill(npv, proj->mpy()/GeV, weight);
    //
    //AtlasDerivationValidation
    hist("npv_distribution")->Fill(npv,weight);
    //
    hist2d("residxy_sumet")->Fill(met->sumet()/GeV, resid->mpx()/GeV, weight);
    hist2d("residxy_sumet")->Fill(met->sumet()/GeV, resid->mpy()/GeV, weight);
    //
    hist2d("residL_sumet")->Fill(met->sumet()/GeV, proj->mpx()/GeV, weight);
    hist2d("residT_sumet")->Fill(met->sumet()/GeV, proj->mpy()/GeV, weight);
    //
*/
    //======
    // Removed by Emma -- these should be filled by the relevant algorithms
    //hist2d("residxy_CSTsumet")->Fill(cst_sumet/GeV, resid->mpx()/GeV, weight);
    //hist2d("residxy_CSTsumet")->Fill(cst_sumet/GeV, resid->mpy()/GeV, weight);
    //
    //hist2d("residL_CSTsumet")->Fill(cst_sumet/GeV, proj->mpx()/GeV, weight);
    //hist2d("residT_CSTsumet")->Fill(cst_sumet/GeV, proj->mpy()/GeV, weight);
    //
    //hist2d("residxy_TSTsumet")->Fill(tst_sumet/GeV, resid->mpx()/GeV, weight);
    //hist2d("residxy_TSTsumet")->Fill(tst_sumet/GeV, resid->mpy()/GeV, weight);
    //======

    //hist2d("residL_TSTsumet")->Fill(tst_sumet/GeV, proj->mpx()/GeV, weight);
    //hist2d("residT_TSTsumet")->Fill(tst_sumet/GeV, proj->mpy()/GeV, weight);
    //
    //hist2d("residxy_LHsumet")->Fill(lht_sumet/GeV, resid->mpx()/GeV, weight);
    //hist2d("residxy_LHsumet")->Fill(lht_sumet/GeV, resid->mpy()/GeV, weight);
    //
    //hist2d("residL_LHsumet")->Fill(lht_sumet/GeV, proj->mpx()/GeV, weight);
    //hist2d("residT_LHsumet")->Fill(lht_sumet/GeV, proj->mpy()/GeV, weight);
    //
  /*  hist2d("residL_pthard")->Fill(pthard->met()/GeV, proj->mpx()/GeV, weight);
    hist2d("residT_pthard")->Fill(pthard->met()/GeV, proj->mpy()/GeV, weight);
    //
    hist2d("scaleL_pthard")->Fill(pthard->met()/GeV, 1. + proj->mpx()/pthard->met(), weight);
    //
    hist2d("softL_pthard")->Fill(pthard->met()/GeV, proj_soft->mpx()/GeV, weight);
    hist2d("softT_pthard")->Fill(pthard->met()/GeV, proj_soft->mpy()/GeV, weight);
    hist2d("met_vs_soft")->Fill(met->met()/GeV, proj_soft->met()/GeV, weight);

    // for CST syst:
    hist3d("pLsoft_vsPthard_vsSumet")->Fill(met->sumet()/GeV, pthard->met()/GeV, proj_soft->mpx()/GeV,weight);
*/
    Int_t njets(0);
    if (m_jetBins) {
      const JetContainer* jets(0);
      ATH_CHECK( evtStore()->retrieve(jets,m_jetcoll) );
      // Number of jets with pT > 20 GeV and passing overlap removal.
      for(const auto& j : *jets) {
        if ((j->pt() > 20000) && (dec_passOR(*j))) njets++;
      }
      std::string jetlabel;
      switch (njets) {
        case 0: jetlabel = "0"; break;
        case 1: jetlabel = "1"; break;
        default: jetlabel = "2";
      }
  //    hist2d("residL_pthard_"+jetlabel+"j")->Fill(pthard->met()/GeV, proj->mpx()/GeV, weight);
    //  hist2d("residT_pthard_"+jetlabel+"j")->Fill(pthard->met()/GeV, proj->mpy()/GeV, weight);
      //
    //  hist2d("softL_pthard_"+jetlabel+"j")->Fill(pthard->met()/GeV, proj_soft->mpx()/GeV, weight);
     // hist2d("softT_pthard_"+jetlabel+"j")->Fill(pthard->met()/GeV, proj_soft->mpy()/GeV, weight);
    }

    if (m_writetree) {
      if (!m_jetBins) { // Count good jets
        const JetContainer* jets(0);
        ATH_CHECK( evtStore()->retrieve(jets,m_jetcoll) );
        // Number of jets with pT > 20 GeV and passing overlap removal.
        for(const auto& j : *jets) {
          if ((j->pt() > 20000) && (dec_passOR(*j))) njets++;
        }
      }
      m_njets = njets;
      m_pthard = pthard->met()/GeV;
      m_residl_pthard = proj->mpx()/GeV;
      m_residt_pthard = proj->mpy()/GeV;
      m_avgmu = avgmu;
      m_weight = weight;

      tree("resid_pthard")->Fill();
    }

    return StatusCode::SUCCESS;
  }

  StatusCode METPerfAlg::plotPerfZ(const MissingETContainer* metperf, double weight) {
    ATH_MSG_DEBUG(name() << " -- plotting Z performance plots");
    const MissingET* proj = (*metperf)["Proj_PtZ"];
    const MissingET* ptz = (*metperf)["PtZ"];
    const MissingET* jetterm = (*metperf)[m_jetterm];
    if(!proj) {
      ATH_MSG_ERROR("Didn't find MET projected on ptZ");
      return StatusCode::FAILURE;
    }
    if(!ptz) {
      ATH_MSG_ERROR("Didn't find PtZ");
      return StatusCode::FAILURE;
    }

    ATH_MSG_VERBOSE("  Z px = " <<  ptz->mpx() << ", py = " << ptz->mpy());
    ATH_MSG_VERBOSE("  projZ px = " << proj->mpx() << ", py = " << proj->mpy());
    hist("pL_ptz")->Fill(proj->mpx()/GeV,weight);
    hist2d("pL_ptz_ptz")->Fill(ptz->met()/GeV, proj->mpx()/GeV,weight);
    hist("pT_ptz")->Fill(proj->mpy()/GeV,weight);
    //
  //  hist2d("residL_ptz")->Fill(ptz->met()/GeV, proj->mpx()/GeV, weight);
   // hist2d("residT_ptz")->Fill(ptz->met()/GeV, proj->mpy()/GeV, weight);
    //
 //   hist2d("scaleL_ptz")->Fill(ptz->met()/GeV, 1. + proj->mpx()/ptz->met(), weight);

   // hist2d("sumet_ptz")->Fill(ptz->met()/GeV, jetterm->sumet()/GeV,weight);

    Int_t njets(0);
    double leading_recoJetPt = 0.;
    double leading_recoJetEta = 0.;
    double jet_central = 0.;
    double jet_forward = 0.;
    const JetContainer* jets(0);
    ATH_CHECK( evtStore()->retrieve(jets,m_jetcoll) );
    // Number of jets with pT > 20 GeV and passing overlap removal.
    for(const auto& j : *jets) {
      if (!dec_passOR(*j)) continue;
      if(fabs(j->eta())>2.4 && j->pt()<m_fwdJetMinPt) continue;
      if ((j->pt() > m_jetMinPt)){
	njets++;
	if(TMath::Abs( j->eta() ) < 2.4){
	  jet_central += j->pt();
	}
	else{
	  jet_forward += j->pt();
	}
	
	if (j->pt() > leading_recoJetPt) {
	  leading_recoJetPt = j->pt();
	  leading_recoJetEta = j->eta();
	}
      }
    }
   // ATH_MSG_VERBOSE("Leading jet pT = " << leading_recoJetPt);
  //  hist2d("ratio_jetpt_ptz")->Fill(ptz->met()/GeV, leading_recoJetPt / ptz->met(), weight);
  //  hist2d("ratio_residL_ptz")->Fill(ptz->met()/GeV, 1 - proj->mpx()/ptz->met(), weight);
  //  hist("njets")->Fill(njets, weight);
   // hist2d("forw_cent")->Fill(jet_central/GeV, jet_forward/GeV, weight);
    if (njets == 1) {
     // hist2d("ratio_jetpt_ptz_1j")->Fill(ptz->met()/GeV, leading_recoJetPt / ptz->met(), weight);
    //  hist2d("ratio_residL_ptz_1j")->Fill(ptz->met()/GeV, 1 - proj->mpx()/ptz->met(), weight);
      if (fabs(leading_recoJetEta) < 2.) {
     //   hist2d("ratio_residL_ptz_1j_etacut")->Fill(ptz->met()/GeV, 1 - proj->mpx()/ptz->met(), weight);
      }
    }
/*
    if (m_jetBins) {
      std::string jetlabel;
      switch (njets) {
        case 0: jetlabel = "0"; break;
        case 1: jetlabel = "1"; break;
        default: jetlabel = "2";
      }
      hist2d("residL_ptz"+jetlabel+"j")->Fill(ptz->met()/GeV, proj->mpx()/GeV, weight);
      hist2d("residT_ptz"+jetlabel+"j")->Fill(ptz->met()/GeV, proj->mpy()/GeV, weight);
    }
  
*/  return StatusCode::SUCCESS;
  }

  StatusCode METPerfAlg::plotforChecks(const MissingETContainer* met, const MissingETContainer* metperf, double weight) {
    const MissingET* finalmet = (*metperf)["Final"];
    const MissingET* refjet = (*met)[m_jetterm];
    const MissingET* muons = (*met)[m_muonterm];
    const MissingET* softterm = (*met)[m_softterm];
    const MissingET* eles = (*met)[m_eleterm];
    const xAOD::MuonContainer *goodmuons=0;
    ATH_CHECK(evtStore()->retrieve(goodmuons, "GoodMuons"));
    const ElectronContainer* goodeles = 0;
    ATH_CHECK( evtStore()->retrieve(goodeles, m_elecoll) );
    const JetContainer* goodjets(0);
    ATH_CHECK( evtStore()->retrieve(goodjets,m_jetcoll) );

    if(softterm->met()/GeV > 70){
    hist("jet_met_checks_soft70")->Fill(refjet->met()/GeV,weight);
    hist("met_checks_soft70")->Fill(finalmet->met()/GeV,weight);
    hist("muon_met_checks_soft70")->Fill(muons->met()/GeV,weight);
    hist("ele_met_checks_soft70")->Fill(eles->met()/GeV,weight);
        if(m_evttype==Zmm ){
        for(const auto& iMu : *goodmuons) {
	  hist("dphi_mu_metmu_checks_soft70")->Fill(TVector2::Phi_mpi_pi(iMu->phi()-muons->phi()),weight);
	  hist("dphi_mu_metjet_checks_soft70")->Fill(TVector2::Phi_mpi_pi(iMu->phi()-refjet->phi()),weight);
	  hist("dphi_mu_soft_checks_soft70")->Fill(TVector2::Phi_mpi_pi(iMu->phi()-softterm->phi()),weight);
	  hist("dphi_mu_met_checks_soft70")->Fill(TVector2::Phi_mpi_pi(iMu->phi()-finalmet->phi()),weight);
           for(const auto& iJet : *goodjets){
	     hist("dphi_jet_mu_checks_soft70")->Fill(TVector2::Phi_mpi_pi(iJet->phi()-iMu->phi()),weight);
           }
          }
        }else if(m_evttype==Zee ){
        for(const auto& iEle: *goodeles){
	  hist("dphi_ele_metele_checks_soft70")->Fill(TVector2::Phi_mpi_pi(iEle->phi()-eles->phi()),weight);
	  hist("dphi_ele_metjet_checks_soft70")->Fill(TVector2::Phi_mpi_pi(iEle->phi()-refjet->phi()),weight);
	  hist("dphi_ele_soft_checks_soft70")->Fill(TVector2::Phi_mpi_pi(iEle->phi()-softterm->phi()),weight);
	  hist("dphi_ele_met_checks_soft70")->Fill(TVector2::Phi_mpi_pi(iEle->phi()-finalmet->phi()),weight);
           for(const auto& iJet : *goodjets){
	     hist("dphi_jet_ele_checks_soft70")->Fill(TVector2::Phi_mpi_pi(iJet->phi()-iEle->phi()),weight);
           }
          }   
        }
       for(const auto& iJet : *goodjets){
	 hist("dphi_jet_metmu_checks_soft70")->Fill(TVector2::Phi_mpi_pi(iJet->phi()-muons->phi()),weight);
	 hist("dphi_jet_metele_checks_soft70")->Fill(TVector2::Phi_mpi_pi(iJet->phi()-eles->phi()),weight);
	 hist("dphi_jet_metjet_checks_soft70")->Fill(TVector2::Phi_mpi_pi(iJet->phi()-refjet->phi()),weight);
	 hist("dphi_jet_soft_checks_soft70")->Fill(TVector2::Phi_mpi_pi(iJet->phi()-softterm->phi()),weight);
       }   
       hist("dphi_met_soft_checks_soft70")->Fill(TVector2::Phi_mpi_pi(finalmet->phi()-softterm->phi()),weight);
    }

    if(finalmet->met()/GeV > 200){

      hist("dphi_met_soft_checks_met200")->Fill(finalmet->phi()-softterm->phi(),weight);
     hist("jet_met_checks_met200")->Fill(refjet->met()/GeV,weight);
    hist("met_checks_met200")->Fill(finalmet->met()/GeV,weight);
    hist("muon_met_checks_met200")->Fill(muons->met()/GeV,weight);
    hist("ele_met_checks_met200")->Fill(eles->met()/GeV,weight);
        if(m_evttype==Zmm ){
        for(const auto& iMu : *goodmuons) {
	  hist("dphi_mu_metmu_checks_met200")->Fill(TVector2::Phi_mpi_pi(iMu->phi()-muons->phi()),weight);
	  hist("dphi_mu_metjet_checks_met200")->Fill(TVector2::Phi_mpi_pi(iMu->phi()-refjet->phi()),weight);
	  hist("dphi_mu_soft_checks_met200")->Fill(TVector2::Phi_mpi_pi(iMu->phi()-softterm->phi()),weight);
	  hist("dphi_mu_met_checks_met200")->Fill(TVector2::Phi_mpi_pi(iMu->phi()-finalmet->phi()),weight);
           for(const auto& iJet : *goodjets){
	     hist("dphi_jet_mu_checks_met200")->Fill(TVector2::Phi_mpi_pi(iJet->phi()-iMu->phi()),weight);
           }
          }
        }else if(m_evttype==Zee ){
        for(const auto& iEle: *goodeles){
	  hist("dphi_ele_metele_checks_met200")->Fill(TVector2::Phi_mpi_pi(iEle->phi()-eles->phi()),weight);
	  hist("dphi_ele_metjet_checks_met200")->Fill(TVector2::Phi_mpi_pi(iEle->phi()-refjet->phi()),weight);
	  hist("dphi_ele_soft_checks_met200")->Fill(TVector2::Phi_mpi_pi(iEle->phi()-softterm->phi()),weight);
	  hist("dphi_ele_met_checks_met200")->Fill(TVector2::Phi_mpi_pi(iEle->phi()-finalmet->phi()),weight);
           for(const auto& iJet : *goodjets){
	     hist("dphi_jet_ele_checks_met200")->Fill(TVector2::Phi_mpi_pi(iJet->phi()-iEle->phi()),weight);
           }
          }   
        }
       for(const auto& iJet : *goodjets){
	 hist("dphi_jet_metmu_checks_met200")->Fill(TVector2::Phi_mpi_pi(iJet->phi()-muons->phi()),weight);
	 hist("dphi_jet_metele_checks_met200")->Fill(TVector2::Phi_mpi_pi(iJet->phi()-eles->phi()),weight);
	 hist("dphi_jet_metjet_checks_met200")->Fill(TVector2::Phi_mpi_pi(iJet->phi()-refjet->phi()),weight);
	 hist("dphi_jet_soft_checks_met200")->Fill(TVector2::Phi_mpi_pi(iJet->phi()-softterm->phi()),weight);
       }    
    } 
    if(finalmet->met()/GeV > 150){
    hist("softmet_MET150")->Fill(softterm->met()/GeV,weight);
    }
    hist2d("met_tst")->Fill(softterm->met()/GeV,finalmet->met()/GeV,weight);
    return StatusCode::SUCCESS;
  }
}

// METTreeAlg.cxx

#include "METTreeAlg.h"

#include "xAODEventInfo/EventInfo.h"
#include "xAODJet/JetContainer.h"
#include "xAODMuon/MuonContainer.h"
#include "xAODEgamma/ElectronContainer.h"
#include "xAODMissingET/MissingETAuxContainer.h"
#include "xAODTruth/TruthParticleContainer.h"
#include "xAODMissingET/MissingETComposition.h"

#include "TH1.h"
#include "TGraphAsymmErrors.h"
#include "TGraph.h"
#include "TH2D.h"
#include "TH3D.h"
#include "TVector2.h"
//::: Debugging
#define DEBUG_COUT std::cout << " I got this far ---> " << __FILE__ << ": " << __LINE__ << std::endl;


namespace met {

  const static double GeV=1e3;
  static SG::AuxElement::ConstAccessor<float> dec_avgMu("avgMu");
  static SG::AuxElement::ConstAccessor<float> dec_avgMunocorrection("avgMunocorrection");
  static SG::AuxElement::ConstAccessor<float> dec_pileupWeight("pileupWeight");

  static SG::AuxElement::ConstAccessor<char>  dec_passOR("passOR");
  static SG::AuxElement::ConstAccessor<float> dec_triggerSF("triggerSF");
  static SG::AuxElement::ConstAccessor<float> dec_muonRecoEffSF("muonRecoEffSF");
  static SG::AuxElement::ConstAccessor<float> dec_muonIsoEffSF( "muonIsoEffSF" );
  static SG::AuxElement::ConstAccessor<float> dec_eleRecoEffSF( "eleRecoEffSF" );
  static SG::AuxElement::ConstAccessor<float> dec_eleIDEffSF(   "eleIDEffSF"   );
  static SG::AuxElement::ConstAccessor<float> dec_eleIsoEffSF(  "eleIsoEffSF"  );
//  static SG::AuxElement::ConstAccessor<float> dec_btagEffSF("btagEffSF");

  using std::string;
  using namespace xAOD;

  //**********************************************************************

  METTreeAlg::METTreeAlg(const std::string& name,
			 ISvcLocator* pSvcLocator )
    : ::AthHistogramAlgorithm( name, pSvcLocator ),
    m_passAllEvtSelections("AllEventSelections"),
    m_isPVAcc("isPV"),
    m_evtCleanAcc("eventClean"),
    m_jetCleanAcc("jetClean"),
    m_isZmmAcc("isZmm"),
    m_isZeeAcc("isZee"),
    m_isWmvAcc("isWmv"),
    m_isWevAcc("isWev"),
    m_isttbarAcc("isttbar"),
    m_isgjetAcc("isgjet"),
   // m_isBjetDec("isBjet"),
    m_objLinkAcc("originalObjectLink")
  {

    declareProperty("Suffix",     m_metsuffix   = "RefTST"          );
    declareProperty("JetTerm",    m_jetterm     = "RefJet"          );
    declareProperty("SoftTerm",   m_softterm    = "PVSoftTrk"       );
    declareProperty("EleTerm",    m_eleterm     = "RefEle"          );
    declareProperty("MuonTerm",   m_muonterm    = "Muons"           );
    declareProperty("GammaTerm",  m_gammaterm   = "RefGamma"        );
    declareProperty("TauTerm",    m_tauterm     = "RefTau"          );

    declareProperty("JetColl",    m_jetcoll     = "GoodJets"        );
    declareProperty("EleColl",    m_elecoll     = "GoodElectrons"   );
    declareProperty("MuonColl",   m_muoncoll    = "GoodMuons"       );

    declareProperty("EvtType",    m_evttype     = None              );
    declareProperty("DoJetVeto",  m_dojetveto   = false             );
    declareProperty("DoTauGammaPlots",  m_doTauGammaPlots   = false        );
    declareProperty("JetBins",    m_jetBins     = false             );
    declareProperty("WriteTree",  m_writetree   = false             );
    declareProperty("WriteHist",  m_writehist   = false             );
    declareProperty("SysName",    m_sysName     = ""                );

    declareProperty("WlnMETcut",  m_wlnmetcut   = 25*GeV            );
    declareProperty("JetPtCut",   m_jetMinPt    = 20*GeV            );

    declareProperty("ApplyTriggerSF",  m_applyTriggerSF = true    );
    declareProperty( "DoMuonElossPlots", m_doMuonElossPlots         );

    declareProperty("doPRW",		 m_doPileupReweighting      ); //claire
    declareProperty("noTruthPthard",	 m_noTruthPthard = false    ); // Do not include true MET in pthard.
  }

  //**********************************************************************

  METTreeAlg::~METTreeAlg() { }

  //**********************************************************************

  StatusCode METTreeAlg::initialize() {
    ATH_MSG_INFO("Initializing " << name() << "...");

    TH1::StatOverflows(); //Some of the results (e.g. hadronic recoil) are skewed quite significantly if we don't do this
    if (m_writetree) {
      m_treeName = "Performance";
      m_treeName.append(m_sysName);
      ATH_CHECK (book(TTree(histname(m_treeName), "All MET Performance variables") ) );
      TTree* t = tree(histname(m_treeName));
      t->Branch("weight", 	     &m_weight,        "weight/D");
      t->Branch("met",  	     &m_met, 	       "met/F");
      t->Branch("metphi",        &m_phi,           "phi/F");
      t->Branch("mpx",  	     &m_mpx, 	       "mpx/F");
      t->Branch("mpy",  	     &m_mpy, 	       "mpy/F");
      t->Branch("sumet",         &m_sumet,         "sumet/F");
      t->Branch("jetmet",  	     &m_jetmet, 	   "jetmet/F");
      t->Branch("jetmetphi",     &m_jetphi,        "jetphi/F");
      t->Branch("jetmpx",  	     &m_jetmpx, 	   "jetmpx/F");
      t->Branch("jetmpy",  	     &m_jetmpy, 	   "jetmpy/F");
      t->Branch("jetsumet",      &m_jetsumet,      "jetsumet/F");
      t->Branch("softmet",  	 &m_softmet, 	   "softmet/F");
      t->Branch("softmetphi",    &m_softphi,        "softphi/F");
      t->Branch("softmpx",  	 &m_softmpx, 	   "softmpx/F");
      t->Branch("softmpy",  	 &m_softmpy, 	   "softmpy/F");
      t->Branch("softsumet",     &m_softsumet,      "softsumet/F");
      t->Branch("elemet",  	     &m_elemet, 	       "elemet/F");
      t->Branch("elemetphi",     &m_elephi,           "elephi/F");
      t->Branch("elempx",  	     &m_elempx, 	       "elempx/F");
      t->Branch("elempy",  	     &m_elempy, 	       "elempy/F");
      t->Branch("elesumet",      &m_elesumet,         "elesumet/F");
      t->Branch("muonmet",  	 &m_muonmet, 	       "muonmet/F");
      t->Branch("muonmetphi",    &m_muonphi,           "muonphi/F");
      t->Branch("muonmpx",  	 &m_muonmpx, 	       "muonmpx/F");
      t->Branch("muonmpy",  	 &m_muonmpy, 	       "muonmpy/F");
      t->Branch("muonsumet",     &m_muonsumet,         "muonsumet/F");
      t->Branch("photonmet",  	 &m_photonmet, 	       "photonmet/F");
      t->Branch("photonmetphi",  &m_photonphi,           "photonphi/F");
      t->Branch("photonmpx",  	 &m_photonmpx, 	       "photonmpx/F");
      t->Branch("photonmpy",  	 &m_photonmpy, 	       "photonmpy/F");
      t->Branch("photonsumet",   &m_photonsumet,         "photonsumet/F");
      t->Branch("taumet",  	     &m_taumet, 	       "taumet/F");
      t->Branch("taumetphi",     &m_tauphi,           "tauphi/F");
      t->Branch("taumpx",  	     &m_taumpx, 	       "taumpx/F");
      t->Branch("taumpy",  	     &m_taumpy, 	       "taumpy/F");
      t->Branch("tausumet",      &m_tausumet,         "tausumet/F");
      t->Branch("d0",        	 &m_d0,               "d0/F")    ;
      t->Branch("z0sinT",        &m_z0sinT,           "z0sinT/F");
      t->Branch("d0sig",         &m_d0sig,            "d0sig/F") ;
      t->Branch("Ntrack",        &m_Ntrack,           "Ntrack/I");

      t->Branch("pthard", 		 &m_pthard, 	   "pthard/F");
      t->Branch("ptZ", 			 &m_ptZ, 	       "ptZ/F");
      t->Branch("mZ", 		     &m_mZ, 	       "mZ/F");
      t->Branch("pLPtZ", 		 &m_pLPtZ, 	       "pLPtZ/F");
      t->Branch("pTPtZ", 		 &m_pTPtZ, 	       "pTPtZ/F");
      //t->Branch("residT_pthard", &m_residt_pthard, "residT_pthard/D");
      //t->Branch("residL_pthard", &m_residl_pthard, "residL_pthard/D");
      t->Branch("nJets", 	     &m_njets, 		   "nJets/I");
      t->Branch("avgMu", 	     &m_avgmu, 		   "avgMu/F");
      t->Branch("npv", 	         &m_npv, 		   "npv/I");


    }

    if(m_writehist){

      ATH_CHECK( book( TH1D( histname("met"),   "Magnitude of the total MET", 100, 0., 1000. ) ) );
      ATH_CHECK( book( TH1D( histname("phi"),   "Azimuthal direction of the total MET", 64, -3.2, 3.2 ) ) );
      hist(histname("met"))->Sumw2();
      hist(histname("phi"))->Sumw2();
      ATH_CHECK( book( TH1D( histname("sumet"), "Total Sum ET", 200, 0., 3000. ) ) );
      ATH_CHECK( book( TH1D( histname("mpx"),   "X-component of the total MET", 100, -250., 250. ) ) );
      ATH_CHECK( book( TH1D( histname("mpy"),   "Y-component of the total MET", 100, -250., 250. ) ) );
      hist(histname("sumet"))->Sumw2();
      hist(histname("mpx"))->Sumw2();
      hist(histname("mpy"))->Sumw2();
      ATH_CHECK( book( TH1D( histname("sig"),   "Total MET Significance", 100, 0., 20. ) ) );
      hist(histname("sig"))->Sumw2();
      ATH_CHECK( book( TH1D( histname("sigHT"),   "SUSY MET Significance", 100, 0., 20. ) ) );
      hist(histname("sigHT"))->Sumw2();

      ATH_CHECK( book( TH1D( histname("sumet_npv_mu"), "Total Sum ET", 200, 0., 3000. ) ) );
      hist(histname("sumet_npv_mu"))->Sumw2();

      ATH_CHECK( book( TH1D( histname("mu_distribution"), "Total Sum ET", 50, 0., 50. ) ) );
      hist(histname("mu_distribution"))->Sumw2();

      ATH_CHECK( book( TH1D( histname("mu_distribution_w"), "Total Sum ET", 50, 0., 50. ) ) );
      hist(histname("mu_distribution_w"))->Sumw2();

      ATH_CHECK( book( TH1D( histname("jet_met"),   "Magnitude of the jet MET", 100, 0., 1000. ) ) );
      ATH_CHECK( book( TH1D( histname("jet_phi"),   "Azimuthal direction of the jet MET", 64, -3.2, 3.2 ) ) );
      ATH_CHECK( book( TH1D( histname("jet_sumet"), "Jet Sum ET", 200, 0., 3000. ) ) );
      ATH_CHECK( book( TH1D( histname("jet_mpx"),   "X-component of the jet MET", 100, -250., 250. ) ) );
      ATH_CHECK( book( TH1D( histname("jet_mpy"),   "Y-component of the jet MET", 100, -250., 250. ) ) );
      hist(histname("jet_met"))->Sumw2();
      hist(histname("jet_phi"))->Sumw2();
      hist(histname("jet_sumet"))->Sumw2();
      hist(histname("jet_mpx"))->Sumw2();
      hist(histname("jet_mpy"))->Sumw2();

      ATH_CHECK( book( TH1D( histname("muon_met"),   "Magnitude of the muon MET", 100, 0., 1000. ) ) );
      ATH_CHECK( book( TH1D( histname("muon_phi"),   "Azimuthal direction of the muon MET", 64, -3.2, 3.2 ) ) );
      ATH_CHECK( book( TH1D( histname("muon_sumet"), "Muon Sum ET", 100, 0., 2000. ) ) );
      ATH_CHECK( book( TH1D( histname("muon_mpx"),   "X-component of the muon MET", 100, -250., 250. ) ) );
      ATH_CHECK( book( TH1D( histname("muon_mpy"),   "Y-component of the muon MET", 100, -250., 250. ) ) );
      hist(histname("muon_met"))->Sumw2();
      hist(histname("muon_phi"))->Sumw2();
      hist(histname("muon_sumet"))->Sumw2();
      hist(histname("muon_mpx"))->Sumw2();
      hist(histname("muon_mpy"))->Sumw2();

      ATH_CHECK( book( TH1D( histname("ele_met"),   "Magnitude of the electron MET", 100, 0., 1000. ) ) );
      ATH_CHECK( book( TH1D( histname("ele_phi"),   "Azimuthal direction of the electron MET", 64, -3.2, 3.2 ) ) );
      ATH_CHECK( book( TH1D( histname("ele_sumet"), "Electron Sum ET", 100, 0., 2000. ) ) );
      ATH_CHECK( book( TH1D( histname("ele_mpx"),   "X-component of the electron MET", 100, -250., 250. ) ) );
      ATH_CHECK( book( TH1D( histname("ele_mpy"),   "Y-component of the electron MET", 100, -250., 250. ) ) );
      hist(histname("ele_met"))->Sumw2();
      hist(histname("ele_phi"))->Sumw2();
      hist(histname("ele_sumet"))->Sumw2();
      hist(histname("ele_mpx"))->Sumw2();
      hist(histname("ele_mpy"))->Sumw2();

      ATH_CHECK( book( TH1D( histname("soft_met"),   "Magnitude of the soft MET", 100, 0., 300. ) ) );
      ATH_CHECK( book( TH1D( histname("soft_phi"),   "Azimuthal direction of the soft MET", 64, -3.2, 3.2 ) ) );
      ATH_CHECK( book( TH1D( histname("soft_sumet"), "Soft Sum ET", 100, 0., 1000. ) ) );
      ATH_CHECK( book( TH1D( histname("soft_mpx"),   "X-component of the soft MET", 100, -250., 250. ) ) );
      ATH_CHECK( book( TH1D( histname("soft_mpy"),   "Y-component of the soft MET", 100, -250., 250. ) ) );
      hist(histname("soft_met"))->Sumw2();
      hist(histname("soft_phi"))->Sumw2();
      hist(histname("soft_sumet"))->Sumw2();
      hist(histname("soft_mpx"))->Sumw2();
      hist(histname("soft_mpy"))->Sumw2();

      ATH_CHECK( book( TH1D( histname("photon_met"),   "Magnitude of the photon MET", 100, 0., 300. ) ) );
      ATH_CHECK( book( TH1D( histname("photon_phi"),   "Azimuthal direction of the photon MET", 64, -3.2, 3.2 ) ) );
      ATH_CHECK( book( TH1D( histname("photon_sumet"), "photon Sum ET", 100, 0., 1000. ) ) );
      ATH_CHECK( book( TH1D( histname("photon_mpx"),   "X-component of the photon MET", 100, -250., 250. ) ) );
      ATH_CHECK( book( TH1D( histname("photon_mpy"),   "Y-component of the photon MET", 100, -250., 250. ) ) );
      hist(histname("photon_met"))->Sumw2();
      hist(histname("photon_phi"))->Sumw2();
      hist(histname("photon_sumet"))->Sumw2();
      hist(histname("photon_mpx"))->Sumw2();
      hist(histname("photon_mpy"))->Sumw2();

      ATH_CHECK( book( TH1D( histname("tau_met"),   "Magnitude of the tau MET", 100, 0., 300. ) ) );
      ATH_CHECK( book( TH1D( histname("tau_phi"),   "Azimuthal direction of the tau MET", 64, -3.2, 3.2 ) ) );
      ATH_CHECK( book( TH1D( histname("tau_sumet"), "tau Sum ET", 100, 0., 1000. ) ) );
      ATH_CHECK( book( TH1D( histname("tau_mpx"),   "X-component of the tau MET", 100, -250., 250. ) ) );
      ATH_CHECK( book( TH1D( histname("tau_mpy"),   "Y-component of the tau MET", 100, -250., 250. ) ) );
      hist(histname("tau_met"))->Sumw2();
      hist(histname("tau_phi"))->Sumw2();
      hist(histname("tau_sumet"))->Sumw2();
      hist(histname("tau_mpx"))->Sumw2();
      hist(histname("tau_mpy"))->Sumw2();

      ATH_CHECK( book( TH1D( histname("pThard_distribution"), "pT hard distribution", 100, 0., 1000. ) ) );
      hist(histname("pThard_distribution"))->Sumw2();

      ATH_CHECK( book( TH1D( histname("pL_pthard"), "Long. projection on pthard of the total MET", 100, -250., 250. ) ) );
      ATH_CHECK( book( TH1D( histname("pT_pthard"), "Trans. projection on pthard of the total MET", 100, -250., 250. ) ) );
      hist(histname("pL_pthard"))->Sumw2();
      hist(histname("pT_pthard"))->Sumw2();

      //Histograms for AtlasDerivation20.7 validation

      ATH_CHECK( book( TH1D( histname("track_d0"), "tracks d0", 800, -3, 3 ) ) );
      hist(histname("track_d0"))->Sumw2();

      ATH_CHECK( book( TH1D( histname("best_Dz0sin"), "best delta z0 sin theta", 200, 0, 0.8 ) ) );
      hist(histname("best_Dz0sin"))->Sumw2();

      ATH_CHECK( book( TH1D( histname("track_Dz0sin"), "tracks delta z0 sin theta", 125, 0, 0.5 ) ) );
      hist(histname("track_Dz0sin"))->Sumw2();

      ATH_CHECK( book( TH1D( histname("sig_track_d0"), "significance tracks d0", 100, -6., 6. ) ) );
      hist(histname("sig_track_d0"))->Sumw2();

      ATH_CHECK( book( TH1D( histname("n_Event"), "n_Event", 976810, 1150011041.5, 1150987851.5 ) ) );
      hist(histname("n_Event"))->Sumw2();

      ATH_CHECK( book( TH1D( histname("n_track"), "n_track", 150, 0, 150 ) ) );
      hist(histname("n_track"))->Sumw2();

      ATH_CHECK( book( TH1D( histname("jethard_distribution"), "jet hard distribution", 100, 0., 1000. ) ) );
      hist(histname("jethard_distribution"))->Sumw2();

      ATH_CHECK( book( TH1D( histname("Reco_NonIntMET"), "Reconstructed - Truth MET", 100, 0., 500. ) ) );
      hist(histname("Reco_NonIntMET"))->Sumw2();

      ATH_CHECK( book( TH1D( histname("absReco_NonIntMET"), "|Reconstructed| - |Truth| MET", 100, 0., 500. ) ) );
      hist(histname("absReco_NonIntMET"))->Sumw2();

      ATH_CHECK( book( TH1D( histname("npv_distribution"), "number of primary vertices distribution", 200, 0., 200. ) ) );
      hist(histname("npv_distribution"))->Sumw2();

      ATH_CHECK( book( TH1D( histname("lowjetpt_distribution"), "Jet term for low pt jet events distribution", 100, 0., 1000. ) ) );
      hist(histname("lowjetpt_distribution"))->Sumw2();

      ATH_CHECK( book( TH1D( histname("jetsize60"), "Number of jets with pt>60", 100, 0., 100. ) ) );
      hist(histname("jetsize60"))->Sumw2();
      ATH_CHECK( book( TH1D( histname("jetsize40"), "Number of jets with pt>40", 100, 0., 100. ) ) );
      hist(histname("jetsize40"))->Sumw2();
      ATH_CHECK( book( TH1D( histname("jetsize25"), "Number of jets with pt>25", 100, 0., 100. ) ) );
      hist(histname("jetsize25"))->Sumw2();

      ATH_CHECK( book( TH1D( histname("jetsize60_norm"), "Number of jets with pt>60 normalised to number of jets", 400, 0., 1. ) ) );
      hist(histname("jetsize60_norm"))->Sumw2();
      ATH_CHECK( book( TH1D( histname("jetsize40_norm"), "Number of jets with pt>40 normalised to number of jets", 400, 0., 1. ) ) );
      hist(histname("jetsize40_norm"))->Sumw2();
      ATH_CHECK( book( TH1D( histname("jetsize25_norm"), "Number of jets with pt>25 normalised to number of jets", 400, 0., 1. ) ) );
      hist(histname("jetsize25_norm"))->Sumw2();

      /// MET and MET terms distributions in different jet multiplicity bins
      if (m_jetBins) {
        for (Int_t njets = 0; njets <= 2; njets++) {
          std::string s_njets = std::to_string(njets);
          ATH_CHECK( book( TH1D( histname("met_"+s_njets+"j"), ("Magnitude of the total MET, "+s_njets+" jets").c_str(),
                                 100, 0., 1000. ) ) );
          ATH_CHECK( book( TH1D( histname("jet_met_"+s_njets+"j"),   ("Magnitude of the jet MET, "+s_njets+" jets").c_str(),
                                 100, 0., 1000. ) ) );
          ATH_CHECK( book( TH1D( histname("muon_met_"+s_njets+"j"),  ("Magnitude of the muon MET, "+s_njets+" jets").c_str(),
                                 100, 0., 1000. ) ) );
          ATH_CHECK( book( TH1D( histname("ele_met_"+s_njets+"j"),   ("Magnitude of the electron MET, "+s_njets+" jets").c_str(),
                                 100, 0., 1000. ) ) );
          ATH_CHECK( book( TH1D( histname("soft_met_"+s_njets+"j"),  ("Magnitude of the soft MET, "+s_njets+" jets").c_str(),
                                 100, 0., 1000. ) ) );
          hist(histname("met_"+s_njets+"j"))->Sumw2();
          hist(histname("jet_met_"+s_njets+"j"))->Sumw2();
          hist(histname("muon_met_"+s_njets+"j"))->Sumw2();
          hist(histname("ele_met_"+s_njets+"j"))->Sumw2();
          hist(histname("soft_met_"+s_njets+"j"))->Sumw2();
        }
      }

      // MET and MET terms distributions in different <mu> regions
      std::string muRegion ;
      for (Int_t iMuRegion = 0; iMuRegion <= 3; iMuRegion++) {
        if (iMuRegion == 0 ) muRegion = "_mu0_10" ;
        if (iMuRegion == 1 ) muRegion = "_mu10_15" ;
        if (iMuRegion == 2 ) muRegion = "_mu15_20" ;
        if (iMuRegion == 3 ) muRegion = "_HighMu" ;

        ATH_CHECK( book( TH1D( histname("met"+muRegion),   ("Magnitude of the total MET, "+muRegion).c_str(), 100, 0., 1000. ) ) );
        ATH_CHECK( book( TH1D( histname("jet_met"+muRegion),   ("Magnitude of the jet MET, "+muRegion).c_str(), 100, 0., 1000. ) ) );
        ATH_CHECK( book( TH1D( histname("soft_met"+muRegion),   ("Magnitude of the soft MET, "+muRegion).c_str(), 100, 0., 1000. ) ) );
        ATH_CHECK( book( TH1D( histname("sumet"+muRegion), ("Total Sum ET, "+muRegion).c_str(), 100, 0., 1000. ) ) );
        ATH_CHECK( book( TH1D( histname("pThard_distribution"+muRegion), ("pT hard distribution, "+muRegion).c_str(), 200, 0., 500. ) ) );
        ATH_CHECK( book( TH1D( histname("ele_met"+muRegion),   ("Magnitude of the electron MET, "+muRegion).c_str(), 100, 0., 1000. ) ) );
        ATH_CHECK( book( TH1D( histname("muon_met"+muRegion),   ("Magnitude of the muon MET, "+muRegion).c_str(), 100, 0., 1000. ) ) );
        hist(histname("met"+muRegion))->Sumw2();
        hist(histname("jet_met"+muRegion))->Sumw2();
        hist(histname("soft_met"+muRegion))->Sumw2();
        hist(histname("sumet"+muRegion))->Sumw2();
        hist(histname("pThard_distribution"+muRegion))->Sumw2();
        hist(histname("ele_met"+muRegion))->Sumw2();
        hist(histname("muon_met"+muRegion))->Sumw2();
        //
      }


      ATH_CHECK( book( TH2D( histname("residxy_mu"), "Total MET residuals vs mu",
	  		   50, 0., 50., 100, -100., 100.  ) ) );
      ATH_CHECK( book( TH2D( histname("residL_mu"), "Total MET long. residuals vs mu",
	  		   50, 0., 50., 100, -100., 100.  ) ) );
      ATH_CHECK( book( TH2D( histname("residT_mu"), "Total MET trans. residuals vs mu",
	  		   50, 0., 50., 100, -100., 100.  ) ) );
      hist(histname("residxy_mu"))->Sumw2();
      hist(histname("residL_mu"))->Sumw2();
      hist(histname("residT_mu"))->Sumw2();
      //
      ATH_CHECK( book( TH2D( histname("residxy_npv"), "Total MET residuals vs npv",
	  		   50, 0., 50., 100, -100., 100.  ) ) );
      ATH_CHECK( book( TH2D( histname("residL_npv"), "Total MET long. residuals vs npv",
	  		   50, 0., 50., 100, -100., 100.  ) ) );
      ATH_CHECK( book( TH2D( histname("residT_npv"), "Total MET trans. residuals vs npv",
	  		   50, 0., 50., 100, -100., 100.  ) ) );
      hist(histname("residxy_npv"))->Sumw2();
      hist(histname("residL_npv"))->Sumw2();
      hist(histname("residT_npv"))->Sumw2();
      //
      ATH_CHECK( book( TH2D( histname("residxy_sumet"), "Total MET residuals vs sumet",
	  		   50, 0., 1000., 100, -100., 100.  ) ) );
      ATH_CHECK( book( TH2D( histname("residL_sumet"), "Total MET long. residuals vs sumet",
	  		   50, 0., 1000., 100, -100., 100.  ) ) );
      ATH_CHECK( book( TH2D( histname("residT_sumet"), "Total MET trans. residuals vs sumet",
	  		   50, 0., 1000., 100, -100., 100.  ) ) );
      hist(histname("residxy_sumet"))->Sumw2();
      hist(histname("residL_sumet"))->Sumw2();
      hist(histname("residT_sumet"))->Sumw2();
      //
      ATH_CHECK( book( TH2D( histname("residxy_CSTsumet"), "Total MET residuals vs CSTsumet",
                             50, 0., 1000., 100, -100., 100.  ) ) );
      ATH_CHECK( book( TH2D( histname("residL_CSTsumet"), "Total MET long. residuals vs CSTsumet",
                             50, 0., 1000., 100, -100., 100.  ) ) );
      ATH_CHECK( book( TH2D( histname("residT_CSTsumet"), "Total MET trans. residuals vs CSTsumet",
                             50, 0., 1000., 100, -100., 100.  ) ) );
      hist(histname("residxy_CSTsumet"))->Sumw2();
      hist(histname("residL_CSTsumet"))->Sumw2();
      hist(histname("residT_CSTsumet"))->Sumw2();
      //
      ATH_CHECK( book( TH2D( histname("residxy_TSTsumet"), "Total MET residuals vs TSTsumet",
                             50, 0., 1000., 100, -100., 100.  ) ) );
      ATH_CHECK( book( TH2D( histname("residL_TSTsumet"), "Total MET long. residuals vs TSTsumet",
                             50, 0., 1000., 100, -100., 100.  ) ) );
      ATH_CHECK( book( TH2D( histname("residT_TSTsumet"), "Total MET trans. residuals vs TSTsumet",
                             50, 0., 1000., 100, -100., 100.  ) ) );
      hist(histname("residxy_TSTsumet"))->Sumw2();
      hist(histname("residL_TSTsumet"))->Sumw2();
      hist(histname("residT_TSTsumet"))->Sumw2();
      //
      //ATH_CHECK( book( TH2D( histname("residxy_LHsumet"), "Total MET residuals vs LocHadTopo sumet",
      //    		   50, 0., 1000., 100, -100., 100.  ) ) );
      //ATH_CHECK( book( TH2D( histname("residL_LHsumet"), "Total MET long. residuals vs LocHadTopo sumet",
      //    		   50, 0., 1000., 100, -100., 100.  ) ) );
      //ATH_CHECK( book( TH2D( histname("residT_LHsumet"), "Total MET trans. residuals vs LocHadTopo sumet",
      //    		   50, 0., 1000., 100, -100., 100.  ) ) );
      //hist(histname("residxy_LHsumet"))->Sumw2();
      //hist(histname("residL_LHsumet"))->Sumw2();
      //hist(histname("residT_LHsumet"))->Sumw2();
      //
      ATH_CHECK( book( TH2D( histname("residL_pthard"), "Total MET long. residuals vs ptHard",
	  		   100, 0., 1000., 800, -200., 200.  ) ) );
      ATH_CHECK( book( TH2D( histname("residT_pthard"), "Total MET trans. residuals vs ptHard",
	  		   100, 0., 1000., 400, -100., 100.  ) ) );
      //
      ATH_CHECK( book( TH2D( histname("scaleL_pthard"), "Total MET long. scale vs ptHard",
	  		   100, 0., 1000., 50, 0., 5.  ) ) );
      hist(histname("residL_pthard"))->Sumw2();
      hist(histname("residT_pthard"))->Sumw2();
      hist(histname("scaleL_pthard"))->Sumw2();

      ATH_CHECK( book( TH2D( histname("softL_pthard"), "MET soft term long. residuals vs ptHard",
	  		   200, 0., 1000., 800, -200., 200.   ) ) );
      ATH_CHECK( book( TH2D( histname("softT_pthard"), "MET soft term trans. residuals vs ptHard",
	  		   200, 0., 1000.,  400, -100., 100.  ) ) );
      hist(histname("softL_pthard"))->Sumw2();
      hist(histname("softT_pthard"))->Sumw2();

      // pLsoft_vsPthard_vsSumEt - for CST syst studies
      ATH_CHECK( book( TH3D( histname("pLsoft_vsPthard_vsSumet"),
                       "soft term long proj (pL) on pthard vs pthard vs sumet" ,200, 0., 2000.,40, 0., 200.,1400, -80., 200. ) ) );//x:sumet, y:pThard, z:pLsoft
      hist3d(histname("pLsoft_vsPthard_vsSumet"))->Sumw2();


      if (m_jetBins) {
        for (Int_t njets = 0; njets <= 2; njets++) {
          std::string s_njets = std::to_string(njets);
          ATH_CHECK( book( TH2D( histname("residL_pthard_"+s_njets+"j"),
                                 ("Total MET long. residuals vs pthard, "+s_njets+" jets").c_str(),
                                 100, 0., 1000., 800, -200., 200.  ) ) );
          ATH_CHECK( book( TH2D( histname("residT_pthard_"+s_njets+"j"),
                                 ("Total MET trans. residuals vs pthard, "+s_njets+" jets").c_str(),
                                 100, 0., 1000., 400, -100., 100.  ) ) );
          hist2d(histname("residL_pthard_"+s_njets+"j"))->Sumw2();
          hist2d(histname("residT_pthard_"+s_njets+"j"))->Sumw2();

          ATH_CHECK( book( TH2D( histname("softL_pthard_"+s_njets+"j"),
                                 ("Total MET long. soft term vs pthard, "+s_njets+" jets").c_str(),
                                 200, 0., 1000., 800, -200., 200.   ) ) );
          ATH_CHECK( book( TH2D( histname("softT_pthard_"+s_njets+"j"),
                                 ("Total MET trans. soft term vs pthard, "+s_njets+" jets").c_str(),
                                 200, 0., 1000., 400, -100., 100.  ) ) );
          hist2d(histname("softL_pthard_"+s_njets+"j"))->Sumw2();
          hist2d(histname("softT_pthard_"+s_njets+"j"))->Sumw2();
        }
      }

      if( m_evttype==Zmm || m_evttype==Zee ) {
        ATH_CHECK( book( TH1D( histname("mZ"), "Z mass", 100, 50., 150. ) ) );
        ATH_CHECK( book( TH1D( histname("ptZ"), "Z transverse momentum", 100, 0., 1000. ) ) );
        ATH_CHECK( book( TH1D( histname("pthard"), "Hard objects vector sum", 100, 0., 1000. ) ) );
        hist(histname("mZ"))->Sumw2();
        hist(histname("ptZ"))->Sumw2();
        hist(histname("pthard"))->Sumw2();
        //
        ATH_CHECK( book( TH1D( histname("leadingLep_pt"), "Z mass", 200, 0., 1000. ) ) );
        ATH_CHECK( book( TH1D( histname("subleadingLep_pt"), "subleading lepton pT", 200, 0., 1000. ) ) );
        hist(histname("leadingLep_pt"))->Sumw2();
        hist(histname("subleadingLep_pt"))->Sumw2();
        //
        ATH_CHECK( book( TH1D( histname("pL_ptz"), "Long. projection on ptZ of the total MET", 100, -250., 250. ) ) );
        ATH_CHECK( book( TH2D( histname("pL_ptz_ptz"), "Long. projection on ptZ of the total MET vs ptZ", 40, 0., 200., 100, -250., 250. ) ) );
        ATH_CHECK( book( TH1D( histname("pT_ptz"), "Trans. projection on ptZ of the total MET", 100, -250., 250. ) ) );
        hist(histname("pL_ptz"))->Sumw2();
        hist2d(histname("pL_ptz_ptz"))->Sumw2();
        hist(histname("pT_ptz"))->Sumw2();
        //
        ATH_CHECK( book( TH2D( histname("residL_ptz"), "Total MET long. residuals vs ptZ",
	  		     100, 0., 1000., 200, -200., 200.  ) ) );
        ATH_CHECK( book( TH2D( histname("residT_ptz"), "Total MET trans. residuals vs ptZ",
	  		     100, 0., 1000., 200, -100., 100.  ) ) );
        hist2d(histname("residL_ptz"))->Sumw2();
        hist2d(histname("residT_ptz"))->Sumw2();
        //
        ATH_CHECK( book( TH2D( histname("scaleL_ptz"), "Total MET long. scale vs ptZ",
	  		     100, 0., 1000., 50, 0., 5. ) ) );
        hist2d(histname("scaleL_ptz"))->Sumw2();

        ATH_CHECK( book( TH2D( histname("ratio_jetpt_ptz"), "Ratio of leading jet pT to ptZ vs ptZ",
	  		     40, 0., 200., 100, 0., 5. ) ) );
        hist2d(histname("ratio_jetpt_ptz"))->Sumw2();
        ATH_CHECK( book( TH2D( histname("ratio_jetpt_ptz_1j"), "Ratio of leading jet pT to ptZ vs ptZ",
	  		     40, 0., 200., 100, 0., 5. ) ) );
        hist2d(histname("ratio_jetpt_ptz_1j"))->Sumw2();

        ATH_CHECK( book( TH2D( histname("ratio_residL_ptz"), "1 - Ratio of total MET long. residuals to ptZ vs ptZ",
	  		     40, 0., 200., 200, -10., 10.  ) ) );
        hist2d(histname("ratio_residL_ptz"))->Sumw2();
        ATH_CHECK( book( TH2D( histname("ratio_residL_ptz_1j"), "1 - Ratio of total MET long. residuals to ptZ vs ptZ",
	  		     40, 0., 200., 200, -10., 10.  ) ) );
        hist2d(histname("ratio_residL_ptz_1j"))->Sumw2();
        ATH_CHECK( book( TH2D( histname("ratio_residL_ptz_1j_etacut"), "1 - Ratio of total MET long. residuals to ptZ vs ptZ",
	  		     40, 0., 200., 200, -10., 10.  ) ) );
        hist2d(histname("ratio_residL_ptz_1j_etacut"))->Sumw2();
        ATH_CHECK( book( TH1D( histname("njets"), "NJets", 15, 0, 15 ) ) );
        hist(histname("njets"))->Sumw2();
        ATH_CHECK( book( TH2D( histname("sumet_ptz"), "Jet sumet vs ptZ",
	  		     100, 0., 1000., 100, 0., 1000.  ) ) );
        hist2d(histname("sumet_ptz"))->Sumw2();
        ATH_CHECK( book( TH2D( histname("forw_cent"), "Forward jet vs Central jet sums",
	  		     100, 0., 1000., 100, 0., 1000.  ) ) );
        hist2d(histname("forw_cent"))->Sumw2();

        if (m_jetBins) {
          for (Int_t njets = 0; njets <= 2; njets++) {
            std::string s_njets = std::to_string(njets);
            ATH_CHECK( book( TH2D( histname("residL_ptz"+s_njets+"j"),
                                   ("Total MET long. residuals vs ptZ, "+s_njets+" jets").c_str(),
                                   100, 0., 1000., 100, -100., 100.  ) ) );
            ATH_CHECK( book( TH2D( histname("residT_ptz"+s_njets+"j"),
                                   ("Total MET trans. residuals vs ptZ, "+s_njets+" jets").c_str(),
                                   100, 0., 1000., 100, -100., 100.  ) ) );
            hist2d(histname("residL_ptz"+s_njets+"j"))->Sumw2();
            hist2d(histname("residT_ptz"+s_njets+"j"))->Sumw2();
          }
        }
      }

      ATH_CHECK( book( TH1D( histname("jetrecosums"), "Sum of reco jets matched to truth", 100, 0., 1000. ) ) );
      hist(histname("jetrecosums"))->Sumw2();
      ATH_CHECK( book( TH2D( histname("forw_cent_truth"), "Forward vs Central truth matched jet sums",
        		     100, 0., 1000., 100, 0., 1000.  ) ) );
      hist2d(histname("forw_cent_truth"))->Sumw2();

      if( m_evttype==Wmv || m_evttype==Wev || m_evttype==ttbar ) {
        ATH_CHECK( book( TH1D( histname("WmT"), "W transverse mass", 100, 0., 150. ) ) );
        //
        ATH_CHECK( book( TH2D( histname("residxy_truth"), "Deviation of MET magnitude vs truth MET",
                               100, 0., 1000., 100, -100., 100. ) ) );
        ATH_CHECK( book( TH2D( histname("lin_truth"), "Deviation of MET magnitude vs truth MET",
	  		     100, 0., 1000., 240, -2., 10. ) ) );
        ATH_CHECK( book( TH2D( histname("residL_truth"), "Total MET long. residuals vs truth MET",
	  		     100, 0., 1000., 100, -100., 100. ) ) );
        ATH_CHECK( book( TH2D( histname("residT_truth"), "Total MET trans. residuals magnitude vs truth MET",
	  		     100, 0., 1000., 100, -100., 100. ) ) );
        ATH_CHECK( book( TH2D( histname("biasL_truth"), "Deviation of projected MET magnitude vs truth MET",
	  		     100, 0., 1000., 50, -5., 5. ) ) );
        ATH_CHECK( book( TH2D( histname("reco_vs_truth"), "Total reco MET vs Truth MET",
                               100, 0., 1000., 40, 0., 200. ) ) );
        //
        ATH_CHECK( book( TH2D( histname("residPhi_truth"), "Total MET phi residuals vs truth MET",
	  		     100, 0., 1000., 64, -3.2, 3.2 ) ) );
        hist(histname("WmT"))->Sumw2();
        hist(histname("residxy_truth"))->Sumw2();
        hist(histname("lin_truth"))->Sumw2();
        hist(histname("residL_truth"))->Sumw2();
        hist(histname("residT_truth"))->Sumw2();
        hist(histname("biasL_truth"))->Sumw2();
        hist(histname("reco_vs_truth"))->Sumw2();
        hist(histname("residPhi_truth"))->Sumw2();
      }

      double jetbins[21] = {10.,20., 20.*sqrt(2), 40., 40.*sqrt(2), 80., 80.*sqrt(2), 160., 160.*sqrt(2),
	  		  320., 320.*sqrt(2), 640., 640.*sqrt(2), 1280., 1280*sqrt(2),
	  		  2560., 2560.*sqrt(2), 5120., 5120.*sqrt(2), 10240., 10240.*sqrt(2)};
      ATH_CHECK( book( TH2D( histname("jetresponse"),   "Ratio of reco to true jet pt", 20, jetbins, 100, 0., 2. ) ) );
      hist(histname("jetresponse"))->Sumw2();

      ATH_CHECK( book( TH1D( histname("jetsizeHisto"), "jets size", 50, 0., 50.) ) );
      hist(histname("jetsizeHisto"))->Sumw2();

      if(m_doMuonElossPlots){

      	ATH_CHECK( book( TH1D( histname("elosstruth"),   "Energy Loss Truth", 100, 0., 1000. ) ) );
      	ATH_CHECK( book( TH1D( histname("eloss_fit_truth"),   "Energy Loss Fit minus Truth difference", 100, -50., 50. ) ) );
      	ATH_CHECK( book( TH1D( histname("elossfit"),   "Energy Loss Fit", 100, 0., 1000. ) ) );
      	ATH_CHECK( book( TH1D( histname("elossmeas"),   "Measured Energy Loss", 100, 0., 1000. ) ) );
      	ATH_CHECK( book( TH1D( histname("elossparam"),   "Parametrized Energy Loss", 100, 0., 1000. ) ) );
      	ATH_CHECK( book( TH1D( histname("elossfact"),   "Energy Loss Scale Factor", 100, 0., 1. ) ) );
      	ATH_CHECK( book( TH2D( histname("jetpt_eloss"),   "Correlation plot of jet p_{T} vs Muon Eloss", 100, 0., 1000., 100, 0., 1000. ) ) );
      	ATH_CHECK( book( TH2D( histname("muonpt_met"),   "Correlation plot of muon p_{T} vs MET", 100, 0., 1000., 100, 0., 1000. ) ) );
      	ATH_CHECK( book( TH2D( histname("muon_pt_ip_met"),   "Correlation plot of muon p_{T,IP} vs MET", 100, 0., 1000., 100, 0., 1000. ) ) );
      	ATH_CHECK( book( TH2D( histname("muon_pt_ip_jet_met"),   "Correlation plot of muon p_{T,IP} vs Jet MET", 100, 0., 1000., 100, 0., 1000. ) ) );
        hist(histname("elosstruth"))->Sumw2();
        hist(histname("eloss_fit_truth"))->Sumw2();
        hist(histname("elossfit"))->Sumw2();
        hist(histname("elossmeas"))->Sumw2();
        hist(histname("elossparam"))->Sumw2();
        hist(histname("elossfact"))->Sumw2();
        hist2d(histname("muonpt_met"))->Sumw2();
        hist2d(histname("muon_pt_ip_met"))->Sumw2();
        hist2d(histname("muon_pt_ip_jet_met"))->Sumw2();

      }
    }

    return StatusCode::SUCCESS;
  }

  //**********************************************************************

  StatusCode METTreeAlg::finalize() {
    ATH_MSG_INFO ("Finalizing " << name() << "...");
    return StatusCode::SUCCESS;
  }

  //**********************************************************************

  StatusCode METTreeAlg::execute() {
    ATH_MSG_DEBUG("Executing " << name() << "...");

    const xAOD::EventInfo* eventinfo = 0;
    ATH_CHECK( evtStore()->retrieve( eventinfo, "EventInfo" ) );

    bool isSim = eventinfo->eventType(xAOD::EventInfo::IS_SIMULATION);
    double weight=1;
    if(isSim) {
      double mcweight = eventinfo->mcEventWeight();
      float pileupweight(1.);
      if (m_doPileupReweighting) pileupweight = dec_pileupWeight(*eventinfo);

      ATH_MSG_VERBOSE("Pileup weight: " << pileupweight);
      //ATH_MSG_VERBOSE("Avg mu: "<<avgmu);

      weight = mcweight * pileupweight;
      // Muon scale factors
      const xAOD::MuonContainer *goodmu=0;
      CHECK(evtStore()->retrieve(goodmu, m_muoncoll));

      // Muon eff and iso scale factors
      float muonRecoEffSF = dec_muonRecoEffSF(*eventinfo);
      float muonIsoEffSF  = dec_muonIsoEffSF( *eventinfo);

      ATH_MSG_VERBOSE("Total muon reco scale factor: " << muonRecoEffSF);

      ATH_MSG_VERBOSE("Total muon iso scale factor: "  << muonIsoEffSF );
      weight *= muonRecoEffSF * muonIsoEffSF;

      // Electron efficiency scale factors
      float eleRecoEffSF = dec_eleRecoEffSF(*eventinfo);
      float eleIDEffSF   = dec_eleIDEffSF(  *eventinfo);
      float eleIsoEffSF  = dec_eleIsoEffSF( *eventinfo);

      ATH_MSG_VERBOSE("Total electron reco scale factor: " << eleRecoEffSF);
      ATH_MSG_VERBOSE("Total electron ID scale factor: "   << eleIDEffSF);
      ATH_MSG_VERBOSE("Total electron iso scale factor: "  << eleIsoEffSF);
      weight *= eleRecoEffSF * eleIDEffSF * eleIsoEffSF;
      // Trigger scale factors
      if (m_applyTriggerSF) {
        double triggerSF = dec_triggerSF(*eventinfo);
        ATH_MSG_VERBOSE("Applying trigger scale factor: " << triggerSF);
        weight *= triggerSF;
      }
      if (m_evttype == ttbar) {
        // If we are looking at ttbar events (which need a b-tag), apply the b-tagging SF.
  //      float btagSF = dec_btagEffSF(*eventinfo);
    //    ATH_MSG_DEBUG("Retrieved b-tagging scale factor: " << btagSF);
    //    weight *= btagSF;
      }
    } // isSim

    const VertexContainer* pvertices = 0;
    unsigned int npv = 0;
    ATH_CHECK( evtStore()->retrieve( pvertices, "PrimaryVertices" ) );
    for(const auto& vx : *pvertices) {
      // Count primary or pileup vertices
      if ((vx->vertexType() == 1) || (vx->vertexType() == 3)) ++npv;
    }

    //if (m_writetree) {
	//	m_npv = npv;
    //}
    if(!m_isPVAcc(*eventinfo)) {
      ATH_MSG_DEBUG("Event has failed primary vertex selection.");
      return StatusCode::SUCCESS;
    }

    if(!m_evtCleanAcc(*eventinfo) || !m_jetCleanAcc(*eventinfo)) {
      ATH_MSG_DEBUG("Event has failed cleaning.");
      return StatusCode::SUCCESS;
    }

    if(!m_passAllEvtSelections(*eventinfo)) {
      ATH_MSG_DEBUG("Event has failed event selections.");
      return StatusCode::SUCCESS;
    }

    switch(m_evttype) {
    case None: break;
    case Zmm:
      if(!m_isZmmAcc(*eventinfo)) return StatusCode::SUCCESS;
      break;
    case Zee:
      if(!m_isZeeAcc(*eventinfo)) return StatusCode::SUCCESS;
      break;
    case Wmv:
      if(!m_isWmvAcc(*eventinfo)) return StatusCode::SUCCESS;
      break;
    case Wev:
      if(!m_isWevAcc(*eventinfo)) return StatusCode::SUCCESS;
      break;
    case ttbar:
      if(!m_isttbarAcc(*eventinfo)) return StatusCode::SUCCESS;
      break;
    case gjet:
      if(!m_isgjetAcc(*eventinfo)) return StatusCode::SUCCESS;
      break;
    default:
      return StatusCode::SUCCESS;
    }

    // retrieve MET containers
    ATH_MSG_DEBUG("Retrieve MET " << m_metsuffix);
    const MissingETContainer* met = 0;
    ATH_CHECK( evtStore()->retrieve(met, "MET_"+m_metsuffix) );
    // Do jet veto according to CST SumET
    //const MissingETContainer* met_cst = 0;
    // ATH_CHECK( evtStore()->retrieve(met_cst, "MET_RefCST") );
    // //if(m_dojetveto && (*met_cst)[m_jetterm]->met()>1e-9) return StatusCode::SUCCESS;
    // if(m_dojetveto && (*met_cst)[m_jetterm]->sumet()>1e-9) return StatusCode::SUCCESS;

    // Old jet veto
    //if(m_dojetveto && (*met)[m_jetterm]->met()>1e-9) return StatusCode::SUCCESS;
    // New jet veto following Peter's suggestion, for no jets require also no gamma and tau term
    //if(m_dojetveto && ((*met)[m_jetterm]->met()>1e-9 || (*met)[m_gammaterm]->met()>1e-9 || (*met)[m_tauterm]->met()>1e-9)) return StatusCode::SUCCESS;
    // // Apply cuts on MET and mT using TST MET
    if ((m_evttype == Wev) || (m_evttype == Wmv)) {
//      const MissingETContainer* met_tst = 0;
//      ATH_CHECK( evtStore()->retrieve(met_tst, "MET_RefTST") );
//      const MissingET* pMET = (*met_tst)["Final"];
//
      // Apply cut on MET
      // // ATH_MSG_DEBUG("Applying MET cut.");
      // if (pMET->met() < m_wlnmetcut) {
      if ((*met)["Final"]->met() < m_wlnmetcut) {
        ATH_MSG_DEBUG("Event fails Wln MET cut.");
        return StatusCode::SUCCESS;
      }
      // Following Marianna's suggestion try run I cuts, a cut of  WmT < 50 GeV, MET < 25 GeV
      if (m_evttype == Wmv) {
        ATH_MSG_DEBUG("Applying mT cut (Wmv).");
        const MuonContainer* muon_fromW = 0;
        ATH_CHECK( evtStore()->retrieve(muon_fromW,"muonFromW") );
        const xAOD::Muon* pMu = muon_fromW->front();
        // Apply cut on mT
        IParticle::FourMom_t Wboson;
        IParticle::FourMom_t nu;
        nu.SetPtEtaPhiM( (*met)["Final"]->met(),0, (*met)["Final"]->phi(),0);
        Wboson=pMu->p4()+nu;
        double WmT=Wboson.Mt();
        //if (WmT<40e3) {
        if (WmT<50e3) {
          ATH_MSG_DEBUG("Event fails Wln mT cut.");
          return StatusCode::SUCCESS;
        }
      }
      else if (m_evttype == Wev) {
        ATH_MSG_DEBUG("Applying mT cut (Wev).");
        const ElectronContainer* ele_fromW = 0;
        ATH_CHECK( evtStore()->retrieve(ele_fromW,"eleFromW") );
        const xAOD::Electron* pEl = ele_fromW->front();
        // Apply cut on mT
        IParticle::FourMom_t Wboson;
        IParticle::FourMom_t nu;
        nu.SetPtEtaPhiM( (*met)["Final"]->met(),0, (*met)["Final"]->phi(),0);
        Wboson=pEl->p4()+nu;
        double WmT=Wboson.Mt();
        //if (WmT<40e3) {
        if (WmT<50e3) {
          ATH_MSG_DEBUG("Event fails Wln mT cut.");
          return StatusCode::SUCCESS;
        }
      }

    }

    //retrieve the PRW corrected <mu> or the uncorrected one. NB: If slimmed xAOD was produced enabling PRW, you can remove its effect by disabling PRW at plotting stage
    float avgmu = dec_avgMu(*eventinfo);
    if(!(m_doPileupReweighting))
       avgmu = dec_avgMunocorrection(*eventinfo);

    // Fill histograms of mu distributions after event selection
    //hist(histname("mu_distribution")  )->Fill(avgmu); // unweighted
    //hist(histname("mu_distribution_w"))->Fill(avgmu, weight); // weighted

    //ATH_MSG_DEBUG("Retrieve LocHadTopo");
    //const MissingETContainer* lht = 0;
    //ATH_CHECK( evtStore()->retrieve(lht, "MET_LocHadTopo") );

    //// use LocHadTopo + Muon sumet as common sumet reference
    //double lht_sumet = (*lht)["LocHadTopo"]->sumet() + (*met)["Muons"]->sumet();

    //ATH_MSG_DEBUG("LHT+mu sumet = " << lht_sumet);

    // get some MET objects for convenience
    MissingETContainer* metperf = new MissingETContainer();
    MissingETAuxContainer* metperfaux = new MissingETAuxContainer();
    metperf->setStore(metperfaux);
    ATH_CHECK( evtStore()->record(metperf,"MET_"+name()) );
    ATH_CHECK( evtStore()->record(metperfaux,"MET_"+name()+"Aux.") );

    MissingET* met_final = new MissingET();
    metperf->push_back(met_final);
    *met_final = *(*met)["Final"];

    MissingET* met_jetterm = new MissingET();
    metperf->push_back(met_jetterm);
    *met_jetterm = *(*met)[m_jetterm];

    MissingET* met_soft  = new MissingET();
    metperf->push_back(met_soft);
    *met_soft = *(*met)[m_softterm];

    MissingET* met_hard  = new MissingET();
    metperf->push_back(met_hard);
    *met_hard = *met_final - *met_soft;
    met_hard->setName("Hard");

    ATH_MSG_DEBUG("Retrieve Truth");
    const MissingETContainer* truth = 0;
    MissingET* met_truth = 0;
    if(isSim) {
      ATH_CHECK( evtStore()->retrieve(truth, "MET_Truth") );
      met_truth = new MissingET();
      metperf->push_back(met_truth);
      *met_truth = (*(*truth)["NonInt"]);
      met_truth->setName("Truth");
    } else {
      met_truth = new MissingET(0.,0.,0.);
      metperf->push_back(met_truth);
      met_truth->setName("Truth");
    }

    MissingET* met_resid = new MissingET();
    double abs_met_resid = 0;
    metperf->push_back(met_resid);
    if ((m_evttype == Wev) || (m_evttype == Wmv) || (m_evttype == ttbar)) {
      *met_resid = *met_final - *met_truth;
      abs_met_resid = fabs(met_final->met()) - fabs(met_truth->met());
    }
    else{
      *met_resid = *met_final;
      abs_met_resid = fabs(met_final->met());
    }
    met_resid->setName("Resid");
    //histogram for AtlasDerivation validation
    //hist(histname("Reco_NonIntMET")  )->Fill(met_resid->met()/GeV,weight);
    //hist(histname("absReco_NonIntMET")  )->Fill(abs_met_resid/GeV,weight);
    ATH_MSG_VERBOSE("Absolute value of MET residual = "<<abs_met_resid);

    MissingET* pthard  = new MissingET();
    metperf->push_back(pthard);
    // noTruthPthard is used for closer comparison with data.
    if (isSim && !m_noTruthPthard) {
      *pthard = *met_truth - *met_hard;
    }
    else { // This is data, or noTruthPthard flag is set.
      MissingET zero_vector(0.,0.,0.);
      *pthard = zero_vector - *met_hard;
    }
    pthard->setName("PtHard");

    MissingET* proj_pthard = new MissingET();
    metperf->push_back(proj_pthard);
    *proj_pthard = projectMET(*met_resid, *pthard);
    proj_pthard->setName("Proj_PtHard");

    // MET soft term projected onto pthard
    MissingET* proj_soft_pthard = new MissingET();
    metperf->push_back(proj_soft_pthard);
    *proj_soft_pthard = projectMET(*met_soft, *pthard);
    proj_soft_pthard->setName("ProjSoft_PtHard");

    ATH_MSG_DEBUG("Final event weight just before plotting: " << weight);
    //retrieve primary vertex of the event
    const VertexContainer* vert = 0;
    xAOD::Vertex *primvtx(0);
    ATH_CHECK( evtStore()->retrieve( vert, "PrimaryVertices" ) );
    for(const auto& vx : *pvertices) {
      //if type is 1, then is primary
      if( vx->vertexType() == 1 ) {
        primvtx=vx;
        break;
      }
    }

    const xAOD::TrackParticleContainer* importedTrackParticles;
    ATH_CHECK( evtStore()->retrieve(importedTrackParticles, "InDetTrackParticles" ) );
    // Check the event contains tracks
    unsigned int nTracks = importedTrackParticles->size();
    for ( unsigned int i=0; i<importedTrackParticles->size(); ++i){
        float Dz=fabs( importedTrackParticles->at(i)->z0() + importedTrackParticles->at(i)->vz() - primvtx->z() );
        float sintheta=sin( importedTrackParticles->at(i)->theta() );
        float d0=importedTrackParticles->at(i)->d0();
        float errd0=sqrt( importedTrackParticles->at(i)->definingParametersCovMatrix()(0,0) );
        if (m_writetree) {
           Dz=fabs( importedTrackParticles->at(i)->z0() + importedTrackParticles->at(i)->vz() - primvtx->z() );
           sintheta=sin( importedTrackParticles->at(i)->theta() );
           d0=importedTrackParticles->at(i)->d0();
           errd0=sqrt( importedTrackParticles->at(i)->definingParametersCovMatrix()(0,0) );
           m_d0 =  d0;
           m_z0sinT = Dz*sintheta;
           m_d0sig = d0/errd0;
           m_Ntrack = nTracks;
        }
    }

    //ATH_CHECK( plotBasics(metperf, weight, avgmu) );
    //ATH_CHECK( plotTerms(met, weight, avgmu) );
    ////ATH_CHECK( plotPerformance(metperf, lht_sumet, avgmu, npv, weight) );
    //ATH_CHECK( plotPerformance(metperf, avgmu, npv, weight) );

    if( m_evttype==Zmm || m_evttype==Zee ) {
      IParticle::FourMom_t Zboson;
      IParticle::FourMom_t leadingLep;
      IParticle::FourMom_t subleadingLep;
      if(m_evttype==Zmm) {
        const MuonContainer* mupair = 0;
        ATH_CHECK( evtStore()->retrieve(mupair, "BestZmm") );
        Zboson = (*mupair)[0]->p4() + (*mupair)[1]->p4();
        if( (*mupair)[0]->pt()>(*mupair)[1]->pt() ){
       		leadingLep = (*mupair)[0]->p4();
            subleadingLep =  (*mupair)[1]->p4();
        }
        else{
          leadingLep = (*mupair)[1]->p4() ;
          subleadingLep =  (*mupair)[0]->p4();	/// need to be corrected!!!! redo the plots!
        }
      } else { // m_evttype==Zee
        const ElectronContainer* elpair = 0;
        ATH_CHECK( evtStore()->retrieve(elpair, "BestZee") );
        Zboson = (*elpair)[0]->p4() + (*elpair)[1]->p4();
        if( (*elpair)[0]->pt()>(*elpair)[1]->pt() ){
          leadingLep = (*elpair)[0]->p4() ;
          subleadingLep =  (*elpair)[1]->p4();
        }
        else{
          leadingLep = (*elpair)[1]->p4() ;
          subleadingLep =  (*elpair)[0]->p4();
        }
      }
      //hist(histname("mZ"))->Fill(Zboson.M()/GeV, weight);
      //hist(histname("ptZ"))->Fill(Zboson.Pt()/GeV, weight);
      //hist(histname("pthard"))->Fill(met_hard->met()/GeV, weight);
      //hist(histname("leadingLep_pt"))->Fill(leadingLep.Pt()/GeV, weight);
      //hist(histname("subleadingLep_pt"))->Fill(subleadingLep.Pt()/GeV, weight);
      // MET constructor assumes you're adding a particle
      MissingET* ptZ = new MissingET(Zboson.Px(),Zboson.Py(),0.);
      ptZ->setName("PtZ");
      metperf->push_back(ptZ);
      MissingET* proj_ptZ = new MissingET();
      metperf->push_back(proj_ptZ);
      *proj_ptZ = projectMET(*met_resid,*ptZ);
      proj_ptZ->setName("Proj_PtZ");
      if (m_writetree) {
        m_mZ = Zboson.M()/GeV;
      	ATH_CHECK( setTreeZ(metperf, weight) );
      }
      //ATH_CHECK( plotPerfZ(metperf, weight) );
    }

    if (m_writetree) {
        m_avgmu = avgmu;
        m_npv = npv;
        int njets = 0;
        if (!m_jetBins) { // Count good jets
          const JetContainer* jets(0);
          ATH_CHECK( evtStore()->retrieve(jets,m_jetcoll) );
          // Number of jets with pT > 20 GeV and passing overlap removal.
          for(const auto& j : *jets) {
            if ((j->pt() > 20000) && (dec_passOR(*j))) njets++;
          }
        }
        m_njets = njets;
    	//ATH_CHECK( fillTree(metperf, weight) );
    	ATH_CHECK( fillTree(met, weight) );
    }

    if( m_evttype==Wmv || m_evttype==Wev || m_evttype==ttbar ) {
      IParticle::FourMom_t Wboson;//marta
      IParticle::FourMom_t nu;
      nu.SetPtEtaPhiM(met_truth->met(),0, met_truth->phi(),0);
      double WmT = 0;
      if(m_evttype==Wmv) {
        const MuonContainer* muons = 0;
        ATH_CHECK( evtStore()->retrieve(muons,"muonFromW") );//m_muoncoll
        const xAOD::Muon* pMu = muons->front();
        //WmT = sqrt(pMu->pt()*met_final->met()*(1-cos(pMu->phi()-met_final->phi())));
        Wboson=pMu->p4()+nu;
        WmT = Wboson.Mt();
      }
      if(m_evttype==Wev) {
        const ElectronContainer* electrons = 0;
        ATH_CHECK( evtStore()->retrieve(electrons,"eleFromW") );//m_elecoll
        const xAOD::Electron* pEl = electrons->front();
        //WmT = sqrt(pEl->pt()*met_final->met()*(1-cos(pEl->phi()-met_final->phi())));
        Wboson=pEl->p4()+nu;
        WmT = Wboson.Mt();
      }
      ATH_MSG_VERBOSE("Reco W mass = "<<WmT);
      //hist(histname("WmT"))->Fill(WmT/GeV, weight);
      //hist2d(histname("lin_truth"))->Fill(met_truth->met()/GeV, met_final->met()/met_truth->met()-1, weight);
      MissingET* proj_truth = new MissingET();
      metperf->push_back(proj_truth);
      *proj_truth = projectMET(*met_resid,*met_truth);
      proj_truth->setName("Proj_Truth");
      //hist2d(histname("residL_truth"))->Fill(met_truth->met()/GeV, proj_truth->mpx()/GeV, weight);
      //hist2d(histname("residxy_truth"))->Fill(met_truth->met()/GeV, met_resid->mpx()/GeV, weight);
      //hist2d(histname("residxy_truth"))->Fill(met_truth->met()/GeV, met_resid->mpy()/GeV, weight);
      //hist2d(histname("residT_truth"))->Fill(met_truth->met()/GeV, proj_truth->mpy()/GeV, weight);
      //hist2d(histname("biasL_truth"))->Fill(met_truth->met()/GeV,  proj_truth->mpx()/met_truth->met(), weight);
      //hist2d(histname("reco_vs_truth"))->Fill(met_truth->met()/GeV,  met_final->met()/GeV, weight);
      //hist2d(histname("residPhi_truth"))->Fill(met_truth->met()/GeV, TVector2::Phi_mpi_pi(met_final->phi()-met_truth->phi()), weight);
    }

    const JetContainer* jets(0);
    ATH_CHECK( evtStore()->retrieve(jets,m_jetcoll) );
    if(isSim)
    {
      const JetContainer* truthjets(0);
      ATH_CHECK( evtStore()->retrieve(truthjets,"AntiKt4TruthJets") );
      double recojets = 0.;
      double recojets_central = 0.;
      double recojets_forward = 0.;
      for(const auto& j : *jets) {
        if (!dec_passOR(*j)) continue;
        double dRmin=99.;
        double truthPt = 0.;
        double recoPt = j->pt();
        for(const auto& tj : *truthjets) {
          double dR = j->p4().DeltaR(tj->p4());
          if(dR < dRmin) {dRmin = dR; truthPt = tj->pt();}
        }
        ATH_MSG_VERBOSE("Best truth jet dR = " << dRmin << ", pt ratio = " << recoPt/truthPt);
        //if(dRmin<0.1) { hist2d(histname("jetresponse"))->Fill(truthPt/1e3, recoPt/truthPt, weight);}
        if(dRmin<0.3 && truthPt > 10e3) {
		recojets += recoPt;
		if(TMath::Abs( j->eta() ) < 2.5){
			recojets_central += recoPt;
		}
		else{
			recojets_forward += recoPt;
		}
	}
      }
      //hist(histname("jetrecosums"))->Fill(recojets/GeV, weight);
      //hist2d(histname("forw_cent_truth"))->Fill(recojets_forward/GeV,recojets_central/GeV, weight);
    }

    //const JetContainer* jetcheck = 0;
    //ATH_CHECK( evtStore()->retrieve(jetcheck, m_jetcoll) );
    //hist(histname("jetsizeHisto"))->Fill(jetcheck->size(), weight);
    //ATH_MSG_VERBOSE( "In GoodJets there are N jets =  " << jetcheck->size()  );


   //for AtlasDerivation Validation:
   double lowjetterm=0;//what if the jet term is reconstructed only from low jet pt events?
   bool islow=true;
   const JetContainer* jetsDer(0);
   ATH_CHECK( evtStore()->retrieve(jetsDer,m_jetcoll) );
   double leading_Jet = 0.;
     for(const auto& j : *jetsDer) {
        if (j->pt() > leading_Jet)
               leading_Jet = j->pt();

           //for AtlasDerivation Validation:
           if(islow){
              if(j->pt()<(200*GeV))
                 lowjetterm+=j->pt();
              else
                 islow=false;
           }

     }
        //for AtlasDerivation Validation:
        //if(islow)
            //hist(histname("lowjetpt_distribution"))->Fill(lowjetterm/GeV,weight);
            //hist(histname("jethard_distribution"))->Fill(leading_Jet/GeV,weight);


    const JetContainer* jetnumtemp = 0;
    ATH_CHECK( evtStore()->retrieve(jetnumtemp, m_jetcoll) );
    int jet25=0, jet40=0, jet60=0;
    for(const auto& j : *jetnumtemp) {
       if( j->pt()>(25*GeV) ){
           ++jet25;
           if( j->pt()>(40*GeV) ){
               ++jet40;
               if( j->pt()>(60*GeV) )
                   ++jet60;
           }
       }
    }
    //hist(histname("jetsize60"))->Fill(jet60, weight);
    //hist(histname("jetsize40"))->Fill(jet40, weight);
    //hist(histname("jetsize25"))->Fill(jet25, weight);
    ////the following 3 distributions makes sense if there is 1 jet at least
    //if(jetnumtemp->size()!=0){
    //   hist(histname("jetsize60_norm"))->Fill((float)jet60/jetnumtemp->size(), weight);
    //   hist(histname("jetsize40_norm"))->Fill((float)jet40/jetnumtemp->size(), weight);
    //   hist(histname("jetsize25_norm"))->Fill((float)jet25/jetnumtemp->size(), weight);
    //}
    //retrieve primary vertex of the event
    //const VertexContainer* vert = 0;
    //xAOD::Vertex *primvtx;
    //ATH_CHECK( evtStore()->retrieve( vert, "PrimaryVertices" ) );
    //for(const auto& vx : *pvertices) {
    //  //if type is 1, then is primary
    //  if( vx->vertexType() == 1 ) {
    //    primvtx=vx;
    //    break;
    //  }
    //}
    //  const xAOD::TrackParticleContainer* importedTrackParticles;
    //  ATH_CHECK( evtStore()->retrieve(importedTrackParticles, "InDetTrackParticles" ) );
	//
    //// Check the event contains tracks
    //unsigned int nTracks = importedTrackParticles->size();
    //     hist(histname("n_track"))->Fill(nTracks,weight);
    //for ( int i=0; i<importedTrackParticles->size(); ++i){
    //    float Dz=fabs( importedTrackParticles->at(i)->z0() + importedTrackParticles->at(i)->vz() - primvtx->z() );
    //    float sintheta=sin( importedTrackParticles->at(i)->theta() );
    //    float d0=importedTrackParticles->at(i)->d0();
    //    float errd0=sqrt( importedTrackParticles->at(i)->definingParametersCovMatrix()(0,0) );
    //    hist(histname("track_d0"))->Fill( d0,weight);
    //    hist(histname("track_Dz0sin"))->Fill( fabs( Dz*sintheta ),weight);
    //    hist(histname("sig_track_d0"))->Fill( d0/errd0,weight);
    //}

/*
    //number of events
    const xAOD::EventInfo* infoevent = 0;
    ATH_CHECK( evtStore()->retrieve( infoevent, "EventInfo" ) );
    unsigned int number_of_event=infoevent->eventNumber();
    ATH_MSG_INFO("Event # " << number_of_event);
    //         hist(histname("n_Event"))->Fill(number_of_event,number_of_event);
*/

    if(m_doMuonElossPlots){

	//const xAOD::TruthParticleContainer* TruthMuons = evtStore()->tryRetrieve< xAOD::TruthParticleContainer >("MuonTruthParticles");
	//if (!TruthMuons) {
	//	ATH_MSG_VERBOSE ("Couldn't retrieve TruthMuons container with key: " << "MuonTruthParticles");
     	//	return StatusCode::SUCCESS;
   	//}

	//typedef ElementLink< xAOD::MuonContainer > MuonLink;
   	//ATH_MSG_VERBOSE("Retrieved truth muons " << TruthMuons->size());

	//float px,py,pz = 0;
	//float Px,Py,Pz = 0;
	//float trutheloss = 0;

        //for (const auto truthMu: *TruthMuons){
	//	MuonLink link;

	//	if( truthMu->isAvailable< MuonLink >("recoMuonLink") ) link = truthMu->auxdata< MuonLink >("recoMuonLink");

	//	if( link.isValid() ){

	//		 const xAOD::TrackParticle* tp  = (*link)->primaryTrackParticle();
        //		 const xAOD::Muon* muon = truthMu;

	//	if(tp) {
	//
	//		px = (*muon)->auxdata<float>("MuonEntryLayer_px");
	//		py = (*muon)->auxdata<float>("MuonEntryLayer_py");
	//		pz = (*muon)->auxdata<float>("MuonEntryLayer_pz");
	////		Px = (*muon)->auxdata<float>("CaloEntryLayer_px");
	////		Py = (*muon)->auxdata<float>("CaloEntryLayer_py");
	////		Pz = (*muon)->auxdata<float>("CaloEntryLayer_pz");
	//		Px = (*muon)->auxdata<float>("px");
	//		Py = (*muon)->auxdata<float>("py");
	//		Pz = (*muon)->auxdata<float>("pz");
	//		trutheloss = TMath::Abs(TMath::Sqrt(px*px+py*py+pz*pz) - TMath::Sqrt(Px*Px+Py*Py+Pz*Pz));
        //		float eloss = (*link)->floatParameter(xAOD::Muon::EnergyLoss);
        //		unsigned char ElossType = (*link)->auxdata<unsigned char>("energyLossType");
        //		if((unsigned int)ElossType == 3){ // ElossType=3 is TAIL,1 - Not Isolated, 2 - MOP, 0 - Parametrized
        //			ATH_MSG_DEBUG ("You have selected elossType" << (unsigned int)ElossType << "muons" );
        //		}
	//		hist(histname("elosstruth"))->Fill(trutheloss/GeV,weight);
	//		hist(histname("eloss_fit_truth"))->Fill((eloss-trutheloss)/GeV,weight);
	//	}

	//}


      const xAOD::MissingETAssociationMap *metMap=0;
      ATH_CHECK(evtStore()->retrieve(metMap, "METAssoc_AntiKt4EMTopo"));
      metMap->resetObjSelectionFlags();
      const xAOD::MuonContainer *goodmuons=0;
      ATH_CHECK(evtStore()->retrieve(goodmuons, "GoodMuons"));
      if (!goodmuons) {
        ATH_MSG_ERROR ("Couldn't retrieve Muons container with key: " << "GoodMuons");
        return StatusCode::FAILURE;
      }
      ATH_MSG_VERBOSE("Retrieved muons " << goodmuons->size());

      bool originalInputs = !goodmuons->front()->isAvailable<ElementLink<IParticleContainer> >("originalObjectLink");
      float muonpx(0.), muonpy(0.), muonpt(0.);
      float muon_px(0.), muon_py(0.), muon_pt_ip(0.);
      for(const auto& iMu : *goodmuons) {
        const IParticle* orig = iMu;
        if(!originalInputs) { orig = *m_objLinkAcc(*iMu); }
        const xAOD::Muon* muon = iMu;

        bool isolated = MissingETComposition::getRefJet(metMap, orig)==0; // use flag to test if muons is isolated from jet

        float px,py,pz = 0;
        float Px,Py,Pz = 0;
        float trutheloss = 0;

        float eloss = muon->floatParameter(xAOD::Muon::EnergyLoss);
        float elossMeas = muon->floatParameter(xAOD::Muon::MeasEnergyLoss);
        float elossParam = muon->floatParameter(xAOD::Muon::ParamEnergyLoss);
        float eloss_corr = 1. - eloss / muon->e();

        ElementLink< xAOD::TruthParticleContainer > truthLink;

        if( muon->isAvailable< ElementLink< xAOD::TruthParticleContainer > > ("truthParticleLink") ) truthLink = muon->auxdata< ElementLink< xAOD::TruthParticleContainer >  >("truthParticleLink");


        if(truthLink.isValid()) {
          px = (*truthLink)->auxdata<float>("MuonEntryLayer_px");
          py = (*truthLink)->auxdata<float>("MuonEntryLayer_py");
          pz = (*truthLink)->auxdata<float>("MuonEntryLayer_pz");
          //Px = (*truthLink)->auxdata<float>("CaloEntryLayer_px");
          //Py = (*truthLink)->auxdata<float>("CaloEntryLayer_py");
          //Pz = (*truthLink)->auxdata<float>("CaloEntryLayer_pz");
          Px = (*truthLink)->p4().Px();
          Py = (*truthLink)->p4().Py();
          Pz = (*truthLink)->p4().Pz();
          trutheloss = TMath::Abs(TMath::Sqrt(px*px+py*py+pz*pz) - TMath::Sqrt(Px*Px+Py*Py+Pz*Pz));
          //		unsigned char ElossType = (*muon)->auxdata<unsigned char>("energyLossType");
          //		if((unsigned int)ElossType == 3){ // ElossType=3 is TAIL,1 - Not Isolated, 2 - MOP, 0 - Parametrized
          //			ATH_MSG_DEBUG ("You have selected elossType" << (unsigned int)ElossType << "muons" );
          //		}
          hist(histname("elosstruth"))->Fill(trutheloss/GeV,weight);
          hist(histname("eloss_fit_truth"))->Fill((eloss-trutheloss)/GeV,weight);
        }

        if( eloss>muon->e()){
          ATH_MSG_WARNING("Muon Eloss " << eloss << " > muon E " << muon->e() << "!");
        }
        else{
          if (isolated){
            hist(histname("elossfit"))->Fill(eloss/GeV,weight);
            hist(histname("elossfact"))->Fill(eloss_corr,weight);
            hist(histname("elossmeas"))->Fill(elossMeas/GeV,weight);
            hist(histname("elossparam"))->Fill(elossParam/GeV,weight);
            muonpx += muon->p4().Px();
            muonpy += muon->p4().Py();
            muon_px += eloss_corr*muon->p4().Px();
            muon_py += eloss_corr*muon->p4().Py();
            const JetContainer* jets(0);
            ATH_CHECK( evtStore()->retrieve(jets,m_jetcoll) );
            //In METPerformance 2.X was different, check if needed!
            //ATH_CHECK( evtStore()->retrieve(jets,m_jetcoll_ovRem) );//before was m_jetcoll
            double leading_recoJetPt = 0.;
            for(const auto& j : *jets) {
              if (!dec_passOR(*j)) continue;
              if (j->pt() > leading_recoJetPt) {
                leading_recoJetPt = j->pt();
              }
            }

            hist2d(histname("jetpt_eloss"))->Fill(leading_recoJetPt/GeV,eloss/GeV,weight);
          }
        }

      }
      muonpt = TMath::Sqrt(muonpx*muonpx + muonpy*muonpy);
      muon_pt_ip = TMath::Sqrt(muon_px*muon_px + muon_py*muon_py);
      hist2d(histname("muonpt_met"))->Fill(muonpt/GeV,(*met)["Final"]->met()/GeV,weight);
      hist2d(histname("muon_pt_ip_met"))->Fill(muon_pt_ip/GeV,(*met)["Final"]->met()/GeV,weight);
      hist2d(histname("muon_pt_ip_jet_met"))->Fill(muon_pt_ip/GeV,(*met)[m_jetterm]->met()/GeV,weight);

    }

    return StatusCode::SUCCESS;
  }

  StatusCode METTreeAlg::plotBasics(const MissingETContainer* metperf, double weight, double avgmu) {
    ATH_MSG_DEBUG(name() << " -- plotting basic MET plots");
    const MissingET* met = (*metperf)["Final"];
    const MissingET* proj = (*metperf)["Proj_PtHard"];
    const MissingET* pthard = (*metperf)["PtHard"];
    const MissingET* jetterm = (*metperf)[m_jetterm];
    if(!met) {
      ATH_MSG_ERROR("Didn't find total MET");
      return StatusCode::FAILURE;
    }
    if(!proj) {
      ATH_MSG_ERROR("Didn't find MET projected on pthard");
      return StatusCode::FAILURE;
    }
    if(!pthard) {
      ATH_MSG_ERROR("Didn't find pthard");
      return StatusCode::FAILURE;
    }
    if(!jetterm) {
      ATH_MSG_ERROR("Didn't find jet term");
      return StatusCode::FAILURE;
    }
    //
    hist(histname("met"))->Fill(met->met()/GeV, weight);
    hist(histname("phi"))->Fill(met->phi(), weight);
    hist(histname("mpx"))->Fill(met->mpx()/GeV, weight);
    hist(histname("mpy"))->Fill(met->mpy()/GeV, weight);
    hist(histname("sumet"))->Fill(met->sumet()/GeV, weight);
    //
    hist(histname("sig"))->Fill(met->met()/TMath::Sqrt(met->sumet() * GeV), weight);
    hist(histname("sigHT"))->Fill(met->met()/TMath::Sqrt(jetterm->sumet() * GeV),weight);
    //
    hist(histname("pThard_distribution"))->Fill(pthard->met()/GeV,weight);
    //
    hist(histname("pL_pthard"))->Fill(proj->mpx()/GeV,weight);
    hist(histname("pT_pthard"))->Fill(proj->mpy()/GeV,weight);

    ATH_MSG_DEBUG( "MET hist now with " << hist(histname("met"))->GetEntries() << " entries" );

    // Fill histos in different jet multiplicity bins
    Int_t njets(0);
    if (m_jetBins) {
      const JetContainer* jets(0);
      ATH_CHECK( evtStore()->retrieve(jets,m_jetcoll) );
      // Number of jets with pT > 20 GeV and passing overlap removal.
      for(const auto& j : *jets) {
        if ((j->pt() > 20000) && (dec_passOR(*j))) njets++;
      }
      std::string jetlabel;
      switch (njets) {
        case 0: jetlabel = "0"; break;
        case 1: jetlabel = "1"; break;
        default: jetlabel = "2";
      }
      hist(histname("met_"+jetlabel+"j"))->Fill(met->met()/GeV, weight);
    }

    // Fill histos in different mu regions
    Int_t muBin(0);
    std::string muRegionLabel;

    if (avgmu <10)               muBin = 0;
    if (avgmu >=10 && avgmu<15 ) muBin = 1;
    if (avgmu >=15 && avgmu<20 ) muBin = 2;
    if (avgmu >=20 )             muBin = 3;

    switch (muBin) {
      case 0: muRegionLabel = "_mu0_10"; break;
      case 1: muRegionLabel = "_mu10_15"; break;
      case 2: muRegionLabel = "_mu15_20"; break;
      case 3: muRegionLabel = "_HighMu"; break;
    }

    hist(histname("met"+muRegionLabel))->Fill(met->met()/GeV, weight);
    hist(histname("sumet"+muRegionLabel))->Fill(met->sumet()/GeV, weight);
    hist(histname("pThard_distribution"+muRegionLabel))->Fill(pthard->met()/GeV,weight);

    return StatusCode::SUCCESS;
  }

  StatusCode METTreeAlg::plotTerms(const MissingETContainer* met, double weight, double avgmu) {
    const MissingET* refjet = (*met)[m_jetterm];
    hist(histname("jet_met"))->Fill(refjet->met()/GeV,weight);
    hist(histname("jet_phi"))->Fill(refjet->phi(),weight);
    hist(histname("jet_sumet"))->Fill(refjet->sumet()/GeV,weight);
    hist(histname("jet_mpx"))->Fill(refjet->mpx()/GeV,weight);
    hist(histname("jet_mpy"))->Fill(refjet->mpy()/GeV,weight);
    //
    const MissingET* muons = (*met)[m_muonterm];
    hist(histname("muon_met"))->Fill(muons->met()/GeV,weight);
    hist(histname("muon_phi"))->Fill(muons->phi(),weight);
    hist(histname("muon_sumet"))->Fill(muons->sumet()/GeV,weight);
    hist(histname("muon_mpx"))->Fill(muons->mpx()/GeV,weight);
    hist(histname("muon_mpy"))->Fill(muons->mpy()/GeV,weight);
    //
    const MissingET* refele = (*met)[m_eleterm];
    hist(histname("ele_met"))->Fill(refele->met()/GeV,weight);
    hist(histname("ele_phi"))->Fill(refele->phi(),weight);
    hist(histname("ele_sumet"))->Fill(refele->sumet()/GeV,weight);
    hist(histname("ele_mpx"))->Fill(refele->mpx()/GeV,weight);
    hist(histname("ele_mpy"))->Fill(refele->mpy()/GeV,weight);
    //
    const MissingET* softterm = (*met)[m_softterm];
    hist(histname("soft_met"))->Fill(softterm->met()/GeV,weight);
    hist(histname("soft_phi"))->Fill(softterm->phi(),weight);
    hist(histname("soft_sumet"))->Fill(softterm->sumet()/GeV,weight);
    hist(histname("soft_mpx"))->Fill(softterm->mpx()/GeV,weight);
    hist(histname("soft_mpy"))->Fill(softterm->mpy()/GeV,weight);

    if (m_doTauGammaPlots) {
      const MissingET* gammaterm = (*met)[m_gammaterm];
      hist(histname("photon_met"))->Fill(  gammaterm->met()/GeV,weight);
      hist(histname("photon_phi"))->Fill(  gammaterm->phi(),weight);
      hist(histname("photon_sumet"))->Fill(gammaterm->sumet()/GeV,weight);
      hist(histname("photon_mpx"))->Fill(  gammaterm->mpx()/GeV,weight);
      hist(histname("photon_mpy"))->Fill(  gammaterm->mpy()/GeV,weight);

      const MissingET* tauterm = (*met)[m_tauterm];
      hist(histname("tau_met"))->Fill(  tauterm->met()/GeV,weight);
      hist(histname("tau_phi"))->Fill(  tauterm->phi(),weight);
      hist(histname("tau_sumet"))->Fill(tauterm->sumet()/GeV,weight);
      hist(histname("tau_mpx"))->Fill(  tauterm->mpx()/GeV,weight);
      hist(histname("tau_mpy"))->Fill(  tauterm->mpy()/GeV,weight);
    }

    // Fill histos in different jet multiplicity bins
    Int_t njets(0);
    if (m_jetBins) {
      const JetContainer* jets(0);
      ATH_CHECK( evtStore()->retrieve(jets,m_jetcoll) );
      // Number of jets with pT > 20 GeV and passing overlap removal.
      for(const auto& j : *jets) {
        if ((j->pt() > 20000) && (dec_passOR(*j))) njets++;
      }
      std::string jetlabel;
      switch (njets) {
        case 0: jetlabel = "0"; break;
        case 1: jetlabel = "1"; break;
        default: jetlabel = "2";
      }
      hist(histname("jet_met_"+jetlabel+"j"))->Fill(refjet->met()/GeV, weight);
      hist(histname("muon_met_"+jetlabel+"j"))->Fill(muons->met()/GeV, weight);
      hist(histname("ele_met_"+jetlabel+"j"))->Fill(refele->met()/GeV, weight);
      hist(histname("soft_met_"+jetlabel+"j"))->Fill(softterm->met()/GeV, weight);
    }

    // Fill histos in different mu regions
    Int_t muBin(0);
    std::string muRegionLabel;

    if (avgmu <10)               muBin = 0;
    if (avgmu >=10 && avgmu<15 ) muBin = 1;
    if (avgmu >=15 && avgmu<20 ) muBin = 2;
    if (avgmu >=20 )             muBin = 3;

    switch (muBin) {
      case 0: muRegionLabel = "_mu0_10"; break;
      case 1: muRegionLabel = "_mu10_15"; break;
      case 2: muRegionLabel = "_mu15_20"; break;
      case 3: muRegionLabel = "_HighMu"; break;
    }

    hist(histname("soft_met"+muRegionLabel))->Fill(softterm->met()/GeV,weight);
    hist(histname("jet_met"+muRegionLabel))->Fill(refjet->met()/GeV,weight);
    hist(histname("ele_met"+muRegionLabel))->Fill(refele->met()/GeV,weight);
    hist(histname("muon_met"+muRegionLabel))->Fill(muons->met()/GeV,weight);

    return StatusCode::SUCCESS;
  }

  //StatusCode METPerfAlg::plotPerformance(const MissingETContainer* metperf, double lht_sumet, double avgmu, unsigned int npv, double weight) {
  StatusCode METTreeAlg::plotPerformance(const MissingETContainer* metperf, double avgmu, unsigned int npv, double weight) {
    ATH_MSG_DEBUG(name() << " -- plotting MET performance plots");
    const MissingET* met = (*metperf)["Final"];
    const MissingET* resid = (*metperf)["Resid"];
    const MissingET* pthard = (*metperf)["PtHard"];
    const MissingET* proj = (*metperf)["Proj_PtHard"];
    const MissingET* proj_soft = (*metperf)["ProjSoft_PtHard"];

    const MissingETContainer* tst_met = 0;
    ATH_CHECK( evtStore()->retrieve(tst_met, "MET_RefTST") );
    double tst_sumet = (*tst_met)["Final"]->sumet();

    const MissingETContainer* cst_met = 0;
    ATH_CHECK( evtStore()->retrieve(cst_met, "MET_RefCST") );
    double cst_sumet = (*cst_met)["Final"]->sumet();


    if(!met) {
      ATH_MSG_ERROR("Didn't find total MET");
      return StatusCode::FAILURE;
    }
    if(!resid) {
      ATH_MSG_ERROR("Didn't find MET residuals");
      return StatusCode::FAILURE;
    }
    if(!pthard) {
      ATH_MSG_ERROR("Didn't find PtHard");
      return StatusCode::FAILURE;
    }
    if(!proj) {
      ATH_MSG_ERROR("Didn't find MET projected on pthard");
      return StatusCode::FAILURE;
    }
    if(!proj_soft) {
      ATH_MSG_ERROR("Didn't find MET soft term projected on pthard");
      return StatusCode::FAILURE;
    }
    //
    hist2d(histname("residxy_mu"))->Fill(avgmu, resid->mpx()/GeV, weight);
    hist2d(histname("residxy_mu"))->Fill(avgmu, resid->mpy()/GeV, weight);
    //
    hist2d(histname("residL_mu"))->Fill(avgmu, proj->mpx()/GeV, weight);
    hist2d(histname("residT_mu"))->Fill(avgmu, proj->mpy()/GeV, weight);
    //
    hist2d(histname("residxy_npv"))->Fill(npv, resid->mpx()/GeV, weight);
    hist2d(histname("residxy_npv"))->Fill(npv, resid->mpy()/GeV, weight);
    //
    hist2d(histname("residL_npv"))->Fill(npv, proj->mpx()/GeV, weight);
    hist2d(histname("residT_npv"))->Fill(npv, proj->mpy()/GeV, weight);
    //
    //AtlasDerivationValidation
    hist(histname("npv_distribution"))->Fill(npv,weight);
    //
    hist2d(histname("residxy_sumet"))->Fill(met->sumet()/GeV, resid->mpx()/GeV, weight);
    hist2d(histname("residxy_sumet"))->Fill(met->sumet()/GeV, resid->mpy()/GeV, weight);
    //
    hist2d(histname("residL_sumet"))->Fill(met->sumet()/GeV, proj->mpx()/GeV, weight);
    hist2d(histname("residT_sumet"))->Fill(met->sumet()/GeV, proj->mpy()/GeV, weight);
    //
    hist2d(histname("residxy_CSTsumet"))->Fill(cst_sumet/GeV, resid->mpx()/GeV, weight);
    hist2d(histname("residxy_CSTsumet"))->Fill(cst_sumet/GeV, resid->mpy()/GeV, weight);
    //
    hist2d(histname("residL_CSTsumet"))->Fill(cst_sumet/GeV, proj->mpx()/GeV, weight);
    hist2d(histname("residT_CSTsumet"))->Fill(cst_sumet/GeV, proj->mpy()/GeV, weight);
    //
    hist2d(histname("residxy_TSTsumet"))->Fill(tst_sumet/GeV, resid->mpx()/GeV, weight);
    hist2d(histname("residxy_TSTsumet"))->Fill(tst_sumet/GeV, resid->mpy()/GeV, weight);
    //
    hist2d(histname("residL_TSTsumet"))->Fill(tst_sumet/GeV, proj->mpx()/GeV, weight);
    hist2d(histname("residT_TSTsumet"))->Fill(tst_sumet/GeV, proj->mpy()/GeV, weight);
    //
    //hist2d(histname("residxy_LHsumet"))->Fill(lht_sumet/GeV, resid->mpx()/GeV, weight);
    //hist2d(histname("residxy_LHsumet"))->Fill(lht_sumet/GeV, resid->mpy()/GeV, weight);
    //
    //hist2d(histname("residL_LHsumet"))->Fill(lht_sumet/GeV, proj->mpx()/GeV, weight);
    //hist2d(histname("residT_LHsumet"))->Fill(lht_sumet/GeV, proj->mpy()/GeV, weight);
    //
    hist2d(histname("residL_pthard"))->Fill(pthard->met()/GeV, proj->mpx()/GeV, weight);
    hist2d(histname("residT_pthard"))->Fill(pthard->met()/GeV, proj->mpy()/GeV, weight);
    //
    hist2d(histname("scaleL_pthard"))->Fill(pthard->met()/GeV, 1. + proj->mpx()/pthard->met(), weight);
    //
    hist2d(histname("softL_pthard"))->Fill(pthard->met()/GeV, proj_soft->mpx()/GeV, weight);
    hist2d(histname("softT_pthard"))->Fill(pthard->met()/GeV, proj_soft->mpy()/GeV, weight);

    // for CST syst:
    hist3d(histname("pLsoft_vsPthard_vsSumet"))->Fill(met->sumet()/GeV, pthard->met()/GeV, proj_soft->mpx()/GeV,weight);
    if (  (npv>=8 && npv<=9 )  && ( avgmu>=10 && avgmu<=15  )){
      hist(histname("sumet_npv_mu"))->Fill(met->sumet()/GeV,weight);
    }

    Int_t njets(0);
    if (m_jetBins) {
      const JetContainer* jets(0);
      ATH_CHECK( evtStore()->retrieve(jets,m_jetcoll) );
      // Number of jets with pT > 20 GeV and passing overlap removal.
      for(const auto& j : *jets) {
        if ((j->pt() > 20000) && (dec_passOR(*j))) njets++;
      }
      std::string jetlabel;
      switch (njets) {
        case 0: jetlabel = "0"; break;
        case 1: jetlabel = "1"; break;
        default: jetlabel = "2";
      }
      hist2d(histname("residL_pthard_"+jetlabel+"j"))->Fill(pthard->met()/GeV, proj->mpx()/GeV, weight);
      hist2d(histname("residT_pthard_"+jetlabel+"j"))->Fill(pthard->met()/GeV, proj->mpy()/GeV, weight);
      //
      hist2d(histname("softL_pthard_"+jetlabel+"j"))->Fill(pthard->met()/GeV, proj_soft->mpx()/GeV, weight);
      hist2d(histname("softT_pthard_"+jetlabel+"j"))->Fill(pthard->met()/GeV, proj_soft->mpy()/GeV, weight);
    }

    if (m_writetree) {
      if (!m_jetBins) { // Count good jets
        const JetContainer* jets(0);
        ATH_CHECK( evtStore()->retrieve(jets,m_jetcoll) );
        // Number of jets with pT > 20 GeV and passing overlap removal.
        for(const auto& j : *jets) {
          if ((j->pt() > 20000) && (dec_passOR(*j))) njets++;
        }
      }
      m_njets = njets;
      m_pthard = pthard->met()/GeV;
      m_residl_pthard = proj->mpx()/GeV;
      m_residt_pthard = proj->mpy()/GeV;
      m_avgmu = avgmu;
      m_weight = weight;

      tree(histname("resid_pthard"))->Fill();
    }

    return StatusCode::SUCCESS;
  }

  StatusCode METTreeAlg::fillTree(const MissingETContainer* metperf, double weight) {
    const MissingET* met 	= (*metperf)["Final"];
    //const MissingET* proj 	= (*metperf)["Proj_PtHard"];
    //const MissingET* pthard = (*metperf)["PtHard"];
    const MissingET* jet 	= (*metperf)[m_jetterm];
    const MissingET* muon 	= (*metperf)[m_muonterm];
    const MissingET* ele 	= (*metperf)[m_eleterm];
    const MissingET* soft 	= (*metperf)[m_softterm];
    const MissingET* gamma 	= (*metperf)[m_gammaterm];
    const MissingET* tau 	= (*metperf)[m_tauterm];

    //m_pthard = pthard->met()/GeV;

    m_met 	    = met->met()/GeV;
    m_sumet 	= met->sumet()/GeV;
    m_phi 	    = met->phi();
    m_mpx 	    = met->mpx()/GeV;
    m_mpy 	    = met->mpy()/GeV;
    m_jetmet    = jet->met()/GeV;
    m_jetsumet  = jet->sumet()/GeV;
    m_jetphi    = jet->phi();
    m_jetmpx    = jet->mpx()/GeV;
    m_jetmpy    = jet->mpy()/GeV;
    m_softmet   = soft->met()/GeV;
    m_softsumet = soft->sumet()/GeV;
    m_softphi   = soft->phi();
    m_softmpx   = soft->mpx()/GeV;
    m_softmpy   = soft->mpy()/GeV;
    if(!ele) {
    	ATH_MSG_DEBUG("No Electrons found!");
    }
    else{
        m_elemet    = ele->met()/GeV;
        m_elesumet  = ele->sumet()/GeV;
        m_elephi    = ele->phi();
        m_elempx    = ele->mpx()/GeV;
        m_elempy    = ele->mpy()/GeV;
    }
    if(!muon) {
       ATH_MSG_DEBUG("No Muons found!");
    }
    else{
       m_muonmet   = muon->met()/GeV;
       m_muonsumet   = muon->sumet()/GeV;
       m_muonphi   = muon->phi();
       m_muonmpx   = muon->mpx()/GeV;
       m_muonmpy   = muon->mpy()/GeV;
    }
    if(!gamma) {
        ATH_MSG_DEBUG("No Photons found!");
    }
    else{
      m_photonmet = gamma->met()/GeV;
      m_photonsumet = gamma->sumet()/GeV;
      m_photonphi = gamma->phi();
      m_photonmpx = gamma->mpx()/GeV;
      m_photonmpy = gamma->mpy()/GeV;
    }

    if(!tau) {
    	ATH_MSG_DEBUG("No Taus found!");
    }
    else{
       m_taumet    = tau->met()/GeV;
       m_tauphi    = tau->phi();
       m_taumpx    = tau->mpx()/GeV;
       m_taumpy    = tau->mpy()/GeV;
       m_tausumet    = tau->sumet()/GeV;
    }
    m_weight    = weight;

    tree(histname("Performance"))->Fill();
    return StatusCode::SUCCESS;
  }
  StatusCode METTreeAlg::setTreeZ(const MissingETContainer* metperf, double weight) {

    const MissingET* proj = (*metperf)["Proj_PtZ"];
    const MissingET* ptz = (*metperf)["PtZ"];

    m_ptZ    = ptz->met()/GeV;
    m_pLPtZ  = proj->mpx()/GeV;
    m_pTPtZ  = proj->mpy()/GeV;


    ATH_MSG_VERBOSE("weight value = "<<weight);

    return StatusCode::SUCCESS;

  }

  StatusCode METTreeAlg::plotPerfZ(const MissingETContainer* metperf, double weight) {
    ATH_MSG_DEBUG(name() << " -- plotting Z performance plots");
    const MissingET* proj = (*metperf)["Proj_PtZ"];
    const MissingET* ptz = (*metperf)["PtZ"];
    const MissingET* jetterm = (*metperf)[m_jetterm];
    if(!proj) {
      ATH_MSG_ERROR("Didn't find MET projected on ptZ");
      return StatusCode::FAILURE;
    }
    if(!ptz) {
      ATH_MSG_ERROR("Didn't find PtZ");
      return StatusCode::FAILURE;
    }

    ATH_MSG_VERBOSE("  Z px = " <<  ptz->mpx() << ", py = " << ptz->mpy());
    ATH_MSG_VERBOSE("  projZ px = " << proj->mpx() << ", py = " << proj->mpy());
    hist(histname("pL_ptz"))->Fill(proj->mpx()/GeV,weight);
    hist2d(histname("pL_ptz_ptz"))->Fill(ptz->met()/GeV, proj->mpx()/GeV,weight);
    hist(histname("pT_ptz"))->Fill(proj->mpy()/GeV,weight);
    //
    hist2d(histname("residL_ptz"))->Fill(ptz->met()/GeV, proj->mpx()/GeV, weight);
    hist2d(histname("residT_ptz"))->Fill(ptz->met()/GeV, proj->mpy()/GeV, weight);
    //
    hist2d(histname("scaleL_ptz"))->Fill(ptz->met()/GeV, 1. + proj->mpx()/ptz->met(), weight);

    hist2d(histname("sumet_ptz"))->Fill(ptz->met()/GeV, jetterm->sumet()/GeV,weight);

    Int_t njets(0);
    double leading_recoJetPt = 0.;
    double leading_recoJetEta = 0.;
    double jet_central = 0.;
    double jet_forward = 0.;
    const JetContainer* jets(0);
    ATH_CHECK( evtStore()->retrieve(jets,m_jetcoll) );
    // Number of jets with pT > 20 GeV and passing overlap removal.
    for(const auto& j : *jets) {
      if (!dec_passOR(*j)) continue;
      if ((j->pt() > m_jetMinPt)){
		njets++;
		if(TMath::Abs( j->eta() ) < 2.5){
			jet_central += j->pt();
		}
		else{
			jet_forward += j->pt();
		}
      }
      if (j->pt() > leading_recoJetPt) {
        leading_recoJetPt = j->pt();
        leading_recoJetEta = j->eta();
      }
    }
    ATH_MSG_VERBOSE("Leading jet pT = " << leading_recoJetPt);
    hist2d(histname("ratio_jetpt_ptz"))->Fill(ptz->met()/GeV, leading_recoJetPt / ptz->met(), weight);
    hist2d(histname("ratio_residL_ptz"))->Fill(ptz->met()/GeV, 1 - proj->mpx()/ptz->met(), weight);
    hist(histname("njets"))->Fill(njets, weight);
    hist2d(histname("forw_cent"))->Fill(jet_central/GeV, jet_forward/GeV, weight);
    if (njets == 1) {
      hist2d(histname("ratio_jetpt_ptz_1j"))->Fill(ptz->met()/GeV, leading_recoJetPt / ptz->met(), weight);
      hist2d(histname("ratio_residL_ptz_1j"))->Fill(ptz->met()/GeV, 1 - proj->mpx()/ptz->met(), weight);
      if (fabs(leading_recoJetEta) < 2.) {
        hist2d(histname("ratio_residL_ptz_1j_etacut"))->Fill(ptz->met()/GeV, 1 - proj->mpx()/ptz->met(), weight);
      }
    }

    if (m_jetBins) {
      std::string jetlabel;
      switch (njets) {
        case 0: jetlabel = "0"; break;
        case 1: jetlabel = "1"; break;
        default: jetlabel = "2";
      }
      hist2d(histname("residL_ptz"+jetlabel+"j"))->Fill(ptz->met()/GeV, proj->mpx()/GeV, weight);
      hist2d(histname("residT_ptz"+jetlabel+"j"))->Fill(ptz->met()/GeV, proj->mpy()/GeV, weight);
    }
    return StatusCode::SUCCESS;
  }

}

